using QCommon.Extensions;
using Qis.Common.Scripting.Events.EventHandlerArguments;
using Qis.Common.Scripting.Events.EventHandlerAttributes;

namespace Qis.Module.Scripts
{
    public class BusinessProcess
    {
        [RepositoryConnected]
        public static void AddSubTemplates(RepositoryConnectedEventArgs args)
        {
            //KavliLog.LogToConsoleFile("Metamodel modifications business process", string.Format("Debug"));
            DynamicMetamodel.AddTemplateParent(
                args.Repository.Metamodel,
                "BusinessProcess",
                "subProcessHACCP",
                new TabDisplayName { Name = "subProcessHACCP", DisplayName = "HACCP" }.AsCollection());
        }
    }

}