using System;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Data;
using System.Diagnostics;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using QCommon.Extensions;
using QCommon.Log;
using Qef.Common;
using Qef.Common.Log;
using Qef.Common.Mail;
using Qef.Common.Users;
using Qis.Common;
using Qis.Common.AttributeValues;
using Qis.Common.Metamodel;
using Qis.Common.Scripting;
using Qis.Common.Qsql;
using Qis.Common.Qsql.SelectQuery;
using Qis.Common.Qsql.Operations.Logical;
using Qis.Common.Qsql.Operations;
using Qis.Module.Scripts.GovernanceMethods.Boolean;
using Qis.Module.Scripts.GovernanceMethods.String;
using Qis.Module.Scripts.GovernanceMethods.TableValued;
using Qis.Module.Scripts.GovernanceMethods;
using QCommon.Utils;
using System.Net.Mail;
using System.Web;


namespace Qis.Module.Scripts
{
    /// <summary> 
    ///     GweDynamicCode class.
    /// </summary>
    public partial class GweDynamicCode
    {

        public static bool DiagramHasStatus(GovernanceMethodData self, MessageBlock message)
        {
            RepositoryObject obj = message.GweObject;
            var methodParameters = ExtractDynamicMethodParameters(self);
            string status = GetDynamicMethodParameterNamed(methodParameters, "Status");
			string approvalState = GetDynamicMethodParameterNamed(methodParameters, "ApprovalState");
            if (obj.IsDiagram)
                return ObjHasStatus(obj, status, approvalState);

			RepositoryObjectList diagList = obj.GetContainedIn();
			foreach(RepositoryObject diag in diagList)
			{
				if(diag.ConfigurationInfo.IsDefaultRevision)
				{
					if (ObjHasStatus(diag, status, approvalState))
						return true;
				}
			}
            return false;
        }

        private static bool ObjHasStatus(RepositoryObject obj, string status, string approvalState)
        {
            IConfiguration configuration = obj.GetConfiguration();
            var configurationCtx = ConfigurationContext.Create(configuration);
			
			if(!string.IsNullOrEmpty(approvalState))
			{
				
				string appstate = ObjectScripts.GetAttributeValue(obj, "ApprovalState").ToString();
				//KavliLog.LogToConsoleFile("ObjHasStatus - 1", string.Format("Using approval state:  {0} [{1}]", obj.Name, appstate));
				if(appstate == status)
				{
					return true;
				}
				else 
				{
					return false;
				}
				
			}
			

            GweStateMachine gweSM = new GweStateMachine(configurationCtx, CallContext.QisServer.CurrentSession.UserId, obj);
			
            //GweStateMachine(configurationCtx, CallContext.QisServer.CurrentSession.UserId, transitionThis);
            if (gweSM.CurrentState == null)
                return false;

            return gweSM.CurrentState.Name.ToLower() == status.ToLower();
        }


        /// <summary>
        ///  Check if related objects are approved
        /// </summary>
        /// <param name="self"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static bool HACCPApproveByRelation(GovernanceMethodData self, MessageBlock message)
        {
            IConfiguration configuration = message.GweObject.GetConfiguration();
            RepositoryObject localObject = message.GweObject;
            RepositoryObject objectToCheck = null;
           // KavliLog.LogToConsoleFile("HACCPApproveByRelation - 1", string.Format("Objekt:  {0} [{1}]", localObject.Name, localObject.Template));
            if (localObject.Template == "FoodSafetyHazardPlan")
            {
                RepositoryObject anlaysis = localObject.AttributeGetSingleLinkSafely("AppliesTo");
                objectToCheck = anlaysis.AttributeGetSingleLinkSafely("FoodSafetyHazardConcerns");
            }
            
            else if (localObject.Template == "FoodSafetyHazardAnalysis")
            {
                objectToCheck = localObject.AttributeGetSingleLinkSafely("FoodSafetyHazardConcerns");
            }

            if(objectToCheck !=null)
            {
                string state = ObjectScripts.GetAttributeValue(objectToCheck, "ApprovalState").ToString();
                if (state == "Godkjent")
                {
                    KavliLog.LogToConsoleFile("HACCPApproveByRelation - 3 ", string.Format("Object approved by relation{0} [{1}]  ", localObject.Name, localObject.Template));
                    return true;
                }
                else
                {
                    return false;
                }
            }
           else
            {
                KavliLog.LogToConsoleFile("HACCPApproveByRelation - INGEN objekt � sjekke mot ", string.Format("Objekt: {0}[{1}] ", localObject.Name, localObject.Template));
            }
            return false;
        }

        /// <summary>
        /// Trigger of GWE for related objects
        /// </summary>
        /// <param name="self"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public static string HACCPPostByRelation(GovernanceMethodData self, MessageBlock message)
        {
            IConfiguration configuration = message.CurrentContext.Configuration;
            RepositoryObject localObject = message.GweObject;
            if (configuration.Id != localObject.GetConfiguration().Id)
            {
                localObject = configuration.FindObject(localObject.Id);
            }
            List<string> results = new List<string>();
            var messageFormat = ExtractMessageFormat(self, "{0}: {1} - Post Error Message:\n{2}");
            var methodParameters = ExtractDynamicMethodParameters(self);
            string postStrategy = GetDynamicMethodParameterNamed(methodParameters, "PostStrategy");

            RepositoryObjectList objList = new RepositoryObjectList();
            objList = KavliHaccp.GetRelatedHACCPObjects(localObject, configuration);
            KavliLog.LogToConsoleFile("HACCPPostByRelation - 1", string.Format("Degbug object {0} [{1}]", localObject.Name,localObject.Template));
            if (string.IsNullOrEmpty(postStrategy))
            {
                postStrategy = "Asynchronous";
            }
            if (objList.Count > 0)
            {
                // RepositoryObjectList objectList = GetListByRelation(localObject, relations, templates, message.CycleOwner);
                //if (objectList.Count > 0)
                //{
                foreach (RepositoryObject repObject in objList)
                {
                    KavliLog.LogToConsoleFile("HACCPPostByRelation - 2", string.Format("Degbug haccp objekt {0} [{1}]", repObject.Name,repObject.Template));
                    try
                    {
                        switch (postStrategy)
                        {
                            case "Synchronous":
                                DoPostObject(message.CurrentContext, repObject);
                                break;
                            case "Asynchronous":
                                DoPostObjectAsync(message.CurrentContext, repObject);
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        results.Add(string.Format(messageFormat, MethodBase.GetCurrentMethod().Name, repObject.Name, ex.ToString()));
                    }
                }
                //}
            }
            return results.Count > 0
                ? string.Join(Environment.NewLine, results.ToArray())
                : c_resultOk;
        }
		  /// <summary>
        ///  Check against template type
        ///  Updated to 6.6 version
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static bool IsOfGivenTemplate(GovernanceMethodData self, MessageBlock message)
        {
            RepositoryObject obj = message.GweObject;
            var methodParameters = ExtractDynamicMethodParameters(self);
            string template = GetDynamicMethodParameterNamed(methodParameters, "Template");

            foreach (string aTemplate in template.Split(new Char[] { ';' }))
            {
                string aTemplateName = aTemplate.Trim();
                if (obj.Template == aTemplateName)
                {
                    //InovynLog.LogToConsoleFile("Is Of Given Template: ", string.Format("Object : {0}\t Template: {1}\t", obj.Name, aTemplateName));
                    return true;
                }

            }
            return false;
        }
    }
}