using System;
using System.Collections.Generic;
using System.Linq;
using Qef.Common;
using Qis.Common;
using Qis.Common.Qsql;
using Qis.Common.Qsql.Operations;
using Qis.Common.Qsql.Operations.Logical;
using Qis.Common.Qsql.SelectQuery;
using Qis.Common.AttributeValues;
using Qis.Common.Metamodel;
using Qis.Common.Scripting.Events.EventHandlerArguments;
using Qis.Common.Scripting.Events.EventHandlerAttributes;
using Qis.Web.Forms.Common;
using Qis.Web.Forms.Common.Scripting;
using IRepository = Qis.Common.IRepository;
using IUser = Qef.Common.Users.IUser;

namespace Qis.Module.Scripts
{
    public class KavliHaccp
    {

        public static RepositoryObjectList GetRelatedHACCPObjects(RepositoryObject obj, IConfiguration config)
        {
            RepositoryObjectList haccpObjs = new RepositoryObjectList();

            if (obj.Template == "WorkFlowDiagram")
            {
                RepositoryObjectList activities = obj.GetContains().FilterByTemplate("Activity");

                foreach (RepositoryObject activity in activities)
                {
                    RepositoryObjectList analysisObjs = ObjectScripts.GetBackAttributeRelations(config, activity.Id, "FoodSafetyHazardConcerns");
                    haccpObjs.AddRange(analysisObjs);

                    foreach (RepositoryObject analysisObj in analysisObjs)
                    {
                        haccpObjs.Add(analysisObj.AttributeGetSingleLinkSafely("FoodSafetyHazardRelatedHazard"));
                        //Add plan objects
                        RepositoryObjectList planObjs = ObjectScripts.GetBackAttributeRelations(config, analysisObj.Id, "AppliesTo");
                        haccpObjs.AddRange(planObjs);
                        // Add HazardObjects
                        haccpObjs.AddRange(ObjectScripts.GetBackAttributeRelations(config, analysisObj.Id, "HasRelevantHazardObject"));

                    }
                }
            }

            if (obj.Template == "FoodSafetyHazardAudit")
            {
                haccpObjs.AddRange(ObjectScripts.GetBackAttributeRelations(config, obj.Id, "FoodSafetyHazardPrerequisiteConcerns"));
                haccpObjs.AddRange(ObjectScripts.GetBackAttributeRelations(config, obj.Id, "TargetObject"));
            }

            return haccpObjs;
        }
    }
}