using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Script.Serialization;
using QCommon.Extensions;
using QCommon.Log;
using Qef.Common;
using Qef.Common.Log;
using Qef.Common.Security;
using Qef.Common.Users;
using Qis.Common;
using Qis.Common.AttributeValues;
using Qis.Common.Qsql;
using Qis.Common.Qsql.SelectQuery;
using Qis.Common.Scripting;
using Qis.Common.Scripting.Events.EventHandlerArguments;
using Qis.Common.Scripting.Events.EventHandlerAttributes;
using Qis.Common.Utils;
using Qis.Module.Scripts.Gwe;
using Qis.Web.Forms.Common.Scripting;
using Qis.Web.Forms.Common.UIActions;

namespace Qis.Module.Scripts
{
    public static class KavliHashtags
    {
        #region Event Handlers
        [ObjectRevisionCreated]
        public static void CopyHashtagFix(ObjectRevisionCreatedEventArgs args)
        {
            var configurationCtx = ConfigurationContext.Create(args.Configuration);
            var repositoryObject = configurationCtx.Configuration.FindObject(args.ObjectId);
           // KavliLog.LogToConsoleFile("Revision created", string.Format("Object - {0}", repositoryObject.Name));
            var parentRepositoryObject = configurationCtx.Configuration.FindObject(new Oid(args.ObjectId.Id + "/" + repositoryObject.ParentRevision));

            var objectRevId = repositoryObject.Id.Revision;
            var parentRevId = parentRepositoryObject.Id.Revision;
          //  KavliLog.LogToConsoleFile("Revision created", string.Format("RevID - {0} ParentRevID {1} ", objectRevId.ToString(), parentRevId.ToString()));
            //RepositoryObjectList parentHashtags = ObjectScripts.GetAttributeRelations(configurationCtx, parentRepositoryObject.Id, "Hashtags");

            IAttributeValue hasParentHashtags = ObjectScripts.GetAttributeValue(parentRepositoryObject, "Hashtags");

            QHyperLinkList hasParentHashtagsLinks = hasParentHashtags != null
                ? hasParentHashtags.GetHyperLinks()
                : new QHyperLinkList();

            MultiLink parentHastagsLinks = new MultiLink(hasParentHashtagsLinks);
            ObjectScripts.SetAttributeValue(repositoryObject, "Hashtags", parentHastagsLinks);
            repositoryObject.SaveData();

        }

        #endregion
    }
}
