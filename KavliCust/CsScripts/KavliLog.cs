﻿using System;
using System.IO;

namespace Qis.Module.Scripts
{
    public class KavliLog
    {
        public static void LogToConsoleFile(string scope, string txt)
        {
            string dirPath = "C:\\QSDebugLog";
            if (!Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);
            string file = dirPath + @"\log.txt";
            File.AppendAllText(file, DateTime.Now + ": " + scope + ":" + txt + "\n");
        }
    }
}
