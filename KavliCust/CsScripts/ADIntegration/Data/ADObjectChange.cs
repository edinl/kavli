﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ad_sync_filter.Business;
using ad_sync_filter.Data.Mapping;
using Qef.Common.Log;
using Qis.Common;

namespace ad_sync_filter.Data
{
    public class ADObjectChange
    {
        public Dictionary<string, ADObjectDifferences> differenceses;

        private string _typeImport;
        public bool IsNew { get; set; }
        public bool IsChanged { get; set; }
        public string Problem { get; set; }
        public bool WasUpdated { get; set; }

        public ADObjectChange(string typeImport)
        {
            this._typeImport = typeImport;
            this.IsNew = false;
            this.IsChanged = false;
            this.WasUpdated = false;

            this.differenceses = new Dictionary<string, ADObjectDifferences>();
        }

        public void ExecuteDiff(RepositoryObject QisObj, Dictionary<string, List<string>> ADkeyValue)
        {
            if (QisObj == null)
            {
                if (ADkeyValue == null)
                {
                    this.Problem = ADMessages.ERROR_NO_ADKEYVALUE_PRESENT + "### ";
                }
                else
                {
                    // Object is new.
                    this.IsNew = true;
                }
            }
        }

        public void GetAttributeDiff(RepositoryObject QisObj, Dictionary<string, List<string>> ADkeyValue)
        {
            // Loop every attribute.
            foreach (KeyValuePair<string, List<string>> keyValue in ADkeyValue)
            {
                ADObjectDifferences diff = new ADObjectDifferences
                {
                    WasUpdated = false,
                    IsChanged = false
                };

                ADMapping ADMapper;
                if (!ADImportConfig.GetADMapping(this._typeImport).TryGetValue(keyValue.Key, out ADMapper))
                    continue; // Must be a dependant field.

                try
                {
                    Dictionary<string, object> args = null;
                    diff.IsChanged = ADMapper.IsChanged(QisObj, ADkeyValue, out diff.OldValue, out diff.NewValue, ref args);
                    if (diff.IsChanged)
                    {
                        this.IsChanged = true;
                        string isSimulate = ADImportConfig.GetTextConfig(this._typeImport, "/Simulate", true);
                        if (!isSimulate.Equals("0"))
                        {
                            diff.WasUpdated = true;
                            continue;
                        }
                        diff.WasUpdated = ADMapper.DoChange(ref QisObj, ADkeyValue, args);
                        if (diff.WasUpdated)
                            this.WasUpdated = true;
                        else
                        {
                            diff.Problem = ADMessages.ERROR_UPDATING_ATTR + keyValue.Key + ADMessages.VALUE + keyValue.Value.PrintCollection() + ADMessages.QISATTR + ADMapper.QISkey;
                            this.Problem += ADMessages.ERROR_UPDATING_ATTR + keyValue.Key + " ### ";
                        }
                    }
                }
                catch (Exception e)
                {
                    diff.Problem = e.ToString();
                    this.Problem = ADMessages.ERROR_MODIFYING_OBJ + "" + "### ";
                }
                

                if(!this.differenceses.ContainsKey(keyValue.Key))
                    this.differenceses.Add(keyValue.Key, diff);
            }
        }
        public class ADObjectDifferences
        {
            public string OldValue; // String representation of the old value.
            public string NewValue; // String representation of the new value.
            public bool WasUpdated { get; set; }
            public bool IsChanged { get; set; }
            public string Problem { get; set; }
        }
    }
}
