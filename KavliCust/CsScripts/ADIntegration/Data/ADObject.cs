﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ad_sync_filter.Business;
using ad_sync_filter.Data.Abstract;
using ad_sync_filter.Data.Interface;
using No.Qualisoft.WCF.Beeline.Business.Parser;
using Qis.Common;
using Qis.Common.Qsql.SelectQuery;
using Qis.Module.Scripts;

namespace ad_sync_filter.Data
{
    class ADObject : IADObject
    {
        private Dictionary<string, List<string>> ADKeyValue;
        private RepositoryObject QISObj;
        private string ParseScriptValue;
        private string _typeImport;
        private ADObjectChange changes;

        public ADObject(string typeImport)
        {
            this._typeImport = typeImport;
            this.ADKeyValue = new Dictionary<string, List<string>>();
        }
        public void AddKeyValue(string key, List<string> value)
        {
           this.ADKeyValue.Add(key, value); 
        }

        public ADObjectChange GetChanges()
        {
            if (changes == null)
            {
                changes = new ADObjectChange(this._typeImport);
                changes.ExecuteDiff(this.GetQISObj(), ADKeyValue);
                string isSimulate = ADImportConfig.GetTextConfig(this._typeImport, "/Simulate", true);
                if (changes.IsNew && isSimulate.Equals("0"))
                {
                    try
                    {
                        this.QISObj = ADImportConfig.GetCreateObjDelegator(this._typeImport).CreateQISObject(ADKeyValue);
                    }
                    catch (Exception e)
                    {
                        changes.Problem = ADMessages.ERROR_CREATING_OBJ + e.ToString();
                        return changes;
                    }
                }
                if (this.QISObj == null)
                {
                    changes.Problem = ADMessages.MODIFYING_OBJ_CREATE_DELEGATOR_DENIED;
                }
                else if (!LDAPSync.IsRepositoryObjectLocked(this.QISObj))
                {
                    this.QISObj.LockObject();
                    AModifyObjDelegator dlg = ADImportConfig.GetModifyObjDelegator(this._typeImport);
                    if (dlg == null || dlg.CheckIfModify(this.QISObj, ADKeyValue))
                        changes.GetAttributeDiff(QISObj, ADKeyValue);
                    /*else
                        changes.Problem = ADMessages.MODIFYING_OBJ_MODIFY_DELEGATOR_DENIED;*/
                    this.QISObj.ReleaseObjectLocks();
                }
                else
                {
                    changes.Problem = ADMessages.ERROR_COULD_NOT_OBTAIN_LOCK;
                }
            }
            return changes;
        }
        private RepositoryObject GetQISObj()
        {
            if (this.QISObj == null)
            {
                WhereExpression expression = ParseTree.Filter2QSQL(GetParseScriptValue(ADImportConfig.GetTextConfig(this._typeImport, "/QISSearch")));
                string template = ADImportConfig.GetTextConfig(this._typeImport, "/QIStemplate");
                RepositoryObjectList objList = LDAPSync.FindObjects(ADImportConfig.GetConfiguration(this._typeImport), template, expression);

                if (objList != null && objList.Count > 0)
                {
                    this.QISObj = objList.First();
                }
            }
            return QISObj;
        }
        
        public string GetQISObjtest()
        {
            return GetParseScriptValue(ADImportConfig.GetTextConfig(this._typeImport, "/QISSearch"));
        }
        public string GetParseScriptValue(string script)
        {
            if (!String.IsNullOrEmpty(this.ParseScriptValue))
                return this.ParseScriptValue;

            int startVar = script.IndexOf("$$", StringComparison.Ordinal);
            while (startVar > 0)
            {
                int endVar = script.IndexOf("$$", startVar + 1, StringComparison.Ordinal);
                if (endVar < 0 || endVar <= startVar + 1)
                    break;

                string key = script.Substring(startVar + 2, endVar - startVar - 2).ToLower();

                script = script.Replace(script.Substring(startVar, endVar - startVar + 2), "'" +
                    ADImportConfig.GetADMapping(this._typeImport)[key].GetScriptValue(ADKeyValue) + "'");

                if (endVar >= script.Length - 1)
                    break;
                startVar = script.IndexOf("$$", endVar + 2, StringComparison.Ordinal);
            }
            this.ParseScriptValue = script;

            return this.ParseScriptValue;
        }
    }
}
