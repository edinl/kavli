﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ad_sync_filter.Business;
using ad_sync_filter.Data.Abstract;
using ad_sync_filter.Data.Mapping;
using No.Qualisoft.WCF.Beeline.Business.Parser;
using Qis.Common;
using Qis.Common.AttributeValues;
using Qis.Common.Qsql.Operations;
using Qis.Common.Qsql.Operations.Logical;
using Qis.Common.Qsql.SelectQuery;
using Qis.Module.Scripts;

namespace ad_sync_filter.Data.Delegators
{
    public class OrgUnitDelegator : ADMapping
    {
        private OrgUnitDelegator(string ADkey, string QISKey, string typeImport)
            : base(ADkey, QISKey, typeImport)
        {
        }

        public override bool DoChange(ref RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue, Dictionary<string, object> args)
        {
            /*
            {"currentOrgUnits", currentOrgUnits},
            {"newOrgUnit", objList},
            {"wasManager", wasManager},
            {"isManager", IsManager(ADKeyValue)}*/
            RepositoryObject newObj;

            // Remove from Old - not implemented (?)
            foreach (RepositoryObject currOrg in (RepositoryObjectList)args["currentOrgUnits"])
            {
                if (LDAPSync.IsRepositoryObjectLocked(currOrg))
                    throw new Exception(ADMessages.ERROR_COULD_NOT_OBTAIN_LOCK + currOrg.Name);
                currOrg.LockObject();

                if(currOrg.Name!= GetValue(ADKeyValue[this.ADkey], 0))
                {

                    currOrg.Name = GetValue(ADKeyValue[this.ADkey], 0);
                }
                SingleLink manager = currOrg.Attributes["HasResponsible"].Value as SingleLink;
                if (manager != null && !manager.IsEmpty && (bool)args["wasManager"])
                {
                    QHyperLink managerHyper = manager.HyperLink;
                    if (managerHyper != null && managerHyper.ObjectId.Id.Equals(repObj.Id.Id))
                        currOrg.Attributes["HasResponsible"].Value = new SingleLink(null);
                }
                else
                {
                    MultiLink resources = currOrg.Attributes["HasResources"].Value as MultiLink;
                    if (resources != null)
                    {

                        QHyperLinkList resourcesHyper = resources.GetHyperLinks();
                        QHyperLinkList newList = new QHyperLinkList();
                        foreach (QHyperLink resourceHyper in resourcesHyper)
                        {

                            if (!resourceHyper.ObjectId.Id.ToString().Equals(repObj.Id.Id.ToString()))
                            {
                                newList.Add(resourceHyper);
                            }
                        }
                        currOrg.Attributes["HasResources"].Value = new MultiLink(newList);
                    }
                }
                currOrg.SaveData();
                currOrg.ReleaseObjectLocks();
            }
			//FHILog.LogToConsoleFile("AD LOG", string.Format("Debug nr: 2..."));
            // Add to new
            if (args["newOrgUnit"] == null || (args["newOrgUnit"] as RepositoryObjectList).Count == 0)
            {
				string testName = new PlainText(GetValue(ADKeyValue[this.ADkey], 0));
				
				//FHILog.LogToConsoleFile("AD LOG", string.Format("Debug nr: 3...{0}",testName));
                newObj = LDAPSync.CreateObject(
                    ADImportConfig.GetConfiguration(this._typeImport),
                   testName,// + " " + GetValue(ADKeyValue[this.ADkey], 1),
                    "OrganizationUnit"
                );
                newObj.Attributes["PhysicalReference"].Value = new PlainText(GetValue(ADKeyValue[this.ADkey], 0));
                // Now using PhysicalReference and not FTE

            }
            else
            {
                newObj = (args["newOrgUnit"] as RepositoryObjectList).First();
            }

            if (LDAPSync.IsRepositoryObjectLocked(newObj))
                throw new Exception(ADMessages.ERROR_COULD_NOT_OBTAIN_LOCK + newObj.Name);

            newObj.LockObject();

            if (newObj.Name != GetValue(ADKeyValue[this.ADkey], 0))
            {
                newObj.Name = GetValue(ADKeyValue[this.ADkey], 0);
            }

            if ((bool)args["isManager"])
            {
                newObj.Attributes["HasResponsible"].Value = new SingleLink(repObj.GetLocalHyperlink());
            }
            else
            {
                MultiLink resources = newObj.Attributes["HasResources"].Value as MultiLink;
                QHyperLinkList resourcesHyper = resources.GetHyperLinks();
                bool isInResources = false;
                if(resources!= null && !resources.IsEmpty)
                {

                    QHyperLinkList resourcesHyper2 = resources.GetHyperLinks();
                   
                    foreach (QHyperLink resourceHyper2 in resourcesHyper2)
                    {

                        if (resourceHyper2.ObjectId.Id.ToString().Equals(repObj.Id.Id.ToString()))
                        {
                            isInResources = true;
                        }
                    }
                    if(isInResources == false)
                    {
                        resourcesHyper.Add(repObj.GetLocalHyperlink());
                    }
                }
                else
                {
                    resourcesHyper.Add(repObj.GetLocalHyperlink());
                }
                newObj.Attributes["HasResources"].Value = new MultiLink(resourcesHyper);
            }
            newObj.SaveData();
            newObj.ReleaseObjectLocks();

            return true;
        }


        public override bool IsChanged(RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue, out string oldValue, out string newValue, ref Dictionary<string, object> args)
        {
            newValue = "";
            oldValue = "";
            bool wasManager = false;
            bool isManager;
            // Find what org unit Person is connected to now.
            RepositoryObjectList currentOrgUnits = this.GetCurrentOrgUnit(repObj);
            if (currentOrgUnits == null)
            {
                throw new Exception(ADMessages.ERROR_MISSING_QIS_ATTR + ADMessages.MAPPING_MISSING_DATA_ORGUNIT);
            }
            else if (ADKeyValue[this.ADkey].Count > 1)
            {
                throw new Exception(ADMessages.ERROR_MAPPING + ADMessages.MAPPING_PERSON_MULTIPLE_ORGUNIT);
            }
            else if (currentOrgUnits.Count == 1)
            {
                RepositoryObject currOrgUnit = currentOrgUnits.First();
                SingleLink manager = currOrgUnit.Attributes["HasResponsible"].Value as SingleLink;
                if (repObj != null && manager != null)
                {
                    QHyperLink managerHyper = manager.HyperLink;
                    if (managerHyper != null && managerHyper.ObjectId.Id.Equals(repObj.Id.Id))
                        wasManager = true;
                }
                oldValue = currentOrgUnits.First().Name;
            }
            else if (currentOrgUnits.Count > 1)
            {
                foreach (RepositoryObject rep in currentOrgUnits)
                    oldValue += rep.Name + ", ";
            }

            WhereExpression expression = ParseTree.Filter2QSQL("CurrInst.PhysicalReference == '" + GetValue(ADKeyValue[this.ADkey], 0) + "'");
            RepositoryObjectList objList = LDAPSync.FindObjects(ADImportConfig.GetConfiguration(this._typeImport), "OrganizationUnit", expression);
            isManager = GetValue(ADKeyValue[this.ADkey], 2) == "Mgr";
            args = new Dictionary<string, dynamic>
            {
                {"currentOrgUnits", currentOrgUnits},
                {"newOrgUnit", objList},
                {"wasManager", wasManager},
                {"isManager", isManager}
            };

            if (objList == null || objList.Count.Equals(0))
            {
                // Remove links or create a new one...
                newValue = ADKeyValue[this.ADkey].First();
                return true;
            }
            else if (objList.Count > 1)
            {
                throw new Exception(ADMessages.ERROR_MAPPING + ADMessages.MAPPING_MULTIPLE_ORGUNIT + ADKeyValue[this.ADkey]);
            }
            else
            {
                newValue = objList.First().Name;
                if (currentOrgUnits.Count == 1 && objList.First().Id.Equals(currentOrgUnits.First().Id))
                {
                    if (wasManager && isManager || !wasManager && !isManager)
                        return false;
                }
            }
            return true;
        }

        private RepositoryObjectList GetCurrentOrgUnit(RepositoryObject repObj)
        {
            RepositoryObjectList currentOrgUnitsADSync = new RepositoryObjectList();


            IConfiguration config = repObj.GetConfiguration();
            IRepository rep = config.GetRepository();
            RepositoryObjectList currentOrgUnits = rep.GetUsingObjects(repObj.Id, config.Id, false, ObjectReferenceType.AttributeReference, "HasResources");
            currentOrgUnits.AddRange(rep.GetUsingObjects(repObj.Id, config.Id, false, ObjectReferenceType.AttributeReference, "HasResponsible"));
            foreach (RepositoryObject obj in currentOrgUnits)
            {
                if (!obj.Template.Equals("OrganizationUnit"))
                    continue;
                if (!obj.GetConfiguration().IsDefaultObjectRevision(obj.Id))
                    continue;
                if (!obj.Attributes.Contains("DataSource"))
                    continue;
                if (!obj.Attributes["DataSource"].Value.Equals("ADIntegration"))
                    continue;

                currentOrgUnitsADSync.Add(obj);
            }

            return currentOrgUnitsADSync;
        }
        private bool IsManager(Dictionary<string, List<string>> ADKeyValue)
        {
            List<string> cn, department;
            if (!ADKeyValue.TryGetValue("cn", out cn) || !ADKeyValue.TryGetValue("manager", out department))
                return false;

            if (cn.Count > 1 || department.Count > 1)
                throw new Exception(ADMessages.ERROR_MAPPING + ADMessages.MAPPING_MULTIPLE_MANAGER + department.PrintCollection());

            string cnStr = cn.First();
            string managerStr = GetValue(department, 2);

            int startVar = managerStr.IndexOf("CN=", StringComparison.Ordinal);
            int endVar = managerStr.IndexOf(",OU=", startVar + 1, StringComparison.Ordinal);

            string subManager = managerStr.Substring(startVar + 3, endVar - startVar - 3).Replace(@"\", "").ToLower();

            if (subManager.Equals(cnStr.ToLower()))
                return true;

            return false;
        }

        private string GetValue(List<string> department, int index)
        {
            string val = department.PrintCollection();
            string[] valArr = val.Split(';');

            if (valArr.Length <= index)
                return "";

            return valArr[index];
        }
    }
}
