﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ad_sync_filter.Business;
using ad_sync_filter.Data.Abstract;
using ad_sync_filter.Data.Interface;
using ad_sync_filter.Data.Mapping;
using Qis.Common;
using Qis.Common.AttributeValues;
using Qis.Module.Scripts;

namespace ad_sync_filter.Data.Delegators
{
    class CreateFromName : ACreateObjDelegator
    {
        private CreateFromName(string typeImport) : base(typeImport){  }

        private bool containsQISLogin;
        private string GUIDAttr;
        private string ADnameAttr; // Example shows how one can require QIS attributes to be set.

        public override RepositoryObject CreateQISObject(Dictionary<string, List<string>> ADKeyValue)
        {
            // Find if there is any attribute tagged with name.
            RepositoryObject retObj = null;

            if (String.IsNullOrEmpty(ADnameAttr))
            {
                foreach (KeyValuePair<string, List<string>> keyValuePair in ADKeyValue)
                {
                    ADMapping ADMapper;
                    if (!ADImportConfig.GetADMapping(this._typeImport).TryGetValue(keyValuePair.Key, out ADMapper))
                        continue;

                    if (ADMapper.QISkey.Equals("Name"))
                    {
                        ADnameAttr = keyValuePair.Key;
                    }
                    else if (ADMapper.ADkey.Equals("objectguid"))
                    {
                        GUIDAttr = keyValuePair.Key;
                    }
                    else if (ADMapper.QISkey.Equals("LOGIN"))
                    {
                        containsQISLogin = true;
                    }
                }
            }

            if (containsQISLogin)
            {
                IConfiguration conf = ADImportConfig.GetConfiguration(this._typeImport);
                string name = ADKeyValue[ADnameAttr].PrintCollection();
                string template = ADImportConfig.GetTextConfig(this._typeImport, "/QIStemplate");
                if (String.IsNullOrEmpty(GUIDAttr))
                {
                    retObj = LDAPSync.CreateObject(conf, name, template);
                }
                else
                {
                    retObj = LDAPSync.CreateObject(conf, name, template, ADKeyValue[GUIDAttr].PrintCollection());
                }

                if (retObj.Attributes.Contains("DataSource"))
                {
                    retObj.Attributes["DataSource"].Value = new PlainText("ADIntegration");
                    retObj.SaveData();
                }
            }
            return retObj;
        }

    }
}
