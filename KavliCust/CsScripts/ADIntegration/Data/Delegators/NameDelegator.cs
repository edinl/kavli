﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ad_sync_filter.Business;
using ad_sync_filter.Data.Mapping;
using Qis.Common;
using Qis.Common.AttributeValues;

namespace ad_sync_filter.Data.Delegators
{
    class NameDelegator : ADMapping
    {
        private NameDelegator(string ADkey, string QISKey, string typeImport)
            : base(ADkey, QISKey, typeImport)
        {
        }

        public override bool DoChange(ref RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue, Dictionary<string, object> args)
        {
            try
            {
                if (repObj.Attributes.Contains(this.QISkey))
                {
                    repObj.Name = this.GetNewValue(repObj, ADKeyValue);
                    repObj.SaveData();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public override bool IsChanged(RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue, out string oldValue, out string newValue, ref Dictionary<string, object> args)
        {
            newValue = this.GetNewValue(repObj, ADKeyValue);
            oldValue = "";

            if (repObj == null)
            {
                return false;
            }

            oldValue = this.GetOldValue(repObj, ADKeyValue);
            return !newValue.Equals(oldValue);
        }
		// modified 2016.11.17 - DisplayName: Last Name,SirName 
        public override string GetNewValue(RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue)
        {
            string name = base.GetNewValue(repObj, ADKeyValue);
            int endVar = name.IndexOf(",", StringComparison.Ordinal);
            string lastName = name.Substring(0, endVar);
            string firstName = name.Substring(endVar + 1, name.Length - endVar - 1);
            return firstName + " " + lastName;
        }
    }
}
