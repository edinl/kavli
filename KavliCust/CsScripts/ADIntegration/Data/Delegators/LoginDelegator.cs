﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ad_sync_filter.Business;
using ad_sync_filter.Data.Abstract;
using ad_sync_filter.Data.Mapping;
using Qis.Common;
using Qis.Common.AttributeValues;

namespace ad_sync_filter.Data.Delegators
{
    public class LoginDelegator : ADMapping
    {
        private LoginDelegator(string ADkey, string QISKey, string typeImport)
            : base(ADkey, QISKey, typeImport)
        {
        }

        public override bool DoChange(ref RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue, Dictionary<string, object> args)
        {
            try
            {
                if (repObj.Attributes.Contains(this.QISkey))
                {
                    repObj.Attributes[QISkey].Value = new PlainText(this.GetNewValue(repObj, ADKeyValue));
                    repObj.SaveData();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public override bool IsChanged(RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue, out string oldValue, out string newValue, ref Dictionary<string, object> args)
        {
            newValue = this.GetNewValue(repObj, ADKeyValue);
            oldValue = "";

            if (repObj == null)
            {
                return false;
            }

            oldValue = this.GetOldValue(repObj, ADKeyValue);
            return !newValue.Equals(oldValue);
        }

        public override string GetNewValue(RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue)
        {
            if (!ADKeyValue.ContainsKey("objectcategory") || !ADKeyValue.ContainsKey("samaccountname"))
                return "";

            string category = ADKeyValue["objectcategory"].First();
            string domain = "";
            int startVar = category.IndexOf("DC=", StringComparison.Ordinal);
            while (startVar > 0)
            {
                int endVar = category.IndexOf(",", startVar + 1, StringComparison.Ordinal);
                if (endVar < 0 || endVar <= startVar + 1)
                    endVar = category.Length;

                domain += category.Substring(startVar + 3, endVar - startVar - 3) + (endVar == category.Length ? "" : ".");

                if (endVar >= category.Length)
                    break;
                startVar = category.IndexOf("DC=", endVar, StringComparison.Ordinal);
            }

            return ADKeyValue["samaccountname"].First() + "@" + domain;
        }
        public override string GetScriptValue(Dictionary<string, List<string>> ADKeyValue)
        {
            return this.GetNewValue(null, ADKeyValue);
        }
    }
}
