﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ad_sync_filter.Business;
using ad_sync_filter.Data.Mapping;
using Qis.Common;
using Qis.Common.AttributeValues;

namespace ad_sync_filter.Data.Delegators
{
    class ManagerNameDelegator : ADMapping
    {
        private ManagerNameDelegator(string ADkey, string QISKey, string typeImport)
            : base(ADkey, QISKey, typeImport)
        {
        }

        public override bool DoChange(ref RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue, Dictionary<string, object> args)
        {
            try
            {
                if (repObj.Attributes.Contains(this.QISkey))
                {
                    if (repObj.Attributes[QISkey].Value is XhtmlText)
                    {
                        repObj.Attributes[QISkey].Value = new XhtmlText(this.GetNewValue(repObj, ADKeyValue));
                    }
                    else if (repObj.Attributes[QISkey].Value is PlainText)
                    {
                        repObj.Attributes[QISkey].Value = new PlainText(this.GetNewValue(repObj, ADKeyValue));
                    }
                    repObj.SaveData();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public override bool IsChanged(RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue, out string oldValue, out string newValue, ref Dictionary<string, object> args)
        {
            newValue = this.GetNewValue(repObj, ADKeyValue);
            oldValue = "";

            if (repObj == null)
            {
                return false;
            }

            oldValue = this.GetOldValue(repObj, ADKeyValue);
            return !newValue.Equals(oldValue);
        }

        public override string GetNewValue(RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue)
        {
            return GetManagerName(ADKeyValue);
        }

        private static string GetManagerName(Dictionary<string, List<string>> ADKeyValue)
        {
            List<string> manager;
            if (!ADKeyValue.TryGetValue("manager", out manager))
                return "";

            if (manager.Count > 1)
                throw new Exception(ADMessages.ERROR_MAPPING + ADMessages.MAPPING_MULTIPLE_MANAGER + manager.PrintCollection());

            string managerStr = manager.First();

            int startVar = managerStr.IndexOf("CN=", StringComparison.Ordinal);
            int endVar = managerStr.IndexOf(",OU=", startVar + 1, StringComparison.Ordinal);

            string subManager = managerStr.Substring(startVar + 3, endVar - startVar - 3).Replace(@"\", "");

            return subManager;
        }
    }
}
