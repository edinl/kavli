﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ad_sync_filter.Business;
using ad_sync_filter.Data.Abstract;
using ad_sync_filter.Data.Mapping;
using No.Qualisoft.WCF.Beeline.Business.Parser;
using Qis.Common;
using Qis.Common.AttributeValues;
using Qis.Common.Qsql.Operations;
using Qis.Common.Qsql.Operations.Logical;
using Qis.Common.Qsql.SelectQuery;
using Qis.Module.Scripts;

namespace ad_sync_filter.Data.Delegators
{
    public class LocationDelegator : ADMapping
    {
        private LocationDelegator(string ADkey, string QISKey, string typeImport)
            : base(ADkey, QISKey, typeImport)
        {
        }

        public override bool DoChange(ref RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue, Dictionary<string, object> args)
        {
            RepositoryObject newObj;

            // Remove from Old
            foreach (RepositoryObject currOrg in (RepositoryObjectList)args["currentLocations"])
            {
                if (LDAPSync.IsRepositoryObjectLocked(currOrg))
                    throw new Exception(ADMessages.ERROR_COULD_NOT_OBTAIN_LOCK + currOrg.Name);
                currOrg.LockObject();
                MultiLink inhabitedByCur = currOrg.Attributes[this.QISkey].Value as MultiLink;
                if (inhabitedByCur != null)
                {
                    QHyperLinkList inhabitedHyperCur = inhabitedByCur.GetHyperLinks();
                    foreach (QHyperLink resourceHyper in inhabitedHyperCur)
                    {
                        if (resourceHyper.ObjectId.Id.Equals(repObj.Id.Id))
                        {
                            inhabitedHyperCur.Remove(resourceHyper);
                            break;
                        }
                    }
                    currOrg.Attributes[this.QISkey].Value = new MultiLink(inhabitedHyperCur);
                }
                
                currOrg.SaveData();
                currOrg.ReleaseObjectLocks();
            }

            // Add to new
            if (args["newLocations"] == null || (args["newLocations"] as RepositoryObjectList).Count == 0)
            {
                newObj = LDAPSync.CreateObject(ADImportConfig.GetConfiguration(this._typeImport), ADKeyValue[this.ADkey].First(), "Location");
                newObj.Attributes["DataSource"].Value = new PlainText("ADIntegration");
            }
            else
            {
                newObj = (args["newLocations"] as RepositoryObjectList).First();
            }

            if (LDAPSync.IsRepositoryObjectLocked(newObj))
                throw new Exception(ADMessages.ERROR_COULD_NOT_OBTAIN_LOCK + newObj.Name);

            newObj.LockObject();
            MultiLink inhabitedBy = newObj.Attributes[QISkey].Value as MultiLink;
            QHyperLinkList inhabitedHyper = inhabitedBy.GetHyperLinks();
            inhabitedHyper.Add(repObj.GetLocalHyperlink());

            newObj.Attributes[QISkey].Value = new MultiLink(inhabitedHyper);
            newObj.SaveData();
            newObj.ReleaseObjectLocks();

            return true;
        }


        public override bool IsChanged(RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue, out string oldValue, out string newValue, ref Dictionary<string, object> args)
        {
            newValue = "";
            oldValue = "";

            // Find what Location Person is connected to now.
            RepositoryObjectList currentLocations = this.GetCurrentLocation(repObj);
            if (currentLocations == null)
            {
                throw new Exception(ADMessages.ERROR_MISSING_QIS_ATTR + this.QISkey);
            }
            else if (ADKeyValue[this.ADkey].Count > 1)
            {
                throw new Exception(ADMessages.ERROR_MAPPING + ADMessages.MAPPING_PERSON_MULTIPLE_LOCATION);
            }
            else if (currentLocations.Count == 1)
            {
                oldValue = currentLocations.First().Name;
            }
            else if (currentLocations.Count > 1)
            {
                foreach (RepositoryObject rep in currentLocations)
                    oldValue += rep.Name + ", ";
            }

            WhereExpression expression = ParseTree.Filter2QSQL("CurrInst.Name == '" + ADKeyValue[this.ADkey].First() + "' && CurrInst.DataSource == 'ADIntegration'");
            RepositoryObjectList objList = LDAPSync.FindObjects(ADImportConfig.GetConfiguration(this._typeImport), "Location", expression);
            args = new Dictionary<string, dynamic>
            {
                {"currentLocations", currentLocations},
                {"newLocations", objList}
            };

            if (objList == null || objList.Count.Equals(0))
            {
                // Remove links or create a new one...
                newValue = ADKeyValue[this.ADkey].First();
                return true;
            }
            else if (objList.Count > 1)
            {
                throw new Exception(ADMessages.ERROR_MAPPING + ADMessages.MAPPING_MULTIPLE_LOCATION + ADKeyValue[this.ADkey]);
            }
            else
            {
                newValue = objList.First().Name;
                if (currentLocations.Count > 0 && objList.First().Id.Equals(currentLocations.First().Id))
                {
                    return false;
                }
            }
            return true;
        }

        private RepositoryObjectList GetCurrentLocation(RepositoryObject repObj)
        {
            RepositoryObjectList currentOrgUnitsADSync = new RepositoryObjectList();


            IConfiguration config = repObj.GetConfiguration();
            IRepository rep = config.GetRepository();
            RepositoryObjectList currentOrgUnits = rep.GetUsingObjects(repObj.Id, config.Id, false, ObjectReferenceType.AttributeReference, QISkey);
            foreach (RepositoryObject obj in currentOrgUnits)
            {
                if (!obj.Template.Equals("Location"))
                    continue;
                if (!obj.GetConfiguration().IsDefaultObjectRevision(obj.Id))
                    continue;
                if (!obj.Attributes.Contains("DataSource"))
                    continue;
                if (!obj.Attributes["DataSource"].Value.Equals("ADIntegration"))
                    continue;

                currentOrgUnitsADSync.Add(obj);
            }

            return currentOrgUnitsADSync;
        }
    }
}
