﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ad_sync_filter.Business;
using ad_sync_filter.Data.Abstract;
using ad_sync_filter.Data.Mapping;
using Qis.Common;
using Qis.Module.Scripts;

namespace ad_sync_filter.Data.Delegators
{
    class ModifyIfChanged : AModifyObjDelegator
    {
        private ModifyIfChanged(string typeImport) : base(typeImport) { }

        public override bool CheckIfModify(RepositoryObject obj, Dictionary<string, List<string>> ADKeyValue)
        {
            List<string> whenChanged;
            if (ADKeyValue.TryGetValue("whenchanged", out whenChanged))
            {
                string qisAttr = ADImportConfig.GetADMapping(this._typeImport)["whenchanged"].QISkey;
                if (obj.Attributes.Contains(qisAttr))
                    if (whenChanged.PrintCollection().Equals(obj.Attributes[qisAttr].Value.ToString()))
                        return false;
            }
            return true;
        }
    }
}
