﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ad_sync_filter.Business;
using ad_sync_filter.Data.Abstract;
using ad_sync_filter.Data.Mapping;
using Qis.Common;
using Qis.Common.AttributeValues;

namespace ad_sync_filter.Data.Delegators
{
    public class TextDelegator : ADMapping
    {
        private TextDelegator(string ADkey, string QISKey, string typeImport)
            : base(ADkey, QISKey, typeImport)
        {
        }

        public override bool DoChange(ref RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue, Dictionary<string, object> args)
        {
            try
            {
                if (repObj.Attributes.Contains(this.QISkey))
                {
                    if (repObj.Attributes[QISkey].Value is XhtmlText)
                    {
                        repObj.Attributes[QISkey].Value = new XhtmlText(this.GetNewValue(repObj, ADKeyValue));
                    }
                    else if (repObj.Attributes[QISkey].Value is PlainText)
                    {
                        repObj.Attributes[QISkey].Value = new PlainText(this.GetNewValue(repObj, ADKeyValue));
                    }
                    repObj.SaveData();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public override bool IsChanged(RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue, out string oldValue, out string newValue, ref Dictionary<string, object> args)
        {
            newValue = this.GetNewValue(repObj, ADKeyValue);
            oldValue = "";

            if (repObj == null)
            {
                return false;
            }
                
            oldValue = this.GetOldValue(repObj, ADKeyValue);
            return !newValue.Equals(oldValue);
        }
    }
}
