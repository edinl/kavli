﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ad_sync_filter.Data.Interface
{
    interface IADObject
    {
        void AddKeyValue(string key, List<string> value);
        ADObjectChange GetChanges();
    }
}
