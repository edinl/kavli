﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Qis.Common;

namespace ad_sync_filter.Data.Interface
{
    interface IModifyObjDelegator
    {
        bool CheckIfModify(RepositoryObject obj, Dictionary<string, List<string>> ADKeyValue);
    }
}
