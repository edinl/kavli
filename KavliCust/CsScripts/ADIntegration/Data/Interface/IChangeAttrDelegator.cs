﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ad_sync_filter.Data.Abstract;
using Qis.Common;

namespace ad_sync_filter.Data.Interface
{
    interface IChangeAttrDelegator
    {
        string GetScriptValue(Dictionary<string, List<string>> ADKeyValue);
        string GetOldValue(RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue);
        string GetNewValue(RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue);

        bool IsChanged(RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue, out string oldValue, out string newValue, ref Dictionary<string, object> args);
        bool DoChange(ref RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue, Dictionary<string, object> args);
    }
}
