﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Qis.Common;

namespace ad_sync_filter.Data.Interface
{
    interface ICreateObjDelegator
    {
        RepositoryObject CreateQISObject(Dictionary<string, List<string>> ADKeyValue);
    }
}
