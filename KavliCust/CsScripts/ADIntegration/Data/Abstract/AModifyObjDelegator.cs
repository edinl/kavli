﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using ad_sync_filter.Data.Interface;
using ad_sync_filter.Data.Mapping;
using Qis.Common;

namespace ad_sync_filter.Data.Abstract
{
    public abstract class AModifyObjDelegator : IModifyObjDelegator
    {
        /// <summary>
        /// Checks if integration should do any modification to entry.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="ADKeyValue"></param>
        /// <returns>True: Modify entry. False: Do not modify entry.</returns>
        public abstract bool CheckIfModify(RepositoryObject obj, Dictionary<string, List<string>> ADKeyValue);
        protected string _typeImport;

        protected AModifyObjDelegator(string typeImport)
        {
            this._typeImport = typeImport;
        }
        public static AModifyObjDelegator CreateModifyObjDelegator(string typeImport, string delegator)
        {
            Type delegatorType = Type.GetType("ad_sync_filter.Data.Delegators." + delegator);
            if (delegatorType != null)
            {
                try
                {
                    return (AModifyObjDelegator)Activator.CreateInstance(delegatorType, BindingFlags.NonPublic | BindingFlags.Instance,
                        null, new object[] { typeImport }, null);
                }
                catch (Exception e)
                {
                    throw new Exception(ADMessages.ERROR_NO_DELEGATOR + e.ToString());
                }
            }
            return null;
        }
    }
}
