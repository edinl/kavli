﻿using System;
using System.Collections.Generic;
using System.Reflection;
using ad_sync_filter.Business;
using ad_sync_filter.Data.Interface;
using Qis.Common;

namespace ad_sync_filter.Data.Mapping
{
    public abstract class ADMapping : IChangeAttrDelegator
    {
        public string ADkey { get; private set; }
        public string QISkey { get; private set; }
        protected readonly string _typeImport;

        protected ADMapping(string ADkey, string QISKey, string typeImport)
        {
            this.ADkey = ADkey;
            this.QISkey = QISKey;
            this._typeImport = typeImport;
        }

        public static ADMapping CreateADMapping(string ADkey, string QISKey, string typeImport, string delegator)
        {
            Type delegatorType = Type.GetType("ad_sync_filter.Data.Delegators." + delegator);
            if (delegatorType != null)
            {
                try
                {
                    return (ADMapping)Activator.CreateInstance(delegatorType, BindingFlags.NonPublic | BindingFlags.Instance,
                        null, new object[] { ADkey, QISKey, typeImport }, null);
                }
                catch (Exception e)
                {
                    throw new Exception(ADMessages.ERROR_NO_DELEGATOR + e.ToString());
                }
            }
            return null;
        }

        public virtual string GetOldValue(RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue)
        {
            if (repObj.Attributes.Contains(this.QISkey))
            {
                return repObj.Attributes[this.QISkey].Value.ToString();               
            }
            return null;
        }
        public virtual string GetNewValue(RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue)
        {
            return ADKeyValue[this.ADkey].PrintCollection();
        }
        public virtual string GetScriptValue(Dictionary<string, List<string>> ADKeyValue)
        {
            if (!ADKeyValue.ContainsKey(this.ADkey))
                return null;
            return ADKeyValue[this.ADkey].PrintCollection();
        }

        public abstract bool IsChanged(RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue, out string oldValue, out string newValue, ref Dictionary<string, object> args);
        public abstract bool DoChange(ref RepositoryObject repObj, Dictionary<string, List<string>> ADKeyValue, Dictionary<string, object> args);
    }
}
