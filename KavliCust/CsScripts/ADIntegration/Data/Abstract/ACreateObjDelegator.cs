﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using ad_sync_filter.Data.Interface;
using ad_sync_filter.Data.Mapping;
using Qis.Common;

namespace ad_sync_filter.Data.Abstract
{
    public abstract class ACreateObjDelegator : ICreateObjDelegator
    {
        public abstract RepositoryObject CreateQISObject(Dictionary<string, List<string>> ADKeyValue);
        protected readonly string _typeImport;

        protected ACreateObjDelegator(string typeImport)
        {
            this._typeImport = typeImport;
        }
        public static ACreateObjDelegator CreateCreateObjDelegator(string typeImport, string delegator)
        {
            Type delegatorType = Type.GetType("ad_sync_filter.Data.Delegators." + delegator);
            if (delegatorType != null)
            {
                try
                {
                    return (ACreateObjDelegator)Activator.CreateInstance(delegatorType, BindingFlags.NonPublic | BindingFlags.Instance,
                        null, new object[]{typeImport}, null);
                }
                catch (Exception e)
                {
                    throw new Exception(ADMessages.ERROR_NO_DELEGATOR + e.ToString());
                }
            }
            return null;
        }
    }
}
