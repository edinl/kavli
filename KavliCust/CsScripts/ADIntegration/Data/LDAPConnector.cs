﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ad_sync_filter.Data
{
    class LDAPConnector : IDisposable
    {
        private string host;
        private string user;
        private string password;

        private DirectoryEntry directoryEntry;

        public LDAPConnector(string host, string user = null, string password = null)
        {
            this.host = Regex.Replace(host, @"ldap://", @"LDAP://", RegexOptions.IgnoreCase);
            this.user = user;
            this.password = password;
        }

        ~LDAPConnector()
        {
            this.Dispose();
        }
        public DirectoryEntry Connect()
        {
            if(this.directoryEntry != null)
                return directoryEntry;

            if (String.IsNullOrEmpty(this.host))
                return null;

            if(String.IsNullOrEmpty(user) && String.IsNullOrEmpty(password))
                this.directoryEntry = new DirectoryEntry(host);
            else
                this.directoryEntry = new DirectoryEntry(host, user, password, AuthenticationTypes.None);

           /* if (!String.IsNullOrEmpty(path))
                this.directoryEntry.Path = path;*/

            this.directoryEntry.RefreshCache();
            return this.directoryEntry;
        }


        public void Dispose()
        {
            if (this.directoryEntry == null)
                return;
            this.directoryEntry.Close();
            this.directoryEntry.Dispose();
        }
    }
}
