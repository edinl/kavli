﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using ad_sync_filter;
using ad_sync_filter.Business;
using ad_sync_filter.Data;
using ad_sync_filter.Data.Abstract;
using ad_sync_filter.Data.Delegators;
using ad_sync_filter.Data.Mapping;
using Qef.Common;
using Qef.Common.Log;
using Qef.Common.Security;
using Qis.Common;
using Qis.Common.Qsql;
using Qis.Common.Qsql.Operations;
using Qis.Common.Qsql.Operations.Logical;
using Qis.Common.Qsql.SelectQuery;
using Qis.Common.Scripting;

namespace Qis.Module.Scripts
{
	class LDAPSync
	{
		#region Public Methods
		// Print(callServerScript(RepositoryID,"LDAPSync","QLM_ImportAD","PersonImport"));
		public static void QLM_ImportAD(GeneralScriptArgs args)
		{
			string typeImport = args == null ? "PersonImport" : args.Parameters[0];
			ImportAD(typeImport);
		}

		// Keep this for backwards compatability.
        public static void ImportAD_63(string type)
		{
			ImportAD(type);
		}

		public static void ImportAD(string type)
		{
			var typeImport = type ?? "PersonImport";
			ADLogger logger = new ADLogger(typeImport);
			try
			{
				logger.Initalize();
			}
			catch (Exception e)
			{
				throw new Exception(ADMessages.ERROR_CONFIG_NOT_FOUND_P1 + typeImport + ADMessages.ERROR_CONFIG_NOT_FOUND_P2 + e.ToString());
				return;
			}

			// Forceably reload config cache.
            ADImportConfig.ClearCache(typeImport);

			try
			{
				ADImportConfig.ConnectToConfig(CallContext.QisServer, typeImport);
			}
			catch (Exception e)
			{
				logger.AddRow(ADMessages.ERROR_CONNECTING_TO_QIS + e.ToString(), ADLogLevels.Fatal);
				return;
            }

            Thread a = new Thread(() => RunImportAD(typeImport, logger));
            a.Start();
        }
        #endregion

        #region Private Methods

        private static void ImportToQIS(string typeImport, ADLogger logger, List<ADObject> adObjects)
        {
            int totalCount = adObjects.Count;
            int totalModified = 0;
            int totalNew = 0;
            int totalProblem = 0;
            logger.AddRow(ADMessages.STARTING_IMPORT_TO_QIS, ADLogLevels.Info);

            foreach (ADObject adObject in adObjects)
            {
                try
                {
                    logger.AddRow(ADMessages.STARTING_GETTING_CHANGES_FOR_OBJ_P1 +
                    adObject.GetParseScriptValue(ADImportConfig.GetTextConfig(typeImport, "/QISSearch")) + ADMessages.STARTING_GETTING_CHANGES_FOR_OBJ_P2, ADLogLevels.Debug);
                    ADObjectChange changes = adObject.GetChanges();
                    if (String.IsNullOrEmpty(changes.Problem))
                    {
                        if (changes.IsNew)
                            totalNew++;
                        else if (changes.WasUpdated)
                            totalModified++;
                    }
                    else
                    {
                        totalProblem++;
                    }
                    logger.AddRow(adObject, ADLogLevels.Info);
                }
                catch (Exception e)
                {
                    logger.AddRow(ADMessages.ERROR_IMPORT_TO_QIS + e.ToString(), ADLogLevels.Fatal);
                }

            }

            logger.AddRow(ADMessages.FINISH_IMPORT_TO_QIS, ADLogLevels.Info);
            logger.Finish(totalCount, totalModified, totalNew, totalProblem);
        }
        private static void RunImportAD(string typeImport, ADLogger logger)
        {
            if (String.IsNullOrEmpty(ADImportConfig.GetTextConfig(typeImport, "")))
                return;

            logger.AddRow(ADMessages.CONFIG + ADImportConfig.GetXMLConfig(typeImport, "").InnerXml, ADLogLevels.Info);
            List<ADObject> adObjects;

            FetchADValues(typeImport, logger, out adObjects);
            ImportToQIS(typeImport, logger, adObjects);
        }
        private static List<ADObject> FetchADValues(string typeImport, ADLogger logger, out List<ADObject> adObjects)
        {
            adObjects = new List<ADObject>();
            logger.AddRow(ADMessages.STARTING_CONNECTING_TO_AD, ADLogLevels.Debug);
            try
            {
                #region Connect to AD and initialze config
                string user = ADImportConfig.GetTextConfig(typeImport, "/User", true);
                string pass = ADImportConfig.GetTextConfig(typeImport, "/Password", true);
                string host = ADImportConfig.GetTextConfig(typeImport, "/Host", true);
                using (LDAPConnector ldapConnector = new LDAPConnector(host, user, pass))
                {
                    using (DirectorySearcher searcher = new DirectorySearcher(ldapConnector.Connect()))
                    {
                        searcher.SizeLimit = int.MaxValue;
                        searcher.PageSize = 1000;
                        searcher.Filter = ADImportConfig.GetTextConfig(typeImport, "/Filter", true);

                        logger.AddRow(ADMessages.FINISH_CONNECTING_TO_AD, ADLogLevels.Debug);
                        logger.AddRow(ADMessages.STARTING_FETCHING_AD_OBJECTS, ADLogLevels.Info);
                        foreach (XmlNode node in ADImportConfig.GetXMLConfig(typeImport, "/Attributes"))
                        {
                            string ADkey = node.Attributes.GetNamedItem("ADkey").Value.ToLower(); // Remember this to be lowercase!!
                            string Delegator = node.Attributes.GetNamedItem("Delegator").Value;
                            string QISkey = node.Attributes.GetNamedItem("QISkey").Value;
                            string dependantFields = node.Attributes.GetNamedItem("DependantADFields").Value;
                            searcher.PropertiesToLoad.Add(ADkey);
                            foreach (string str in dependantFields.Split(','))
                            {
                                searcher.PropertiesToLoad.Add(str);
                            }
                            ADImportConfig.AddADMapping(typeImport, ADkey, ADMapping.CreateADMapping(ADkey, QISkey, typeImport, Delegator));
                        }

                        string createDelegator = ADImportConfig.GetTextConfig(typeImport, "/QISCreateDelegator", true);
                        ADImportConfig.AddCreateObjDelegator(typeImport, ACreateObjDelegator.CreateCreateObjDelegator(typeImport, createDelegator));

                        string modifyDelegator = ADImportConfig.GetTextConfig(typeImport, "/QISPreModifyDelegator", true);
                        ADImportConfig.AddModifyObjDelegator(typeImport, AModifyObjDelegator.CreateModifyObjDelegator(typeImport, modifyDelegator));
                #endregion
                        #region Fetch values from AD
                        int objectsIterated = 0;
                        int maxObjects;
                        Int32.TryParse(ADImportConfig.GetTextConfig(typeImport, "/Limit", true), out maxObjects);
                        foreach (XmlNode node in ADImportConfig.GetXMLConfig(typeImport, "/Paths"))
                        {
                            searcher.SearchRoot.Path = Regex.Replace(node.InnerText, @"ldap://", @"LDAP://", RegexOptions.IgnoreCase);

                            try
                            {
                                searcher.SearchRoot.RefreshCache();
                                using (SearchResultCollection searchResults = searcher.FindAll())
                                {

                                    foreach (SearchResult result in searchResults)
                                    {
                                        ADObject adObject = new ADObject(typeImport);
                                        logger.AddRow(ADMessages.STARTING_FETCHING_AD_SINGLE, ADLogLevels.Info);
                                        foreach (string propertyName in result.Properties.PropertyNames)
                                        {
                                            if (propertyName.Equals("adspath"))
                                                continue;
                                            try
                                            {
                                                List<string> Value = new List<string>();
                                                foreach (Object key in result.GetDirectoryEntry().Properties[propertyName])
                                                {
                                                    //retStr += key.ToString() + ",";
                                                    Value.Add(GetADString(key));
                                                    //Value.Add(key.ToString());
                                                }
                                                logger.AddRow(ADMessages.FETCH_AD_ATTR + propertyName + ADMessages.VALUE + Value.PrintCollection() + ADMessages.FETCH_AD_ATTR_END, ADLogLevels.Debug);
                                                adObject.AddKeyValue(propertyName, Value);
                                            }
                                            catch (Exception e)
                                            {
                                                logger.AddRow(ADMessages.ERROR_FETCHING_ATTRIBUTE + e.ToString(), ADLogLevels.Fatal);
                                            }
                                        }
                                        logger.AddRow(ADMessages.FINISH_FETCHING_AD_SINGLE, ADLogLevels.Info);
                                        objectsIterated++;
                                        adObjects.Add(adObject);

                                        if (maxObjects != 0 && objectsIterated > maxObjects)
                                            break;
                                    }
                                }
                            }
                            catch (Exception e)
                            {
                                logger.AddRow(ADMessages.ERROR_CONNECTING_TO_PATH + node.InnerText + " - " + e.Message, ADLogLevels.Fatal);
                            }
                        }

                        logger.AddRow(ADMessages.FINISH_FETCHING_AD_OBJECTS, ADLogLevels.Info);
                    }
                }
                        #endregion
            }
            catch (Exception e)
            {
                logger.AddRow(ADMessages.ERROR_CONNECTING_TO_AD + e.ToString(), ADLogLevels.Fatal);
            }

            return adObjects;
        }
        #endregion

        #region Utils
        public static RepositoryObjectList FindObjects(IConfiguration config, string template, WhereExpression whereExpression)
        {
            QueryOptions queryOptions = new QueryOptions
            {
                Comparison = SqlComparison.DefaultCaseInsensitive
            };
            SelectExpression query = new SelectExpression(
                new ColumnExpressions(new[] { new ObjectIdExpression() }),
                new TemplateTable(template),
                whereExpression,
                null);
            return config.GetRepository().RunQsqlExpression(query, config.Id, queryOptions);
        }
        public static bool IsRepositoryObjectLocked(RepositoryObject obj)
        {
            return (
                       obj.IsDeleted || obj.IsStub || obj.IsFrozen ||
                       (
                           obj.GetLockStatus().Type != ObjectLockResultType.NotLocked &&
                           obj.GetLockStatus().SessionId.ToString() != obj.CurrentSession.Id.ToString()
                       )
                   );

        }
        // Wrapper method
        public static RepositoryObject CreateObject(IConfiguration config, string name, string template, string guid = null)
        {
            if (String.IsNullOrEmpty(guid))
                return config.CreateObject(name, template);
            return config.CreateObjectWithId(new Oid(guid).NewRevision(), name, template);
        }

        private static string GetADString(Object value)
        {
            Byte[] val = value as Byte[];
            if (val != null)
            {
                return new Guid(val).ToString();
            }
            else
                return value.ToString();
        }
        #endregion
    }
}
