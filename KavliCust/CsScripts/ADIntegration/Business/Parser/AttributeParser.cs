﻿
using System.Collections.Generic;
using No.Qualisoft.WCF.Beeline.Business.Assembler;

namespace No.Qualisoft.WCF.Beeline.Business.Parser
{
    class AttributeParser : IParserNode
    {
        private readonly static string Operand = "CurrInst.";
        
        public override bool WillParse(string script)
        {
            return script.Length > Operand.Length &&
                script.Substring(0, Operand.Length) == Operand;
        }

        public override IAssemblerNode Parse(string script)
        {
            string firstPart = script.Substring(Operand.Length);

            firstPart = firstPart.Replace("AuditMD", "sys_Modified");
            firstPart = firstPart.Replace("AuditMN", "sys_ModifiedBy");
            firstPart = firstPart.Replace("AuditCD", "sys_Created");
            firstPart = firstPart.Replace("AuditCN", "sys_CreatedBy");
            firstPart = firstPart.Replace("ObjID", "sys_ObjectId");
            firstPart = firstPart.Replace("ObjId", "sys_ObjectId");

            return new AttributeAssembler(firstPart);
        }
    }
}
