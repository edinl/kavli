﻿
using System.Collections.Generic;
using No.Qualisoft.WCF.Beeline.Business.Assembler;

namespace No.Qualisoft.WCF.Beeline.Business.Parser
{
    class GreaterOrEqualParser : IParserNode
    {
        private readonly static string Operand = ">=";
        
        public override bool WillParse(string script)
        {
            return script.Contains(Operand);
        }

        public override IAssemblerNode Parse(string script)
        {
            int operandPos = script.LastIndexOf(Operand, System.StringComparison.OrdinalIgnoreCase);
            string firstPart = script.Substring(0, operandPos).Trim();
            string secondPart = script.Substring(operandPos + Operand.Length).Trim();

            IParserNode firstParser = null;
            IParserNode secondParser = null;
            foreach (IParserNode child in Children)
            {
                if (child.WillParse(firstPart))
                {
                    firstParser = child;
                }
                if (child.WillParse(secondPart))
                {
                    secondParser = child;
                }
            }

            if ( firstParser == null )
                throw new ParseException("Failed to parse (" + firstPart + ")");
            if (secondParser == null)
                throw new ParseException("Failed to parse (" + secondPart + ")");

            return new GreaterOrEqualAssembler(
                firstParser.Parse(firstPart), 
                secondParser.Parse(secondPart)
            );
        }
    }
}
