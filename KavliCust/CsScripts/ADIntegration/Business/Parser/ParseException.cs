﻿using System;

namespace No.Qualisoft.WCF.Beeline.Business.Parser
{
    [Serializable]
    class ParseException : Exception
    {
        public ParseException(string msg) : base(msg) { }
    }
}
