﻿
using System.Collections.Generic;
using No.Qualisoft.WCF.Beeline.Business.Assembler;

namespace No.Qualisoft.WCF.Beeline.Business.Parser
{
    class ConstantParser : IParserNode
    {
        private readonly static string Operand = "'";
        
        public override bool WillParse(string script)
        {
            return script.Substring(0, 1) == Operand && script.Substring(script.Length-1, 1) == Operand;
        }

        public override IAssemblerNode Parse(string script)
        {
            string firstPart = script.Substring(1,script.Length-2);


            return new ConstantAssembler(firstPart);
        }
    }
}
