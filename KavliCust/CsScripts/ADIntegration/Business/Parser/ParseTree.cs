﻿
using System.Collections.Generic;
using No.Qualisoft.WCF.Beeline.Business.Assembler;
using Qis.Common.Qsql;
using Qis.Common.Qsql.SelectQuery;

namespace No.Qualisoft.WCF.Beeline.Business.Parser
{
    class ParseTree : IParserNode
    {
        public static WhereExpression Filter2QSQL(string script)
        {
            if (string.IsNullOrEmpty(script))
                return null;

            ParseTree parser = new ParseTree();

            IAssemblerNode assembler = parser.Parse(script);

            if (assembler == null)
                return null;

            IOperand operand = assembler.Assemble();

            if (operand == null)
                return null;

            if (!(operand is ILogicalExpression))
                throw new AssembleException("Assembler: Operand was not logical expression!");

            return new WhereExpression(operand as ILogicalExpression);
        }

        public ParseTree()
        {
            IParserNode logAnd = new AndParser();
            IParserNode logOr = new OrParser();

            IParserNode logEq = new EqualParser();
            IParserNode logNeq = new NotEqualParser();
            IParserNode logGt = new GreaterParser();
            IParserNode logGet = new GreaterOrEqualParser();
            IParserNode logLt = new LessParser();
            IParserNode logLet = new LessOrEqualParser();

            IParserNode scalConst = new ConstantParser();
            IParserNode scalAttr = new AttributeParser();

            logAnd.Children = new List<IParserNode>
                {
                    logEq, logNeq, logGt, logGet, logLt, logLet,
                    logAnd, logOr, 
                    
                };
            logOr.Children = new List<IParserNode>
                {
                    logEq, logNeq, logGt, logGet, logLt, logLet,
                    logAnd, logOr, 
                };

            logEq.Children = new List<IParserNode>
                {
                    scalAttr,scalConst
                };
            logNeq.Children = new List<IParserNode>
                {
                    scalAttr,scalConst
                };
            logGt.Children = new List<IParserNode>
                {
                    scalAttr,scalConst
                };
            logGet.Children = new List<IParserNode>
                {
                    scalAttr,scalConst
                };
            logLt.Children = new List<IParserNode>
                {
                    scalAttr,scalConst
                };
            logLet.Children = new List<IParserNode>
                {
                    scalAttr,scalConst
                };



            Children = new List<IParserNode>
                {
                    logEq, logNeq, logGt, logGet, logLt, logLet,
                    logAnd, logOr, 
                };
        }

        public override bool WillParse(string script)
        {
                return true;
        }

        public override IAssemblerNode Parse(string script)
        {
            if (script.Trim() == "")
                return null;

            IParserNode parser = null;
            foreach (IParserNode child in Children)
            {
                if (child.WillParse(script))
                    parser = child;
            }

            if (parser == null)
                throw new ParseException("Failed to parse (" + script + ")");

            return parser.Parse(script);
        }
    }
}
