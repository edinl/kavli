﻿
using System.Collections.Generic;
using No.Qualisoft.WCF.Beeline.Business.Assembler;

namespace No.Qualisoft.WCF.Beeline.Business.Parser
{
    abstract class IParserNode
    {
        public List<IParserNode> Children;

        public abstract bool WillParse(string script);
        public abstract IAssemblerNode Parse(string script);
    }
}
