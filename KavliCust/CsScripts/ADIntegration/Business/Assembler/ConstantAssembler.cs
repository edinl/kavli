﻿using No.Qualisoft.WCF.Beeline.Business.Parser;
using Qis.Common.Qsql;
using Qis.Common.Qsql.Operations;
using Qis.Common.Qsql.Operations.Logical;

namespace No.Qualisoft.WCF.Beeline.Business.Assembler
{
    class ConstantAssembler : IAssemblerNode
    {
        public string Value;

        public ConstantAssembler(string value)
        {
            Value = value;
        }

        public IOperand Assemble()
        {
            return new ConstantOperand(Value);
        }
    }
}
