﻿using No.Qualisoft.WCF.Beeline.Business.Parser;
using Qis.Common.Qsql;
using Qis.Common.Qsql.Operations.Logical;

namespace No.Qualisoft.WCF.Beeline.Business.Assembler
{
    class AndAssembler : IAssemblerNode
    {
        public IAssemblerNode FirstParam;
        public IAssemblerNode SecondParam;

        public AndAssembler(IAssemblerNode first, IAssemblerNode second)
        {
            FirstParam = first;
            SecondParam = second;
        }

        public IOperand Assemble()
        {
            IOperand firstOperand = FirstParam.Assemble();
            IOperand secondOperand = SecondParam.Assemble();

            if ( !(firstOperand is ILogicalExpression) )
                throw new AssembleException("AND Assembler: First operand was not logical expression!");

            if (!(secondOperand is ILogicalExpression))
                throw new AssembleException("AND Assembler: Second operand was not logical expression!");

            return new AndOperationExpression(
                firstOperand as ILogicalExpression,
                secondOperand as ILogicalExpression
            );
        }
    }
}
