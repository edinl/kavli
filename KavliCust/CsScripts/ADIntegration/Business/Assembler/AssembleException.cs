﻿using System;

namespace No.Qualisoft.WCF.Beeline.Business.Parser
{
    [Serializable]
    class AssembleException : Exception
    {
        public AssembleException(string msg) : base(msg) { }
    }
}
