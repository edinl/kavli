﻿
using Qis.Common.Qsql;

namespace No.Qualisoft.WCF.Beeline.Business.Assembler
{
    interface IAssemblerNode
    {
        IOperand Assemble();
    }
}
