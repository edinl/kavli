﻿using System;
using No.Qualisoft.WCF.Beeline.Business.Parser;
using Qis.Common.Qsql;
using Qis.Common.Qsql.Operations;
using Qis.Common.Qsql.Operations.Logical;

namespace No.Qualisoft.WCF.Beeline.Business.Assembler
{
    class AttributeAssembler : IAssemblerNode
    {
        public string Value;

        public AttributeAssembler(string value)
        {
            Value = value;
        }

        public IOperand Assemble()
        {
            return new AttributeValueOperand(Value);
        }
    }
}
