﻿using No.Qualisoft.WCF.Beeline.Business.Parser;
using Qis.Common.Qsql;
using Qis.Common.Qsql.Operations.Logical;

namespace No.Qualisoft.WCF.Beeline.Business.Assembler
{
    class EqualAssembler : IAssemblerNode
    {
        public IAssemblerNode FirstParam;
        public IAssemblerNode SecondParam;

        public EqualAssembler(IAssemblerNode first, IAssemblerNode second)
        {
            FirstParam = first;
            SecondParam = second;
        }

        public IOperand Assemble()
        {
            IOperand firstOperand = FirstParam.Assemble();
            IOperand secondOperand = SecondParam.Assemble();

            if ( !(firstOperand is IScalarExpression) )
                throw new AssembleException("EQUALS Assembler: First operand was not scalar expression!");

            if (!(secondOperand is IScalarExpression))
                throw new AssembleException("EQUALS Assembler: Second operand was not scalar expression!");

            return new LikeOperationExpression(
                firstOperand as IScalarExpression,
                secondOperand as IScalarExpression
            );
        }
    }
}
