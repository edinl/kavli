﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ad_sync_filter.Data;
using ad_sync_filter.Data.Mapping;
using Qef.Common.Log;

namespace ad_sync_filter.Business
{
    class ADLogger
    {
        private string _typeImport;
        private string filePath;

        public ADLogger(string typeImport)
        {
            this._typeImport = typeImport;
        }

        public void AddRow(string row, ADLogLevels logLevel)
        {
            if (String.IsNullOrEmpty(this.filePath))
                return;
            try
            {
                using (FileStream filestream = File.Open(this.filePath, FileMode.Append, FileAccess.Write))
                {
                    using (StreamWriter streamWriter = new StreamWriter(filestream))
                    {
                        streamWriter.WriteLine(row);
                    }
                }
            }
            catch (Exception e)
            {
                
            }
        }

        public void AddRow(ADObject adObject, ADLogLevels logLevel)
        {
            if (String.IsNullOrEmpty(this.filePath))
                return;
            try
            {
                using (FileStream filestream = File.Open(this.filePath, FileMode.Append, FileAccess.Write))
                {
                    using (StreamWriter streamWriter = new StreamWriter(filestream))
                    {
                        ADObjectChange changes = adObject.GetChanges();

                        if (changes.IsNew)
                            streamWriter.WriteLine(ADMessages.STARTING_CREATING_OBJ_P1 + "" + ADMessages.STARTING_CREATING_OBJ_P2);
                        else if (!changes.WasUpdated)
                            streamWriter.WriteLine(ADMessages.MODIFYING_OBJ_NOCHANGES);
                        else
                            streamWriter.WriteLine(ADMessages.MODIFYING_OBJ_P1 + "" + ADMessages.MODIFYING_OBJ_P2);

                        if (!String.IsNullOrEmpty(changes.Problem))
                            streamWriter.WriteLine(changes.Problem);

                        foreach (KeyValuePair<string, ADObjectChange.ADObjectDifferences> keyValue in changes.differenceses)
                        {
                            ADMapping ADMapper;
                            if (!ADImportConfig.GetADMapping(this._typeImport).TryGetValue(keyValue.Key, out ADMapper))
                                continue;

                            if (!String.IsNullOrEmpty(keyValue.Value.Problem))
                                streamWriter.WriteLine(keyValue.Value.Problem);
                            else if (changes.IsNew)
                                streamWriter.WriteLine(ADMessages.MODIFYING_ATTR_P1 + keyValue.Key + ADMessages.QISATTR + ADMapper.QISkey +
                                                       ADMessages.NEW_VALUE + keyValue.Value.NewValue + ADMessages.MODIFYING_ATTR_P2);
                            else if (keyValue.Value.WasUpdated)
                                streamWriter.WriteLine(ADMessages.MODIFYING_ATTR_P1 + keyValue.Key + ADMessages.QISATTR + ADMapper.QISkey + ADMessages.OLD_VALUE + keyValue.Value.OldValue +
                                                       ADMessages.NEW_VALUE + keyValue.Value.NewValue + ADMessages.MODIFYING_ATTR_P2);
                        }

                        streamWriter.WriteLine(ADMessages.FINISH_CREATING_OBJ_P1 + "" + ADMessages.FINISH_CREATING_OBJ_P2);
                    }
                }
            }
            catch (Exception e)
            {
                
            }
        }

        public void Initalize()
        {
            string dirPath = ADImportConfig.GetTextConfig(this._typeImport, "/LogPath");
            if (!Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);

            this.filePath = dirPath + DateTime.Now.ToString("u").Replace(":","-") + "__" + _typeImport + ".txt";
            try
            {
                using (FileStream filestream = File.Open(this.filePath, FileMode.Append, FileAccess.Write))
                {
                    using (StreamWriter streamWriter = new StreamWriter(filestream))
                    {
                        streamWriter.WriteLine(ADMessages.STARTING_AD_INTEGRATION_P1 + this._typeImport + ADMessages.AT + DateTime.Now + ADMessages.STARTING_AD_INTEGRATION_P2);
                    }
                }
            }
            catch (Exception e)
            {
                
            }
        }

        public void Finish(int total, int totalModified, int totalNew, int totalProblem)
        {
            if (String.IsNullOrEmpty(this.filePath))
                return;
            try
            {
                using (FileStream filestream = File.Open(this.filePath, FileMode.Append, FileAccess.Write))
                {
                    using (StreamWriter streamWriter = new StreamWriter(filestream))
                    {
                        streamWriter.WriteLine(ADMessages.FINISH_AD_TOTALCOUNT + total + ADMessages.FINISH_AD_TOTALCOUNT_MODOBJS + totalModified
                                               + ADMessages.FINISH_AD_TOTALCOUNT_NEWOBJS + totalNew + ADMessages.FINISH_AD_TOTALCOUNT_PROBOBJS + totalProblem + ADMessages.FINISH_AD_TOTALCOUNT_END);
                        streamWriter.WriteLine(ADMessages.FINISH_AD_INTEGRATION_P1 + DateTime.Now + ADMessages.FINISH_AD_INTEGRATION_P2);
                    }
                }
            }
            catch (Exception e)
            {
                
            }
        }
    }
}
