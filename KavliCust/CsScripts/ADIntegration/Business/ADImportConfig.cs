﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using ad_sync_filter.Data;
using ad_sync_filter.Data.Abstract;
using ad_sync_filter.Data.Interface;
using ad_sync_filter.Data.Mapping;
using Qef.Common;
using Qis.Common;

namespace ad_sync_filter.Business
{
    public static class ADImportConfig
    {
        private const string c_configFile = @"C:\Program Files (x86)\QualiWare\QLM\Models\KavliCust\CsScripts\ADIntegration\config.xml";
        private static XmlDocument configDoc;

        #region Config IConfiguration
        private static readonly Dictionary<string, IConfiguration> Configurations = new Dictionary<string, IConfiguration>();
        public static bool ConnectToConfig(IQisServer qisServer, string typeImport)
        {
            if (!Configurations.ContainsKey(typeImport))
            {
                string repository = GetTextConfig(typeImport, "/QISrepository");
                if (String.IsNullOrEmpty(repository))
                    return false;

                IRepository rep = qisServer.FindRepository(GetTextConfig(typeImport, "/QISrepository"));
                if (!rep.IsConnected)
                {
                    rep.Connect(QisConst.DefaultRepositoryRole);
                }
                string configuration = GetTextConfig(typeImport, "/QISconfiguration");

                if (String.IsNullOrEmpty(configuration))
                    return false;

                Configurations.Add(typeImport, rep.FindConfiguration(GetTextConfig(typeImport, "/QISconfiguration")));
            }
            if (!Configurations[typeImport].IsConnected)
            {
                Configurations[typeImport].Connect();
            }
            return true;
        }
        public static IConfiguration GetConfiguration(string typeImport)
        {
            if (!Configurations.ContainsKey(typeImport))
                return null;

            IRepository rep = Configurations[typeImport].GetRepository();
            if (!rep.IsConnected)
            {
                rep.Connect(QisConst.DefaultRepositoryRole);
            }

            if (!Configurations[typeImport].IsConnected)
            {
                Configurations[typeImport].Connect();
            }
            
            return Configurations[typeImport];
        }
#endregion
        #region Config CreateObjDelegator
        private static readonly Dictionary<string, ACreateObjDelegator> ConfigCreateDelegators = new Dictionary<string, ACreateObjDelegator>();
        public static ACreateObjDelegator GetCreateObjDelegator(string typeImport)
        {
            if (!ConfigCreateDelegators.ContainsKey(typeImport))
                return null;
            return ConfigCreateDelegators[typeImport];
        }

        public static void AddCreateObjDelegator(string typeImport, ACreateObjDelegator createObjDelegator)
        {
            if (createObjDelegator == null)
                return;

            if (!ConfigCreateDelegators.ContainsKey(typeImport))
                ConfigCreateDelegators.Add(typeImport, createObjDelegator);

            else
                ConfigCreateDelegators[typeImport] = createObjDelegator;
        }
#endregion
        #region Config ModifyObjDelegator
        private static readonly Dictionary<string, AModifyObjDelegator> ConfigModifyDelegators = new Dictionary<string, AModifyObjDelegator>();
        public static AModifyObjDelegator GetModifyObjDelegator(string typeImport)
        {
            if (!ConfigModifyDelegators.ContainsKey(typeImport))
                return null;
            return ConfigModifyDelegators[typeImport];
        }

        public static void AddModifyObjDelegator(string typeImport, AModifyObjDelegator modifyObjDelegator)
        {
            if (modifyObjDelegator == null)
                return;

            if (!ConfigModifyDelegators.ContainsKey(typeImport))
                ConfigModifyDelegators.Add(typeImport, modifyObjDelegator);

            else
                ConfigModifyDelegators[typeImport] = modifyObjDelegator;
        }
        #endregion
        #region Config ADMapping
        private static readonly Dictionary<string, Dictionary<string, ADMapping>> ConfigADMapping = new Dictionary<string, Dictionary<string, ADMapping>>();
        public static Dictionary<string, ADMapping> GetADMapping(string typeImport)
        {
            if (!ConfigADMapping.ContainsKey(typeImport))
                return null;
            return ConfigADMapping[typeImport];
        }

        public static void AddADMapping(string typeImport, string key, ADMapping value)
        {
            if (!ConfigADMapping.ContainsKey(typeImport))
                ConfigADMapping.Add(typeImport, new Dictionary<string, ADMapping>());

            if (ConfigADMapping[typeImport].ContainsKey(key))
                ConfigADMapping[typeImport][key] = value;
            else
                ConfigADMapping[typeImport].Add(key, value);
        }
        #endregion
        #region Config Text
        private static readonly Dictionary<string, Dictionary<string, string>> ConfigTextCache = new Dictionary<string, Dictionary<string, string>>();
        public static string GetTextConfig(string typeImport, string key, bool canBeEmpty = false)
        {
            if (!ConfigTextCache.ContainsKey(typeImport))
            {
                ConfigTextCache.Add(typeImport, new Dictionary<string, string>());
                if (configDoc == null)
                {
                    configDoc = new XmlDocument();
                    configDoc.Load(c_configFile);
                }
            }

            if (!ConfigTextCache[typeImport].ContainsKey(key))
            {
                XmlNode xnode = configDoc.SelectSingleNode("/configuration/ADImports/" + typeImport + key);

                if (xnode == null)
                    return null;

                if (!canBeEmpty && (xnode.InnerText == null || xnode.InnerText.IsNull() || xnode.InnerText.Equals("")))
                    return null;

                ConfigTextCache[typeImport].Add(key, xnode.InnerText);
            }

            return ConfigTextCache[typeImport][key];
        }
        #endregion
        #region Config XML
        private static readonly Dictionary<string, Dictionary<string, XmlNode>> ConfigXMLCache = new Dictionary<string, Dictionary<string, XmlNode>>();
        public static XmlNode GetXMLConfig(string typeImport, string key)
        {
            if (!ConfigXMLCache.ContainsKey(typeImport))
            {
                ConfigXMLCache.Add(typeImport, new Dictionary<string, XmlNode>());
                if (configDoc == null)
                {
                    configDoc = new XmlDocument();
                    configDoc.Load(c_configFile);
                }
            }

            if (!ConfigXMLCache[typeImport].ContainsKey(key))
            {
                XmlNode xnode = configDoc.SelectSingleNode("/configuration/ADImports/" + typeImport + key);

                if (xnode == null)
                    return null;

                ConfigXMLCache[typeImport].Add(key, xnode);
            }

            return ConfigXMLCache[typeImport][key];
        }
        #endregion

        #region Utils
        public static void ClearCache(string typeImport)
        {
            configDoc = null;

            if (ConfigTextCache.ContainsKey(typeImport)) { 
                ConfigTextCache[typeImport].Clear();
                ConfigTextCache.Remove(typeImport);
            }

            if (ConfigXMLCache.ContainsKey(typeImport))
            {
                ConfigXMLCache[typeImport].Clear();
                ConfigTextCache.Remove(typeImport);
            }

            if (ConfigADMapping.ContainsKey(typeImport))
            {
                ConfigADMapping[typeImport].Clear();
                ConfigADMapping.Remove(typeImport);
            }

            if (ConfigCreateDelegators.ContainsKey(typeImport))
                ConfigCreateDelegators.Remove(typeImport);

            if (Configurations.ContainsKey(typeImport))
                Configurations.Remove(typeImport);
        }


        public static string PrintCollection(this List<string> list)
        {
            return String.Join(",", list);
        }
        #endregion
    }
}
