﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
// ReSharper disable InconsistentNaming

namespace ad_sync_filter
{
    public static class ADMessages
    {
        public const string STARTING_AD_INTEGRATION_P1 = "----- STARTING Active Directory Integration with type: ";
        public const string STARTING_AD_INTEGRATION_P2 = " -----";
        public const string STARTING_IMPORT_TO_QIS = "##########  Starting importing objects to QIS  ##########";
        public const string STARTING_FETCHING_AD_OBJECTS = "##########  Starting fetching objects from AD  ##########";
        public const string STARTING_FETCHING_AD_SINGLE = "-------  Starting fetching data from AD object  -------";
        public const string STARTING_CREATING_OBJ_P1 = "--- Starting creating object: ";
        public const string STARTING_CREATING_OBJ_P2 = " ---";
        public const string STARTING_GETTING_CHANGES_FOR_OBJ_P1 = "-- Starting getting changes for object with query: ";
        public const string STARTING_GETTING_CHANGES_FOR_OBJ_P2 = " --";
        public const string STARTING_CONNECTING_TO_AD = "Trying to connect to Active Directory";

        public const string MODIFYING_OBJ_P1 = "--- Starting modifying object: ";
        public const string MODIFYING_OBJ_P2 = " ---";
        public const string MODIFYING_ATTR_P1 = "-- Starting modifying AD attribute: ";
        public const string MODIFYING_ATTR_P2 = " --";
        public const string MODIFYING_OBJ_CREATE_DELEGATOR_DENIED = "### CreateDelegator denied creation of object. ###";
        public const string MODIFYING_OBJ_MODIFY_DELEGATOR_DENIED = "### QISPreModifyDelegator denied modification of object. ###";
        public const string MODIFYING_OBJ_NOCHANGES = "# No changes found for object. #";

        public const string FINISH_AD_TOTALCOUNT = "## Total count for all objects: ";
        public const string FINISH_AD_TOTALCOUNT_NEWOBJS = ", New objects: ";
        public const string FINISH_AD_TOTALCOUNT_MODOBJS = ", Modified objects: ";
        public const string FINISH_AD_TOTALCOUNT_PROBOBJS = ", Objects with problems: ";
        public const string FINISH_AD_TOTALCOUNT_END = " ##";
        public const string FINISH_AD_INTEGRATION_P1 = "----- FINISHED Active Directory Integration at ";
        public const string FINISH_AD_INTEGRATION_P2 = " -----";
        public const string FINISH_FETCHING_AD_SINGLE = "-------  Finished fetching data from AD object  -------";
        public const string FINISH_IMPORT_TO_QIS = "##########  Finished importing objects from AD  ##########";
        public const string FINISH_FETCHING_AD_OBJECTS = "##########  Finished fetching objects from AD  ##########";
        public const string FINISH_CREATING_OBJ_P1 = "--- Finished object: ";
        public const string FINISH_CREATING_OBJ_P2 = " ---";
        public const string FINISH_CONNECTING_TO_AD = "Connection to Active Directory established.";

        public const string ERROR_CONNECTING_TO_PATH = "Error connecting to path: ";
        public const string ERROR_NO_ADKEYVALUE_PRESENT = "Object has no values in AD";
        public const string ERROR_CONNECTING_TO_AD = "Error connecting to AD. Exception: ";
        public const string ERROR_FETCHING_ATTRIBUTE = "Error fetching an attribute from AD. Exception: ";
        public const string ERROR_CREATING_OBJ = "There was an issue creating QIS object: ";
        public const string ERROR_MODIFYING_OBJ = "There was an issue modifying QIS object.";
        public const string ERROR_UPDATING_ATTR = "Could not update AD attribute: ";
        public const string ERROR_IMPORT_TO_QIS = "There was a fatal error importing objects to QIS: ";
        public const string ERROR_CONNECTING_TO_QIS = "There was an error connecting to QIS: ";
        public const string ERROR_FINDING_ATTR_IN_ADKEYVALUE_P1 = "Error finding AD attribute: ";
        public const string ERROR_FINDING_ATTR_IN_ADKEYVALUE_P2 = " in ADkeyValue";
        public const string ERROR_COULD_NOT_OBTAIN_LOCK = "@@ Repository object is locked by someone else @@";
        public const string ERROR_CONFIG_NOT_FOUND_P1 = "Configuration was not found. TypeImport was: ";
        public const string ERROR_CONFIG_NOT_FOUND_P2 = " - Exception: ";
        public const string ERROR_MAPPING = "Mapping error: ";
        public const string ERROR_MISSING_QIS_ATTR = "Missing QIS attribute: ";
        public const string ERROR_NO_DELEGATOR = "Could not find delegator. ";

        public const string MAPPING_PERSON_MULTIPLE_ORGUNIT = "Person linked to multiple AD OrgUnits!";
        public const string MAPPING_PERSON_MULTIPLE_LOCATION = "Person linked to multiple AD Locations";
        public const string MAPPING_MULTIPLE_MANAGER = "Multiple managers, or CN with name: ";
        public const string MAPPING_MULTIPLE_ORGUNIT = "Multiple OrgUnits found with name: ";
        public const string MAPPING_MULTIPLE_LOCATION = "Multiple Locations found with name: ";
        public const string MAPPING_MISSING_DATA_ORGUNIT = "HasResponsible or Resource in OrganizationUnit";

        public const string VALUE = ", Value: ";
        public const string OLD_VALUE = ", Old value: ";
        public const string NEW_VALUE = ", New value: ";
        public const string KEY = ", Key: ";
        public const string QISATTR = ", QISAttr: ";
        public const string CONFIG = "Configuration used: ";
        public const string AT = ", at: ";

        public const string FETCH_AD_ATTR = "-- Fetched AD attribute: ";
        public const string FETCH_AD_ATTR_END = " --";
    }

    public enum ADLogLevels
    {
        Debug,
        Info,
        Warning,
        Error,
        Fatal,
    }
}
