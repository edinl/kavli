using Qis.Common;
using Qis.Common.AttributeValues;
using Qis.Common.Scripting;
using Qis.Web.Forms.Common.Scripting;

namespace Qis.Module.Scripts
{
    class QsQueryResultViewMethods
    {
        public static RepositoryObjectList Revisions(FilterScriptEventArgs args)
        {
            RepositoryObject currInst = args.ScriptContext.Object;
            if (currInst == null)
            {
                string objId = args.Request["ObjId"];
                currInst = args.Configuration.FindObject(new Oid(objId));
            }

            if (currInst == null)
                return new RepositoryObjectList();

            return args.Configuration.FindRevisions(currInst.Id);
        }

    }
}
