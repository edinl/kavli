﻿$.initTemplateSettings = function() {
	var $obj = arguments[0] ? arguments[0] : $$;
	var calb = arguments[1] ? arguments[1] : $.noop;
	var $cnt = arguments[2] ? arguments[2] : $("body");
	
	if ($obj.TemplateDefinition) {
		var settings = $obj.TemplateDefinition["SettingsFile"];
		
		if (!settings) {
			handleTemplateDefinitionError($obj, $cnt);
		}
		
		var $tf = $obj.TemplateDefinition.Functions || {};
		
		var addJsP = $.trim($tf.OnPageLoadJS);
		var addJsO = $.trim($tf.OnObjectLoadJS);
		
		var addCssP = $tf.OnPageLoadCSS || null;
		var addCssO = $tf.OnObjectLoadCSS || null;
				
		if  ($QepSettings.SettingsFile === settings) {
			$.execScript.call($obj, addJsP, $cnt, $$);
			$.initPageContent(null, true);
			$.execScript.call($obj, addJsO, $cnt, $$);
			if (History.enabled) {
				$.async(function(){
					NProgress.done(true);
				}, 250);
			}
			return;
		}
		
		$.execScript.call($obj, addJsP, $cnt, $$);
		if (addCssP) {
			$cnt.append("<style type=\"text/css\">" + addCssP + "</style>");
		}

		if ($QepSettings[settings]) {
			calb.call($obj, $QepSettings[settings], $cnt);
			$.execScript.call($obj, addJsO, $cnt, $$);
			if (addCssO) {
				$cnt.append("<style type=\"text/css\">" + addCssO + "</style>");
			}
			if (History.enabled) {
				$.async(function(){
					NProgress.done(true);
				}, 250);
			}
			return;
		}
		
		$.getScript($QisSession.HTMLLocation
				+ $QisSession.ConfName
				+ "/Scripts/"
				+ settings + "?_t=" + $QisSession.TimeStamp)
			.done(function (script) {				
				$QepSettings[settings] = $.extend(true, {}, $QepSettings.Definitions, eval(script));
				calb.call($obj, $QepSettings[settings], $cnt);
				$.execScript.call($obj, addJsO, $cnt, $$);
				if (addCssO) {
					$("body").append("<style type=\"text/css\">" + addCssO + "</style>");
				}
				if (History.enabled) {
					$.async(function(){
						NProgress.done(true);
					}, 250);
				}
			})
			.fail(function (xhr, status, error) {	
				$.browser.error("Request error: " + (error.message || error.description));
				handleTemplateDefinitionError($obj, $cnt);
			});
	}
	else {
		$obj.TemplateDefinitions(function(){
			$.initTemplateSettings($obj, calb, $cnt);
		});
	}
}

$.initTemplateFields = function () {
	$.cleanUpDom();
	
	$("#contentHeaderLeft").addClass("content-header-left")
		.html("")
		.append($("<div id=\"userinfo\" />"));
	$("#contentHeaderRight").addClass("content-header-right")
		.html("")
		.append($("<div id=\"parentLevel\" />"))
		.append($("<div id=\"contentHeading\" />"));
	$("#contentLeft").addClass("content-left")
		.html("")
		.append("<div id=\"dataLeft\" />");
	$("#contentCenter").addClass("content-center")
		.html("")
		.append($("<div id=\"dataCenter\" />"));
	$("#contentRight").hide().addClass("content-right")
		.html("")
		.append($("<div id=\"dataRight\" />"));
		
	$.initTemplateSettings($$, function(){
		$.initPageFields(arguments[0]);
		$.initPageContent();
	});
	
	loadUserInfo();
}

$.initPageFields = function () {
	var $defs = arguments[0]
		? arguments[0]
		: $QepSettings[$$.TemplateDefinition["SettingsFile"]];

	if (!$defs) {
		return;
	}
	
	$QepSettings.SettingsFile = $$.TemplateDefinition["SettingsFile"];
	loadTopMenu();
	$.async(function(){loadNavButtons($defs)});

	$("#contentHeaderRight").show();
	
	switch ($defs.FieldLeft.Visibility) {
		case "Hidden":
		case "Collapsed":
			$("#contentHeaderLeft,#contentLeft").hide()
				.data("collapsed", true)
				.children()
					.css({
						"min-width": $defs.FieldLeft.Width,
						"margin-left": -$defs.FieldLeft.Width
					});
			break;

		default:
			$("#contentHeaderLeft, #contentLeft")
				.css("display", $.cookie("_ResponsiveLeft_") ? "none" : "")
				.data("collapsed", false)
				.children()
					.css("min-width", $defs.FieldLeft.Width);
			break;
	}
			
	switch ($defs.FieldRight.Visibility) {
		case "Hidden":
		case "Collapsed":
			$("#contentRight").hide()
				.data("collapsed", true)
				.children()
					.css("min-width", $defs.FieldRight.Width);
			break;

		default:
			$("#contentRight").show()
				.data("collapsed", false)
				.children("#dataRight")
					.css("min-width", $defs.FieldRight.Width);
			break;
	}
	
	$.adjustTemplate();
}

$.initPageContent = function () {
	$(".ajaxloader-large").fadeOut("slow", function(){
		$(".ajaxloader-large").remove();
	});
	
	var $cnt = $("#dataCenter");
	
	if (typeof arguments[0] != "undefined" 
	&& typeof arguments[1] == "undefined") {
		$cnt.find(".tabs").each(function () {
			var flipped = $(this).data("flipped");
			if (flipped > 0) {
				var $tab = $(this).getActiveTab(true);
				if ($tab.length) {
					$tab.children("a").trigger($.browser.click);
				}
				else {
					$(this).activateTab();
				}
			}
			$(this).data("flipped", flipped * (-1)).toggle();
		});
		$.adjustTemplate();
		return $cnt;
	}

	var reloadContent = arguments[1] ? arguments[1] : false;
	var refreshCache = true;
	
	if (typeof arguments[2] != "undefined") {
		refreshCache = arguments[2];
	}
	
	var isStub = ($$.Data.Type==8);
	
	if ($$.Data.Template) {
		var $defs = $QepSettings[$$.TemplateDefinition["SettingsFile"]];

		document.title = $$.Data.Name;
		
		var $bc = $("#parentLevel")
			.initBreadcrumb()
			.addClass("text-shadow");

		if (History.enabled) {
			$.async(function(){
				var $links = $bc.find("a");
				$links.first().on($.browser.click, function(e){
					e.preventDefault();
					$(this).instantClick(false);
				});
				$links.not(":first").each(function(){
					$(this).setTarget();
				});
			}, 250);
		}
			
		if (!$defs) {
			return $(this);
		}
		
		var $chd = $("#contentHeading")
			.empty()
			.addClass("text-shadow");
		
		var title = $$.TemplateDefinition["Title"] || $$.Data.Name;
		title = title.replace(/{Name}/g, $$.Data.Name); 
		if (title.match(/{|}/)) {
			title = $.parseString(title);
		}
		
		$chd.html($("<h3 />")
				.addClass("enable-highlight")
				.html($("<span/>")
					.css("float","left")
					.html(title)
				));
		
		if ($defs.StandardContentTabs.Ratings && !isStub) {
			if ($defs.StandardContentTabs.Ratings.Enable) {
				$chd.find("h3").append($$.RatingScore());
			}
		}
		
		if (($defs.Governance.Enable || $defs.Governance.CustomActions) && !isStub) {
			$$.GovernanceActions();
		}
		
		if ($defs.FieldLeft.Visibility != "Hidden") {
			var $lft = $("#dataLeft")
				.empty()
				.initContent($defs.FieldLeft.Content);
				
			if ($defs.StandardTreeView.ObjId) {
				var $tree = $("<div/>");
				$lft.addPanel({
					title: $defs.StandardTreeView.Name,
					collapsible: true,
					collapsed: false,
					counter: false,
					hideempty: false,
					emptytext: $.LL("Initialization failed"),
					content: $tree.ajaxLoaderText()
				});
				loadTreeView.call($tree, $defs, $defs.StandardTreeView);
			}
			
			var $stdInfo = $defs.StandardContentBoxes;
		
			$.each($stdInfo, function(key, $info){
				if ($info.Enable) {
					if (isValidRole($info.RoleFilter)
					&& isValidCondition($info.Condition)){
						$lft.addPanel({
							title: $info.Title,
							collapsible: $info.Collapsible,
							collapsed: $info.Collapsed,
							counter: $info.ShowCounter,
							hideempty: $info.HideEmpty,
							emptytext: $info.EmptyContentText,
							sequence: parseInt($info.SeqNo),
							content: ($info.Name.match(/subscriptions/i)
								? $$.UserSubscriptions(true, false, false, $info.Option, $info.EmptyContentText)
								: $$.GovernanceTasks(true, false, false, $info.Option, $info.EmptyContentText))
						});
					}
				}
			});
		}
		
		if ($defs.FieldRight.Visibility != "Hidden") {
			var $rgt = $("#dataRight")
				.empty()
				.initContent($defs.FieldRight.Content)
		}
		
		if ($defs.FieldCenter || $defs.PopupWindow) {
			var $tab = $(".tabs").getActiveTab(true);
			if ( reloadContent && $tab.length && !refreshCache) {
				$tab.trigger($.browser.click);
			}
			else {
				$center = $.keyCount($defs.FieldCenter.Content) ? 
					$defs.FieldCenter.Content : 
					$defs.PopupWindow.Content;
				$cnt.empty().initContent( $center );
			}
			
			if (!$cnt.children(".tabs").length
			&& !$cnt.children(".iframe").length
			&& !$cnt.children(".HTMLEmbeddedContent").length) {
				$cnt.children().wrap("<div class=\"tabs\"><div class=\"tabContent\"></div></div>");
				$cnt.children()
					.first().addClass("ui-widget ui-widget-content tabs ui-corner-all")
					.css({"position": "relative", "padding": "2px", "font-size": "1em"});
			}
		}
				
		var $anlBtn = $defs.NavigationButtons.Analytics;
		if ($anlBtn && !isStub) {
			if ($anlBtn.Enable) {
				$cnt.on("loadAnalytics", function(){
					if ($.keyCount($defs.Analytics.Content)) {
						$cnt.initAnalyticsContent($defs.Analytics.Content);
						$.adjustTemplate();
					}
					else {
						var $anlMsg = $cnt.find(".analytics-disabled");
						if ($anlMsg.length) {
							$anlMsg.remove();
						}
						else {
							$cnt.append($("<div/>").addClass("analytics-disabled")
								.html($.LL("Analytics not available here."))
								.css({
									"text-align": "center",
									"color": "#666",
									"top": "40%",
									"position": "relative",
									"font-size": "14pt"
								}));
						}
					}
				});
			}
		}
	}
	
	$.adjustTemplate();
		
	if (isStub) {
		$("#dataCenter")
			.height($("#dataCenter").height()-5)
			.addClass("ui-widget-content ui-corner-top ui-corner-bottom")
			.append($("<div/>").addClass("notpublished")
				.html($.LL("Content is not accessible") + "...")
				.css("margin-left", "10px"))
	}
}

$.fn.initContent = function () {
	var $cnt = $(this);
	
	if (!arguments.length ) {
		return $(this);
	}
	
	var $defs = arguments[0];
	var $obj = arguments[1] ? arguments[1] : $$;

	if (!$defs || $obj.Data.Type==8) {
		return $(this);
	}

	$.each($defs, function(){
		switch (this.Widget) {	
			case "HTMLContentTab":
				$cnt.initContentTabLayout($defs, $obj);
				return false;
				
			case "HTMLContentBlog":
				$cnt.initContentBlogLayout(this, $obj);
				break;
				
			case "HTMLContentBox":
				$cnt.initContentBoxLayout(this, $obj);
				break;
									
			case "HTMLContentTable":
				$cnt.initContentTableLayout(this, $obj);
				break;
			
			case "HTMLEmbeddedContent":
				$cnt.initEmbeddedContent(this, $obj);
				break;

			case "HTMLWebQuery":
				$cnt.initWebQuery(this, $obj);
				break;

			case "GenericQuery":
				$cnt.initGenericQuery(this, $obj);
				break;

			case "ModelPresentation":
				$cnt.initModelPresentation(this, $obj);
				break;
				
			default:
				if (!isValidRole(this.RoleFilter) 
				|| !isValidCondition(this.Condition, $obj)) {
					return;
				}
			
				var $prompt = $("<div/>").addClass("attr-prompt");
				var $value = $("<div/>").addClass("attr-value");
				
				var style = this.Style || "";
				var format = this.Format || "";
				var filter = this.Filter || "";
				
				if (this.Prompt) {
					$prompt
						.hide()
						.html(this.Prompt)
						.appendTo($cnt);
				}
				
				var valueControl = function() {
					$value.appendTo($cnt).onchange(function(){
						$value.onchange(false);
						var text = $.trim($value.text()) || $value.html().match(/<img|embed|iframe|object/i);
						if (text) {
							$prompt.show();
							if (format.match(/list/i)) { 
								$value.toList(false);
							}
							$cnt.find(".attr-value").each(function() {
								if ($(this).text() && $(this).next().length) {
									$(this).css("margin-bottom", "10px");
								}
							});
						}
					});
				};
				
				if (this.Attribute) {
					$value.addClass(this.Attribute.replace(/,/g, " "));
					if (this.Attribute === "HTMLDrawing") {
						$prompt.show();
						$value.remove();
						
						var matrixArr = ["GenericQuery", "Matrix", "ProductRuleTable", "RuleFamilyTable"];
						if ($.inArray($obj.Data.Template, matrixArr) > -1) {
							$cnt.initMatrixData($obj, style);
						}
						else {
							loadGraphicalData($cnt, $obj, style);
						}						
					}
					else if (this.Attribute === "HTMLDataTable") {
						$cnt.initGenericQuery({
							Concerns: $obj.Data.ObjId
						}, $obj);
					}
					else {						
						valueControl();
						$obj.attr(this.Attribute, filter, style, $value, format);
					}
				}
				else if (this.Function) {
					valueControl();
					$.execScript.call($value, this.Function, $obj, $prompt, style, format, filter);
				}
		}
	});
	
	return $(this);
}

$.fn.initAnalyticsContent = function () {		
	var $defs = arguments[0];
	var $obj = arguments[1] ? arguments[1] : $$;
	
	if (!$defs) {
		return $(this);
	}
	
	var $cnt = $(this).find(".HTMLAnalyticsTab");
	
	if ($cnt.length) {
		return $cnt;
	}
	else {
		$cnt = $("<div><ul></ul></div>")
		.data("flipped", 1)
		.addClass("tabs HTMLAnalyticsTab")
		.tabs()
		.hide();
	}
	
	var $usr = $QisSession.UserInfo;
	
	$.each($defs, function() {
		var $tab = this;
		if (isValidRole($tab.RoleFilter)
		&& isValidCondition($tab.Condition, $obj)) {
			$cnt.addTab({
				title: $tab.Title, 
				emptytext: $tab.EmptyContentText,
				hideempty: $tab.HideEmpty,
				content: function() {
					$(this).initContent($tab.Content, $obj);
				}
			})
		}
	})
	
	$cnt.jtabs();
	
	return $(this).append($cnt);
}

$.fn.initContentTabLayout = function () {
	if (!arguments.length) {
		return $(this);
	}
	
	var $defs = arguments[0];
	var $obj = arguments[1] ? arguments[1] : $$;
	
	if (!$defs) {
		return $(this);
	}

	var $cnt = $("<div><ul></ul></div>")
		.data("flipped", -1)
		.addClass("tabs HTMLContentTab")
		.tabs();
		
	var $usr = $QisSession.UserInfo;
	
	$.each($defs, function() {
		var $tab = this;
		if (isValidRole($tab.RoleFilter)
		&& isValidCondition($tab.Condition, $obj)) {
			$cnt.addTab({
				title: $tab.Title, 
				emptytext: $tab.EmptyContentText,
				hideempty: $tab.HideEmpty,
				content: function() {
					$(this).initContent($tab.Content, $obj);
				}
			})
		}
	})
	
	$defs = $QepSettings[$obj.TemplateDefinition["SettingsFile"]];
	
	if (!$defs) {
		return $(this);
	}
	
	$.each($defs.StandardContentTabs, function(index, $tab){
		if ($tab.Enable) {
			if ($tab.Name.match("^GovernanceHistory$|^AcknowledgeHistory$|^Ratings$")
			&& !$obj.Data.AuditGC) {
				return;
			}
			if (isValidRole($tab.RoleFilter)) {
				$cnt.addTab({
					title: $tab.Title,
					emptytext: $tab.EmptyContentText,
					hideempty: $tab.HideEmpty,
					sequence: parseInt($tab.SeqNo),
					content: function() {
						$.execScript.call(this, "load" + $tab.Name, $defs, $obj, $tab);
					}
				});
			}
		}
	})
	
	$obj.GenericRelation({
		relation: "LinkedBy",
		attribute: "RelevantFor",
		templateFilter: ["ModelPresentation"],
		sortOrder: "asc",
		sortAttribute: "Name",
		callback: function() {								
			if (!this.length) {
				return;
			}
			var $olist = this;
			$.each($olist, function(key, $model) {
				$cnt.addTab({
					title: $model.Data.Name,
					emptytext: $.LL("No presentation"),
					hideempty: false,
					content: function(){
						$(this).initModelPresentation($defs, $obj, $model);
					}
				});
				$.adjustTemplate();
				$cnt.jtabs();
			});
		}
	});
	
	$cnt.activateTab().jtabs();
	
	return $(this).append($cnt);
}

$.fn.initContentBlogLayout = function () {
	if (!arguments.length) {
		return $(this);
	}

	var $defs = arguments[0];
	var $obj = arguments[1] ? arguments[1] : $$;
	
	if (!$defs) {
		return $(this);
	}
	
	var $cnt = $("<div />")
		.addClass("HTMLContentBlog")
		.ajaxLoaderText();
	
	if ($defs.Function) {
		$.execScript.call($obj, $defs.Function, function(){
			$cnt.addBlog(this, $defs.Content);
		});
	} 
	else {
		$obj.GenericRelation({
			relation: $defs.RelationType,
			templateFilter: $defs.Filter ? $defs.Filter.split(",") : [],
			attribute: $defs.Attribute, 
			sortAttribute: $defs.SortOrder,
			sortOrder: $defs.Descending ? "desc" : "asc",
			callback: function () {
				$cnt.addBlog(this, $defs.Content);
			}
		});
	}

	return $(this).append($cnt);
}

$.fn.initContentBoxLayout = function () {
	if (!arguments.length) {
		return $(this);
	}

	var $defs = arguments[0];
	var $obj = arguments[1] ? arguments[1] : $$;

	if (!$defs) {
		return $(this);
	}

	var $cnt = $(this);
	
	if (isValidRole($defs.RoleFilter)
	&& isValidCondition($defs.Condition, $obj)) {
		$cnt.addPanel({
			title: $defs.Title,
			collapsible: $defs.Collapsible,
			collapsed: $defs.Collapsed,
			counter: $defs.ShowCounter,
			hideempty: $defs.HideEmpty,
			emptytext: $defs.EmptyContentText,
			content: function(){
				$(this).addClass("HTMLContentBox");
				switch ($.trim($defs.Format)) {
					case "Table":
						$(this).initBoxTableFormat($defs, $obj);
						break;
						
					default:				
						$(this).initContent($defs.Content, $obj);
						break;					
				}
			}
		}); 
	}
}

$.fn.initBoxTableFormat = function () {
	if (!arguments.length) {
		return $(this);
	}
	
	var $defs = arguments[0];
	var $obj = arguments[1] ? arguments[1] : $$;
	
	if (!$defs) {
		return $(this);
	}
	
	var $table = $("<table/>")
		.addClass("tableformat")
		.attr("border", 0)
		.attr("cellpadding", 0)
		.attr("cellspacing", 0);
		
	$.each($defs.Content, function(){
		if (!isValidRole(this.RoleFilter)
		|| !isValidCondition(this.Condition, $obj)) {
			return;
		}
		
		var $val = $("<div/>").initContent({0:this}, $obj);

		var $prmpt = $val.find(".attr-prompt").show();
		var $value = $val.find(".attr-value").show();
		
		$table.append($("<tr/>")
			.append($("<td/>").html($prmpt))
			.append($("<td/>").html($value)));	
	});
	
	$(this).append($table);
}

$.fn.initWebQuery = function () {
	if (!arguments.length) {
		return $(this);
	}
	
	var $defs = arguments[0];
	var $obj = arguments[1] ? arguments[1] : $$;
	
	if (!$defs) {
		return $(this);
	}

	var $cnt = $(this);
	var $parms = $defs.Parameters;
	
	if ($parms.Relation && $parms.Attribute) {
		if (!$parms.BFS) {
			$obj.HierarchyQuery({
				level: parseInt($parms.Level) || 0,
				attribute: $parms.Attribute,
				templateFilter: $parms.SingleQuery ? $parms.TemplateFilter : "",
				relation: $parms.Relation || "",
				relationFilter: $parms.RelationFilter,
				includeParent: $parms.IncludeParent || false,
				callback: function() {
					if (typeof $parms.Function === "function") {
						$parms.Function.call(this);
					}
					else if (typeof window[$parms.Function] === "function"){
						window[$parms.Function].call(this);
					}
					else {
						var $objlist = this;
						$.each($objlist, function(idx, $o) {
							$cnt.append($o.attr("Object", $parms.TemplateFilter, $parms.Style));
						});
					}
				}
			});
		}
		else {
			$cnt.append($obj.BFSLinks(
				$parms.Attribute, 
				$parms.TemplateFilter, 
				$parms.Relation, 
				$parms.RelationFilter,
				$parms.Level,
				$parms.Style,
				"List",
				null,
				null,
				null,
				{
					onStart: function(){$cnt.ajaxLoaderText();},
					onFinish: function(){
						$cnt.onchange(true);
						$cnt.ajaxLoaderText(false);
					}
				}
			));
		}
	}
	else if ($parms.RelationType) {
		$cnt.ajaxLoaderText();
		$cnt.append($obj.GenericRelation({
			relation: $parms.RelationType,
			templateFilter: $parms.TemplateFilter ? $parms.TemplateFilter.split(",") : [],
			attribute: $parms.Attribute, 
			sortAttribute: $parms.SortAttribute,
			sortOrder: $parms.SortOrder ? "desc" : "asc",
			callback: function () {
				var $objlist = this;
				$.each($objlist, function(idx, $o) {
					$cnt.append($o.HyperLink());
				});
				$cnt.ajaxLoaderText(false);
				if ($objlist.length) {
					$cnt.toList();
				}
			}
		}));
	}
	else if ($parms.Method) {
		$cnt.ajaxLoaderText();
		$obj.GenericQuery({
			method: $parms.Method,
			templates: $parms.Templates,
			filter: $parms.Filter,
			filterType: $parms.FilterType,
			detectMetamodelFormat: $parms.DetectFormat,
			sortAttribute: $parms.SortAttribute,
			sortOrder: $parms.SortOrder,
			top: parseInt($parms.Top ? $parms.Top : 0),
			skip: parseInt($parms.Skip ? $parms.Skip : 0),
			callback: function(){
				$cnt.ajaxLoaderText(false);
				var $objlist = this;
				if (!$objlist.length) {
					return;
				}
				$.each($objlist, function(idx, $obj1){
					if ($.keyCount($parms.Content)>1 && 0) {
						var $div = $("<div/>")
							.css("margin-bottom", "5px")
							.appendTo($cnt);
						$.each($parms.Content, function(idx, $val){
							if (!idx) { return; }
							$div.initContent($val, $obj1);
						});
					}
					else {
						$cnt.append($obj1.HyperLink());
					}
				})
				
				if ($.keyCount($parms.Content)<2) {
					$cnt.toList(false);
				}
			}
		});
	}
	
	return $(this);
}

$.fn.initContentTableLayout = function() {
	if (!arguments.length) {
		return $(this);
	}
	
	var $defs = arguments[0];
	var $obj = arguments[1] ? arguments[1] : $$;
	
	if (!$defs) {
		return $(this);
	}
	
	switch ($defs.Type) {
		case "Generic":
			$(this).initGenericLayout($defs, $obj);
			break;
			
		case "PropertySheet":
			$(this).initPropertySheetLayout($defs, $obj);
			break;
		
		case "DataTable":
			$(this).initDataTableLayout($defs, $obj);
			break;
			
		default: 
			$(this).initPropertySheetLayout($defs, $obj);
			break;
	}
	
	return $(this);
}

$.fn.initGenericLayout = function() {
	if (!arguments.length) {
		return $(this);
	}
	
	var $defs = arguments[0];
	var $obj = arguments[1] ? arguments[1] : $$;
	
	if (!$defs) {
		return $(this);
	}
	
	var $cnt = $("<div/>")
		.addClass("GenericLayout")
		.css({
			"margin": "10px 5px",
			"font-size": "9pt"
		});
		
	$cnt.initContent($defs.Content, $obj);
	
	return $(this).append($cnt);
}

$.fn.initPropertySheetLayout = function() {
	if (!arguments.length) {
		return this;
	}
	
	var $this = this;
	var $defs = arguments[0];
	var $obj = arguments[1] || $$;
	var hideempty = arguments[2] || false;
	
	if (!$defs) {
		return $this;
	}
	
	if ($defs.HideEmpty) {
		hideempty = $defs.HideEmpty;
	}
	
	var $cnt = $("<div/>")
		.addClass("HTMLPropertySheet")
		.ajaxLoaderText()
		.appendTo($this);
		
	if (!$defs.TableWidth || $defs.TableWidth=="100%") {
		$cnt.width("100%");
	}
	
	if (!$defs.TableHeight || $defs.TableHeight=="100%") {
		$cnt.height("100%");
	}

	var $tbl = $("<table/>")
		.addClass("propertysheet " + $obj.Data.Template)
		.css({
			"width": "100%",
			"height": "100%",
			"border": $defs.TableBorder ? $defs.TableBorder: "0",
			"background": $defs.TableBgColor ? $defs.TableBgColor : "transparent"
		})
		.appendTo($cnt)
		
	var $bdy = $("<tbody/>").css("display", "table-row-group");		
	
	var widthL = $defs.WidthLeftCol
	var widthR = $defs.WidthRightCol;
	var colL = $defs.ColorLeftCol;
	var colR = $defs.ColorRightCol;
	
	var showprompt = false;
	
	$.each($defs.Content, function(){
		if (!isValidRole(this.RoleFilter)) {
			return;
		}
		
		if (this.Prompt) {
			showprompt = true;
		}

		var $val = $("<div/>").initContent({0:this}, $obj);

		if (showprompt) {
			$val.find(".attr-prompt").show();
		}
		
		$bdy.append($("<tr/>").hide()
			.append($("<td/>").addClass("propertysheet-odd").html($val.find(".attr-prompt")))
			.append($("<td/>").addClass("propertysheet-even").html($val.find(".attr-value"))));	
	});
	
	$bdy.append($("<tr/>").addClass("empty-prop-row")
		.append($("<td/>").css({
			"width": widthL,
			"background-color": colL
		}))
		.append($("<td/>").css({
			"width": widthR,
			"background-color": colR
		})
	));
		
	var $colsL = $bdy.find("td.propertysheet-odd").css("background-color", colL);
	var $colsR = $bdy.find("td.propertysheet-even").css("background-color", colR);
	
	if (!showprompt) {
		$colsL.hide();
	}
	else {
		$colsL.width(widthL);
		$colsR.width(widthR);
	}
	
	$colsR.find("div:first").css("margin-bottom", 0);	

	$cnt.ajaxLoaderText(false);
	$bdy.appendTo($tbl);
	
	$.async(function(){ $.adjustTemplate();});
	
	if (hideempty) {
		$bdy.onchange(function(){
			$bdy.children("tr").each(function(){
				if  ($(this).isLoading()) {
					return;
				}
				
				var text = $.trim($(this).find(".attr-value").text());
				if (text || $(this).find(".media-container").length) {
					$(this).show();
				}
			})
		});
	}
	else {
		$bdy.children("tr").show();		
	}
			
	return $this;
}

$.fn.initDataTableLayout = function(){
	if (!arguments.length) {
		return $(this);
	}
	
	var $parent = $(this);
	
	var $defs = arguments[0];
	var $obj = arguments[1] ? arguments[1] : $$;
	
	if (!$defs) {
		return $(this);
	}
		
	var $cnt = $("<div/>")
		.addClass("HTMLDataTable")
		.appendTo($(this));

	if (!$defs.TableWidth || $defs.TableWidth=="100%") {
		$cnt.width($parent.width());
	}
	else {
		$cnt.width($defs.TableWidth);
	}
	
	if (!$defs.TableHeight || $defs.TableHeight=="100%") {
		$cnt.height($parent.height() - 5);
	}
	else {
		$cnt.height($defs.TableHeight);
	}

	var $tbl = $("<table/>").appendTo($cnt);
	var $hdr = $("<thead/>").html("<tr></tr>").appendTo($tbl);
	
	var $query = $defs.WebQuery;
	var $parms = $query ? $query.Parameters : null;
	var $content = $defs.Content;

	var length = $.keyCount($content) || $content.length;
	var cWidth = [];
	$.each($content, function(idx, $item) {
		$hdr.children().append("<th>" + $item.Prompt + "</th>");
		cWidth.push((100/length) + "%");
	});

	var paginate = $defs.bPaginate;
	$defs.bPaginate = false;
	
	var $opts = $.extend(false, {}, $defs);
	
	$.extend(true, $opts, {
		sScrollX: $cnt.width(),
		sScrollY: $defs.bPaginate && !$defs.TableHeight ? "" : $cnt.height() - 100,
		qWidth: cWidth,
		oTable: true
	});
	
	var $dTbl = $tbl.addDataTable($opts, true);
	$dTbl.find(".dataTables_empty").empty().html($.LL("Loading") + "...");

	var $info = $("<div/>")
		.prependTo($cnt.find(".fg-toolbar:first"))
		.css({
			position: "absolute",
			left: "10px",
			top: "10px"
		})
		.ajaxLoaderText();
	
	var buildDataTable = function() {
		var nitems = this.length;		
		var $objlist = this;
		
		if ($objlist.jquery) {
			var olist = [];
			$objlist.find("*").each(function(){
				if ($(this).data("objid")) {
					olist.push(new RepositoryObject(this));
				}
			});		
			$objlist.remove();
			$objlist = olist;
		}
		
		$.each($objlist, function(index, $obj1) {
			if (!($obj1 instanceof RepositoryObject)) {
				return true;
			}
			
			var data = [];
			var text = [];
			var $val = "";
			
			var uid = $("<div/>").uniqueId().attr("id");
			
			$.each($content, function(idx, $item){
				$val = $("<div/>").initContent({0: $.extend({}, $item, {Prompt: null})}, $obj1);
				text.push("<span class=\""+ uid +"\" data-index=\""+ (parseInt(idx)-1) +"\">" + $val.text() + "</span>");
				data.push($val);
			});
			
			$.async(function(){
				$tbl.dataTable().fnAddData(text);
				$tbl.find("." + uid).each(function(){
					$(this).html(data[$(this).data("index")]);
				});
			});
		});
		
		$.async(function(){ 
			$info.remove();
			try { $dTbl.fnDestroy(); }
			catch(e) {}
			$opts.bPaginate = paginate;
			$tbl.addDataTable($opts);
		}, nitems*100 + 1000);
	}
	
	$cnt.find(".dataTables_empty").html($.LL("Loading") + "...");

	if (!$query) {
		if (!$defs.Attribute) {
			$cnt.find(".dataTables_empty").html($dTbl.dataTableSettings[0].oLanguage.sEmptyTable);
			return $cnt;
		}
		
		var $list = $("<div/>").onchange(function() {
			if ($list.isLoading()) {
				return;
			}
			
			if (!$list.text()) {
				$cnt.find(".dataTables_empty").html($dTbl.dataTableSettings[0].oLanguage.sEmptyTable);
				return;
			}
			
			var $olist = [];
			$list.find("a").each(function(){
				$olist.push(new RepositoryObject($(this)));
			});
			
			buildDataTable.call($olist);
		});
		
		$obj.attr($defs.Attribute, $defs.TemplateFilter, null, $list);
	}	
	else if ($parms.RelationType) {
		$obj.GenericRelation({
			relation: $parms.RelationType,
			templateFilter: $parms.TemplateFilter ? $parms.TemplateFilter.split(",") : [],
			attribute: $parms.Attribute, 
			sortAttribute: $parms.SortAttribute,
			sortOrder: $parms.SortOrder,
			callback: function () {
				buildDataTable.call(this);
			}
		})
	}
	else if($parms.Method) 
	{
		$obj.GenericQuery({
			method: $parms.Method,
			templates: $parms.Templates,
			filter: $parms.Filter,
			filterType: $parms.FilterType,
			detectMetamodelFormat: $parms.DetectFormat,
			sortAttribute: $parms.SortAttribute,
			sortOrder: $parms.SortOrder,
			top: parseInt($parms.Top ? $parms.Top : 0),
			skip: parseInt($parms.Skip ? $parms.Skip : 0),
			callback: function(){
				buildDataTable.call(this);
			}
		});
	}
	else if ($parms.Function) {
		if ($.execScript.call($cnt, $parms.Function, $obj, function(list) {
				buildDataTable.call(list);
			}) === false) {
			$cnt.find(".dataTables_empty").html($dTbl.dataTableSettings[0].oLanguage.sEmptyTable);
		}
	}
	else {
		var start = new Date();
		$obj.BFSLinks($parms.Attribute, $parms.TemplateFilter, $parms.Relation, $parms.RelationFilter, $parms.Level, null, null, null, null, false, { 
			onObject: function(){
				var $obj1 = new RepositoryObject($(this));
				
				var data = [];
				var text = [];
				var $val = "";
				
				var uid = $("<div/>").uniqueId().attr("id");
				
				$.each($content, function(idx, $item){
					if ($item.Attribute=="Name") {
						if (String($item.Style).toLowerCase() != "plaintext") {
							$val = $obj1.HyperLink();
						}
						else {
							$val = $obj1.attr("Name");
						}
					}
					else {
						$val = $obj1.attr($item.Attribute, $item.Filter, $item.Style, null, $item.Format);
					}
					
					text.push("<span class=\""+ uid +"\" data-index=\""+ (parseInt(idx)-1) +"\">" + $val.text() + "</span>");
					data.push($val);
				});
				
				$.async(function(){
					$tbl.dataTable().fnAddData(text);
					$tbl.find("." + uid).each(function(){
						$(this).html(data[$(this).data("index")]);
					})
				});
			},
			onInit: function(){	
				var attr = arguments[0];
				var levl = arguments[1];
				var nitm = arguments[2];
				$info.html($.LL("Processing Level") + " " + levl + " - " + this.Data.Name + ": "+ attr + "... (<span>" + nitm + "</span>)");
			},
			onFinish: function(){ 
				$.browser.log("BFSLinks Processed: " + $info.find("span:last").text() + " - Time: " + ((new Date()).getTime() - start)/1000 + "s.");
				$info.remove();
				try { $dTbl.fnDestroy(); }
				catch(e) {}
				$opts.bPaginate = paginate;
				$tbl.addDataTable($opts);
			}
		});
	}
	
	return $cnt;
}

$.fn.initEmbeddedContent = function () {
	if (!arguments.length) {
		return $(this);
	}
	
	var $defs = arguments[0];
	var $obj = arguments[1] ? arguments[1] : $$;
	
	if (!$defs) {
		return $(this);
	}
	
	var height = $(this).height() - 3;
	var $children = $(this).children();
	
	$children.each(function(){
		height -= $(this).outerHeight() + 10;
	})

	var $cnt = $("<div/>")
		.addClass("HTMLEmbeddedContent")
		.css("background-color", "#fff");
		
	$cnt.height(Math.max(height, 250));
		
	if ($children.length) {
		$cnt.css("margin-top", "10px");
	}
	
	var idx = 0;
	var src = $defs.Url
	
	if ($obj.Data.Template == "HTMLEmbeddedContent") {
		src = src.replace(/\$Url\$/gi, $obj.attr("Url").text());
		if ($defs.Content) {
			$.each($defs.Content, function(){ 
				if (this.Value== "$HTMLEmbeddedContentFields$") {
					$obj.xml("HTMLEmbeddedContentFields").find("item")
						.each( function() {
							src += (idx ? "&" : "?");
							src += $(this).children("attribute").text();
							src += "=" + $(this).children("value").text();
							idx++;
						})
				}
			});
		}		
	}
	
	$.each($defs.Content, function() {
		if (this.Attribute) {
			src += (idx ? "&" : "?");
			src += this.Attribute + "=" + this.Value;
			idx++;
		}
	})

	src = src
		.replace(/\$Concerns\$/gi, $defs.Concerns)
		.replace(/\$HTMLBase\$/gi, $QisSession.HTMLBase)
		.replace(/\$RepId\$/gi, $QisSession.RepId)
		.replace(/\$ConfId\$/gi, $QisSession.ConfId)
		.replace(/\$ConfName\$/gi, $QisSession.ConfName)
		.replace(/\$ObjId\$/gi, $obj.Data.ObjId)
		.replace(/\$ObjectId\$/gi, $obj.Data.ObjId)
		.replace(/\$RevId\$/gi, $obj.Data.RevisionId)
		.replace(/\$RevisionId\$/gi, $obj.Data.RevisionId)
		.replace(/\$LangCode\$/gi, $QisSession.LangCode)
		.replace(/\$ObjLink\$/gi, "qlm://" + $obj.Data.ObjId + "/?" + $QisSession.RepId);
		
	if ($defs.Url.toLowerCase().indexOf("queryresultview") > -1 ){
		if ($defs["Concerns"]) {
			var $obj1 = new RepositoryObject($defs.Concerns);
			$obj1.init(function(){
				$cnt.initQueryResultView($defs, $obj1, src);
			})
		}
		else {
			$cnt.initQueryResultView($defs, $obj, src);
		}
	}
	else {
		$cnt.append($.initIFrameView({
			url: src
		}));
	}
	
	return $(this).append($cnt);
}

$.fn.initModelPresentation = function() {
	if (!arguments.length) {
		return $(this);
	}
	
	var $cnt = $(this);
	
	var $defs = arguments[0];
	
	if (!$defs) {
		return $(this);
	}
	
	var $obj = arguments[1] || $$;
	var $model = arguments[2] || new RepositoryObject($defs.ObjId);

	$model.init(function(){
		if (parseInt($model.attr("UseWalkthrough").text())) {
			loadModelSimulation.call($cnt, $obj, $defs, $model);
		}
		else {
			loadModelPresentation.call($cnt, $obj, $defs, $model);
		}
	});
	
	return $cnt;
}

$.initPdfView = function () {	
	var $opts = {
		width: "100%",
		height: "100%",
		name: "iframeview",
		cssClass: "iframeview",
		text: $.LL("Loading") + "...",
		id: "",
		border: "0",
		onInit: function () { },
		onLoad: function () { }
	};
	
	if (arguments.length) {
		$opts = $.extend($opts, arguments[0]);
	}
	
	var $cnt = $("<div/>")
		.width($opts.width)
		.height($opts.height);
	
	var $object = $("<object />").css({
			"width": "100%",
			"height": "100%"
		}).append($("<param/>")
			.attr("name", "wmode")
			.attr("value", "transparent"))
		.addClass($opts.cssClass + " pdfview")
		.appendTo($cnt);
		
	var $embed = $("<embed />")
		.attr("wmode", "transparent")
		.attr("type", "application/pdf")
		.attr("src", $opts.url + "#navpanes=1&view=FitH&toolbar=1&statusbar=1&messages=1")
		.attr("width", "100%")
		.attr("height", "100%")
		.addClass($opts.cssClass + " pdfview")
		.appendTo($object);

	return $cnt;
}

$.initIFrameView = function () {	
	var $opts = {
		url: "about:blank",
		width: "100%",
		height: "100%",
		name: "iframeview",
		cssClass: "iframeview",
		text: $.LL("Loading") + "...",
		id: "",
		border: "0",
		onInit: function () { },
		onLoad: function () { }
	};
	
	if (arguments.length) {
		$opts = $.extend($opts, arguments[0]);
	}
	
	if ($opts.url.match(/.pdf/i)) {
		return $.initPdfView($opts);
	}
	
	var $cnt = $("<div/>")
		.width($opts.width)
		.height($opts.height);
		
	$.loader({container: $cnt, text: $opts.text});
	
	var $ifrm = $("<iframe />")
		.attr("src", $opts.url)
		.attr("width", "100%")
		.css("border", $opts.border)
		.attr("seamless", "seamless")
		.addClass($opts.cssClass)
		.load(function () {
			$(this).attr("height","100%");
			$opts.onLoad($ifrm);
			$.loader(false, $cnt);
		})
		.appendTo($cnt);
	  
	$opts.onInit($ifrm);
	
	return $cnt;
}

$.fn.initQueryResultView = function () {
	if (!arguments.length) {
		return $(this);
	}
	
	var $cnt = this;

	var $defs = arguments[0];
	var $obj = arguments[1] ? arguments[1] : $$;
	
	if ($obj.attr("RepositoryExplorerViewFilterCmd").text() === "ClientSide.GridControl"
	|| $obj.attr("DataSource").text() !== "") {
		return $cnt.initGridView($obj, $defs);
	}
	
	var url = arguments[2];

	if (!$defs) {
		return $cnt;
	}
		
	if (!url) {
		url = $QisSession.HTMLBase + "/QueryResultView.aspx";
		url += "?File=" + $QisSession.RepId + "/" + $QisSession.ConfId;
		url += "/" + $obj.Data.ObjId + "-" + $obj.Data.Language + ".xquery";
		url += "&ConfId" + $QisSession.ConfId;
		url += "&RepId" + $QisSession.RepId;
	}
	
	var isSearchQuery = false;
	
	if ($obj.Data.ObjId === $QepSettings.Search.Query) {
		isSearchQuery = true;
	}

	if (isSearchQuery) {
		var searchVals = $.browserCache("get", "persistent-search-" + $QisSession.ConfId);
		
		if (!searchVals) {
			$.async(function(){
				$("#btn-Search").addClass("ui-state-hover").trigger($.browser.click);
			},100);
			return $cnt;
		}
		else {
			var searchParams = "";
			$.each($.parseJSON(searchVals), function(key, val){
				if (key==="Template") {
					key = "Templates";
					val = val.replace(/,/g,";");
				}
				searchParams += "&" + key + "=" + val;
			})
			url += searchParams;
		}
	}
	else {
		var queryTemplates = "";
		var attr = $obj.Data["RepositoryExplorerViewTemplates"];
		if (attr) {
			switch (attr.Format) {
				case "Xhtml":
					$($obj.xml("RepositoryExplorerViewTemplates").text())
						.find("template")
						.each(function (index, value) {
							queryTemplates += (queryTemplates.length ? ";" : "");
							queryTemplates += value.textContent;
						});
					break;
				case "PlainText":
					$obj.xml("RepositoryExplorerViewTemplates")
						.find("template")
						.each(function (index, value) {
							queryTemplates += (queryTemplates.length ? ";" : "");
							queryTemplates += value.textContent;
						});
					break;
			}
		}
	
		if (queryTemplates) {
			url += "&Templates=" + queryTemplates;
		}
		
		var queryParams = "";
		$obj.xml("RepositoryExplorerViewParams")
			.find("item").each(function(){
				queryParams += "&" + $(this).children("Parameter").text();
				queryParams += "=" + $(this).children("Value").text();
			})
			
		if (queryParams) {
			url += queryParams;
		}
	}
	
	var  $xmldata = $obj.xml("RepositoryExplorerViewCols");
	
	if (!$xmldata.text()){
		url = replaceUrlParam(url, "File", "Default.xquery");
	}
		
	var $ifrm = $("<iframe />")
		.attr("src", url)
		.attr("name", "iframeview")
		.attr("class", "queryresultview iframeview")
		.attr("width", "100%")
		.attr("height", "99.8%")
		.css({
			"border": "0",
			"position": "relative",
			"visibility": "hidden",
			"border-bottom": "solid 1px #888"
		});
	
	$.loader();
		
	$ifrm.load(function () {
		$(this).css("visibility", "visible");
		
		if (!$(this).contents().text()) {
			return;
		}
		
		$.async(function(){
			$.loader(false);
		});

		$(this).contents().find(".itemsTable").css("margin", 0);
		$(this).initSessionFilter($defs, $obj);
		$(this).initQueryData($defs, $obj, $xmldata);
	}).appendTo($cnt);
	
	return $cnt;
}

$.fn.initSessionFilter = function () {
	var $defs = arguments[0];
	var $obj = arguments[1] ? arguments[1] : $$;
	var $iframe = $(this);
	
	var $row = $iframe.contents().find(".rgFilterRow");
	var $flt = $row.find("input");
	
	var filter = [];
	var _p = "persistent-qrv-";
	
	$flt.each(function(index, data){
		var id = $(this).attr("id");
		var val = $.browserCache("get", _p + $obj.Data.ObjId + id);
		
		if (val && !$(this).val()) {
			filter.push(index);
			$(this).val(val);
		}
		
		var timeout = 0;
		$(this).bind("keydown", function(e){
			var key = e.which || e.keyCode;
			if ( key == 13) {
				e.preventDefault();
				var filterVal = $(this).val();
				if (filterVal) {
					$.browserCache("set", _p + $obj.Data.ObjId + id, filterVal);
				}
				else {
					$.browserCache("del", _p + $obj.Data.ObjId + id);
				}
				$(this).trigger("onchange");
			}
		})
	})
	
	$.each(filter, function(idx, val){
		$flt.eq(val).trigger("onchange");
	})
}

$.fn.initQueryButtons = function () {
	var $obj = arguments.length ? arguments[0] : $$;
	var $iframe = $(this);
	var $list = $iframe.contents().find(".rgCommandRow");
	
	$($iframe.get(0).contentWindow.document).unbind("keydown").bind("keydown", function (e) {
		var $target = $(e.target);
		if ($target.hasClass("rgFilterBox")){
			return;
		}
		
		var key = e.which || e.keyCode;
		if ( key == 116 && !e.ctrlKey ) {
			e.preventDefault();
			$$.init(function() {
				if (arguments[0] === false) {
					return window.location.replace($QisSession.HTMLLocation);
				}
				$.initPageContent(); 
				$(".jgrowl-container .reload").remove();
			});
		}
		else if (key == 13) {
			$target.blur();
			$.async(function(){
				var $rows = $iframe.contents().find("tr.rgSelectedRow");
				var $link = $rows.find(".hyperlink").first();
				if ($link.length) {
					$link.trigger($.browser.click);
					return false;
				}
				$list.find("a").each(function(){
					if (($(this).attr("id").match(/_edit/i) && $rows.length==1)
					||  $(this).attr("id").match(/_confirmcreate/i)
					||  $(this).attr("id").match(/_update/i) ) {
						e.preventDefault();
						$(this).trigger($.browser.click);
						$.async(function(){
							$iframe.contents().find(".RadWindow")
								.css({"top": 0, "height": "100%"})
									.children().css("height", "100%");
						}, 100);
									
						return false;
					}
				})
			}, 100);
		}
		else if (key == 27) {
			$target.blur();
			$.async(function(){
				$list.find("a").each(function(){
					if ($(this).attr("id").match(/_cancel/i)) {
						e.preventDefault();
						$(this).trigger($.browser.click);
						return false;
					}
				})
			}, 100);
		}
		else if (key == 38){
			var $selected = $iframe.contents().find("tr.rgSelectedRow");
			if (!$selected.length) {
				$iframe.contents().find(".rgDataDiv tbody").children().first().trigger($.browser.click);
			}
			else if ( $selected.prev("tr").length ){
				$selected.removeClass("rgSelectedRow").prev("tr").trigger($.browser.click);
			}
		}
		else if (key == 40){
			var $selected = $iframe.contents().find("tr.rgSelectedRow");
			if (!$selected.length) {
				$iframe.contents().find(".rgDataDiv tbody").children().first().trigger($.browser.click);
			}
			else if ( $selected.next("tr").length ){
				$selected.removeClass("rgSelectedRow").next("tr").trigger($.browser.click);
			}
		}
	});
	
	return $(this);
}

$.fn.initQueryData = function () {
	if (!arguments.length) {
		return $(this);
	}
	
	var $defs = arguments[0];
	var $obj = arguments[1] ? arguments[1] : $$;
	var $xml = arguments[2] ? arguments[2] : $obj.xml("RepositoryExplorerViewCols");
	
	if (!$defs) {
		return $(this);
	}
	
	var linkImage = $QepSettings[$$.TemplateDefinition["SettingsFile"]].ListViewImage;
	var iconColmn = parseInt($obj.attr("RepositoryExplorerViewShowIconColumn").text()) || 0;
	
	if (!$obj.Data.RepositoryExplorerViewCols) {
		iconColmn = 1;
	}
	
	if ($obj.TemplateDefinition) {
		if ($QepSettings[$obj.TemplateDefinition["SettingsFile"]]) {
			linkImage = $QepSettings[$obj.TemplateDefinition["SettingsFile"]].ListViewImage;
		}
	}
	
	$(this).initQueryButtons($obj);

	var $this = $(this).width("99.9%");
	
	$this.contents().find("div > table.rgMasterTable").width("100%");

	var $hdr = $this.contents().find(".rgHeaderDiv");
	var $cnt = $this.contents().find(".rgDataDiv").scrollTop(0);
	
	var $hdrtbl = $hdr.children("table:first");
	var $cnttbl = $cnt.children("table:first");
	
	$cnttbl.addClass("qep iframeview");

	$hdrtbl.find("th:first").html("<span style=\"width:16px; display:block;\"/>");
	$cnttbl.find("tr").each(function () {
		var $tr = $(this);
		$.async(function(){
			$tr.dblclick(function(e){
				return false;
			})
			.find("td").each(function(idx){
				if ($(this).hasClass("nowrap")) {
					if (!idx) {
						$(this).width("16px");
					}
					return true;
				}
				
				var $text = $(this).addClass($xml.find("item").eq(idx-1).find("format").text())
					.children("span").removeAttr("title")
					.find(".formattedTextContainer").contents().filter(function(){
						return (this.nodeType === 3 && $.trim(this.nodeValue));
					});
					
				var $pars = $(this).find("p");						
				var $links = $(this).find("a");
				
				$links.each(function() {
					$(this).setTarget();
				});				
				
				if ((idx > iconColmn) && !$pars.length && !$text.length) {
					$links.each(function(){	$(this).setIcon(); });
				}
			});
		});
	})
	
	var queryWidth = $cnttbl.width();
	var queryChnge = function() {
		var $table = $cnt.children("table:first");
		if (!$table.hasClass("qep")
		|| $table.width() != queryWidth) {
			try {
				$this.initSessionFilter($defs, $obj);
				$this.initQueryData($defs, $obj);
			}
			catch(e) {
				$.browser.warn("QueryResultView: " + e);
			}
		}
		else {
			$.async(queryChnge, 100);
		}
	};
	
	$.async(queryChnge, 100);
	return $(this);
}

$.fn.initMatrixData = function() {
	var $parent = $(this).empty();
	
	var $cnt = $("<div/>")
		.addClass("HTMLMatrix")
		.ajaxLoaderText()
		.appendTo($(this).css("overflow","auto"));
		
	var $obj = arguments[0] || $$;
	var styl = arguments[1] || null;
	var dtbl = true;
	
	var width = $parent.width() - 17;
	var height = $parent.height() - 140;
	
	if (typeof styl === "string") {
		if (styl.match(/auto/i)) {
			width = null;
			height = null;
		}
	}
	else if (styl){
		if (typeof styl["width"] != "undefined") {
			width = styl.width;
		}
		if (typeof styl["height"] != "undefined") {
			height = styl.height;
		}
		if (typeof styl["datatable"] != "undefined") {
			dtbl = styl.datatable;
		}
	}
	
	var src = encodeURI( $QisSession.ConfName
			+ "/Matrix/" 
			+ $obj.Data.RevisionId
			+ "-" + $QisSession.LangCode 
			+ ".htm");
			
	if ($obj.cache("get")){
		src += "?_t=" + $obj.Date($obj.Data.AuditMD, null, null, true, true, true);
	}
	else {
		src += "?_t=" + (new Date()).getTime();
	}

	$.ajax({
		dataType: "html",
		url: src,
		success: function (data) {
			var $tbl = $cnt.html(data).children("table");
			$tbl.addDataTable({
				aaSorting: [],
				bProcessing: false,
				sAjaxSource: src.replace(".htm",".txt"),
				sScrollY: height,
				sScrollX: width,
				aLengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
				bAutoWidth: false,
				bDeferRender: true,
				oColReorder: {
					iFixedColumns: 1
				},
				fnDrawCallback: function( oSettings ) {
					delete oSettings.oInit.fnDrawCallback;
					if (!dtbl) {
						$(this).dataTable().fnDestroy();
						$(this).find("th").removeClass("ui-state-default")
							.css({
								"border-color": "#d3d3d3",
								"border-style": "solid",
								"border-width": "0 0 1px 1px"
							}).last().css({
								"border-right-width": "1px"
							});
					}
					$cnt.find("[data-objid]").each(function(){
						$(this)
							.addClass("hyperlink")
							.setTarget(false)
							.setIcon()
							.setTooltip();
					});
					$cnt.find("div.custom").each(function(){
						var $div = $(this).formatText();
						var color = $div.css("background-color");
						$div.parent("td").css({
							"background-color": $div.css("background-color")
						});
					});
				}

			}, true, true);
		},
		error: function() {
			$cnt.html($.LL("Matrix is not available") + "...")
			.addClass("notpublished");
		}
	});
}

$.fn.initGenericQuery = function() {
	var $parent = $(this);
		
	var $defs = arguments[0] || null;
	var $obj = arguments[1] ? arguments[1] : $$;
	
	var emptytext = arguments[2] || $.LL("No content");
	var filter = true;
	var filterText = $.LL("Compact data");
	var fullText = $.LL("Full data");
	
	if (typeof arguments[3] === "boolean") {
		filter = arguments[3];
	}
	else if (typeof arguments[3] === "object"){
		var args = arguments[3];
		filter = args.filter;
		filterText = args.filterText || filterText;
		fullText = args.fullText || fullText;
	}
	
	if (!$defs) {
		return $(this).html(emptytext);
	}
	
	if (!$defs) {
		return $parent
			.html(emptytext)
			.addClass("emptytext");
	}
	
	if (!$defs.Concerns) {
		return $parent
			.html(emptytext)
			.addClass("emptytext");
	}
	
	var attrCallback = $defs.attrCallback || {};
	
	var $cnt = $("<div/>")
		.addClass("GenericQuery")
		.ajaxLoaderText();
		
	var height = $parent.height();
	var width = $parent.width();
	
	var $query = new RepositoryObject($defs.Concerns);
	
	$query.init();
	
	$obj.GenericDataTableQuery({
		container: $cnt,
		query: $defs.Concerns,
		filter: filter,
		emptytext: emptytext,
		onLoad: function() {
			if (!arguments.length) {
				return;
			}
			
			var $content = arguments[0];                    
			var $hdrs = $content[0];
			var $data = $content[1];
			
			if (!$hdrs) {
				return $cnt.addClass("message-warning")
					.ajaxLoaderText(false)
					.append("<span class=\"message-icon\">")
					.append("<span class=\"emptytext\">" 
						+ $.LL("System was unable to retrieve data") + "</span>");
			}

			var hdrInit = false;

			if (!$data 
			|| (typeof $data.aaData === "undefined")
			|| !$data.aaData.length) {
				$data = { aaData: [] };
			}
			
			var $table = $("<table />")
				.addClass("genericquery")
				.append("<colgroup></colgroup><thead></thead><tbody></tbody>");
			
			var $clgrp = $table.find("colgroup");
			var $thead = $table.find("thead");
			var $tbody = $table.find("tbody");
			
			var $tr = $("<tr/>").appendTo($thead);
			$.each($hdrs.Structure, function(idx, val){
				if (val.Visible) {		
					$clgrp.append("<col/>");					
					$("<th/>")
						.attr("data-field", val.Name)
						.attr("data-title", val.Header)
						.appendTo($tr);
				}
			});
			
			$.each($data.aaData, function () {
				var $data = this;	
				var $obj = new RepositoryObject($data);
				
				$tr = $("<tr/>").appendTo($tbody);
				$.each($hdrs.Structure, function (key, val) {
					if (val.Visible) {
						var $td = $("<td/>").appendTo($tr);
						if (val.Name in attrCallback) {
							attrCallback[val.Name].call($td, $obj, val);
						}
						else {
							$td.html($obj.attr(val.Name, null, val.Style, null, val.Format));
						}
					}
				})
			});
			
			$.async(function() {
				$cnt.attr("id", $query.Data.ObjId)
					.html($table).initGridView($obj, {
						definition: $defs, 
						query: $query, 
						empty: emptytext,
						filter: filter,
						filterText: filterText,
						fullText: fullText
					});
			});
		}
	});

	return $parent.append($cnt);
}

$.initPrintContent = function() {
	if (!arguments.length) {
		return;
	}

	var $obj = this;
	var $doc = new RepositoryObject(arguments[0]);
	var data = arguments[1] || {};

	var reportName = data.name || "";
	var isStatic = data.static || false;
	var isDisabled = data.disabled || false;
	var oTemplate = data.template || "DocumentStructure";
	
	
	if (!($obj instanceof RepositoryObject)) {
		$obj = $$;
	}
	
	if (isStatic && !isDisabled
	&& oTemplate == "DocumentStructure") {
		var src = $QisSession.HTMLLocation + $QisSession.ConfName 
			+ "/Reports/" + $obj.Data.ObjId + "/" + reportName.replace(/:|\/|\\/g,"_") + "/" + $obj.Data.Name.replace(/:|\/|\\/g,"_") + " - rev." + $obj.Data.Revision + ".rtf";
		$().openDocumentFile(src);
	}
	else if (oTemplate=="ReportDefinition") {
		var reportUrl = $QisSession.HTMLBase + "/ReportDefinitionOnObject.aspx" 
			+ "?RefObj=qlm://" + $obj.Data.ObjId + "/?" + $QisSession.RepId
			+ "&Id=qlm://" + $doc.Data.ObjId + "/?" + $QisSession.RepId
			+ "&RepId="+ $QisSession.RepId
			+ "&ConfId=" + $QisSession.ConfId;
			
		var $iframe = $.initIFrameView({
			width: Math.min(1025, $(window).width() * 0.95),
			height: $(window).height() * 0.95,
			url: reportUrl,
			onLoad: function(){
				$.overlay(false);
			}	
		});
		
		$.fancybox($iframe, {
			scrolling: "no"					
		});
	}
	else if ($doc.Data.ObjId) {		
		$.async(function(){
			$.overlay({loader:true, transparent:true});
		},100);
		$doc.init(function(){	
			var isQcl = parseInt($doc.attr("Disabled").text());			
			if (isQcl) {
				var docCmd = $doc.attr("InitDocument").text();
				if (!docCmd) {
					$.fancybox($("<div/>").html("<h1>" + $.LL("Warning") + "</h1><div>" + $.LL("No print content") + "</div>")
						.width(350).height(80));
					return;
				}
				else {				
					initQclEngine.call($obj, docCmd, function(){
						return loadPrintReport.call(this, $obj);
					});
				}
			}
			else {
				var simpleReport = function(){
					var $iframe = $.initIFrameView({
						width: Math.min(1025, $(window).width() * 0.95),
						height: $(window).height() * 0.95,
						url: $QisSession.HTMLBase + "/BaseReport.aspx?ObjID=" + $obj.Data.ObjId + "&ID=" + $doc.Data.ObjId + "&RepId="+ $QisSession.RepId + "&ConfId=" + $QisSession.ConfId,
						onLoad: function(){
							$.overlay(false);
						}	
					});
					
					$.fancybox($iframe, {
						scrolling: "no"					
					});
				};
				
				var advancedReport = function(){
					var $cnt = $("<div/>").width(380).height(100)
						.loadingText($.LL("Generating report"), "<h2/>", 500, 5);
					
					var rname = $obj.Data.Name.replace(/:|\/|\\/g,"_") + " - rev." + $obj.Data.Revision + ".rtf";
					
					var docCmd = "";
					docCmd += "local doc = ObjID2Entity(\"" + $doc.Data.ObjId + "\");\n";
					docCmd += "local filename = StrCat(HTMLGetFileLocation(), \"\\\\Reports\\\\" + $obj.Data.ObjId + "\\\\" + "\");\n";
					docCmd += "FileMakeDir(filename);\n";
					docCmd += "StrAppend(filename, \"" + rname + "\");\n";
					docCmd += "DocRun(doc ,0, filename, 1);\n";
					
					$.fancybox($cnt, {modal: true});
					
					initQclEngine.call($obj, docCmd, function(){
						if (parseInt(this.Result)) {
							$cnt.html("<h2>" + $.LL("Print report successfully generated") + "</h2>");
							var url = $QisSession.HTMLLocation + $QisSession.ConfName + "/Reports/" + $obj.Data.ObjId + "/" + rname;
							$cnt.addButton(rname, "print.png", null, function(){
								$(this).openDocumentFile(url);
							}, false)
							.css("font-size", "20px");
						}
						else {
							if (this.Messages && !this.Result) {
								$cnt.append("<div><b>" + $.LL("Messages:")+ "</b><br/>" + this.Messages + "</div>");
							}
							
							if (this.Result) {
								$cnt.append("<hr/>" + this.Result);
							}
						}
						$.overlay(false);
						$.fancybox($cnt);
					});
				};
				
				$doc.GenericRelation({
					relation: "Contains",
					templateFilter: "DocumentFragment",
					callback: function(){
						if (this.length === 1) {
							var $frag = this[0];
							return $frag.init(function(){
								if ($frag.attr("FragmentType").text().indexOf(-1)==0) {
									simpleReport();
								}
								else {
									advancedReport();
								}
							});
						}
						advancedReport();
					}
				})
			}
		});
	}
}

loadPrintReport = function() {
	var $res = this;
	var $obj = arguments[0] || $$;
	var $cnt = $("<div/>");
	
	var $report = $("<div/>").addClass("report")
		.html($res.Result || $res.Message)
		.appendTo($cnt);
	
	if ($res.Result) {		
		var a4p = "210mm";
		var a4l = "297mm";
		var a3p = "297mm";
		var a3l = "420mm";
				
		var $menu = $("<div/>").addClass("print-menu")
			.prependTo($cnt);
		
		var resizeFancybox = function() {
			var landscape = $report.children().hasClass("landscape");
			var fancysize = 0;
			
			if ($report.children().hasClass("print-size-a4")){
				size = landscape ? a4l : a4p;
			}
			else {
				size = landscape ? a3l : a3p;
			}
			
			$cnt.width(size);
			$.fancybox.update();
		};
		
		$report.find(".print-caption").first().html($.LL("Not controlled"));
			
		$menu.addButton($.LL("Print Report"), "Print.png", "", function(){
			$.overlay({loader:true, transparent:true});
			$.async(function(){$.overlay(false)}, 2000);
			$cnt.find(".report").printVersion();
		}, false);
		
		$menu.addButton($.LL("A4"), null, "print-a4 print-button-selected", function(){
			var $btn = this;
			if ($btn.hasClass("print-button-selected")) {
				return;
			}
			$.overlay({loader:true, transparent:true});
			$btn.addClass("print-button-selected");
			$menu.find(".print-a3").removeClass("print-button-selected");
			$report.children().removeClass("print-size-a3").addClass("print-size-a4");
			$.async(function(){$.overlay(false)}, 1000);
			resizeFancybox();
		}, false);
		
		$menu.addButton($.LL("A3"), null, "print-a3", function(){
			var $btn = this;
			if ($btn.hasClass("print-button-selected")) {
				return;
			}
			$.overlay({loader:true, transparent:true})
			$btn.addClass("print-button-selected");
			$menu.find(".print-a4").removeClass("print-button-selected");
			$report.children().removeClass("print-size-a4").addClass("print-size-a3");
			$.async(function(){$.overlay(false)}, 1000);
			resizeFancybox();
		}, false).css("margin-left", "0");
		
		$menu.addButton($.LL("Portrait"), null, "print-portrait print-button-selected", function(){
			var $btn = this;
			if ($btn.hasClass("print-button-selected")) {
				return;
			}
			
			$.overlay({loader:true, transparent:true})
			$btn.addClass("print-button-selected");
			$menu.find(".print-landscape").removeClass("print-button-selected");
			$report.children().removeClass("landscape");
			$.async(function(){$.overlay(false)}, 1000);
			resizeFancybox();
		}, false);
		
		$menu.addButton($.LL("Landscape"), null, "print-landscape", function(){
			var $btn = this;
			if ($btn.hasClass("print-button-selected")) {
				return;
			}
			$.overlay({loader:true, transparent:true})
			$btn.addClass("print-button-selected");
			$menu.find(".print-portrait").removeClass("print-button-selected");
			$report.children().addClass("landscape");
			$.async(function(){$.overlay(false)}, 1000);
			resizeFancybox();
		}, false).css("margin-left", "0");
			
		if (window["PrintableSaveAsPDF"] == 1) {
			$menu.addButton($.LL("Save as PDF"), "Subscribe.png", "", function(e){
				$.overlay({loader:true, transparent:true});
				$.async(function(){
					var fn = $obj.Data.Name + " - Rev." + $obj.Data.Revision + ".pdf";
					kendo.drawing.drawDOM($(".report"), {
						forcePageBreak: ""
					}).then(function(root) {
						root.options.set("pdf", {
							multiPage: true,
							paperSize: "auto",
							margin: {
								left   : "10mm",
								top    : "20mm",
								right  : "10mm",
								bottom : "20mm"										
							}
						});
						return kendo.drawing.exportPDF(root);
					})
					.done(function(data) {
						kendo.saveAs({
							dataURI: data,
							fileName: fn
						});
						$.async(function(){$.overlay(false)}, 600);
					});
				}, 750);
			}, false);
		}
	}

	$.overlay(false);
	
	$.fancybox($cnt.css({
		width: $res.Result ? "210mm" : "500px",
		height: $res.Result ? $(window).height()*0.9 - 30 : "auto",
		"overflow-y": "auto",
		"margin-top": "30px"
	}));	
}

loadRatings = function () {
	var $cnt = $("<div />")
		.attr("id", "ratings")
		.addClass("Ratings")
		.ajaxLoaderText()
		.appendTo($(this));
	
	var $defs = arguments[0] ? arguments[0] : $QepSettings[$obj.TemplateDefinition["SettingsFile"]];
	var $obj = arguments[1] ? arguments[1] : $$;

	if (!$defs) {
		return $cnt;
	}
	
	var $opts = {
		EmptyContentText: $.LL("No Ratings")
	}
	
	if (arguments[2]) {
		$.extend(true, $opts, arguments[0]);
	}
	
	if (!$.keyCount($obj.Data.Rating)) {
		return $cnt
			.html($opts.EmptyContentText)
			.addClass("emptytext");
	}

	var $list = [];
			
	$.each($obj.Data.Rating.Data || {}, function(){
		var $obj1 = new RepositoryObject();
		$obj1.Data.AuditCD = $obj1.Date(this.RatingDate);
		$obj1.Data.AuditCN = this.UserName;
		$obj1.Data.RatingComment = this.RatingComment;
		$obj1.Data.RatingScore = $("<div/>")
			.rateit()
			.rateit('value', this.RatingScore)
			.rateit('readonly',true);
		$list.push($obj1)
	});
	
	$list = $obj.sortObjectList($list, {
		sortAttribute: "AuditCD", 
		sortOrder: "desc"
	});
		
	$cnt.addBlog($list, {
		HeaderAttribute: "RatingScore",
		HeaderCaption: "Rated {AuditCD} by {AuditCN}",
		ContentPrompt: $.LL("Comment:"),
		ContentAttribute: "RatingComment"
	});

	return $(this);
}

loadAcknowledgeHistory = function () {
	var $cnt = $(this).addClass("acknowledgehistory");
	
	var $defs = arguments[0] ? arguments[0] : $QepSettings[$$.TemplateDefinition["SettingsFile"]];
	var $obj = arguments[1] ? arguments[1] : $$;
	var $hist = arguments[2];
	
	var emptytext =  $.LL("No history");
	
	if (!$defs) {
		return $cnt.html(emptytext)
			.addClass("emptytext");
	}
		
	if (!$hist) {
		$hist = $defs.StandardContentTabs ? 
			$defs.StandardContentTabs.GovernanceHistory :
			null;
			
		if (!$hist || !$hist.GenericQuery) {
			return $cnt.html(emptytext)
			.addClass("emptytext");
		}
	}
	
	$hist.Concerns = $hist.GenericQuery;
	$hist.attrCallback = {
		AcknowledgeFlag: function($obj, val){
			var $dom = $("<span/>").appendTo(this);
			var flag = parseInt($obj.attr(val.Name).text());
			switch (flag) {
				case 1: 
					$dom.html($.LL("Acknowledged"));
					break;
				default: 
					$dom.html($.LL("Not acknowledged"));
			}
			$dom.attr("data-status-flag", (flag==1?1:0));
		}
	};

	$cnt.initGenericQuery($hist, $obj, emptytext, {
		filter: true,
		filterText: "Compact history",
		fullText: "Full history"
	});
	
	return $cnt;
}

loadGovernanceHistory = function () {
	var $cnt = $(this).addClass("governancehistory");
	
	var $defs = arguments[0] ? arguments[0] : $QepSettings[$$.TemplateDefinition["SettingsFile"]];
	var $obj = arguments[1] ? arguments[1] : $$;
	var $hist = arguments[2];
		
	var emptytext =  $.LL("No history");
	
	if (!$defs) {
		return $cnt.html(emptytext)
			.addClass("emptytext");
	}
		
	if (!$hist) {
		$hist = $defs.StandardContentTabs ? 
			$defs.StandardContentTabs.GovernanceHistory :
			null;
			
		if (!$hist || !$hist.GenericQuery) {
			return $cnt.html(emptytext)
			.addClass("emptytext");
		}
	}
	
	$hist.Concerns = $hist.GenericQuery;
	$cnt.initGenericQuery($hist, $obj, emptytext, {
		filter: true,
		filterText: "Compact history",
		fullText: "Full history"
	});
	
	return $cnt;
}

loadRASCI = function () {
	var $cnt = $("<div/>")
		.addClass("RASCI");
		
	var $defs = arguments[0] ? arguments[0] : $QepSettings[$$.TemplateDefinition["SettingsFile"]];
	var $obj = arguments[1] ? arguments[1] : $$;
	
	var isdgm = $TemplateSettings[$obj.Data.Template]["Type"] == "Diagram";

	if (!$defs) {
		return $(this);
	}
	
	var $rasci = $defs.RASCI;
	
	if (!$rasci) {
		return $(this).html("Not defined")
	}
	
	var $tbl = $("<table/>")
		.addClass("rasci-view")
		.attr("cellpading", 0)
		.attr("cellspacing", 0);

		
	var addRASCIRow = function($tbl, $obj, $rasci, isdgm) {
		var $a = $obj.HyperLink();
		var $tr = $tbl.find("tr#" + $obj.Data.ObjId);
		
		var cache = $obj.cache("get");

		if (!$tr.length) {
			var $tr = $("<tr/>")
				.attr("id", $obj.Data.ObjId);
			
			$tbl.find("tr:first").find("td").each(function(){
				$tr.append($("<td/>")
					.addClass($(this).attr("class").toLowerCase()));				
			})
						
			$tbl.append($tr);
			
			if (cache) {
				$obj.merge(cache);
			}
		}
		
		$tr.find("td").each( function() {
			var $td = $(this);
			
			switch ($td.attr("class")) {
				case "responsible":
					cache ? $td.html($obj.attr($rasci.Responsible.Attribute)) : $td.ajaxLoaderText();
					break;
					
				case "accountable": 
					cache ? $td.html($obj.attr($rasci.Accountable.Attribute)) : $td.ajaxLoaderText();
					break;
					
				case "support": 
					cache ? $td.html($obj.attr($rasci.Support.Attribute)) : $td.ajaxLoaderText();
					break;
					
				case "consulted": 
					cache ? $td.html($obj.attr($rasci.Consulted.Attribute)) : $td.ajaxLoaderText();
					break;
					
				case "informed": 
					cache ? $td.html($obj.attr($rasci.Informed.Attribute)) : $td.ajaxLoaderText();
					break;
					
				default:
					$td.html($a)
						.removeClass("ui-state-focus")
						.addClass("ui-state-default");
					break;
			}
			
			if (!$td.text()) {
				$td.text("-");
			}
		})

		if (!cache) {
			$.async(function() {
				$obj.init(function(){
					addRASCIRow($tbl, $obj, $rasci, isdgm);
				})
			}, 1000);
		}
	}
	
	var $tr = $("<tr/>")
		.addClass("rasci-header ui-state-focus")
		.appendTo($tbl);

	if (isdgm) {
		$tr.append($("<td/>").addClass("name"))
	}
	
	if ($rasci.Responsible.Enable) {
		$tr.append($("<td/>").addClass("responsible").html($rasci.Responsible.Label));
	}
	
	if ($rasci.Accountable.Enable) {
		$tr.append($("<td/>").addClass("accountable").html($rasci.Accountable.Label));
	}
	
	if ($rasci.Support.Enable) {
		$tr.append($("<td/>").addClass("support").html($rasci.Support.Label));
	}
	
	if ($rasci.Consulted.Enable) {
		$tr.append($("<td/>").addClass("consulted").html($rasci.Consulted.Label));
	}
	
	if ($rasci.Informed.Enable) {
		$tr.append($("<td/>").addClass("informed").html($rasci.Informed.Label));
	}
	
	addRASCIRow($tbl, $obj, $rasci, isdgm);

	if (isdgm) {
		$obj.GenericRelation({
			relation: "Contains",
			templateFilter: ["Activity", "BusinessProcess", "BusinessFunction", "ProjectActivity"],
			callback: function () {
				$.each(this, function(){
					addRASCIRow($tbl, this, $rasci, isdgm);
				})
			}
		})
	}
	
	return $(this).append($cnt.html($tbl));
}
	
loadContextView = function () {
	var $cnt = $("<div/>")
		.addClass("ContextView")
		.css("min-height", "300px");
		
	var $defs = arguments[0] ? arguments[0] : $QepSettings[$$.TemplateDefinition["SettingsFile"]];
	var $obj = arguments[1] ? arguments[1] : $$;
	
	if (!$defs) {
		return $(this);
	}
	
	var $cview = $defs.ContextView;
	
	if (!$cview) {
		return $cnt.html($.LL("Not defined"));
	}
	
	var $tbl = $("<table/>")
		.addClass("context-view")
		.attr("cellpading", 0)
		.attr("cellspacing", 0);
	
	var parentHeight = $(this).height();
	var parentWidth = $(this).width();
	
	var buildContextView = function($obj, $tbl, filterFrom, filterTo) {
		if (!$tbl.children().length) {
			$tbl.html("<tr><td/><td/><td/><td/><td/></tr>");
		}
		
		var $cols = $tbl.data("offset", false).find("td");
		var $linkedby = $cols.eq(0).empty();
		var $linkedbyimg = $cols.eq(1).css({"width": 100, "text-align": "center"}).hide();
		var $context = $cols.eq(2);
		var $linksimg = $cols.eq(3).css({"width": 100, "text-align": "center"}).hide();
		var $links = $cols.eq(4).empty();
		
		if (parentWidth < 800) {
			$linkedby.css("min-width", "125px");
			$linkedbyimg.css("width", "75px");
			$linksimg.css("width", "75px");
			$links.css("min-width", "125px");
		}

		var diff = parentHeight/2;
		var delta = - Math.min(diff - 25, 50);
		
		var scrollContextView = function(offset) {
			if (typeof offset == "undefined") {
				offset = 0;
			}
			
			if (offset) {
				$tbl.data("offset", true);
			}
						
			if ($tbl.height() < parentHeight) {
				delta = Math.min(($tbl.height() - parentHeight)/3, delta)
			}
			
			diff = parentHeight/2 + offset + delta;
			
			var animspeed = $tbl.data("offset") ? 500 : 1;
			
			$context.children().stop().animate({"top": diff - $context.children().outerHeight()/2}, animspeed);
			$linkedbyimg.children().stop().animate({"top": diff + 1}, animspeed);
			$linksimg.children().stop().animate({"top": diff + 1}, animspeed);
			
			var heightLeft = 0;
			$linkedby.children().each(function(){
				heightLeft += $(this).outerHeight();
			});		
			
			var heightRight = 0;
			$links.children().each(function(){
				heightRight += $(this).outerHeight();
			});
			
			diff = heightLeft - heightRight;

			if (heightLeft) {
				if (parentHeight > heightLeft) {
					diff = parentHeight - heightLeft;
					if ($tbl.data("offset") || offset) {
						$linkedby.children().stop().animate({"top": Math.max(diff/2 + offset + delta, 0)}, animspeed);
					}
					else {
						$linkedby.children().css({"top": Math.max(diff/2 + delta, 0)});
					}
				}
				else if (diff < 0) {
					diff = heightLeft - parentHeight - delta;
					$linkedby.children().stop().animate({"top": Math.max(offset - diff, 0)});
				}
				$linkedbyimg.show();
				$linkedby.children().css("visibility", "visible");
			}
			
			if (heightRight) {
				if (parentHeight > heightRight) {
					diff = parentHeight - heightRight;
					if ($tbl.data("offset") || offset) {
						$links.children().stop().animate({"top": Math.max(diff/2 + offset + delta, 0)}, animspeed);
					}
					else {
						$links.children().css({"top": Math.max(diff/2 + delta, 0)});
					}
				}
				else if (diff > 0) {
					diff = heightRight - parentHeight - delta;
					$links.children().stop().animate({"top": Math.max(offset - diff, 0)}, animspeed).show();
				}
				$linksimg.show();
				$links.children().css("visibility", "visible");
			}
		}
		
		$.fn.makeDraggable = function() {
			$(this).draggable({
				snap: ".context-in a",
				revert: "invalid",
				helper: function( event ) {
					return $(this).clone().width($(this).outerWidth());
				},
				start: function(e, ui) {$(this).disableTooltip(); ui.helper.removeAttr("title");},
				end: function(e, ui) {$(this).disableTooltip(false);}
			})
			
			return $(this);
		}
	
		if (!$context.children(".context-in").length) {
			$context.html($("<div/>")
				.addClass("context-in")
				.addClass("ui-state-active")
				.ajaxLoaderText());
				
			$linkedbyimg.html($("<div/>")
				.addClass("context-line")
				.append($("<span/>").html($cview.ContextFromLabel)));
				
			$linksimg.html($("<div/>")
				.addClass("context-line")
				.append($("<span/>").html($cview.ContextToLabel)));

			$context.children().css({"top": diff - 100});
			$linkedbyimg.children().css({"top": diff - 50 + 1});
			$linksimg.children().css({"top": diff - 50 + 1});
		}
		
		var queryIdx = 4;
		
		$obj.GenericRelation({
			relation: "ContainedIn",
			templateFilter: filterFrom,
			callback: function () {
				queryIdx--;
				if (!this.length) {
					return;
				}
				
				$.each(this, function(){
					var $a = this.HyperLink()
						.restrictTextLength(35)
						.makeDraggable();
					
					$linkedby.append($a.wrap("<div class=\"context-from\"/>")
						.parent()
							.append($("<span/>").addClass("relation").html("Contains")));
				});
			}
		});
		
		$obj.GenericRelation({
			relation: "LinkedBy",
			templateFilter: filterFrom,
			callback: function () {
				queryIdx--;
				if (!this.length) {
					return;
				}
				
				$.each(this, function(){
					var $a = this.HyperLink()
						.restrictTextLength(35)
						.makeDraggable();
					
					$linkedby.append($a.wrap("<div class=\"context-from\"/>")
						.parent()
							.append($obj.relationattr($a.data("objid"), true)));
				});
			}
		});
		
		$obj.GenericRelation({
			relation: "LinksTo",
			templateFilter: filterTo,
			callback: function () {
				queryIdx--;
				if (!this.length) {
					return;
				}
				
				$.each(this, function(){
					var $a = this.HyperLink()
						.restrictTextLength(35)
						.makeDraggable();

					$links.append($a.wrap("<div class=\"context-to\"/>")
						.parent()
							.append($obj.relationattr($a.data("objid"))));
				});
			}
		});
		
		$obj.GenericRelation({
			relation: "Contains",
			templateFilter: filterTo,
			callback: function () {
				queryIdx--;
				if (!this.length) {
					return;
				}
        
				$.each(this, function(){
					var $a = this.HyperLink()
						.restrictTextLength(35)
						.makeDraggable();
						
					if ($a.hasClass("disabled")) {
						$a.addClass("hyperlink");
					}

					$links.append($a.wrap("<div class=\"context-to\"/>")
						.parent()
							.append($("<span/>").addClass("relation").html("ContainedIn")));
				});
			}
		});
			
		$tbl.appendTo($cnt);
		
		var timeout = 0;		
		$.async(function() {
			if (queryIdx > 0) {
				return;
			}
			$.async(false, this);
			scrollContextView();
			$context.children(".context-in")
				.html($obj.HyperLink())
				.removeClass("ui-state-active")
				.addClass("ui-state-default")
				.droppable({
					 activeClass: "ui-state-active",
					 tolerance: "pointer",
					 drop: function(e, ui) {
						$context.find(".context-in")
							.empty()
							.removeClass("ui-state-default")
							.ajaxLoaderText();
						var oid = ui.draggable.data("objid");
						if (oid) {
							var $obj1 = new RepositoryObject(oid);
							$.async( function(){
								$obj1.init(function(){
									buildContextView($obj1, $tbl, filterFrom, filterTo);
									$cnt.parent().scrollTop(0);
								})
							});
						}
					}
				});
			
			$cnt.parent().scroll(function(e){
				var $this = $(this);
				if (timeout) {
					$.async(false, timeout);
				}
				timeout = $.async( function() {
					scrollContextView($this.scrollTop());
				}, 50);
			})
		}, 500, true);
	}
	
	var filterFrom = [];
	$.each($cview.ExcludeFromTemplates, function(index, item){
		filterFrom.push("-" + item);
	})
	
	var filterTo = [];
	$.each($cview.ExcludeToTemplates, function(index, item){
		filterTo.push("-" + item);
	})
	
	buildContextView($obj, $tbl, filterFrom, filterTo);
	
	return $(this).append($cnt);
}

init = function(){};

loadGraphicalData = function () {
	var args = arguments;
		
	var $cnt = args[0] || $();
	var $obj = args[1] || $$;
	var view = args[2] || false;
	var imap = args[3] || false;
	var calb = args[5] || $.noop;
	var link = args[6] || false;
	var anim = true;

	if (!$cnt.isLoading()) {
		$cnt.ajaxLoaderText();
	}
	
	if (!$obj.Data.RevisionId 
	&& !this._audit) {
		delete $obj.Data.AuditMD;
		return $obj.audit(function() {
			loadGraphicalData.apply({ _audit: true }, args);
		});
	}
	
	if (typeof args[4] != "undefined") {
		anim = args[4];
	}
	else if (typeof view == "object") {
		if (typeof view["animation"] != "undefined") {
			anim = view.animation;
		}
		if (typeof view["view"] != "undefined") {
			view = view.view;
		}
		if (typeof view["imagemap"] != "undefined") {
			imap = view.imagemap;
		}
		if (typeof view["width"] != "undefined") {
			$cnt.width(width);
		}
		if (typeof view["height"] != "undefined") {
			$cnt.width(height);
		}		
	}
	
	var svgSupport = !($.browser.compatible);
	var isSvg = svgSupport && !imap;
	var info = getRepositoryInfo($obj.Data.RepId);
	
	if ((view && $obj.Data.Template === "GraphicalMatrix")
	|| $obj.Data.Template === "BusinessChart") {
		isSvg = false;
	}
	
	var $graph = $cnt.find(".graphicalData");

	if ($graph.length) {
		$graph.remove()
	}

	$graph = $("<div/>").addClass("graphicalData");
		
	var config = info.RepId===$QisSession.RepId ? 
		getConfigurationInfo($obj.Data.ConfId) :
		{
			Name: info.ConfName,
			Id: info.ConfId
		};
	
	var src = encodeURI(config.Name + "/Diagrams/");
	
	if (info.RepId !== $QisSession.RepId) {
		src = $QisSession.HTMLLocation.replace(location.pathname, "/" + info.WebApplication + "/") + src;
	}
	
	var timestamp = "?_t=" + ($obj.Data.AuditMD ? 
		$obj.Date($obj.Data.AuditMD, null, null, true, true, true) :
		$QisSession.TimeStamp);
	
	var oid = $obj.Data.RevisionId || $obj.Data.ObjId;
	var url = src + oid + "-" + $QisSession.LangCode 
			+ (isSvg ? ".svg" : ".htm") + timestamp;
	
	if (view) {
		$graph.addClass("viewonly");
	}
	
	if (anim) {
		$graph.addClass("anim");
	}
	
	if (isSvg) {
		$graph.addClass("svg");
	}

	var	init = function() {
		return;
	}

	if  (!view) {
		$graph.css("opacity", 0);
	}
	
	$.ajax({
		url: url,
		cache: false,
		dataType: "html",
		success: function(data) {
			$cnt.ajaxLoaderText(false);
			$graph.html(data).find("img").each(function(){
				$(this).attr("src", src + $(this).attr("src") + timestamp);
			});

			$graph.find("image").each(function(){
				var href = $(this).attr("xlink:href");
				if (typeof href == "undefined") {
					return;
				}
				href = href.replace("./","");
				if (href.toLowerCase().indexOf("data:") == 0){
					return;
				}
				href = $QisSession.HTMLLocation + $QisSession.ConfName + "/Diagrams/" + href + timestamp;
				$(this).attr("xlink:href", href);
			}).end();
			
			if (!view) {
				$graph.animate({opacity:1 },500);
			}

			if (isSvg) {
				adjustSVGData.call($obj, $graph, view, anim, url, link);
			}
			else {
				adjustMapData.call($obj, $graph, view, anim, url, link);
			}
			
			if (typeof window["OnDiagramLoad"] === "function") {
				window.OnDiagramLoad.call($graph, $obj, url, view);
			}

			calb.call($graph, $obj, url, true);
		},
		error: function(){
			$cnt.ajaxLoaderText(false);
			$graph.show().css("opacity", 1)
				.html($.LL("Model is not published") + "...")
				.addClass("notpublished");
			calb.call($graph, $obj, url, false);
		}
	});
	
	return $graph.appendTo($cnt);
}

adjustSVGData = function () {
	var $obj = this;
	
	var $graph = arguments.length 
		? arguments[0]
		: $(".tabs").getActiveTab().find(".graphicalData");
	
	if (!$graph.length) {
		return;
	}
	
	var view = arguments[1] || $graph.hasClass("viewonly");
	var anim = arguments[2] || $graph.hasClass("anim");
	var surl = arguments[3] || null;
	var link = arguments[4] || false;
	
	var $cnt = $graph.parent();	
	var $svg = $graph.children("svg:first");

	if (!$svg.length) {
		return;
	}
	
	$svg.find("image").each( function(){
		var imgref = $(this).attr("xlink:href");
		if (imgref.substring(0,1)==".") {
			imgref = $QisSession.ConfName + "/Diagrams/" + imgref.substring(1); 
			$(this).attr("xlink:href", imgref);
		}
	})
	
	var $box = $svg.get(0).viewBox

	if ($box) {
		$svg
			.attr("height", $box.baseVal.height)
			.css({
				"vertical-align": "top",
				"overflow": "hidden"
			})
		
		if(!$svg.width()){
			$svg.width($box.baseVal.width)
		}
		
		if(!$svg.height()){
			$svg.height($box.baseVal.height)
		}
	}
	
	$graph.data({
		width: parseInt($svg.attr("width") || $svg.width()),
		height: parseInt($svg.attr("height") || $svg.height())
	});

	if (!view) {
		$.async(function(){
			$svg.find("text").each(function () {
				$(this).css({
					  "font-family": $QepSettings.FontFamily
				});
			});
		});
		
		var $defs = $obj.TemplateDefinition ? 
			$QepSettings[$obj.TemplateDefinition["SettingsFile"]] :
			null;
			
		if (!$defs) {
			$defs = $QepSettings.Definitions;
		}
		
		if ($defs.Svg.ZoomFit) {
			$.async(function(){
				$graph.svgZoom(-100);
			}, 50);
		}
		
		if ($defs.Svg.ZoomControl) {
			$cnt.svgMapControl();
			$graph.bind('mousewheel', function (event, delta) {
				event.preventDefault();
				$cnt.svgZoom(delta, 0);
				return false;
			});
		}
		
		if ($svg.find("a:last").data("template") === "Note" 
		&& $svg.find("a[date-template=Note]").length == 1) {
			var $note = $svg.find("a:last");
			if ($note.find("image").length) {
				$note.detach();
				$svg.find("a").css("opacity", 0).end().prepend($note);
			}
		}
	}
	else {
		var rtX = $svg.width() / ($cnt.width() || $cnt.data("width") || 1);
		var rtY = $svg.height() / ($cnt.height() || $cnt.data("height") || 1);
		
		var ratio = rtX > rtY ? rtX : rtY;
		
		ratio = (ratio < 1) ? 1 : ratio;
		
		var width = $svg.width()/ratio;
		var height = $svg.height()/ratio;
		
		if ($$.Data.Template === "PersonalPage") {
			$svg
				.stop()
				.animate({
					"width": width,
					"height": height
				},
				{
					duration: 450*(anim?1:0),
					easing: "easeOutCubic"
				});
		}
		else {
			var url = surl || encodeURI($QisSession.ConfName + "/Diagrams/" + $graph.attr("id") + "-" + $QisSession.LangCode+ ".svg");
			$svg.width(width*2).height(height*2);
			
			$graph
				.html($("<img/>").attr("src", url)
					.attr("data", url)
					.css({
						"width": "100%",
						"height": "100%"
					}))
				.css({
					"width": $svg.width(),
					"height": $svg.height()
				})
				.show()
				.stop()
				.animate({
					"width": width,
					"height": height
				},
				{
					duration: 600*(anim?1:0),
					easing: "linear"
				});
		}
			
		if (link) {
			var $link = $obj.HyperLink(null, null, null, null, false);
			$graph.attr("title", $obj.Data.Name)
				.setTooltip(null, null, true)
				.css("cursor", "pointer")
				.on($.browser.click, function(){
					var api = $graph.qtip("api");
					if (api) {
						api.toggle(false);
						api.disable(true);
						$.async(function(){
							api.destroy();
						}, 100);
					};
					$link.openTarget();
				});
		}
	}

	if (view === false || view < 1) {
		$.async(function(){
			$svg.find("a").each(function () {
				if ($(this).data("objid")===$obj.Data.ObjId) {
					$(this).disableHyperLink();
				}
				else {
					$(this).setTarget(true, true);
				}
			});
		});
	}
}

adjustMapData = function () {
	var $obj = this;
	
	var $graph = arguments.length 
		? arguments[0]
		: $(".tabs").getActiveTab().find(".graphicalData");
	
	if (!$graph.length) {
		return;
	}
	
	var $cnt = $graph.parent();
	
	var view = arguments[1] || $graph.hasClass("viewonly");
	var url = arguments[3] || encodeURI($QisSession.ConfName + "/Diagrams/" + $graph.attr("id") + "-" + $QisSession.LangCode+ ".htm");
	
	if (view) {
		var $img = $graph.find("img");
		
		var width = $img.data("width");
		var height = $img.data("height");
		
		var rtX = width / $cnt.width();
		var rtY = height / $cnt.height();
		
		var ratio = rtX > rtY ? rtX : rtY;
		
		if (ratio > 1) {
			$img.css({
				"width": width / ratio,
				"height": height / ratio
			});
		}
		else {
			$img.css({
				width: "auto",
				height: "auto"
			});
		}
	}

	if (view === false || view < 1) {
		$.async(function(){
			$graph.find("area").each(function(){
				if ($(this).data("objid")===$obj.Data.ObjId) {
					$(this).disableHyperLink();
				}
				else {
					$(this).setTarget(true, true);
				}
			});
		});
	}
	else {
		$graph
			.find("img").removeAttr("usemap").end()
			.find("map").remove();
	}
};

loadGraphicalXmlData = function(){
	if (!arguments.length) {
		return;
	}
	
	var $obj = arguments[0] || $$;
	var showErr = arguments[1] || false;
	
	$QisSession.Init[$obj.Data.ObjId + "-export"] = 1;
		
	var info = getRepositoryInfo($obj.Data.RepId);
	var config = info.RepId===$QisSession.RepId ? 
	getConfigurationInfo($obj.Data.ConfId) :
	{
		Name: info.ConfName,
		Id: info.ConfId
	};

	var src = encodeURI(config.Name + "/Diagrams/");
	
	if (info.RepId !== $QisSession.RepId) {
		src = $QisSession.HTMLLocation.replace(location.pathname, "/" + info.WebApplication + "/") + src;
	}
	
	var timestamp = "?_t=" + ($obj.Data.AuditMD ? 
		$obj.Date($obj.Data.AuditMD, null, null, true, true, true) :
		$QisSession.TimeStamp);
	
	var oid = $obj.Data.RevisionId || $obj.Data.ObjId;
	var url = src + oid + "-" + $QisSession.LangCode + ".xml";

	$.ajax({
		url: url + timestamp,
		dataType: "xml"
	})
	.done(function(data) {
		if (window.ActiveXObject){
			data = data.xml;
		}
		else{
			data = (new XMLSerializer()).serializeToString(data);
		}
		$QisSession.Init[$obj.Data.ObjId + "-export"] = data;
	})
	.fail(function(){
		delete $QisSession.Init[$obj.Data.ObjId + "-export"];
		if (showErr) {
			$.jnotify({
				title: $.LL("Error"),
				message: $.LL("Unable to retrieve diagram data from repository"),
				callback: function() {
					location.reload(true);
				}
			});
		}
	});
};

$.fn.initPopupContent = function () {
	if (!arguments.length) {
		return;
	}
	
	var $cnt = $(this);
	var $obj = arguments[0];
	
	var allowFullsize = arguments[1] || false;
	
	if (!$obj) {
		return $(this);
	}

	$obj.init(function() {
		if (arguments[0]===false){
			NProgress.done();
			return;
		}
		$.initTemplateSettings($obj, function(){
			var $defs = arguments[0] 
				? arguments[0]
				: QepSettings[$obj.TemplateDefinition["SettingsFile"]];
				
						
			var title = $obj.TemplateDefinition["Title"] || $obj.Data.Name;
			title = title.replace(/{Name}/g, $obj.Data.Name); 
			if (title.match(/{|}/)) {
				title = $.parseString.call($obj, title);
			}
			
			if (!$defs["PopupWindow"]) {
				return $cnt;
			}

			var $chd = $cnt.parents(".popupwindow");
			var $hdr = $chd.find(".popuptitle:first")
			
			var isStub = ($obj.Data.Type==8);
			
			$cnt.ajaxLoaderText(false);
			
			$hdr.html(title);
			//Qualisoft Added for QRV in IFrame
			$$QsCurrPopupObject=$obj;
			
			if ($chd.hasClass("fancyboxwindow")) {
				$cnt.css({
					"height": $cnt.height() - $hdr.height() + "px",
					"min-height": $cnt.height() - $hdr.height() + "px",
					"width": $cnt.width() + 15,
					"overflow-y": "scroll",
					"overflow-x": "hidden"
				});
			}
			
			if (($defs.Governance.Enable || $defs.Governance.CustomActions) && !isStub) {
				var $funcs = $("<span/>")
					.attr("class", "functions")
					.css({
						"display": "block",
						"line-height": "24px",
						"height": "24px",
						"min-height": "24px",
						"position": "relative",
						"top": "-2px"
					})
					.appendTo($cnt);
					
					$obj.GovernanceActions();
			}
			
			if ($defs.StandardContentTabs.Ratings) {
				if ($defs.StandardContentTabs.Ratings.Enable) {					
					$hdr.append($obj.RatingScore($cnt))
						.find(".rateit").css({
							"float": "",
							"margin-top": "0",
							"line-height": "",
							"top": "2px"
						});
				}
			}
			
			var $initContent = $defs.PopupWindow.Content;
			var $fullContent = $defs.FieldCenter.Content;
			
			if ($.keyCount($fullContent)) {
				$cnt.data("dynamic-content", true);
				if (allowFullsize) {
					$initContent = $fullContent;
				}
				if (!$.keyCount($initContent)) {
					$cnt.data("dynamic-content", true);
					$initContent = $fullContent;
				}
			}

			$.async(function(){
				if ($initContent) {
					$cnt.initContent($initContent, $obj);
				}
				
				var $tabs = $cnt.find(".tabs");
				
				if (!$tabs.length && $chd.hasClass("fancyboxwindow")) {
					$cnt.css({
						"width": $cnt.width() - 15,
						"overflow-y": "auto"
					});
					
					$hdr.css({
						"margin-bottom": 0,
						"padding-bottom": "5px",
						"border-bottom": "groove 1px #fff"
					})
				}
				else {
					$tabs
						.css({
							"font-size": "12.2px",
							"height": $cnt.height() - $cnt.find(".functions").outerHeight() - 5
						})
						.find("ol.ui-tabs-nav-arrows")
							.children().each(function(){
								$(this).css({
									"height": Math.max($(this).height() - 1, 32)
								});
							}).end()
						.end()
						.find(".ui-tabs-scroll-container").css("height", "37px")
						.end()
						.find(".ui-tabs-panel").css({
							"height": $cnt.find(".tabs").height() - 62,
							"overflow": "auto"
						})
						.end()
						.adjustTabScroll();
				}
			});
		});
	})
	
	return $cnt;
}

onFancyBoxShow = function () {
	if (!arguments.length){
		return;
	}
	
	if (History.enabled) {
		NProgress.start();
	}
	
	var $data = arguments[0];
	var $anch = arguments[1];
	
	var $obj = new RepositoryObject($data.Data.objid + "/" + $data.Data.revid);
	
	$obj.Data.Name = $data.Data.name;
	$obj.Data.Template = $data.Data.template;
	$obj.Data.ConfId = $data.Data.confid;
	$obj.Data.RepId = $data.Data.repid;
	
	var $frm = $data.Container;
		
	$frm
		.append($("<h3/>")
			.addClass("popuptitle")
			.css({
				"font-size": "12pt",
				"margin": "5px"
			}));
	
	var $cnt = $("<div/>")
		.css({
			"overflow": "auto",
			"padding": "3px",
			"min-height": $frm.height() - 12,
			"height": $frm.height() - 12
		});
	
	$cnt.ajaxLoaderText()
		.appendTo($frm)
		.find(".ajaxloader")
			.css({
				"text-align": "center",
				"margin-top":  $cnt.height()/2 - 20,
				"font-size": "13px"
			})     
			.end()
		.initPopupContent($obj);
}

onIFrameShow = function () {
	if (!arguments.length) {
		return;
	}
	
	$.fancybox.defaults.beforeShow();
	
	if (History.enabled) {
		NProgress.start();
	}
	
	var $data = arguments[0];
	var $anch = arguments[1];
	
	var $pos = arguments[2];
	
	var $wnd = $data.Container;
	var $frm = $wnd.getFrame()
		.data("objid", $data.Data.objid)
		.empty();

	var $obj = new RepositoryObject($data.Data.objid + "/" + $data.Data.revid);
	
	$obj.Data.Name = $data.Data.name;
	$obj.Data.Template = $data.Data.template;
	$obj.Data.ConfId = $data.Data.confid;
	$obj.Data.RepId = $data.Data.repid;
	
	$wnd.move($pos.left, $pos.top);
		
	$wnd
		.getHeader().find(".window_title_text")
		.addClass("popuptitle")
		.empty()
		.onchange(function(){
			var txt = $(this).text();
			if (txt) {
				$wnd.setTitle(txt);
				$(this).onchange(false);
			}
		});
	
	var $cnt = $("<div/>")
		.addClass("popupcontent")
		.css({
			"overflow": "auto",
			"padding": "3px",
			"min-height": $frm.height() - 7,
			"height": $frm.height() - 7
		});
	
	$cnt.ajaxLoaderText()
		.appendTo($frm)
		.find(".ajaxloader")
			.css({
				"text-align": "center",
				"margin-top":  $cnt.height()/2 - 20,
				"font-size": "13px"
			})     
			.end()
		.initPopupContent($obj);
}

onIFrameMaximize = function(wnd){
	var $cnt = wnd.getFrame();
	
	if ($.browser.compatible) {
		var $parent = $cnt.parents(".popupwindow");
		var position = $parent.parent().position();
		$parent.css({
			top: position.top,
			left: position.left
		});
	}
}

onIFrameAfterCascade = function(wnd) {
	var $frm = wnd.getFrame();
	var $cnt = $frm.parent().find(".popupcontent");
	if ($cnt.data("dynamic-content")) {
		var $obj = new RepositoryObject($frm.data("objid"));
		$cnt.empty()
			.ajaxLoaderText()
			.initPopupContent($obj, false);
	}
	onIFrameResizeEnd(wnd);
}

onIFrameAfterMaximize = function(wnd) {
	var $frm = wnd.getFrame();
	var $cnt = $frm.parent().find(".popupcontent");
	if ($cnt.data("dynamic-content")) {
		var $obj = new RepositoryObject($frm.data("objid"));
		$cnt.empty()
			.ajaxLoaderText()
			.initPopupContent($obj, true);
	}
	onIFrameResizeEnd(wnd);
}

onIFrameResizeEnd = function(wnd) {
	var $cnt = wnd.getFrame();
	
	var width = $cnt.children().width() - 7;
	var height = $cnt.height() - $cnt.find(".iframefunctions").outerHeight() - 15;

	$cnt.children().css({
		"height": $cnt.height()-7,
		"min-height": $cnt.height()-7
	}).find(".iframetabs")
		.css({
			"width": width,
			"min-height": height
		})
		.adjustTabScroll();
}

onIFrameClose = onFancyBoxClose = function() {
	if (!arguments.length){
		return;
	}

	var $data = arguments[0];
	var $anch = arguments[1];
	var isFancyBox = arguments[2] || false;
	
	if (!isFancyBox) {
		$.fancybox.defaults.beforeClose();
	}
	
	var $wnd = $data.Container;

	var $obj = new RepositoryObject($data.Data.objid + "/" + $data.Data.revid);
	
	$obj.Data.Name = $data.Data.name;
	$obj.Data.Template = $data.Data.template;
	$obj.Data.ConfId = $data.Data.confid;
	$obj.Data.RepId = $data.Data.repid;
	
	if ($anch){
		$anch.disableTooltip(false);
	}
}

loadUserInfo = function () {
	var $cnt = $("#userinfo")
		.addClass("text-shadow");
	
	$cnt.append($("<div/>").addClass("usericon"));
		
	if ($QepSettings.UserNameLabelEnabled) {
		$cnt.append($("<div/>").html($.LL("User:") + " " + $QisSession.UserInfo.FullName));
	}
	
	if ($QepSettings.UserRoleLabelEnabled) {
		$cnt.append($("<div/>").html($.LL("Role:") + " " + $QisSession.UserInfo.Role));
	}

	if ($QisSession.UserInfo.AltRoles && $QepSettings.UserRoleChangeEnabled) {
		$cnt.children("div").last().append($("<a/>")
			.html("[" + $.LL("change") + "]")
			.on($.browser.click, function(){ 
				$.initUserRoleChange(); 
			}));
	}
}

loadTreeView = function() {
	var $defs = arguments[0] ? arguments[0] : $QepSettings[$$.TemplateDefinition["SettingsFile"]];
	
	var $data = arguments[1] ? arguments[1] : $defs.StandardTreeView;
	
	if (!$defs) {
		return $(this);
	}
	
	var $cnt = this;
	var conf = getConfigurationInfo($QisSession.ConfId);
	
	var src = encodeURI(conf.Name + "/TreeView/" 
		+ $data.ObjId
		+ "-" + $QisSession.LangCode 
		+ ".htm") + "?t_=" + $QisSession.TimeStamp;
	
	$cnt.load(src, function() {
		var $this = $(this);
		var $view = $this.find(".treeview:first");
		
		if (!$view.length){
			return;
		}
		
		$view.fancytree({
			renderNode: function(event, data) {
				var node = data.node;
				if (node.span && node.data.template) {
					var $span = $(node.span);
					var $data = $this.data(node.data);
					var $anch = (new RepositoryObject($data)).HyperLink();
					
					$span.find("> span.fancytree-icon").remove();
					$span.find("> span.fancytree-title").html($anch)
						.css("max-width", 230 - node.data.level*20);
				}
			}
		});
		
		var $tree = $view.fancytree("getTree");
		$tree.activateKey($$.Data.ObjId);
	});
}

loadTopMenu = function () {
	if (typeof $.fn.dcMegaMenu == "undefined") {
		$.async(function(){
			loadTopMenu();
		}, 50)
		return;
	}
		
	var $defs = (typeof $$ != "undefined") ? 
		$QepSettings[$$.TemplateDefinition["SettingsFile"]] :  
		$QepSettings.Definitions;
	
	if (!$defs) {
		return;
	}
	
	if ($defs.Menu.Hidden) {
		return;
	}
	
	if(typeof $defs.Menu["ObjId"] == "undefined"){
		return;
	}
	
	var force = arguments[0] || false;
	var $top = $("#topmenu");
	var conf = getConfigurationInfo($QisSession.ConfId);
		
	var src = encodeURI(conf.Name + "/Menu/" 
		+ $defs.Menu.ObjId
		+ "-" + $QisSession.LangCode 
		+ ".htm") + "?t_=" + $QisSession.TimeStamp;
				
	var clb = arguments.length ? arguments[0] :  $.noop;
	
	if ($QepSettings.MenuFile === src && !force) {
		var $menu = $top.find("#mega-menu");
		clb.call($menu);
		return;
	}
	
	$QepSettings.MenuFile = src;
	
	$top.empty().hide().load(src, function () {
		var $cnt = $(this);
		var $menu = $(this).find("#mega-menu");
		
		var hdrHeight = $("#header").outerHeight();
		var barHeight = $("#bar").outerHeight();
		
		$menu.validMenuItems().dcMegaMenu({
			rowItems: $MegaMenuSettings.NCols,
			speed: 'fast',
			effect: 'fade',
			event: 'hover',
			beforeOpen: function () {
				$.fancybox.defaults.beforeShow();
				$(this).children(".sub-container")
					.css({
						"opacity": String($$.Data.Template).match(/personalpage/i) ? 0.925 : 1
					})
					.toggleClass("ui-widget-content ui-corner-bottom")
					.adjustSubMenu($MegaMenuSettings, hdrHeight + barHeight);
			},
			beforeClose: function() {
				$.fancybox.defaults.beforeClose();
				$(this).children(".sub-container")
					.toggleClass("ui-widget-content ui-corner-bottom");
			}
		});
			
		$menu.children("li").each(function () {
			$(this)
				.hide()
				.children("a").each(function () {
					$(this)
						.addClass("text-shadow")
						.addMenuTooltip()
						.addMenuStyle($MegaMenuSettings)
						.hover(function () {
							$(this).addMenuHoverStyle($MegaMenuSettings);
						}, function () {
							$(this).addMenuStyle($MegaMenuSettings);
						})
						.on($.browser.click, function(e){ 
							if ($(this).attr("href")=="#") {
								e.preventDefault();
							} 
						});
					var menuImg = $(this).attr("data-icon");
					if (!menuImg) {
						$(this)
							.css({
								"margin-top": "75px",
								"line-height": barHeight + "px"
							})
					}
					else {
						$(this)
							.prepend($("<img border='0' rel=''/>")
							.attr("src", "images/menu/" + menuImg)
							.css({
								"height": "64px",
								"margin": "8px auto 11px auto",
								"display": "block"
							})
						)
						.css({
							"height": hdrHeight + barHeight + "px",
							"text-align": "center"
						})
					}
					$(this)
						.children("span.dc-mega-icon")
							.css({
								"top": (menuImg ? hdrHeight : 0) + (barHeight / 2) - 2 + "px"
							})
						.end()
				});
		});

		$cnt
			.prepend(
				$("<span rel=\"nav-prev\" />")
					.attr("title", "Previous menu option")
					.adjustMenuNavigation()
			)
			.append(
				$("<span rel=\"nav-next\" />")
					.attr("title", "Next menu option")
					.adjustMenuNavigation()
			)
			.adjustMenu();
			
		$.async(function(){
			var confId = $.browserCache("get", "persistent-Configuration");
			if (confId) {
				if ( confId != $QisSession.ConfId ) {
					var title = $.LL("Configuration Changed");
					var caption = $.LL("You are now in") + " " + $QisSession.ConfName + " " + ($QisSession.IsInPrivateWorkspace?" (" + $.LL("Private Workspace") + ")" : "");
					$.jbar({
						title: title,
						caption: caption,
						onHide: function(){
							if ($QisSession.BaseConfName != $QisSession.ConfName) {
								$.jribbon(getConfigurationInfo($QisSession.ConfId).Title);
							}
						}
					});
					$.browserCache("clear");
					$.browserCache("set", "persistent-Configuration", $QisSession.ConfId);
				}
				else if(confId != $QisSession.BaseConfId) {
					$.jribbon(getConfigurationInfo($QisSession.ConfId).Title);
				}			
			}
		},100);
		
		clb.call($menu);
		
	}).fadeIn();
}

loadNavButtons = function () {
	var $defs = arguments[0] || $QepSettings[$$.TemplateDefinition["SettingsFile"]];
	var ppage = arguments[1] || false;
	
	var $btns = {
		SlidePanel: { Enable: false },
		SharePage: { Enable: false },
		PrintPage: { Enable: false },
		Language: { Enable: false },
		Configuration: { Enable: false },
		Application: { Enable: false },
		Analytics: { Enable: false },
		Search: { Enable: false },
		PersonalPage: { Enable: false },
		Logout: { Enable: false }
	};
	
    var $cnt = $("#bar:first").empty().addClass("navigation-panel");
	
	$.extend(true, $btns, $defs.NavigationButtons);
	
	if ($QepSettings.PersonalPage.Enable 
	&& $QepSettings.PersonalPage.Content
	&& $QepSettings.StartPage) {
		$btns.PersonalPage.Enable = true;
		$btns.PersonalPage.Title = ppage ? $.LL("Home page") : $.LL("Personal page");
	}

	$.each($.extend({}, true, $defs.NavigationButtons, $btns), function(nam, $opts){
		if (!$opts.Enable || !isValidRole($opts.RoleFilter)) {
			return;
		}
		
		var $btn = $("<button/>").addClass("navbutton")
			.attr("id","btn-" + nam)
			.attr("title", $opts.Title)
			.appendTo($cnt)
			.on($.browser.click, function(e){
				e.preventDefault();
				
				if (!$(this).hasClass("ui-state-hover")){
					e.stopImmediatePropagation();
					return false;
				}
				
				$(this).blur();
			});
		
		switch (nam) {
			case "SlidePanel": 
				$btn.initNavSlideBtn($defs);
				break;
			
			case "SharePage": 
				$btn.initNavShareBtn();
				break;
				
			case "PrintPage": 
				$btn.initPrintPageBtn();
				break;
			
			case "Language": 
				 $btn.initLangChgBtn();
				 break;
				 
			case "Configuration": 
				$btn.initConfChgBtn($opts);
				break;
				 
			case "Application": 
				$btn.initApplicationBtn();
				break;
				
			case "Analytics": 
				$btn.initAnalyticsBtn();
				break;
				
			case "Search": 
				$btn.initNavSearchBtn();
				break;
				
			case "PersonalPage": 
				$btn.initPersonalPageBtn(ppage);
				break;
				
			case "Logout": 
				$btn.initNavLogoutBtn();
				break;
			
			default:
				$btn.initNavCustomBtn($opts);
				break;
		}
	});

    $cnt.show();
}

initDiagramEditor = function() {
	var $obj = arguments[0];
	
	if (!($obj instanceof RepositoryObject)){
		return;
	}
		
	if (!$QisSession.Init[$obj.Data.ObjId + "-export"]) {
		$(document).unbind("keydown");
		$(document).bind("keydown", function(e){
			var key = e.which ? e.which : e.charCode;
			if (key == 116) {
				location.reload(true);
			}
		});
		
		loadGraphicalXmlData($obj, true);
	}
	
	$.loader({text: "Loading data"});

	if ($(".webmodeler").length) {
		$(".webmodeler").remove();
	}			
	var $wm = $("<table/>").addClass("webmodeler").hide().uniqueId();
	var uid = $wm.attr("id");
	
	$("<tr/>")
		.append($("<td/>")
			.addClass("diagram-window")
			.attr("rowspan", 2)
			.append("<div class=\"diagram-editor-menu ui-widget-header\"></div>")
			.append("<div id=\"diagram-" + uid + "\" class=\"diagram-container\"></div>"))
		.append($("<td/>")
			.addClass("palette-window default-window")
			.append("<div class=\"ui-widget-header\">" + $.LL("Symbols") + "</div>")
			.append("<div id=\"palette-" + uid + "\" class=\"palette-container\"></div>"))
		.appendTo($wm);
	$("<tr/>")
		.append($("<td/>")
			.addClass("overview-window default-window")
			.append("<div class=\"ui-widget-header\">" + $.LL("Overview") + "</div>")
			.append("<div id=\"overview-" + uid + "\" class=\"overview-container\"></div>"))
		.appendTo($wm);
		
	var adjustDiagramEditor = function () {
		if (!(".webmodeler").length) {
			$("#dataCenter").off("resize", adjustDiagramEditor);
		}
		
		$.async(function(){
			var height = $("#dataCenter").height();
			var offsetH = $wm.find(".ui-widget-header").first().outerHeight() + 5;
			var overviewH = $wm.find(".overview-container").first().outerHeight() + 5;
			
			$wm.find(".diagram-container").height(height - offsetH);
			$wm.find(".palette-container").height(height - offsetH*2 - overviewH);
		},100);
	};
	
	adjustDiagramEditor();
	$(window).resize(adjustDiagramEditor);
	
	$("#contentHeaderLeft").hide();
	$("#contentLeft").hide();
	$("#btn-SlidePanel").hide();
	
	$("#dataCenter").contents().fadeOut(function(){
		$.adjustTemplate();
		$(window).triggerHandler("resize");
	})
	.end().append($wm);
	
	initDiagramLayout.call($obj, $wm);
}

loadRoleSwitcher = function(){
	var $cnt = $("a.role-button");
	if ($cnt.length) {
		$cnt.detach().remove();
	}
	
	$cnt = $("<a/>").addClass("role-button")
	.html($.LL("Change role"))
	.button({ icons: { primary: "ui-icon-person" }})
	.css({
		"position": "relative",
		"margin": "35px 5px 0 0",
		"float": "right",
		"font-size": "11px"
	})
	.insertAfter($("#header"))
	.on($.browser.click, function(e) {
		$.initUserRoleChange();
	});
}

loadThemeSwitcher = function() {
	var $cnt = $("a.theme-button");
	if ($cnt.length) {
		$cnt.detach().remove();
	}
	
	$cnt = $("<a/>").addClass("theme-button")
		.html($.LL("Change theme"))
		.button({ icons: { primary: "ui-icon-image" }})
		.css({
			"position": "relative",
			"margin": "35px 25px 0 0",
			"float": "right",
			"font-size": "11px"
		})
		.insertAfter($("#header"))
		.on($.browser.click, function(e) {
			e.preventDefault();
			$.ThemeSelector(function(){
				var theme = this;
				if (theme.Id === $.browserCache("get", "persistent-sessiontheme" + $QisSession.ConfId)) {
					return;
				}
				$.async(function() {
					$.browserCache("set", "persistent-sessiontheme" + $QisSession.ConfId, theme.Id);
					$(".session-theme").detach().remove();
					$.initSessionStyles();
					$.addTemplateLogo();
					$.async(function(){
						$.overlay(false);
						$(document).trigger("reload:tiles");
					}, 1000);
					
					var bgImage = $QepSettings.PersonalPage.BackgroundImage;
					if (bgImage) {
						if (bgImage.split(",").length > 1) {
							var  src = "";
							switch (theme.Id) {
								case "qep-metro-light":
									src = bgImage.split(",")[9]; break;
								case "qep-metro-dark": 
									src = bgImage.split(",")[8]; break;
								default: src = bgImage.split(",")[0];
							}
							$("img.wallpaper-image").data("image-transition", false)
								.attr("src", "./images/backgrounds/"+src);
								
						}
					}
					
				}, 2000);
				$.overlay({
					text: $.LL("Applying theme") + ": " + theme.Name, 
					loader: true
				});
			});
		});		
}

loadPersonalPage = function() {
	if (History.enabled) {
		NProgress.configure({
			minimum: 0.1,
			speed: 750,
			trickle: true
		}).start();
	}
	
	document.title = $QisSession.RepName + ": " + $QisSession.UserInfo.FullName;
	
	$$ = new RepositoryObject($QisSession.UserInfo.ObjId);
	$$.Data.Template = "PersonalPage";
	$$.TemplateDefinition = { SettingsFile: "PersonalPage" };
	$.extend(true, $QepSettings[$$.TemplateDefinition["SettingsFile"]], $QepSettings.Definitions);
	
	if ($QepSettings[$$.TemplateDefinition["SettingsFile"]].NavigationButtons) {
		$QepSettings[$$.TemplateDefinition["SettingsFile"]].NavigationButtons.SlidePanel = false;
		$QepSettings[$$.TemplateDefinition["SettingsFile"]].NavigationButtons.SharePage = false;
		$QepSettings[$$.TemplateDefinition["SettingsFile"]].NavigationButtons.Analytics = false;
		loadNavButtons(null, true);
	}

	delete $QepSettings.SettingsFile;
	
	$.async(function(){
		loadTopMenu(function(){
			if ($.browser.chrome){
				$(this).find(".dc-mega-icon").css("top", "87.5px");
			}
		});
	});
	
	if (typeof onPersonalPageInit === "function") {
		onPersonalPageInit.call();
	}

	$("#content").hide();
	
	var bgImage = $QepSettings.PersonalPage.BackgroundImage;
	
	if (bgImage !== null) {
		$("#main").addClass("no-background");
		$("#header").css("opacity", 0.65);
		$("#bar").css("opacity", 0.75);

		if ($(".wallpaper").length) {
			$(".wallpaper").remove();
		}
				
		var $wp = $("<div/>").addClass("wallpaper")
			.prependTo($("body"))
			.css({
				"position": "fixed",
				"width": "100%",
				"height": "100%",
				"top": 0,
				"overflow": "hidden"
			});
			
		var picAdj = function() {
			var $img = arguments[0] || $("body").children("img.wallpaper-image");
			var scal = true;
			
			if (typeof arguments[1] !== "undefined") {
				scal = arguments[1];
			}
			
			if (scal) {
				if (!$img.length) {
					$img = $("img.wallpaper-image");
					if (!$img.length) {
						$(window).off("resize", picAdj);
						return;
					}
				}
				if (!$img.data("animation")) {
					$img.width($(window).width())
						.height($(window).height());
				}
				return;
			}

			var width = $img.width();
			var diff = $(window).width() - width + 20;
			$img.detach().prependTo($wp).css({
				"visibility": "visible",
				"opacity": 1,
				"height": $(window).height(),
				"width": width
			});

			if (diff<0 && $QepSettings.PersonalPage.Content 
			&& ($QepSettings.PersonalPage.Animation || $.cookie("_QISDEMO_"))) {
				$img.data("animation", true)
					.animate({"left": diff + "px"}, -175/diff*2000000,"linear");
			}
			else {
				$img.data("animation", false)
					.width($(window).width());
			}
			
		};
		
		var picUrl = function(){
			var src = arguments[0] || 0;
			if (typeof src === "number") {
				return ("./images/backgrounds/" + bgImage.split(",")[src])
			}
			else {
				return (isValidUrl(src) ? src : ("./images/backgrounds/"+src));
			}
		};
		
		var picAdd = function(){
			var pnum = arguments[0] || 0;
			var $img = $("<img/>").appendTo($("body"))
				.attr("src", picUrl(pnum))
				.addClass("wallpaper-image")
				.css({
					"visibility": "hidden",
					"position": "absolute"
				});
			picAdj($img, false);
		};
		
		var picChg = function(pnum){
			var $img = $wp.children("img.wallpaper-image");
			picAdd(pnum);
			$.async(function(){
				$img.animate({"opacity": 0}, 850, function(){ $img.remove(); });
			}, 5000);
		};
		
		var nImages = bgImage.split(",").length;
		
		picAdd(nImages>1 ? null : bgImage);
		
		if (nImages>1) {
			var picArr = function() {
				var arr = [];
				for (var i=0; i<nImages; i++) {	arr.push(i); }
				return arr;
			};
			var pics = picArr();
			$.async(function(){
				if (!$("body").children(".wallpaper").length) {
					$.async(false, this);
				}
				if ($("body").hasClass("fancybox-lock") || $("body").hasClass("overlay")) {
					return;
				}
				if ($("img.wallpaper-image").data("image-transition")) {
					$("img.wallpaper-image").removeData("image-transition");
					return;
				}
				if (!pics.length) {
					pics = picArr();
				}
				var pnum = pics.splice(Math.floor(Math.random() * pics.length), 1);
				picChg(pnum.pop());
			}, 15000, true);
		}
		
		$(window).on("resize", picAdj);
	}
		
	var getPersonalData = function(){
		var $cnt = $(".quali-wall");
		
		if ($cnt.length) {
			$cnt.empty();
		}
		else {
			$cnt = $("<div/>")
				.addClass("quali-wall")
				.attr("id", "qualiwall")
				.appendTo($("#main td:first")
					.find("#content")
						.find("tbody:first>tr>td").empty()
						.end()
					.end());
		}
		
		$cnt.css("overflow-hidden");
		
		var isresizing = -1;
		
		var wall = new freewall("#qualiwall");		
		wall.reset({
			selector: '.brick',
			animate: true,
			fixSize: false,
			cellW: 170,
			cellH: 150,
			delay: 0,
			onResize: function() {
				$.async(false, isresizing);
				isresizing = $.async(function(){
					wall.fitWidth();
				}, 50);
			}
		});
		
		wall.fitWidth();
		$.adjustTemplate();
		
		if (History.enabled) {
			$.async(function(){
				NProgress.inc(0.3);
			}, 55);
		}
		
		var $tiles = $QepSettings.PersonalPage.Content;
		
		if (!$tiles) {
			$cnt.initDefaultPersonalPage(wall);
		}
		else {
			$.each($tiles, function() {
				var $t = this;
				if (isValidRole($t.RoleFilter) 
				&& isValidConfiguration($t.ConfigurationFilter)) {
					var $item = $("<div/>")
						.addClass("brick animation flip")
						.addClass("size" + ($t.TileWidth||1) + ($t.TileHeight||1))
						.data("color", $t.TileColor)
						.html("<div class=\"read-more\"></div>");

					wall.appendBlock($item);

					var $opts = {
						icon: $t.TileIcon ? $t.TileIcon.toLowerCase() : null,
						title: $t.Title,
						link: $t.HTMLTileLink,
						method: $t.HTMLTileMethod,
						parms: [],
						sortable: $t.HTMLTileSortable
					};
										
					if ($t.HTMLTileAnimation) {
						$opts.animInterval = parseInt($t.HTMLTileAnimationInterval) || 8000,
						$opts.animType = $t.HTMLTileAnimationType || "Counter",
						$opts.animAttr = $t.HTMLTileAnimationAttribute || null,
						$opts.animStyle = $t.HTMLTileAnimationStyle || "Flip - Vertical",
						$opts.animCustomContent = $t.HTMLTileCustomAnimation
					}
					else {
						$opts.animation = false;
					}
					
					if ($t.HTMLTileContent) {
						if ($t.HTMLTileMethod === "Feeds") {
							$opts.content = {};
							$.each($t.HTMLTileContent, function(){
								$opts.content[this.Prompt] = this.Attribute;
							});
						}
						else {
							$opts.header = [];
							$opts.attrs = [];
							$opts.style = [];
							$opts.format = [];
							$.each($t.HTMLTileContent, function(){
								$opts.header.push(this.Prompt);
								$opts.attrs.push(this.Attribute);
								$opts.style.push(this.Style);
								$opts.format.push(this.Format);
							});
						}
					}
					
					if ($t.HTMLTileParameters) {
						if ($opts.method === "ExternalLink") {
							$opts.onclick = function() {
								var url = ($t.HTMLTileParameters[1]).Value;
								if (url) {
									window.open(url);
								}
							};
							$opts.method = null;
						} 
						else if ($opts.method === "WebForm") {
							var $parms = {
								Customization: null,
								Template: null,
								Attribute: null,
								Width: null,
								Height: null,
								ObjId: null,
								RepId: null,
								ConfId: null,
								Callback: $.noop,
								GovernanceAttribute: null
							};
							
							$.each($t.HTMLTileParameters, function() {
								if ($parms.hasOwnProperty(this.Name)) {
									if (this.Name === "Callback") {
										if (this.Value === "null") {
											$parms[this.Name] = $.noop;
										}
										else if (window[this.Name] === "function") {
											$parms[this.Name] = window[this.Name];
										}
										else {
											try {
												$parms[this.Name] === eval("(" + this.Value + ")");
											}
											catch(e) {
												$parms[this.Name] = $.noop;
												$.browser.warn("Tile Callback: " + e);
											}
										}
										return;
									}
									$parms[this.Name] = this.Value!=="null" ? this.Value : null;
								}
							});
														
							if ($parms.Template || $parms.Customization || $parms.ObjId) {
								$opts.onclick = function() {
									$$.ObjectDialog($parms.Customization, 
										$parms.Template, 
										$parms.Attribute, 
										$parms.Width,
										$parms.Height,
										$parms.ObjId,
										$parms.RepId,
										$parms.ConfId,
										$parms.Callback,
										$parms.GovernanceAttribute);
								};
							}
							$opts.method = null;
						}
						else {
							if ($opts.method === "ServiceHandlers.FindObjects") {
								var $o = {
									key: "Name",
									value: "asc"
								};
								var $p = {
									templates : [],
									filter: [],
									filterType: "And",
									detectMetamodelFormat: false,
									order: [],
									top: 10,
									skip: 0
								};
								$.each($t.HTMLTileParameters, function(){
									var parm = this.Name;
									var value = this.Value;
									switch (value) {
										case "AuditCD":
											value = "sys_Created";
											break;
										case "AuditCN":
											value = "sys_CreatedBy";
											break;
										case "AuditMD":
											value = "sys_Modified";
											break;
										case "AuditMD":
											value = "sys_ModifiedBy";
											break;
										default: break;
									}
									if (parm in $p) {
										switch (typeof $p[parm]) {
											case "string": $p[parm] = value; break;
											case "boolean":
											case "number": $p[parm] = eval(value); break;
											case "object": $p[parm] = value.split(/,|;/g); break;
											default: break;
										}
									}
									else if (parm.match(/sortattribute/i)) {
										$o.key = value;
									}
									else if (parm.match(/sortorder/i)) {
										$o.value = value.match(/asc/i) ? "asc" : "desc";
									}
								});
								$opts.method = "findobjects";
								$opts.parms = $p;
								$opts.parms.order.push($o);
							}
							else {
								if ($opts.method.match(/Tweets|Feeds/)) {
									$opts.parms = {};
									$.each($t.HTMLTileParameters, function(){
										$opts.parms[this.Name] = this.Value;
									});
								}
								else {
									$.each($t.HTMLTileParameters, function(){
										$opts.parms.push({
											Name: this.Name,
											Format: this.Format,
											Value: this.Value
										});
									});
								}
							}
						}
					}
					else if ($opts.method === "GWE_ServiceHandlers.GetGovernanceTasks") {
						var ConfIdList = "";
						$.each($ConfigurationInfo.Data||{}, function(idx, data){
							ConfIdList += (ConfIdList ? "," : "") + data.Id;
						});
						
						$opts.parms = {Name:"ConfigurationFilter", Format:"PlainText", Value:ConfIdList};
					}
					else if ($opts.method === "Custom") {
						$opts.content = $opts.animCustomContent;
					}
					
					if ($t.HTMLTileSortAttribute && $t.HTMLTileSortOrder) {
						var idx = 0;
						var ord = $t.HTMLTileSortOrder.match(/asc/i) ? "asc" : "desc";
						var valid = false;
						$.each($t.HTMLTileContent||{}, function(){
							if (this.Attribute === $t.HTMLTileSortAttribute) {
								valid = true;
								return false;
							}
							idx++;
						});
						if (!valid) { idx = 0; }
						$opts.sortorder = [[idx, ord]];
					}
					else if ($t.HTMLTileLink) {
						$opts.ShowDiagram = $t.ShowDiagram;
					}
					
					$item.ppTileContent($opts);
				}
			});
		}	

		var color = [];
		var initColorPalette = function() {
			switch ($QepSettings.Theme.Css) {
				case "qep-metro-light":
					color = ["#DCDCDC"]; break;
				case "qep-metro-dark": 
					color = ["#434341"]; break;
				default: color = ["#71cc98","#0097AA","#40403e","#73453c","#432815","#74406d","#b23068","#f08030","#d84f46","#BD974C","#3db4de","#4c6578","#414d4b"];
			}
		}

		$cnt.find(".brick").each(function(){
			$(this)
			.on("mouseenter", function(){
				$(this).addClass("active");
			})
			.on("mouseleave", function(){
				$(this).removeClass("active");
			});
		}).each(function(idx) {
			if ($(this).hasClass(".start-page")
			&& $QepSettings.Theme.Css.match(/default/i)){
				return true;
			}
			if (!color.length) { initColorPalette(); }
			var col = $(this).data("color") || color[color.length * Math.random() << 0];
			var cls = $(this).attr("class").split(" ").pop();
			var $w = $(this).wrapInner("<div class='tile-container'/>").children();
			$w.css("background-color", col).toggleClass(cls);
			$(this).toggleClass(cls);
			color.remove(col);
		});

		$(".read-more").append($("<span/>").addClass("qep-icon-16-forward").css("float", "right"));
						
		wall.refresh();
					
		$cnt.find(".brick").each(function(idx){
			var b = $(this);
			$.async(function (){
				b.removeClass("flip");
				$.async(function (){
					b.removeClass("animation")
						.addClass("semi-transparent")
						.css("z-index", 1001);
					wall.refresh();
				}, 350);
			}, 100 * idx);
		});
		
		$(window).bind("resize", function(){
			wall.refresh();
		});
		
		$.async(function(){
			if ($cnt.height() < 100) {
				getPersonalData();
			}
			else if (History.enabled) {
				NProgress.done(true);
			}
		}, $cnt.find(".brick").length * 100);
	}
	
	$.getScript("js/freewall/jquery.freewall.min.js")
		.done(function(){
			if (History.enabled) {
				$.async(function(){
					NProgress.inc(0.3);
				}, 55);
			}
			
			$.async(function(){
				getPersonalData();
				loadUserInfo();				
				if ($QisSession.UserInfo.AltRoles) {
					loadRoleSwitcher();			
				}
				loadThemeSwitcher();			
			}, 250);
			
			var reloadTilesPage = function(e){
				if (!$(".quali-wall").length) {
					$(document).unbind("keydown", reloadTilesPage);
					$(document).bind("keydown", $.pageKeys);
					return $.pageKeys(e);
				}
				var key = e.which ? e.which : e.charCode;
				if (key == 116) {
					if (e.ctrlKey) {
						e.preventDefault();
						$.browserCache("clear");
						window.location.reload(true);
					}
					else {
						e.preventDefault();
						$.fancybox.close();
						$.async(function(){
							$.pageCache("clear");
							getPersonalData();
						}, e.binding ? 250 : 1);	
					}
				}
				else if (key==27) {
					$.fancybox.close();
					$.window.closeAll();
				}
			};
			
			$(document).off("keydown");
			$(document).bind("keydown", reloadTilesPage);
			$(document).bind("reload:tiles", function(){
				reloadTilesPage({which:116, charCode:116, preventDefault: $.noop, binding: true});
			});
		})
		.fail(function(){
			return _$$_($QepSettings.StartPage);
		})
}

$.fn.initDefaultPersonalPage = function() {
	if (!arguments.length) {
		return $(this);
	}
	
	var $cnt = this;
	var wall = arguments[0];
	
	var $opts = {
		Home: true,
		QuickSearch: true,
		SocialListening: $QisSession.UserInfo.Role.match(/architect/i) ? true : false,
		Subscriptions: true,
		Tasks: true,
		Capabilities: $QisSession.UserInfo.Role.match(/management/i) ? true : false,
		ChangeRequests: true,
		NonConformances: true,
		CorrectiveActions: true,
		Audits: true,
		LatestChanges: true,
		Investment: $QisSession.UserInfo.Role.match(/management/i) ? true : false,
		Report: $QisSession.UserInfo.Role.match(/management/i) ? true : false,
		News: true
	};
	
	if (typeof StartPage !== "undefined") {
		$.extend(true, $opts, StartPage);
	}
	
	if ($opts.Home && $QepSettings.StartPage) {
		wall.appendBlock("<div class=\"brick size22 animation flip start-page\"><div class=\"read-more\"></div></div>");
	}
	
	if ($opts.Tasks) {
		wall.appendBlock("<div class=\"brick size21 animation flip user-tasks\"><div class=\"read-more\" /></div>");
	}
	
	if ($opts.SocialListening) {
		wall.appendBlock("<div class=\"brick size23 animation flip tweets\"><div class=\"read-more\"></div>");
		wall.appendBlock("<div class=\"brick size23 animation flip feeds\"><div class=\"read-more\"></div>");
	}
	
	if ($opts.Subscriptions) {
		wall.appendBlock("<div class=\"brick size21 animation flip user-subscriptions\"><div class=\"read-more\"></div></div>");
	}
	
	if ($opts.Capabilities) {
		wall.appendBlock("<div class=\"brick size11 animation flip user-capabilities\"><div class=\"read-more\"></div>");
	}

	if ($opts.Investment) {
		wall.appendBlock("<div class=\"brick size11 animation flip investment\"><div class=\"read-more\"></div>");
	}

	if ($opts.Report) {
		wall.appendBlock("<div class=\"brick size11 animation flip report\"><div class=\"read-more\"></div>");
	}

	if ($opts.Audits) {
		wall.appendBlock("<div class=\"brick size11 animation flip quality-audits\"><div class=\"read-more\"></div>");
	}
	
	if ($opts.ChangeRequests) {
		wall.appendBlock("<div class=\"brick size11 animation flip change-requests\"><div class=\"read-more\"></div>");
	}
	
	if ($opts.QuickSearch) {
		wall.appendBlock("<div class=\"brick size22 animation flip user-search\"><div class=\"read-more\" /></div>");
	}

	if ($opts.NonConformances) {
		wall.appendBlock("<div class=\"brick size11 animation flip non-conformances\"><div class=\"read-more\"></div>");
	}

	if ($opts.CorrectiveActions) {
		wall.appendBlock("<div class=\"brick size11 animation flip corrective-actions\"><div class=\"read-more\"></div>");
	}
	
	if ($opts.LatestChanges) {
		wall.appendBlock("<div class=\"brick size11 animation flip latest-changes\"><div class=\"read-more\" /></div>");
	}
	
	if ($opts.News) {
		wall.appendBlock("<div class=\"brick size31 animation flip latest-news\"><div class=\"read-more\"></div>");
	}
		      
		
	$.async(function(){
		$cnt.find(".user-search").ppTileContent({
			icon: "search",
			title: $.LL("Quick Search"),
			method: "Search",
			parms: [
				{Name:$.LL("Process:"), Format:"", Value: "BusinessProcessNetwork,WorkFlowDiagram,BusinessProcessDiagram"},
				{Name:$.LL("Document:"), Format:"", Value: "ExternalDocument"}
			]
		});
		
		$cnt.find(".user-subscriptions").ppTileContent({
			icon: "notification",
			title: $.LL("My Subscriptions"),
			header: ["Name","Revision","Last modified","Modified by"],
			attrs: ["Name","Revision","AuditMD","AuditMN"],
			objlink: true,
			method: "ServiceHandlers.GetUserSubscriptions",
			parms: [
				{Name:"personal", Format:"PlainText", Value: "true"},
				{Name:"team", Format:"PlainText", Value: "true"}
			] 
		});
		
		$cnt.find(".user-tasks").ppTileContent({
			icon: "tasks",
			title: $.LL("My Governance Tasks"),
			header: ["Name","Governance Area","Last modified","Status"],
			attrs: ["Name","GovernanceWorkFlow","AuditMD","AuditGS"],
			objlink: true,
			method: "GWE_ServiceHandlers.GetGovernanceTasks",
			parms: function(){
				var ConfIdList = "";
				$.each($ConfigurationInfo.Data||{}, function(idx, data){
					ConfIdList += (ConfIdList ? "," : "") + data.Id;
				});
				
				return {Name:"ConfigurationFilter",Format:"PlainText",Value:ConfIdList};
			}
		});
		
		$cnt.find(".user-capabilities").ppTileContent({
			icon: "regulation",
			title: $.LL("My Business Capabilities"),
			header: ["Name","Description","Last modified","Status"],
			attrs: ["Name","Description","AuditMD","AuditGS"],
			objlink: true,
			method: "ServiceHandlers.MyObjectsJSON",
			parms: [
				{Name:"template", Format:"PlainText", Value: "Capability"},
				{Name:"linkattr", Format:"PlainText", Value: "HasResponsible"}
			] 
		});
		
		$cnt.find(".change-requests").ppTileContent({
			icon: "changerequest",
			title: $.LL("My Change Requests"),
			header: ["Name","Responsible","Last modified","Status"],
			attrs: ["Name","HasResponsible","AuditMD","AuditGS"],
			objlink: true,
			method: "ServiceHandlers.MyObjectsJSON",
			parms: [
				{Name:"template", Format:"PlainText", Value: "ChangeRequest"},
				{Name:"linkattr", Format:"PlainText", Value: "OriginatedBy"}
			] 
		});
		
		$cnt.find(".non-conformances").ppTileContent({
			icon: "nonconformance",
			title: $.LL("My Non-Conformances"),
			header: ["Name","Responsible","Last modified","Status"],
			attrs: ["Name","HasResponsible","AuditMD","AuditGS"],
			objlink: true,
			method: "ServiceHandlers.MyObjectsJSON",
			parms: [
				{Name:"template", Format:"PlainText", Value: "NonConformance"},
				{Name:"linkattr", Format:"PlainText", Value: "OriginatedBy"},
				{Name:"sortattr", Format:"PlainText", Value: "sys_Created"}
			] 
		});
		
		$cnt.find(".corrective-actions").ppTileContent({
			icon: "ready",
			title: $.LL("My Corrective Actions"),
			header: ["Name","Responsible","Last modified","Status"],
			attrs: ["Name","HasResponsible","AuditMD","AuditGS"],
			objlink: true,
			method: "ServiceHandlers.MyObjectsJSON",
			parms: [
				{Name:"template", Format:"PlainText", Value: "CorrectiveAction"},
				{Name:"linkattr", Format:"PlainText", Value: "OriginatedBy"},
				{Name:"sortattr", Format:"PlainText", Value: "sys_Created"}
			] 
		});
		
		$cnt.find(".quality-audits").ppTileContent({
			icon: "list",
			title: $.LL("My Audits"),
			header: ["Name","Target process","Last modified","Status"],
			attrs: ["Name","TargetProcess","AuditMD","AuditGS"],
			objlink: true,
			method: "ServiceHandlers.MyObjectsJSON",
			parms: [
				{Name:"template", Format:"PlainText", Value: "QualityAudit"},
				{Name:"linkattr", Format:"PlainText", Value: "HasLeadAuditor"},
				{Name:"sortattr", Format:"PlainText", Value: "sys_Created"}
			] 
		});
		
		$cnt.find(".latest-changes").ppTileContent({
			icon: "document",
			title: $.LL("Latest Changes"),
			header: ["Name","Last modified","Modified by"],
			attrs: ["Name","AuditMD","AuditMN"],
			objlink: true,
			sortable: false,
			animation: false,
			method: "ServiceHandlers.LatestChanges",
			parms: [
				{Name:"count", Format:"PlainText", Value: 30},
				{Name:"days", Format:"PlainText", Value: 30},
				{Name:"templates", Format:"PlainText", Value: "BusinessProcessNetwork,WorkFlowDiagram,BusinessProcessDiagram,ExternalDocument"}
			]
		});
		
		$cnt.find(".investment").ppTileContent({
			icon: "list",
			title: $.LL("Investment Portfolio"),
			animation: false,
			link: {
				ObjId: "0d2b6e30-cee2-4428-b3a5-935aecb5be06",
				Template: "HTMLQueryResultView",
				Name: "Investment Portfolio"
			}
		});
		
		$cnt.find(".report").ppTileContent({
			icon: "chart",
			title: $.LL("Project Initiatives"),
			animation: false,
			link: {
				ObjId: "2c287337-b057-4e16-ad54-1edab553c65e",
				Template: "ReportDefinition"
			}
		});

		$cnt.find(".tweets").ppTweetsContent();
		$cnt.find(".feeds").ppFeedsContent();
		$cnt.find(".start-page").ppStartPage();
		$cnt.find(".latest-news").ppLatestNews();
	});
	
	return $cnt;
}

$.fn.ppTileContent = function() {
	if (!$(this).length) {
		return this;
	}
	
	var $cnt = $(this);
	
	var $opts = {
		icon: null,
		title: "-",
		titleClass: "h3",
		header: [],
		attrs: [],
		style: [],
		format: [],
		objlink: true,
		onclick: null,
		func: null,
		link: null,
		content: null,
		method: null,
		parms: null,
		sortable: true,
		sortorder: [[0, "asc"]],
		callback: $.noop,
		animation: function(){this.defaultTileAnimation.apply(this, arguments);},
		animInterval: 9000,
		animType: "counter",
		animStyle: "auto",
		animVal : function($o){$o.data("count", this.jquery ? this.find("a").length : this.length)},
		animAttr: null
	};
		
	if (arguments.length) {
		$.extend(true, $opts, arguments[0]);
	}
	
	if ($opts.method === "Tweets") {
		$cnt.ppTweetsContent($opts);
		return $cnt;
	}
	else if ($opts.method === "Feeds") {
		$cnt.ppFeedsContent($opts);
		return $cnt;
	}
	else if ($opts.method === "Custom" && $opts.animType === "Custom") {
		$cnt.ppCustomContent($opts);
		return $cnt;
	}
	
	if ($opts.header.length != $opts.attrs.length){
		return $(this);
	}
	
	if (typeof $opts.parms === "function") {
		$opts.parms = $opts.parms.call();
	}
	
	var $tmp = $("<div>/")
		.addClass("tile center")
		.append($("<span/>").addClass("qep-icon-48-" + $opts.icon))
		.append($("<"+$opts.titleClass+"/>").addClass("center white")
			.html($opts.title))
		.appendTo($cnt);
			
	if ($opts.method === "Search") {
		$cnt.ppSearchTile($opts);
		return $cnt;
	}
			
	$cnt
		.append($tmp)
		.find(".read-more").each(function(){
			if ($opts.content instanceof RepositoryObject) {
				return;
			}
			$(this).on($.browser.click, function(e){
				if ($opts.link) {
					var $o = new RepositoryObject($opts.link);
					$o.init(function(){
						var $l = $o.HyperLink();
						$l.openTarget();
					});
					return;
				}
				if ($opts.onclick) {
					$opts.onclick.call();
					return;
				}	
				var $div = $("<div/>")
					.height(Math.min(450, $(window).height()*0.9))
					.width(Math.min(750, $(window).width()*0.9))
					.append($("<h3/>").html($opts.title).css("font-size", "16px"))
					.ajaxLoaderText();
		
				if ($opts.func || $opts.content) {
					$div.ppTileFunction($opts, false, $cnt, e);
				}
				else if ($opts.method) {
					$div.ppTileAjax($opts, false, $cnt, e);
				}
			});
		});
		
	$.async(function(){
		if ($opts.func || $opts.content) {
			$("<div/>").ppTileFunction($opts, true, $cnt);
		}
		else if ($opts.method) {
			if ($QepSettings.PersonalPage.Content) {
				$cnt.find(".tile").after($("<div/>")
					.addClass("tile-indicator tile-spinner"));
			}
			$("<div/>").ppTileAjax($opts, true, $cnt);
		}
		else if ($opts.link && $opts.ShowDiagram) {
			$cnt.ppTileDiagram($opts);
		}
	}, 1000);
	
	return $(this);
}

$.fn.ppTileDiagram = function(){
	if (!arguments.length) {
		return $(this);
	}
	
	var $opts = arguments[0];
	
	var $cnt = $(this);
	var $obj = new RepositoryObject($opts.link);
	
	var loadGraph = function () {
		var $tile = $cnt.find(".tile");
		var $dgm = $cnt.find(".tile-data .tile-diagram");
		
		if ($dgm.length) {
			$dgm.empty();
		}
		else {
			$dgm = $("<div/>").addClass("tile-data tile-diagram")
				.css("text-align", "center")
				.insertAfter($tile);
		}
		
		loadGraphicalData($dgm.css({
			"height": $cnt.height() - $tile.find("h3").height() - 80,
			"width": $cnt.width() - 20,
			"margin": "0 10px 5px 10px",
			"visibility": "hidden"
		}).detach(), $obj, true, false);
		
		$.async(function(){
			if ($dgm.isLoading()) {
				return;
			}
			$.async(false, this);
			$tile.children("h3").animate({"font-size": "20px"}, 700);
			var $icon = $tile.children("span");
			$icon.animate({
				"opacity": 0,
				"margin-top": -$icon.height() - 10 + "px"
			}, 750, function(){
				$tile.after($dgm);
				var diffx = Math.max($dgm.width() - $dgm.children().width(), 0);
				var diffy = Math.max($dgm.height() - $dgm.children().height(), 0);
				$dgm.children().first().css("margin", diffy/2 + "px " + diffx/2 + "px");
				$dgm.hide()
					.css("visibility", "visible")
					.fadeIn("fast");
			});
		}, true, 100);
	};
	
	var idx = (($(".quali-wall").data("graphIndex")) || 0) + 1;
	$.async(function(){
		loadGraph();
	}, 1000 + idx*1000);
	$(".quali-wall").data("graphIndex", idx);
	
	var adjust = -1;
	$(window).bind("resize", function(){
		$cnt.find(".tile-data").empty();
		if (!$(".quali-wall").length) {
			return;
		}
		$.async(false, adjust);
		adjust = $.async(function(){
			$.async(function(){
				loadGraph();
			}, 500);
		}, 250);
	});
	
	return $cnt;
}

$.fn.ppTileFunction = function(){
	if (!arguments.length) {
		return $(this);
	}
	
	var $div = $(this);
	var $opts = arguments[0];
	var silent = arguments[1];
	var $cnt = arguments[2];
	
	if (!silent) {
		if ($opts.content instanceof RepositoryObject) {
			event.preventDefault();
		}
		
		$.fancybox($div, {
			scrolling: "auto",
			afterShow: function() {
				if ($opts.func) {
					$div.append($opts.func.call());
				}
				else if ($opts.content) {
					$div.append($opts.content);
					if (typeof $opts.content === "string") {
						$div.ajaxLoaderText(false);
						return;
					}
				}
				$div.loadTileContent($opts);
			},
			afterClose: function(){}
		});
	}
	else {
		if ($opts.content instanceof RepositoryObject) {
			var $obj = $opts.content;
			$obj.audit(function(valid){
				if (valid) {
					var $link = $obj.HyperLink().appendTo($cnt).hide();
					$cnt.find(".read-more").on($.browser.click, function(){
						$link.trigger($.browser.click);
					});
				}
			});
			return $div;
		}
		else if ($opts.func) {
			$.execScript.call($div, $opts.func, $div);
		}
	}
	
	if ($opts.animation) {
		$.async(function(){
			if ($$.Data.Template !== "PersonalPage") {
				$.async(false, this);
			}
			$opts.animation.call($cnt, $div, $opts.animType);
		}, $opts.animInterval + $cnt.parent(".brick").index()*1000,	true);
	}
	
	return $div;
}

$.fn.ppTileAjax = function(){
	if (!arguments.length) {
		return $(this);
	}
	
	var $div = $(this);
	var $opts = arguments[0];
	var silent = arguments[1];
	var $cnt = arguments[2] ? arguments[2] : $(this);
	
	if (typeof silent == "undefined") {
		silent = false;
	}
	
	var getAjax = function(){
		var callback = arguments[0] ? arguments[0] : $.noop;				
		var parms, url;
		
		if ($opts.method.toLowerCase().indexOf("http")==0) {
			url = $opts.method;
			parms = window.JSON.stringify($opts.parms ? $opts.parms : null);
		}
		else if ($opts.method === "findobjects"){
			url = initQisCsharpMethod($opts.method, null);
			parms = window.JSON.stringify($opts.parms);
		}
		else {
			url = initQisCsharpMethod($opts.method);
			parms = window.JSON.stringify($opts.parms ? {objectAttributes: $opts.parms} : null);
		}
		
		var wsurl = parms + "_"  + url;

		if ($.pageCache("get", wsurl, true)) {
			var data = $.pageCache("get", wsurl);
			callback.call(data);
		}
		else if ($.pageCache("init", wsurl)) {
			$.async( function(){
				if ($.pageCache("init", wsurl)) {
					return;
				}
				$.async(false, this);
				var data = $.pageCache("get", wsurl);
				callback.call(data);
			}, 150, true);
		}
		else {
			$cnt.find(".read-more").addClass("loading");
			$.pageCache("init", wsurl, true);
			$.browser.log("Personal Page Query: " + wsurl);			
			$.ajax({
				type: 'POST',
				url: url,
				data: parms
			})
			.done(function (data) {
				if (!isValidJsonData.call(null, data, $opts.method)) {
					data = "{}";
				}
				$.pageCache("set", wsurl, data, null);
				callback.call(data);
			})
			.fail(function(jqxhr, textStatus, error){
				handleRequestError(jqxhr, textStatus, error, url, parms);
			})
		}
	}
	
	if (!silent) {
		if ($cnt.find(".read-more").hasClass("loading")) {
			$.overlay({init: true, loader: true});
		}
		$.async(function(){
			if ($cnt.find(".read-more").hasClass("loading")) {
				return;
			}
			$.async(false, this);
			$.overlay(false);
			$.fancybox($div, {
				scrolling: "auto",
				afterShow: function() {
					getAjax(function(){
						var $data = this.length ? $.parseJSON(this) : {};
						$div.loadTileContent($opts, $data);
					})
				}
			});
		}, 100, true);
	}
	else {
		getAjax(function(){
			var $data = this.length ? $.parseJSON(this) : {};
			var length = $.keyCount($data) - 1;
			
			$.async(function(){
				$cnt.find(".tile-indicator")
					.removeClass("tile-spinner")
					.addClass("tile-no-spinner")
					.html(length + 1);
			}, 50);
			
			if (length < 1) {
				$cnt.find(".read-more").removeClass("loading");		
			}
			
			var $objlist = [];

			$.each($data, function(idx, val){
				var $obj = new RepositoryObject(val);

				$obj.cache("get");
				$objlist.push($obj);
			});

			$cnt.find(".read-more").removeClass("loading");					
			
			if ($opts.animation) {
				$.async(function(){
					var $parent = $cnt.parents(".brick");
					if (!$parent.length){
						if (!$cnt.hasClass("brick")) {
							return;
						}
						$parent =  $cnt;
					}
					if ($opts.animStyle.match(/static/i)) {
						$cnt.staticTileAnimation($opts, $objlist);
					}
					else {
						$opts.animVal.call($objlist, $div);
						var index = $parent.index();
						$.async(function(){
							$.async(function(){
								if ($$.Data.Template !== "PersonalPage") {
									$.async(false, this);
								}
								if (!$parent.hasClass("active")) {
									$opts.animation.call($cnt, $div, $opts.animType, $objlist, $opts.animAttr);
								}
							}, $opts.animInterval, true);
						}, index*450);
					}
				},100);
			}			
		});
	}
	
	return $div;
}

$.fn.loadTileContent = function() {
	if (!arguments.length) {
		return $(this);
	}
	
	var $div = $(this);
	var $opts = arguments[0];
		
	var $objlist = arguments[1] ? arguments[1] : $div.find("a").detach();
	
	var $tbl = $("<table/>").css("width", "100%");
	var $head = $("<thead><tr></tr></thead>").appendTo($tbl);
	
	$.each($opts.header, function(idx,val){
		$head.children().append("<th>"+$.LL(val)+"</th>");
	});
	
	var count = $objlist.length || $.keyCount($objlist);

	if ( !count ) {
		$div.children(".ajaxloader").remove();
		$div.append($tbl);
		$tbl.addDataTable({	sDom: 'R<"H"ifr>t'});
		return;
	}

	var $tbdy = $("<tbody/>").appendTo($tbl);
	
	$.each($objlist, function(idx, val){
		var $row = $("<tr/>").appendTo($tbdy);

		var $obj = new RepositoryObject(val.ObjId);		
		$obj.merge(val);		
		$obj.TemplateDefinitions(function(){
			$.each($opts.attrs, function(idx, attr){
				var $val = $("<div/>").css({
					"line-height": "16px",
					"margin": "2px"
				});
				if (attr === "Name" && $opts.objlink) {
					if ($opts.method.match(/getgovernancetasks/i)) {
						var $link = $obj.HyperLink();
						$link.setGovernanceTaskIndicator($obj);
						$val.html($link);
					}
					else {
						$val.html($obj.HyperLink());
					}
					$val.css("min-width", "200px");
				}
				else if (typeof attr == "function") {
					$val.html(attr.call($obj));
				}
				else {
					$obj.attr(attr, null, $opts.style[idx], $val, $opts.format[idx]);
				}
				
				$row.append($("<td/>").html($val).css("line-height", "16px"));
			});
			
			$opts.callback.call($row);

			if (!--count) {
				$.async(function(){
					$div.find("h3").siblings().remove();
					$div.append($tbl);
					$tbl.appendTo($div).addDataTable({
						aaSorting: $opts.sortorder,
						bSort: $opts.sortable,
						iDisplayLength: -1,
						sDom: 'R<"H"ifr>t',
						sScrollX: $div.width() - 15,
						sScrollXInner: $div.width() - 18,
						sScrollY: $div.height() - 100,
						onLoad: function() {
							$div.find(".dataTables_scrollBody").css("overflow-y", "scroll");
						}
					});
					$div.find(".dataTables_scrollBody").css("overflow-y", "scroll");
				}, 100);
			}
		}, false, true);
	})
	
	return $(this);
}

$.fn.ppStartPage = function() {
	if (!$(this).length) {
		return $(this);
	}
	
	var $cnt = $(this).wrapInner("<div/>")
		.children().addClass("tile-container");
		
	if ($QepSettings.Theme.Css.match(/default/i)) {
		$cnt.css("background-color","#D1D2D4");
	}
			
	var $tmp = $("<div>/")
		.addClass("tile center")
		.append($("<span/>").addClass("qep-icon-48-home"))
		.append($("<h2/>").addClass("center white")
			.html("<span id=\"home\"></span>"));
	
	var $obj = new RepositoryObject($QepSettings.StartPage);
	
	$cnt.append($tmp)
		.find(".read-more").on($.browser.click, function(){			
			if (!$QepSettings.StartPage) {
				return;
			}
			$cnt.find(".graphicalData").remove();
			$cnt.hide("puff", 1000);
			$.async(function(){
				var $link = $obj.HyperLink();
				if (History.enabled) {
					$link.openTarget();
				}
				else {
					location.href = $link.attr("href");
					$.async(function(){
						loadPersonalPage();
					},100);
				}
			}, 400);
		});
	
	
	if (!$QepSettings.StartPage) {
		return;
	}
	
	if (!$obj.cache()) {
		$obj.audit(function(valid){
			if (valid) {
				$tmp.find("#home").html($obj.Data.Name);
			}
		});
	}
	else {
		$obj.cache("get");
		$tmp.find("#home").html($obj.Data.Name);
	}
	
	var $dgm = $("<div/>").addClass("tilediagram")
	
	var loadTileDiagram = function(){
		$cnt.find(".tilediagram").remove();
		$dgm
			.empty()
			.height($cnt.height() / 2 + 10)
			.width($cnt.outerWidth()*2)
			.css({
				"margin": "0 10px",
				"position": "relative"
			})
			.appendTo($cnt);
		
		if ($cnt.outerWidth() < 250) {
			return;
		}
		
		$.async(function(){
			$dgm.find(".graphicalData").remove();
			loadGraphicalData($dgm.width($cnt.width()-20), $obj, true, false, false)
				.css({
					"display": "block",
					"float": "left",
					"width": $cnt.width() - 20,
					"text-align": "center",
					"margin-right": "20px"
				});
			$obj.info("Description", function(){
				$dgm.find(".Description").remove();
				$dgm.append($obj.attr("Description")
					.css({
						"display": "block",
						"float": "right",
						"width": $cnt.width() - 20,
						"text-align": "left",
						"margin-top": "10px",
						"padding-right": "10px",
						"display": "none"
					}));
			});
		}, 500);
		
		flipInt = $.async(function(){
			if ($("body").hasClass("fancybox-lock") || $("body").hasClass("overlay")) {
				return;
			}
			if ($$.Data.Template !== "PersonalPage") {
				$.async(false, flipInt);
			}
			if (!$.trim($dgm.contents().last().text())) {
				return;
			}
			var flipped = $dgm.hasClass("flipped");
			var $div = $dgm.width($cnt.outerWidth()*2).children();
			$div.each(function(){
				$(this).show().width($cnt.width() - 20)
			})
			
			var dx = flipped ? 0 : $dgm.width()/2 + 10;
			$dgm.toggleClass("flipped").animate({
				"left": -dx
			},
			{
				duration: 2000,
				easing: "easeOutCubic",
				complete: function() {
					flipped ? $div.last().hide() : $div.first().hide();
				}
			});
		}, 10000, true);
	}
	
	var flipInt = rsInt = 0;
	
	$.async(function(){loadTileDiagram();}, 1000);

	$(window).bind("resize", function(){
		$.async(false, rsInt);
		$.async(false,flipInt);
		rsInt = $.async(function(){
			loadTileDiagram();
		}, 150);
	})
}

$.fn.ppLatestNews = function() {
	if (!$(this).length) {
		return $(this);
	}
	
	var $cnt = $(this).wrapInner("<div/>")
		.children()
			.addClass("tile-container");
			
	var $tmp = $("<div>/")
		.appendTo($cnt)
		.addClass("tile")
		.css({
			"float": "right",
			"margin-right": "12%"
		})
		.append($("<span/>").addClass("qep-icon-48-news"))
		.append($("<h2/>").addClass("center white")
			.html($.LL("News")))
		.append($("<div/>").addClass("news-content")
			.css({
				"color": "#666",
				"width": "66%",
				"height": "100%",
				"position": "absolute",
				"background-color": "rgba(255,255,255, 0.85)",
				"top": 0,
				"left": 0
			}));
			
	$cnt.find(".news-content").ajaxLoaderText()
		.find(".ajaxloader").css({
			"margin": "15px"
		});
	
	$.async(function(){
		$$.GenericQuery({
			templates: "NewsItem",
			sortAttribute: "AuditMD",
			sortOrder: "Descending",
			top: 3,
			callback: function(){
				var $news = $("<div/>")
					.css({
						"padding": "0 15px",
						"color": "#000"
					})
					.appendTo($cnt.find(".news-content").ajaxLoaderText(false));
										
				var items = this.length;
				
				if (!items) {
					$news.html("<h3>"+$.LL("No News")+"</h3>");
				}
				else {
					$news.css({
						"position": "relative",
						"top": $cnt.height()
					}).parent().hover(function(){
						$(this).css({
							"background-color": "rgba(255,255,255,0.60)",
							"cursor": "pointer"
						});
					}, function(){
						$(this).css({
							"background-color": "rgba(255,255,255, 0.85)",
							"cursor": "default"
						});
					});
					
					var scrollNews = function(idx){
						var top = parseInt($news.css("top"));
						var h = (idx===1) ? $cnt.height() : $news.find(".news-item").eq(idx-2).height() + 12;
						$news.animate({
							"top": top - Math.max(h, $cnt.height())
						},
						{
							duration: h*20, 
							easing: "swing",
							complete: function() {
								$.async(function(){
									if ($("body").hasClass("fancybox-lock") || $("body").hasClass("overlay")) {
										return;
									}
									if (idx === items) {
										$news.animate({top: top - h - $cnt.height()}, 
											$cnt.find(".news-item:last").height()*25, 
											function(){
												$news.css("top", $cnt.height());
												scrollNews(1);
											})
									}
									else {
										scrollNews(idx+1);
									}
								},5000);
							}
						});
					}
										
					$.each(this, function(idx, $obj){
						$obj.info("ShortDescription", function(){
							$cnt.ajaxLoaderText(false);
							$news.append($("<div/>").addClass("news-item").css({
								"min-height": $cnt.height()
							}).append($("<div/>").html($obj.attr("Name", null, "plaintext").text()).css({
									"font-weight": "bold",
									"font-size": "10pt",
									"margin-top": "12px",
									"margin-bottom": "2px"
							})).on($.browser.click, function(){
								$obj.HyperLink().click();
							}).append("<hr/>").append($obj.attr("ShortDescription").html()));

							if (!idx) {
								$.async(function() {
									scrollNews(1);
								});
							}
						});
					});
				}
			}
		});
	}, 1000);	
	
	$cnt.find(".read-more").remove();
}

$.fn.ppTweetsContent = function() {
	if (!$(this).length) {
		return $(this);
	}
	
	var $opts = {
		title: "Enterprise Architecture Tweets",
		icon: "twitter"
	}
	
	if (window["UseQualiWareTweets"] == 1) {
		$opts.parms = {
			widgetID: "795961420164857856",
			hashtag: "@QualiWareApS"
		};
	}
	else {
		$opts.parms = {
			widgetID: "694180550706401280",
			hashtag: "entarch"
		};
	}
	
	if (arguments[0]) {
		$.extend(true, $opts, arguments[0]);
	}
	
	var $cnt = $(this).wrapInner("<div/>")
		.children().addClass("tile-container");
			
	if ($QepSettings.Theme.Css.match(/default/i)) {
		$cnt.css("background-color","rgba(85, 171, 237, 1)");
	}
			
	var $tmp = $("<div>/")
		.appendTo($cnt)
		.addClass("tile")
		.append($("<span/>").addClass("qep-icon-48-" + $opts.icon))
		.append($("<h2/>").addClass("center white")
			.html($opts.title))
		.append($("<div/>").addClass("twitter-content")
			.css({
				"width": "100%",
				"height": "69%",
				"padding": "0 0 0 2px",
				"position": "absolute",
				"background-color": "rgba(255,255,255, 0.85)",
				"bottom": 0,
				"left": 0
			}));
			
	$.async(function(){
		if ($("#twitter-wjs").length) {
			$("#twitter-wjs").remove();
		}
		var $feed = $("<a/>").ajaxLoaderText()
			.addClass("twitter-timeline")
			.attr("href", "https://twitter.com/hashtag/" + $opts.parms.hashtag)
			.attr("data-widget-id", $opts.parms.widgetID)
			.attr("data-chrome", "noheader nofooter noborders transparent")
			.attr("width", $cnt.width() - 2)
			.attr("height", $cnt.height()*0.69 + 2)
			.css({
				"display": "block",
				"margin": "10px 5px",
				"color":"#666"
			})
			.appendTo($cnt.find(".twitter-content"));
			
		$(".twitter-content").append("<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+\"://platform.twitter.com/widgets.js\";fjs.parentNode.insertBefore(js,fjs);}}(document,\"script\",\"twitter-wjs\");</script>");
		
		var timer = -1;
		$tmp.onchange(function(){
			$.async(false, timer);
			timer = $.async(function(){
				var $ifrm = $cnt.find("iframe");
				 try { 
					if (!$ifrm.length || !$ifrm.contents()) {
						return;
					} 
				 }
				 catch(e) { 
					return; 
				}
				$.async(false, timer);
				$ifrm.contents()
					.find("p").css("font-size", "9pt").end()
					.find(".timeline-Tweet").css("padding-bottom", 0)
						.find(".timeline-Tweet-text").css({
							"font-size": "9pt",
							"margin-bottom": "4px",
							"font-family": '"Helvetica Neue", Roboto, "Segoe UI", Calibri, sans-serif'
						}).end();
			},150);
			
		});
	}, 500);
	
	$cnt.find(".read-more").remove();
}

$.fn.ppFeedsContent = function() {
	if (!$(this).length) {
		return $(this);
	}
	
	var $opts = {
		title: "Enterprise Architecture Voices",
		icon: "megaphone",
		parms: {
			url: location.protocol + "//eavoices.com/?json=get_recent_posts",
			dataType: "jsonp"
		},
		content: {
			Author: "author.name",
			Date: "date",
			Text: "excerpt",
			Title: "title",
			Link: "url"
		}
	};
	
	if (arguments[0]) {
		$.extend(true, $opts, arguments[0]);
	}
	
	var $cnt = $(this).wrapInner("<div/>")
		.children().addClass("tile-container");
			
	if ($QepSettings.Theme.Css.match(/default/i)) {
		$cnt.css("background-color", "rgba(230, 52, 32, 1)")
	}
			
	var $tmp = $("<div>/")
		.appendTo($cnt)
		.addClass("tile")
		.append($("<span/>").addClass("qep-icon-48-" + $opts.icon))
		.append($("<h2/>").addClass("center white")
			.html($opts.title))
		.append($("<div/>").addClass("feed-content")
			.css({
				"width": "100%",
				"height": "69%",
				"overflow-y": "auto",
				"position": "absolute",
				"background-color": "rgba(255,255,255, 0.85)",
				"bottom": 0,
				"left": 0
			}));	
			
	var getFeedProperty = function(){
		if (!arguments[0]) {
			return "";
		}
		
		var prop = arguments[0];
		var $val = this;
		$.each(prop.split("."), function(){
			$val = $val[this];
		});
		
		return $val;
	};
			
	$.async(function(){
		var $link = $("<a/>").ajaxLoaderText()
			.addClass("feeds")
			.attr("href", $opts.parms.url)
			.attr("target", "_blank")
			.css({
				"display": "block",
				"margin": "10px 5px",
				"color":"#666"
			})
			.appendTo($cnt.find(".feed-content"));
		
		$.ajax({
			url: $opts.parms.url,
			dataType: $opts.parms.dataType
		}).done(function($feed){
			if (location.protocol.match(/https:/i)) {
				var str = window.JSON.stringify($feed);
				str = str.replace(/http:\/\//ig, "https://");
				$feed = $.parseJSON(str);
			}
			
			var $div = $("<div/>").addClass("tile-data")
				.appendTo($cnt.find(".feed-content").empty())
				
			if (!$feed.count) {
				$div.html($.LL("No content")).css("padding", "4px");
				return;
			}
			
			$.each($feed.posts, function(){
				var $item = $("<div/>")
					.css({
						"margin": "5px 0px 15px 2px",
						"padding": "4px",
						"color": "#000"
					})
					.appendTo($div);
					
				$item.append("<div class=\"feed-title\">" + getFeedProperty.call(this, $opts.content.Title) + "</div><hr/>");
				
				if ($opts.content.Date && $opts.content.Author) {
					$item.append("<div class=\"feed-audit\">Posted on " + getFeedProperty.call(this, $opts.content.Date) + " by " + getFeedProperty.call(this, $opts.content.Author) + "</div>");
				}
				
				if ($opts.content.Text) {
					$item.append("<div class=\"feed-text\">" + getFeedProperty.call(this, $opts.content.Text) + "</div>");
				}
				
				var url = getFeedProperty.call(this, $opts.content.Link);
				if (url) {
					$item.on("click", function(){
						window.open(url, "qualiware feed");
					}).find("a").attr("href", "#").on("click", function(e){
						e.preventDefault();
					});
				}
			});
			
			$div.css({
				"font": "normal normal normal 9pt/16px \"Helvetica Neue\", Roboto, \"Segoe UI\", Calibri, sans-serif",
				"width": $cnt.find(".feed-content").width() - 15,
				"padding-top": "3px"
			}).children().hover(function(){
				$(this).css({
					"background-color": "rgba(100,100,100,0.25)",
					"cursor": "pointer"
				});
			}, function(){
				$(this).css({
					"background-color": "transparent",
					"cursor": "default"
				});
			}).end()
			.find(".feed-title").css({
				"font-weight": "bold",
				"font-size": "10pt",
				"margin-bottom": "-3px"
			}).end()
			.find(".feed-audit").css({
				"font-size": "8pt",
				"font-weight": "bold",
				"margin-bottom": "3px",
				"margin-top": "-5px"
			}).end()
			.find(".feed-text").css({
			}).find("a").css("color", "#666").end()
			.find("img").each(function(){
				$(this).css({
					"max-width": $cnt.find(".feed-content").width()-28
				}).parent("figure").css("margin", 0);
			}).end();
		})
	}, 500);
	
	$cnt.find(".read-more").remove();
}

$.fn.ppCustomContent = function() {
	if (!$(this).length) {
		return $(this);
	}
	
	var $opts = {
		title: "Custom",
		icon: "megaphone",
		content: ""
	};
	
	if (arguments[0]) {
		$.extend(true, $opts, arguments[0]);
	}
	
	var $cnt = $(this).wrapInner("<div/>")
		.children().addClass("tile-container");
			
	if ($QepSettings.Theme.Css.match(/default/i)) {
		$cnt.css("background-color", "rgba(230, 52, 32, 1)")
	}
			
	var $tmp = $("<div>/")
		.appendTo($cnt)
		.addClass("tile")
		.append($("<span/>").addClass("qep-icon-48-" + $opts.icon))
		.append($("<h3/>").addClass("center white")
			.html($opts.title))
		.append($("<div/>").addClass("custom-content")
			.css({
				"width": "calc(100% - 20px)",
				"height": "0",
				"position": "absolute",
				"background-color": "rgba(255,255,255, 0.85)",
				"color": "#000",
				"padding": "6px 10px",
				"bottom": 0,
				"left": 0
			})
			.html($opts.content));
	
	var height = $cnt.height() - $cnt.find(".tile").outerHeight() - 83;
	$cnt.find(".custom-content").height(Math.min(height, $cnt.height()*0.66));
	
	$cnt.find(".read-more").remove();
}

$.fn.ppSearchTile = function(){
	var $tile = $(this);
	var $opts = arguments[0];
	
	if (!$opts) {
		return $tile;
	}
	
	var $icon =  $tile.find(".tile").children().first();
	
	$tile.find(".read-more").remove();

	var $cnt = $("<div/>").addClass("tile-search")
		.appendTo($tile);
					
	$cnt.css({
		"width": "100%",
		"height": "69%",
		"overflow-y": "auto",
		"position": "absolute",
		"bottom": 0,
		"left": 0
	});	
	
	if (!$opts.parms) {
		return $tile;
	}
		
	var wsurl = initQisCsharpMethod.call($$,"findobjects", null);
	var idx = 0;
	$.each($opts.parms, function() {
		var $parm = this;
		var $item = $("<div/>").addClass("tile-search-parm")
			.css({
				"margin": "10px auto",
				"width": "75%",
				"padding": "8px 20px 7px 20px",
				"color": "#000"
			})
			.appendTo($cnt);
		
		var uid = "search-parm-" + (++idx);
		$item.append($("<label/>").attr("for", uid).html($parm.Name))
			.append($("<div/>").addClass("autocomplete")
				.append($("<input/>").attr("type", "text").attr("name", uid).attr("id", uid))
				.append($("<input/>").attr("type", "text").attr("name", uid).attr("id", uid + "-x")));
		
		var templates = [], filters = [];
		
		$.each($parm.Value.split(/,|;/), function(idx, templ) {
			if (templ.indexOf("[") > -1 && templ.indexOf("]") > 0) {
				var filt = (templ.match(/\[.+?\]/g) || []).map(function(str) { return str.slice(1,-1) });
				if (filt.length) {
					$.each(filt, function(i, condition) {
						if (!condition.match("=")) {
							return;
						}
						var val = condition.split("=");
						if (val.key == "Name") {
							return;
						}
						filters.push({
							key: val[0],
							value: val[1] 
						});
					});
				}
				templ = templ.split("[")[0];
			}
			if (templ) {
				templates.push(templ);
			}
		});
		
		var timer = -1;
		$item.find("input:first").autocomplete($.extend(true, {
			minChars: 3,
			showNoSuggestionNotice: true,
			noSuggestionNotice: $.LL("No matching results"),
			orientation: "auto",
			lookup: function (query, callback) {
				$.async(false, timer);
				timer = $.async(function() {
					var query = $item.find("input:first").val(); 
					if (!query) {
						$icon.removeClass("orbit");
						return;
					}
					
					$icon.addClass("orbit");
					
					var $parms = {
						templates: templates,
						filter: filters.concat({key: "Name", value: query}),
						filterType: "And",
						detectMetamodelFormat: false,
						order: [{
							key: "Name", 
							value: "Ascending"
						}],
						top: 20,
						skip: 0
					};
					
					$.ajax({
						type: 'POST',
						url: wsurl,
						data: window.JSON.stringify($parms)
					})
					.done(function(data){
						var $data = $.parseJSON(data);

						var temp = {};
						$.each($parms.templates, function(){
							temp[this] = [];
						});
						
						$.each($data, function(){
							try {
								this.TemplateVisualName = $.LL(this.Template);
								var $obj = {value: this.Name, data: this};
								if (!$parms.templates.length && !temp.hasOwnProperty(this.Template)) {
									var $tsets = $TemplateSettings[this.Template] || {TemplateSettings:[]};
									if ($tsets.TemplateSettings.length) {
										temp[this.Template] = [];
									}
									else {
										return;
									}
								}
								temp[this.Template].push($obj);
							}
							catch(e) { }
						});
						
						var result = {suggestions:[]};
						$.each(Object.keys(temp).sort(), function(idx, key){
							var value = temp[key];
							result.suggestions = result.suggestions.concat(value);
						});
						
						callback.call($data, result);
						$icon.removeClass("orbit");
					})
					.fail(function() {
						callback.call([], {suggestions:[]});
						$icon.removeClass("orbit");
						$.browser.error("Quick Search Errror: " + arguments[0]);
					})
				}, 75);
			},
			onSelect: function(suggestion) {
				$.loader();
				$.async(function(){
					var $obj = new RepositoryObject(suggestion.data);
					var $dom = $obj.HyperLink();
					$.async(function() {
						if ($dom.data("initialized")===false && !$dom.data("target")) {
							return;
						}
						$.async(false, this);
						$.loader(false);
						$dom.trigger($.browser.click);
					}, 100);
				}, 250);
			},
			onHint: function (hint) {
				$item.find("input:last").val(hint);
			}
		}, templates.length!=1 ? {groupBy: "TemplateVisualName"}:{}));
		
		$item.find("label").css({
			"display": "block",
			"font-size": "10pt",
			"font-weight": "bold",
			"color": "#fff",
			"margin-bottom": "5px"
		}).end()
		.find("div").css({
			"height": "20px",
			"width": "100%",
			"position": "relative"
		})
		.find("input:first").css({
			"height": "20px",
			"width": "100%",
			"position": "absolute",
			"z-index": 2,
			"background": "transparent"
		}).end()
		.find("input:last").attr("disabled", "disabled").css({
			"height": "20px",
			"color": "#999",
			"width": "100%",
			"position": "absolute",
			"z-index": 1
		});
	});
			
	return $tile;
}

$.fn.staticTileAnimation = function(){
	var $tile = $(this);
	var $opts = arguments[0];
	var $olist = arguments[1];
	
	if (!$opts) {
		return $tile;
	}
	
	$tile.find(".tile-indicator").css("color", "#000");
	$.async(function(){
		$tile.find(".tile-indicator").fadeOut(700, function() {
			$tile.find(".tile-indicator").remove();
		});
	}, 1000);
	
	$tile.find(".read-more").remove();
	
	var $cnt = $("<div/>").addClass("tile-data tile-anim")
		.appendTo($tile.find(".tile-container"));
					
	$cnt.css({
		"color": "#000",	
		"width": "100%",
		"height": "69%",
		"overflow-y": "auto",
		"overflow-x": "hidden",
		"position": "absolute",
		"background-color": "rgba(255,255,255, 0.85)",
		"bottom": 0,
		"left": 0
	});	
	
	if (!$olist.length) {
		$cnt.html($.LL("No content")).css("padding", "4px");
		return $tile;
	}
	
	$.each($olist, function() {
		var $obj = this;

		var $item = $("<div/>").addClass("tile-anim")
			.css({
				"margin": "2px 2px 15px 2px",
				"padding": "4px 6px",
				"color": "#000",
				"padding-top": "3px"
			})
			.ajaxLoaderText()
			.appendTo($cnt);
			
		if ($opts.animAttr) {
			$obj.info($opts.animAttr, function() {
				$item.html($obj.attr($opts.animAttr));
			}); 
		}
		else if ($opts.animCustomContent) {
			var $link = $obj.HyperLink();
			$obj.init(function(){
				$item.html($obj.attr($opts.animCustomContent)
					.find("a").disableHyperLink().end())
					.on("click", function(e){
						$link.openTarget(e);
					})
					.hover(function(){
						$(this).css({
							"background-color": "rgba(100,100,100,0.25)",
							"cursor": "pointer"
						});
					}, function(){
						$(this).css({
							"background-color": "transparent",
							"cursor": "default"
						});
					})
					.find("h3").css({
						"font-weight": "bold",
						"font-size": "10pt",
						"margin": 0
					}).end()
					.find("h5").css({
						"font-size": "8pt",
						"font-weight": "bold",
						"margin": "-3px 0 5px 0"
					}).end()
			});
		}
	});
			
	return $tile;
}

$.fn.defaultTileAnimation = function(){
	if ($("body").hasClass("fancybox-lock") || $("body").hasClass("overlay")) {
		return;
	}
	
	var $t = $(this);
	var type = arguments[1];
	
	switch (type) {
		case "counter":
			$t.animationCounter.apply($t, arguments);
			break;
			
		case "content":
			$t.animationContent.apply($t, arguments);
			break;
			
		default: break;
	}
	
	return $t;
}

$.fn.animationCounter = function(){
	if (!arguments[0]) {
		return $(this);
	}
	
	var $t = $(this);
	var $o = arguments[0];
	var callback = arguments[2];
	
	var clr = $t.css("background-color");
	
	var flipped = $t.hasClass("flipped");
	
	if (flipped) {
		nm = $t.data("flipped")
	}
	else {
		var s = String($t.parents(".brick").attr("class").split("size")[1].split(" ")[0]);
		nm = (s.substring(0,1)>s.substring(1,2)) ? 1 : 0;
		$t.data("flipped", nm);
	}
	
	var dFormat = ["rl", "tb"];
	var iFormat = ["lr", "bt"];

	var $s = $t.children(".tile").children("span");
	
	$t.toggleClass("flipped").flip({
		direction: flipped ? iFormat[nm] : dFormat[nm],
		color: clr,
		onBefore: function(){},
		onAnimation: function(){
			if (typeof callback == "function") {
				callback.call($t, $o, flipped);
				return;
			}

			$s.html(flipped ? "" : $o.data("count"))
				.toggleClass("value");
		},
		onEnd: function(){}
	});
	
	return $t;
}

$.fn.animationContent = function(){
	if (!arguments[0]) {
		return $(this);
	}
	
	var $t = $(this);
	var $o = arguments[0];
	
	var objlist = arguments[2];
	var attribute = arguments[3]
	var callback = arguments[4];
	
	var flipped = $(this).hasClass("flipped");
	
	var dy = flipped ? 24 : -55;
	var $s = $t.toggleClass("flipped").children(".tile")
	
	var idx = ($s.data("index") || 0) % objlist.length;
	
	$s.animate({
		"margin-top": dy
	},{
		duration: 1000,
		easing: "easeOutCubic",
		complete: function() {
			if (typeof callback == "function") {
				callback.call($t, $o, flipped, objlist, attribute);
				return;
			}
			if (!flipped) {
				var $d = $("<div/>").addClass("value").appendTo($s);
				var $obj = objlist[idx];
				
				if (typeof attribute == "function"){
					$d.append(attribute.call($obj));
				}
				else {
					$d.append($obj.attr(attribute));
				}
				
				$d.append($obj.HyperLink());
				$s.data("index", idx + 1);
			}
			else {
				$s.find(".value").fadeOut(function(){$(this).remove()});
			}
		}
	});
	
	return $o;
}