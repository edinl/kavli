﻿using System;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Data;
using System.Diagnostics;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using QCommon.Extensions;
using QCommon.Log;
using Qef.Common;
using Qef.Common.Log;
using Qef.Common.Mail;
using Qef.Common.Users;
using Qis.Common;
using Qis.Common.AttributeValues;
using Qis.Common.Metamodel;
using Qis.Common.Scripting;
using Qis.Common.Qsql;
using Qis.Common.Qsql.SelectQuery;
using Qis.Common.Qsql.Operations.Logical;
using Qis.Common.Qsql.Operations;
using Qis.Module.Scripts.GovernanceMethods.Boolean;
using Qis.Module.Scripts.GovernanceMethods.String;
using Qis.Module.Scripts.GovernanceMethods.TableValued;
using Qis.Module.Scripts.GovernanceMethods;
using QCommon.Utils;
using System.Net.Mail;
using System.Web;

namespace Qis.Module.Scripts
{
    public static class GweDynamicCodeExtension
    {
        public class QEPConverterCallBack : IConvertLinksCallback
        {
            #region Fields

            private readonly IConfiguration m_configuration;

            #endregion

            #region Constructors & Destructors

            public QEPConverterCallBack(IConfiguration configuration)
            {
                m_configuration = configuration;
            }

            #endregion

            #region Public Methods

            public void Convert(QHyperLink link, out string replaceWith)
            {
                #region Argument Check

                link.EnsureNotNull("link");

                #endregion

                RepositoryObject linkItem = m_configuration.FindObject(link);
                replaceWith = QEP.CreateHtmlLink(linkItem).ToStringSafely();
            }

            #endregion
        }

        public static string ResolveExpression(this MessageBlock message, string expression)
        {
            string expressionResult = expression;
            string internalAttributeName = string.Empty;

            string[] arrayCheckShift = expression.Split(new string[] { "}+" }, StringSplitOptions.RemoveEmptyEntries);
            string[] arrayShift = expression.Split(new string[] { "+" }, StringSplitOptions.RemoveEmptyEntries);
            int integerShift = 0;
            if (arrayCheckShift.Count() > 1 && !string.IsNullOrEmpty(arrayShift[1]))
            {
                if (int.TryParse(arrayShift[1], out integerShift))
                {
                    expression = arrayShift[0];
                }
            }

            bool resultIsDate = false;
            var dateResult = DateTime.UtcNow;

            int startPosition = expression.IndexOf("{") + 1;
            if (startPosition >= 1)
            {
                int endPosition = expression.IndexOf("}");
                int expressionSize = (endPosition - startPosition);
                if (expressionSize == 0 && startPosition == 1)
                {
                    return string.Empty;
                }
                else
                {
                    if (expressionSize > 0)
                    {
                        internalAttributeName = expression.Substring(startPosition, expressionSize);

                        switch (internalAttributeName)
                        {
                            case "Date":
                                resultIsDate = true ;
                                break;
                            case "GovernanceDateLimitOnState":
                                dateResult = message.AfterDateLimitOnState.ToUniversalTime();
                                resultIsDate = true;
                                break;
                            case "GovernanceOnStatusSince":
                                dateResult = message.OnStatusSince;
                                resultIsDate = true;
                                break;
                            case "GovernanceWarningDate":
                                if (message.WarningDate != null)
                                {
                                    dateResult = ((DateTime) message.WarningDate).ToUniversalTime();
                                }
                                resultIsDate = true;
                                break;
                            case "GovernanceLimitDate":
                                if (message.LimitDate != null)
                                {
                                    dateResult = ((DateTime)message.LimitDate).ToUniversalTime();
                                }
                                resultIsDate = true;
                                break;
                            case "GovernanceExtendedDate":
                                if (message.ExtendedDate != null)
                                {
                                    dateResult = ((DateTime)message.ExtendedDate).ToUniversalTime();
                                }
                                resultIsDate = true;
                                break;
                            case "GovernanceMessageComment":
                                expressionResult = string.Format("{0}", message.MessageComment);
                                break;
                            case "GovernanceCycleOwner":
                                expressionResult = string.Format("{0}", message.CycleOwner.Id.Id);
                                break;
                            case "GovernanceRecyclingCount":
                                expressionResult = string.Format("{0}", message.RecyclingCount);
                                break;
                            case "GovernanceCycleCount":
                                expressionResult = string.Format("{0}", message.CycleCount);
                                break;
                            case "GovernanceNextApprover":
                                GweStateMachine.GetFutureApprovers(message)
                                    .Select(x => x.Id)
                                    .DoForEach(x => expressionResult += string.Format("{0}{1}", expressionResult.IsBlank() ? string.Empty : "; ", x));
                                break;
                            default:
                                if (message.GweObject.Attributes.Contains(internalAttributeName))
                                {

                                    var attributevalue = message.GweObject.Attributes[internalAttributeName].Value;

                                    if (attributevalue.Format == AttributeFormat.DateTime)
                                    {
                                        resultIsDate = true;
                                        if (!((Qis.Common.AttributeValues.QDateTime)attributevalue).IsEmpty)
                                        {
                                            resultIsDate = true;
                                            dateResult = (DateTime)attributevalue.ToDateTime(TimeZoneInfo.Utc);
                                        }
                                        else
                                        {
                                            expressionResult = message.GweObject.GweValueAsString(internalAttributeName);
                                        }
                                    }
                                    else
                                    {
                                        expressionResult = message.GweObject.GweValueAsString(internalAttributeName);
                                    }
                                }
                                else
                                {
                                    var repository = message.CurrentContext.QisServer.FindRepository(message.GweObject.RepositoryId);

                                    var templateMetamodelInfo = repository.Metamodel.FindTemplateDirect(message.GweObject.Template);

                                    if (templateMetamodelInfo.Attributes.Contains(internalAttributeName))
                                    {
                                        expressionResult = string.Empty;
                                    }
                                }
                                break;
                        }
                    }
                }
            }
            if (resultIsDate)
            {
                if(integerShift>0)
                {
                    dateResult = dateResult.AddDays(integerShift);
                }
                IAttributeValue currentDate = new QDateTime(dateResult);
                expressionResult = currentDate.AsPlainText();
            }
            return expressionResult;
        }

        public static string GweValueAsString(this RepositoryObject repositoryObject, string attributeName)
        {
            string result = null;
            var attributevalue = repositoryObject.Attributes[attributeName].Value;
            if (attributevalue.Format == AttributeFormat.SingleLink || attributevalue.Format == AttributeFormat.MultiLink)
            {
                QHyperLinkList allLinks = attributevalue.GetHyperLinks();
                if (allLinks != null && allLinks.Count > 0)
                {
                    foreach (QHyperLink link in allLinks)
                    {
                        result = result + ((result == null) ? "" : ";") + link.ObjectId.Id;
                    }
                }
            }
            else
                if (attributevalue.Format == AttributeFormat.PlainText)
                result = attributevalue.AsPlainText();
            else if (attributevalue.Format == AttributeFormat.Xhtml)
                result = attributevalue.AsPlainText();
            else if (attributevalue.Format == AttributeFormat.DateTime)
                result = attributevalue.AsPlainText();
            else if (attributevalue.Format == AttributeFormat.Unknown)
                result = attributevalue.AsPlainText();
            return result;
        }

        public static DateTime? GetDateTimeSafely(this RepositoryObject repositoryObject, string attributeName)
        {
            DateTime? result = null;
            var attributevalue = repositoryObject.Attributes[attributeName].Value;
            if (attributevalue.Format == AttributeFormat.DateTime)
            {
                result = attributevalue.ToDateTime(TimeZoneInfo.Utc);
            }

            return result;
        }

        public static bool MetamodelAttributesContains(this RepositoryObject obj, string attrName)
        {
            if (obj != null)
            {
                var qisServer = GweDynamicCode.GweCallContextQisServer();
                var repository = qisServer.FindRepository(obj.RepositoryId);
                var templateMetamodelInfo = repository.Metamodel.FindTemplateDirect(obj.Template); //var templateMetamodelInfo = repository.Metamodel.GetTemplateDefinition(obj.Template);
                if (templateMetamodelInfo != null)
                {
                    return templateMetamodelInfo.Attributes.Contains(attrName);
                }
            }

            return false;
        }

        public static string AttributePlainValueConvertLinksSafely(this RepositoryObject repositoryobject, string attributeName)
        {
            if (repositoryobject == null || attributeName.IsBlank())
            {
                return string.Empty;
            }

            if (StringComparer.OrdinalIgnoreCase.Equals(attributeName, "Name"))
            {
                return repositoryobject.Name;
            }

            if (StringComparer.OrdinalIgnoreCase.Equals(attributeName, "Template"))
            {
                return repositoryobject.Template;
            }

            if (StringComparer.OrdinalIgnoreCase.Equals(attributeName, "ObjId"))
            {
                return repositoryobject.Id.Id.ToStringSafely();
            }

            if (StringComparer.OrdinalIgnoreCase.Equals(attributeName, "RevId"))
            {
                return repositoryobject.Id.Revision.ToStringSafely();
            }

            var attributeValue = ObjectScripts.GetAttributeValue(repositoryobject, attributeName, true);

            if (attributeValue.IsEmpty)
            {
                return string.Empty;
            }

            var configuration = repositoryobject.GetConfiguration();
            var callBack = new QEPConverterCallBack(configuration);
            return attributeValue.ConvertLinks(callBack);
        }

        public static bool IsGweObject(this RepositoryObject revision)
        {
            if (revision == null)
            {
                return false;
            }

            var configurationCtx = ConfigurationContext.Create(revision);
            var logWriter = configurationCtx.QisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));

            try
            {
                var gweAdoUtility = new GweAdoUtility(configurationCtx);
                string status = gweAdoUtility.GetStatusOfObject(revision, false);
                return (status != "Not Defined" && status != "") ? true : false;
            }
            catch (Exception ex)
            {
                logWriter.Error(ex.ToString());
                return false;
            }
        }

        public static bool GweCompareTo(this string stringA, string stringB)
        {
            bool result = false;
            if (string.IsNullOrEmpty(stringA))
            {
                if (string.IsNullOrEmpty(stringB))
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            else
            {
                if (String.Equals(stringA, "{}", StringComparison.OrdinalIgnoreCase))
                {
                    if (string.IsNullOrEmpty(stringB))
                        result = true;
                }
                else
                {
                    if (String.Equals(stringA, "!{}", StringComparison.OrdinalIgnoreCase))
                    {
                        if (!string.IsNullOrEmpty(stringB))
                            result = true;
                    }
                    else
                    {
                        result = String.Equals(stringA, stringB, StringComparison.OrdinalIgnoreCase);
                    }
                }
            }
            return result;
        }

        public static void AppendQCLInfo(this StringBuilder fullCommand, string attributeName, int? intAttr)
        {
            string stringIntAttr = string.Format("{0}", intAttr);
            fullCommand.AppendQCLInfo(attributeName, stringIntAttr);
            return;
        }

        public static void AppendQCLInfo(this StringBuilder fullCommand, string attributeName, Guid guidAttr)
        {
            string stringIntAttr = string.Format("{0}", guidAttr);
            fullCommand.AppendQCLInfo(attributeName, stringIntAttr);
            return;
        }

        public static void AppendQCLInfo(this StringBuilder fullCommand, string attributeName, string stringAttr)
        {
            const string stringToAppend = "{0} = \"{1}\";\n";
            fullCommand.AppendFormat(stringToAppend, attributeName, stringAttr);
            return;
        }

        public static void AppendQCLInfo(this StringBuilder fullCommand, string attributeName, bool booleanAttr)
        {
            const string stringToAppend = "{0} = \"{1}\";\n";
            fullCommand.AppendFormat(stringToAppend, attributeName, booleanAttr ? "1" : "0");
            return;
        }

        public static void AppendQCLInfo(this StringBuilder fullCommand, string attributeName, DateTime? dateTime)
        {
            const string stringToAppend = "{0} = \"{1}\";\n";
            if (dateTime == null)
            {
                fullCommand.AppendFormat(stringToAppend, attributeName, "");
            }
            else
            {
                DateTime dateTimeNotNull = (DateTime)dateTime;
                fullCommand.AppendFormat(stringToAppend, attributeName, dateTimeNotNull.ToString(QisConst.DateTimeSortableFormat));
            }

            return;
        }

        public static void AppendQCLInfo(this StringBuilder fullCommand, string attributeName, Oid revisionId)
        {
            const string stringToAppend = "{0} = ObjID2Entity(\"{1}\");\n";

            if (revisionId != null)
            {
                fullCommand.AppendFormat(stringToAppend, attributeName, revisionId.Id);
            }

            return;
        }

        public static void AppendQCLInfoWithRev(this StringBuilder fullCommand, string attributeName, Oid revisionId)
        {
            const string stringToAppend = "{0} = {0}->FndToggleToRevision(\"{1}\");\n";
            fullCommand.AppendQCLInfo(attributeName, revisionId);
            fullCommand.AppendFormat(stringToAppend, attributeName, revisionId.Revision);
            return;
        }

        public static string QCLInfoWithRev(this string command, string attributeName, Oid revisionId)
        {
            const string stringToAppend = "local {0} = ObjID2Entity(\"{1}\");\n{0} = {0}->FndToggleToRevision(\"{2}\");\n";
            return string.Format(stringToAppend + command, attributeName, revisionId.Id, revisionId.Revision);
        }

        public static void GovernanceCounterUpdate(this DataTable dataTable, RepositoryObject repositoryObject)
        {
            var configurationCtx = ConfigurationContext.Create(repositoryObject);
            var gweStateMachine = new GweStateMachine(configurationCtx, repositoryObject);
            if (gweStateMachine.CurrentState == null)
            {
                return;
            }

            var adoUtility = new GweAdoUtility(configurationCtx);
            var revisionStartDate = adoUtility.GetRevisionStartDate(repositoryObject);
            if (revisionStartDate == null)
            {
                return;
            }

            DateTime revisionStartDateNN = revisionStartDate.Value;
            foreach (DataRow periodRow in dataTable.Select(string.Format("Year = '{0}' and Month = '{1}' ", revisionStartDateNN.Year, revisionStartDateNN.Month)))
            {
                periodRow["Registered"] = (int)periodRow["Registered"] + 1;
            }

            if (gweStateMachine.CurrentState.StateType == "End")
            {
                var revisionEndDate = gweStateMachine.StatusDataSet.Tables[0].Rows[0]["OnStatusSince"];
                if (revisionEndDate == null)
                {
                    return;
                }

                DateTime revisionEndDateNN = (DateTime)revisionEndDate;
                foreach (DataRow periodRow in dataTable.Select(string.Format("Year = '{0}' and Month = '{1}' ", revisionEndDateNN.Year, revisionEndDateNN.Month)))
                {
                    periodRow["Finished"] = (int)periodRow["Finished"] + 1;
                }

                foreach (DataRow periodRow in dataTable.Select(string.Format("((Year = '{0}' and Month >= '{1}') or (Year > '{0}')) AND (NOT((Year = '{2}' and Month >= '{3}') or (Year > '{2}')))  ",
                    revisionStartDateNN.Year, revisionStartDateNN.Month,
                    revisionEndDateNN.Year, revisionEndDateNN.Month)))
                {
                    periodRow["Open"] = (int)periodRow["Open"] + 1;
                }

                foreach (DataRow periodRow in dataTable.Select(string.Format("(Year = '{0}' and Month >= '{1}') or (Year > '{0}')  ",
                    revisionEndDateNN.Year, revisionEndDateNN.Month)))
                {
                    Double totalTime = (int)periodRow["Close"] * (Double)periodRow["AverageTime"];
                    TimeSpan duration = revisionEndDateNN.Subtract(revisionStartDateNN.Date);
                    Double durationInDays = duration.TotalDays;
                    totalTime += durationInDays;
                    periodRow["Close"] = (int)periodRow["Close"] + 1;
                    periodRow["AverageTime"] = totalTime / (int)periodRow["Close"];
                }
            }
            else
            {
                foreach (DataRow periodRow in dataTable.Select(string.Format("(Year = '{0}' and Month >= '{1}') or (Year > '{0}')  ",
                    revisionStartDateNN.Year, revisionStartDateNN.Month)))
                {
                    periodRow["Open"] = (int)periodRow["Open"] + 1;
                }
            }

            return;
        }
    }

    /// <summary>
    ///	 GweDynamicCode class.
    /// </summary>
    public partial class GweDynamicCode
    {
        #region Constants

        private const string c_validFromString = "RevisionValidFrom";
        private const string c_validToString = "RevisionValidTo";

        #endregion

        #region Nested Types

        public enum DynamicVariableType
        {
            Unknown = 0,
            String = 1,
            Integer = 2,
            OptionalInt = 3,
            DateTime = 4,
            OptionalDateTime = 5,
            RepositoryObject = 6,
            WorkFlowElement = 7
        }

        public class DynamicVariable
        {
            #region Constructors and Destructors

            private DynamicVariable(object value, DynamicVariableType type)
            {
                Value = value;
                VariableType = type;
            }

            #endregion

            #region Public Properties

            public object Value
            {
                get;
                protected set;
            }

            public DynamicVariableType VariableType
            {
                get;
                protected set;
            }

            #endregion

            #region Public Methods

            public IAttributeValue ToAttributeValue()
            {
                switch (VariableType)
                {
                    case DynamicVariableType.RepositoryObject:
                        var link = ((RepositoryObject)Value).GetRemoteRepositoryHyperlink();
                        return new SingleLink(link);

                    case DynamicVariableType.WorkFlowElement:
                        var element = (IGovernanceWorkFlowElement)Value;
                        var elementLink = new QHyperLink(new Oid(element.ElementId, RevisionId.Empty), true, element.ElementName);
                        return new SingleLink(elementLink);

                    case DynamicVariableType.DateTime:
                        return new QDateTime((DateTime)Value);
                    case DynamicVariableType.OptionalDateTime:
                        return new QDateTime((DateTime?)Value);

                    case DynamicVariableType.Integer:
                    case DynamicVariableType.OptionalInt:
                    case DynamicVariableType.String:
                        return new PlainText(Value.ToString());

                    default:
                        return new PlainText(Value.ToString());
                }
            }

            public static DynamicVariable Create(string value)
            {
                return new DynamicVariable(value.EnsureNotNull(nameof(value)), DynamicVariableType.String);
            }

            public static DynamicVariable Create(int value)
            {
                return new DynamicVariable(value, DynamicVariableType.Integer);
            }

            public static DynamicVariable Create(int? value)
            {
                return new DynamicVariable(value, DynamicVariableType.OptionalInt);
            }

            public static DynamicVariable Create(DateTime value)
            {
                return new DynamicVariable(value, DynamicVariableType.DateTime);
            }

            public static DynamicVariable Create(DateTime? value)
            {
                return new DynamicVariable(value, DynamicVariableType.OptionalDateTime);
            }

            public static DynamicVariable Create(RepositoryObject value)
            {
                return new DynamicVariable(value.EnsureNotNull(nameof(value)), DynamicVariableType.RepositoryObject);
            }

            public static DynamicVariable Create(IGovernanceWorkFlowElement value)
            {
                return new DynamicVariable(value.EnsureNotNull(nameof(value)), DynamicVariableType.WorkFlowElement);
            }

            public static DynamicVariable CreateUnknown(object value)
            {
                return new DynamicVariable(value.EnsureNotNull(nameof(value)), DynamicVariableType.Unknown);
            }

            #endregion
        }

        public class CmsApproveAttribute
        {
            #region Fields

            public bool ApproveContent
            {
                get;
                set;
            }

            public bool SendMailNotification
            {
                get;
                set;
            }

            public bool RollbackOnFailure
            {
                get;
                set;
            }

            public bool SkipGweContent
            {
                get;
                set;
            }

            public string SetApprovalState
            {
                get;
                set;
            }

            #endregion
        }

        public struct CmsPromoteAttribute
        {
            #region Fields

            public bool PromoteContent
            {
                get;
                set;
            }

            public bool SkipGweContent
            {
                get;
                set;
            }

            #endregion
        }

        private class SignatureChecker
        {
            #region Fields

            private readonly RepositoryObject m_methodSignaturerepositoryObject;
            private readonly List<string> m_checks;

            #endregion

            #region Constructors and Destructors

            public SignatureChecker(RepositoryObject methodSignaturerepositoryObject)
            {
                #region Argument Check

                methodSignaturerepositoryObject.EnsureNotNull("methodSignaturerepositoryObject");

                #endregion

                m_methodSignaturerepositoryObject = methodSignaturerepositoryObject;
                m_checks = new List<string>();
            }

            #endregion

            #region Public Methods

            public void CheckExists(string attributeName, string message, bool useAttributeAsFormat)
            {
                var attributeValue = m_methodSignaturerepositoryObject.Attributes.AttributePlainValueSafely(attributeName);
                CheckExists(attributeName, message, attributeValue);
            }

            public void CheckNotExists(string attributeName, string message, bool useAttributeAsFormat)
            {
                var attributeValue = m_methodSignaturerepositoryObject.Attributes.AttributePlainValueSafely(attributeName);
                CheckNotExists(attributeName, message, attributeValue);
            }

            public void CheckExists(string attributeName, string message, params object[] format)
            {
                var attributeValue = m_methodSignaturerepositoryObject.Attributes.AttributePlainValueSafely(attributeName);
                if (attributeValue.IsBlank())
                {
                    m_checks.Add(string.Format(message, format));
                }
            }

            public void CheckNotExists(string attributeName, string message, params object[] format)
            {
                var attributeValue = m_methodSignaturerepositoryObject.Attributes.AttributePlainValueSafely(attributeName);
                if (!attributeValue.IsBlank())
                {
                    m_checks.Add(string.Format(message, format));
                }
            }

            public void CheckNotNull(RepositoryObject repositoryObject, string message, params object[] format)
            {
                if (repositoryObject == null)
                {
                    m_checks.Add(string.Format(message, format));
                }
            }

            public void CheckSameNumber(string firstAttribute, string secondAttribute, string message)
            {
                var firstValue = m_methodSignaturerepositoryObject.Attributes.AttributePlainValueSafely(firstAttribute);
                var secondValue = m_methodSignaturerepositoryObject.Attributes.AttributePlainValueSafely(secondAttribute);

                if (!firstValue.IsBlank() && !secondValue.IsBlank())
                {
                    int firstCount = firstValue.Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries).Count();
                    int secondCount = secondValue.Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries).Count();

                    if (firstCount != secondCount)
                    {
                        m_checks.Add(string.Format(message, firstCount, secondCount));
                    }
                }
            }

            public override string ToString()
            {
                return string.Join(Environment.NewLine, m_checks.ToArray());
            }

            #endregion
        }

        private sealed class ConverterCallBack : IConvertLinksCallback
        {
            #region Fields

            private const string c_governanceTemplate = "GovernanceGetMethod";
            private const string c_replacementTemplate = "http://{0}/QEF/WebModules/WebForms/{1}/{2}.htm";

            private readonly IConfiguration m_configuration;
            private readonly GweDynamicCode m_callBackDynCode;

            #endregion

            #region Constructors & Destructors

            public ConverterCallBack(MessageBlock message)
            {
                m_configuration = message.CurrentContext.Configuration;
                m_callBackDynCode = new GweDynamicCode(message.CurrentContext, message);
            }

            #endregion

            #region Public Methods

            public void Convert(QHyperLink link, out string replaceWith)
            {
                #region Argument Check

                link.EnsureNotNull("link");

                #endregion

                RepositoryObject linkItem = m_configuration.FindObject(link);
                if (linkItem.Template == c_governanceTemplate)
                {
                    replaceWith = m_callBackDynCode.ExecuteMethodSignatureString(linkItem.Id);
                }
                else
                {
                    replaceWith = QEP.CreateHtmlLink(linkItem).ToStringSafely(); //string.Format( c_replacementTemplate, Environment.MachineName, linkItem.Template, linkItem.Id.Id);
                }
            }

            #endregion
        }

        #endregion

        #region Constants

        private const string c_dynamicCodeLogSource = "GweDynamicCode";
        private const string c_consistencyLogSource = "ConsistencyMethod";
        private const string c_resultOk = "0";
        private static HashSet<string> c_connectionTemplateList = new HashSet<string> {
            "Abstraction",
            "ActivityPath",
            "AssemblyFlow",
            "Association",
            "BrowserBackRelation",
            "BrowserGraphicRelation",
            "BrowserRelation",
            "BusinessInteraction",
            "CauseEffect",
            "ComplexRelation",
            "ComponentRealization",
            "ConceptAggregation",
            "ConceptAssociation",
            "ConceptGeneralization",
            "ConceptRelation",
            "Connection",
            "Connector",
            "Containment",
            "ControlCoverage",
            "ControlFlow",
            "ConversationLink",
            "Correspondence",
            "CriterionInfluence",
            "DataApply",
            "DataExtract",
            "DataFlow",
            "DataMapping",
            "DataRelation",
            "Dependency",
            "Deployment",
            "Derivation",
            "ElementImport",
            "ExceptionHandler",
            "Extend",
            "FirewallPolicy",
            "FunctionalityUsage",
            "Generalization",
            "GovernanceConditionalMessage",
            "GovernanceEventMessage",
            "GovernanceStateFeature",
            "GovernanceTransition",
            "GeneralizationSet",
            "Guide",
            "ImpactQuantity",
            "Include",
            "Index",
            "InferentialRelation",
            "Influence",
            "InformationFlow",
            "Inhabitance",
            "InheritanceConnection",
            "IntegrationFlow",
            "Interaction",
            "InterfaceRealization",
            "ITSupport",
            "LifelineChange",
            "LogisticalFlow",
            "Manifestation",
            "Message",
            "MessageFlow",
            "ModuleParameter",
            "NaryRelationship",
            "NetworkConnection",
            "NodeRelation",
            "ObjectAssociation",
            "ObjectDecomposition",
            "ObjectDependency",
            "ObjectFlow",
            "ObjectGeneralization",
            "ObjectLink",
            "Originator",
            "PackageImport",
            "PackageMerge",
            "Performer",
            "PhysicalForeignKey",
            "PositionHolder",
            "ProductInterface",
            "ProfileApplication",
            "Realization",
            "Recycling",
            "RelationshipConstraint",
            "RelConstraint",
            "Resource",
            "Responsibility",
            "RoleBinding",
            "SequenceFlow",
            "StateTransition",
            "StrategicAlignment",
            "Substitution",
            "SystemDependency",
            "Transition",
            "TransportSystem",
            "TypeRelationship",
            "Usage",
            "ValueFlow",
            "Wire"
        };

        #endregion

        #region Fields

        private readonly ConfigurationContext m_configurationCtx;

        private readonly MessageBlock m_message;

        public delegate RepositoryObject GweObjectDynamicFunction(GovernanceMethodData self, MessageBlock message);

        #endregion

        #region Constructors and Destructors

        public GweDynamicCode(ConfigurationContext configurationCtx, MessageBlock message)
        {
            #region Argument Check

            configurationCtx.EnsureNotNull(nameof(configurationCtx));
            message.EnsureNotNull(nameof(message));

            #endregion

            m_configurationCtx = configurationCtx;
            m_message = message;
        }

        #endregion

        #region Private Methods

        private static string ExtractStatusAttributeName(string attributeName)
        {
            string realAttributeName = string.Empty;
            char[] charsToTrim = { '{', '}' };
            switch (attributeName)
            {
                case "{GovernanceCycleCount}":
                    realAttributeName = "GWECycleCount";
                    break;
                case "{GovernanceRecyclingCount}":
                    realAttributeName = "GWERecyclingCount";
                    break;
                case "{GovernanceWarningDate}":
                    realAttributeName = "WarningDate";
                    break;
                case "{GovernanceLimitDate}":
                    realAttributeName = "LimitDate";
                    break;
                case "{GovernanceCycleOwner}":
                    realAttributeName = "GWECycleOwner";
                    break;
                case "{GovernanceDateLimitOnState}":
                    realAttributeName = "DateLimitOnState";
                    break;
                case "{GovernanceSignal}":
                    realAttributeName = "LastSignalFromGWE";
                    break;
                default:
                    realAttributeName = attributeName.Trim(charsToTrim);
                    break;
            }

            return realAttributeName;
        }

        private static string SetGweAttributeStatus(RepositoryObject repositoryObject, DataSet statusSet, string attributeName, IAttributeValue content)
        {
            string errorMessage = string.Empty;
            string contentValue = content.GetContent();
            var configuration = repositoryObject.GetConfiguration();
            try
            {
                var statusAttributeName = ExtractStatusAttributeName(attributeName);
                if (!statusAttributeName.IsBlank())
                {
                    if (contentValue.IsBlank() || contentValue == "{}")
                    {
                        statusSet.Tables[0].Rows[0][statusAttributeName] = string.Empty;
                    }
                    else if (contentValue == "{++}")
                    {
                        int integerValue = Convert.ToInt32(statusSet.Tables[0].Rows[0][statusAttributeName]);
                        statusSet.Tables[0].Rows[0][statusAttributeName] = integerValue + 1;
                    }
                    else if (contentValue == "{Date}")
                    {
                        statusSet.Tables[0].Rows[0][statusAttributeName] = DateTime.UtcNow;
                    }
                    else if (contentValue == "{UserName}")
                    {
                        var person = configuration.ResolveUserObject();
                        statusSet.Tables[0].Rows[0][statusAttributeName] = string.Format("{0}", person.Id.Id);
                    }
                    else if (content.GetType().Name == "SingleLink")
                    {
                        var link = content.GetHyperLinks().First();
                        var singleLinkItem = configuration.FindObject(link);
                        statusSet.Tables[0].Rows[0][statusAttributeName] = string.Format("{0}", singleLinkItem.Id.Id);
                    }
                    else
                    {
                        statusSet.Tables[0].Rows[0][statusAttributeName] = contentValue;
                    }
                }
                else
                {
                    errorMessage = string.Format("{0} - Unrecognized, Can not set content[{1}]", attributeName, contentValue);
                }
            }
            catch (Exception ex)
            {
                errorMessage = string.Format("{0} - {1}", attributeName, ex.ToString());
            }

            return errorMessage;
        }

        private static string SetGweAttributeQlm(RepositoryObject repositoryObject, String attributeName, IAttributeValue content, int valueShift = 0)
        {
            string contentValue = content.GetContent();
            string errorMessage = string.Empty;

            try
            {
                if (contentValue.IsBlank() || contentValue == "{}")
                {
                    repositoryObject.Attributes[attributeName].Value.Clear();
                }
                else if (contentValue == "{++}")
                {
                    int integerValue = Convert.ToInt32(repositoryObject.Attributes[attributeName].Value.AsPlainText());
                    integerValue++;
                    repositoryObject.Attributes[attributeName].Value.SetContent(integerValue.ToString());
                }
                else if (contentValue == "{Date}")
                {
                    if (valueShift > 0)
                    {
                        repositoryObject.Attributes[attributeName].Value = new QDateTime(DateTime.UtcNow + TimeSpan.FromDays(valueShift));
                    }
                    else
                    {
                        repositoryObject.Attributes[attributeName].Value = new QDateTime(DateTime.UtcNow);
                    }
                }
                else if (contentValue == "{UserName}")
                {
                    var person = repositoryObject.GetConfiguration().ResolveUserObject();
                    var personLink = person.GetRemoteRepositoryHyperlink();

                    var attributevalue = repositoryObject.Attributes[attributeName].Value;

                    if (attributevalue.Format == AttributeFormat.SingleLink)
                        attributevalue = new SingleLink(personLink);
                    else if (attributevalue.Format == AttributeFormat.MultiLink)
                        attributevalue = new MultiLink(new QHyperLinkList(new[] { personLink }));
                    else if (attributevalue.Format == AttributeFormat.PlainText)
                        attributevalue = new PlainText(person.Name);
                    else if (attributevalue.Format == AttributeFormat.Xhtml)
                        attributevalue = new XhtmlText(person.Name);
                    else
                        throw new QInvalidScriptOperationException(string.Format("Can not set CurrentUser() for attribute {0} type {1}", attributeName, attributevalue.Format));
                    repositoryObject.Attributes[attributeName].Value = attributevalue;
                }
                else
                {
                    if (valueShift > 0)
                    {
                        DateTime contentValueDateTime;
                        if (DateTime.TryParseExact(contentValue, QisConst.DateTimeSortableFormat,
                              System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.AdjustToUniversal, out contentValueDateTime))
                        {
                            contentValue = (contentValueDateTime + TimeSpan.FromDays(valueShift)).ToString(QisConst.DateTimeSortableFormat);
                            repositoryObject.Attributes[attributeName].Value.SetContent(contentValue);
                        }
                        else
                        {
                            int contentValueInt;
                            if (int.TryParse(contentValue, out contentValueInt))
                            {
                                contentValue = string.Format("{0}", contentValueInt + valueShift);
                                repositoryObject.Attributes[attributeName].Value.SetContent(contentValue);
                            }
                            else
                            {
                                repositoryObject.Attributes[attributeName].Value.SetContent(contentValue);
                            }

                        }
                    }
                    else
                    {
                        repositoryObject.Attributes[attributeName].Value.SetContent(contentValue);
                    }
                }
            }
            catch (Exception ex)
            {
                errorMessage = string.Format("Attribute name: {0}. {1}", attributeName, ex.ToString());
            }

            return errorMessage;
        }

        private static DynamicVariable GetDynamicVariable(MessageBlock message, string content)
        {
            switch (content)
            {
                case "{GovernanceAnswer}":
                    return DynamicVariable.Create(message.Answer);
                case "{GovernanceMessageComment}":
                    return DynamicVariable.Create(message.MessageComment);
                case "{GovernanceStateBeforeName}":
                    return DynamicVariable.Create(message.GovernanceStateBefore?.Name);
                case "{GovernanceStatus}":
                    return DynamicVariable.Create(message.GovernanceStateAfter?.Name);

                case "{GovernanceSLA}":
                    return DynamicVariable.Create(message.MessageSLA);
                case "{GovernancePctCompleted}":
                    return DynamicVariable.Create(message.MsgPctCompleted);
                case "{GovernanceCycleCount}":
                    return DynamicVariable.Create(message.CycleCount);
                case "{GovernanceRecyclingCount}":
                    return DynamicVariable.Create(message.RecyclingCount);

                case "{GovernanceDateLimitOnState}":
                    return DynamicVariable.Create(message.AfterDateLimitOnState);
                case "{GovernanceOnStatusSince}":
                    return DynamicVariable.Create(message.OnStatusSince);
                case "{GovernanceWarningDate}":
                    return DynamicVariable.Create(message.WarningDate);
                case "{GovernanceLimitDate}":
                    return DynamicVariable.Create(message.LimitDate);
                case "{GovernanceExtendedDate}":
                    return DynamicVariable.Create(message.ExtendedDate);

                case "{GovernanceMessageActor}":
                    return DynamicVariable.Create(message.MessageActor);
                case "{GovernanceStateBefore}":
                    return DynamicVariable.Create(message.GovernanceStateBefore);
                case "{GovernanceCurrentState}":
                    return DynamicVariable.Create(message.GovernanceStateAfter);
                case "{GovernanceCycleOwner}":
                    return DynamicVariable.Create(message.CycleOwner);

                default:
                    return DynamicVariable.CreateUnknown(content);
            }
        }

        private static CmsApproveAttribute FillApproveAttribute(
            GovernanceMethodData methodData,
            MessageBlock message,
            CmsApproveAttribute approveAttribute)
        {
            var newApproveAttribute = approveAttribute;
            var attributeNameValues = ExtractDynamicMethodParameters(methodData);
            foreach (var pair in attributeNameValues)
            {
                string attributeName = pair.Item1.Trim();
                string attributeValue = pair.Item2.Trim();
                if (!attributeName.IsBlank() && !attributeValue.IsBlank())
                {
                    switch (attributeName)
                    {
                        case "ApproveContent":
                            newApproveAttribute.ApproveContent = attributeValue.ToBoolean();
                            break;
                        case "SendMailNotification":
                            newApproveAttribute.SendMailNotification = attributeValue.ToBoolean();
                            break;
                        case "RollbackOnFailure":
                            newApproveAttribute.RollbackOnFailure = attributeValue.ToBoolean();
                            break;
                        case "SkipGweContent":
                            newApproveAttribute.SkipGweContent = attributeValue.ToBoolean();
                            break;
                        case "SetApprovalState":
                            newApproveAttribute.SetApprovalState = attributeValue;
                            break;
                        default:
                            break;
                    }
                }
            }
            return newApproveAttribute;
        }

        private static CmsPromoteAttribute FillPromoteAttribute(
            GovernanceMethodData methodData,
            MessageBlock message,
            CmsPromoteAttribute promoteAttribute)
        {
            var attributeNameValues = ExtractDynamicMethodParameters(methodData);
            var newPromoteAttribute = promoteAttribute;

            foreach (var pair in attributeNameValues)
            {
                string attributeName = pair.Item1.Trim();
                string attributeValue = pair.Item2.Trim();

                if (!attributeName.IsBlank() && !attributeValue.IsBlank())
                {
                    switch (attributeName)
                    {
                        case "PromoteContent":
                            newPromoteAttribute.PromoteContent = attributeValue.ToBoolean();
                            break;
                        case "SkipGweContent":
                            newPromoteAttribute.SkipGweContent = attributeValue.ToBoolean();
                            break;
                    }
                }
            }
            return newPromoteAttribute;
        }

        private static IEnumerable<Tuple<string, string>> ExtractDynamicMethodParameters(GovernanceMethodData methodData)
        {
            System.Xml.XmlDocument xmlAttrLine = new System.Xml.XmlDocument();
            const string nodeValue = "/root/item/Value";

            if (!methodData.KeyValueXml.IsBlank())
            {
                string stringParameterList = methodData.KeyValueXml;
                string[] arrayParameterList = stringParameterList.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < arrayParameterList.Count(); i++)
                {
                    if (!string.IsNullOrEmpty(arrayParameterList[i]))
                    {
                        xmlAttrLine.LoadXml(string.Format("<root>{0}</root>", arrayParameterList[i]));
                        System.Xml.XmlNode xmlAttrLineName = xmlAttrLine.SelectSingleNode("/root/item/Parameter");
                        if (xmlAttrLineName.IsNotNull())
                        {
                            string stringNodeEntry = xmlAttrLineName.InnerText;
                            if (!string.IsNullOrEmpty(stringNodeEntry))
                            {
                                System.Xml.XmlNode xmlAttrLineValue = xmlAttrLine.SelectSingleNode(nodeValue);
                                if (xmlAttrLineValue.IsNotNull())
                                {
                                    string stringNodeValue = xmlAttrLineValue.InnerText;
                                    if (!string.IsNullOrEmpty(stringNodeValue))
                                    {
                                        yield return new Tuple<string, string>(stringNodeEntry, stringNodeValue);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            yield break;
        }

        private static string GetDynamicMethodParameterNamed(IEnumerable<Tuple<string, string>> methodParameters, string parameterName)
        {
            if (methodParameters == null)
                return string.Empty;

            foreach (var pair in methodParameters)
            {
                string attributeName = pair.Item1.TrimStart();
                string attributeValue = pair.Item2.TrimStart();
                if (!attributeName.IsBlank() && !attributeValue.IsBlank())
                {
                    if (attributeName == parameterName)
                    {
                        return attributeValue;
                    }
                }
            }

            return string.Empty;
        }

        private static OidList GetObjectIdsToPromote(IConfiguration configuration, Oid objectId, CmsPromoteAttribute promoteAttribute)
        {
            OidList resultObjectIds = new OidList();
            OidList objectIdsToPromote = configuration.GetObjectsForPromotion();

            if (objectIdsToPromote.Contains(objectId))
            {
                resultObjectIds.Add(objectId);
            }

            if (promoteAttribute.PromoteContent)
            {
                RepositoryObjectList containedObjects = configuration.GetRepository().GetUsedObjects(objectId, configuration.Id, ObjectReferenceType.Contains);
                if (promoteAttribute.SkipGweContent)
                {
                    containedObjects = new RepositoryObjectList(containedObjects.Where(item => !item.IsGweObject()));
                }
                resultObjectIds.AddRange(
                    containedObjects
                        .Where(obj => objectIdsToPromote.Contains(obj.Id))
                        .Select(obj => obj.Id));
            }

            return resultObjectIds;
        }

        private static string VerifyQclExpression(IConfiguration configuration, string expression)
        {
            var errors = new StringBuilder();
            var qclInstanceId = QEP.GetHTMLQclInstance(configuration);

            if (qclInstanceId == null)
            {
                errors.Append("Msg21-->There is no Instance to Execute QCL, Check your HTMLProfile");
            }
            else
            {
                var expressionText = expression.Replace("\"", "\\\"");
                expressionText = "SyntaxCheck(\"" + expressionText + "\");";
                var syntaxAnswer = QclEngineExtentions.ExecuteQclCommand(configuration.GetRepository(), qclInstanceId, expressionText);

                if (syntaxAnswer.Result == "1")
                {
                    errors.Append(syntaxAnswer.Messages);
                }
            }

            return errors.ToString();
        }

        private static string VerifyMethodSignature(RepositoryObject methodSignature)
        {
            var log = new CompositeInMemoryLog();

            switch (methodSignature.Template)
            {
                case "GovernanceBoolMethod":
                    var boolMethod = MethodHelper.ReadMethod(
                        methodSignature,
                        MethodHelper.CsBoolMethodCtor,
                        MethodHelper.QclBoolMethodCtor,
                        log);

                    if (boolMethod == null)
                    {
                        return log.ToString();
                    }

                    break;

                case "GovernanceBoolList":
                    break;

                case "GovernanceActionMethod":
                case "GovernanceGetMethod":
                case "RealizedMeasurement":
                    var stringMethod = MethodHelper.ReadMethod(
                        methodSignature,
                        MethodHelper.CsStringMethodCtor,
                        MethodHelper.QclStringMethodCtor,
                        log);

                    if (stringMethod == null)
                    {
                        return log.ToString();
                    }
                    break;

                case "AnalyticDataTableMethod":
                    var tableMethod = MethodHelper.ReadMethod(
                        methodSignature,
                        MethodHelper.CsTableMethodCtor,
                        MethodHelper.QclTableMethodCtor,
                        log);

                    if (tableMethod == null)
                    {
                        return log.ToString();
                    }
                    break;
            }

            if (methodSignature.Attributes.GetInteger("DynamicByExpression") == 1)
            {
                IAttributeValue expressionValue;
                if (methodSignature.Attributes.TryGetValue("Expression", out expressionValue) && !expressionValue.IsEmpty)
                {
                    return VerifyQclExpression(
                        methodSignature.GetConfiguration(),
                        expressionValue.AsPlainText(null));
                }
            }

            return string.Empty;
        }

        private static string ExtractMessageFormat(
            GovernanceMethodData methodData,
            string defaultValue)
        {
            return !methodData.MessageFormat.IsBlank() ? methodData.MessageFormat : defaultValue;
        }

        private static string ExtractMessageLevel(GovernanceMethodData methodData)
        {
            string messageLevel = "Warning";
            if (!methodData.ErrorLevel.IsBlank())
            {
                string errorLevel = methodData.ErrorLevel;
                if (errorLevel == "1")
                {
                    messageLevel = "Error";
                }
            }

            return messageLevel;
        }

        private static DataTable GetEmptyCounterTable(GovernanceMethodData methodData)
        {
            DataTable dataTable = new DataTable();

            var methodParameters = ExtractDynamicMethodParameters(methodData);
            string periodFormat = GetDynamicMethodParameterNamed(methodParameters, "PeriodFormat");
            if (string.IsNullOrEmpty(periodFormat))
            {
                periodFormat = "yyyy/MMM";
            }

            int intTimeInterval = 12;
            string stringTimeInterval = GetDynamicMethodParameterNamed(methodParameters, "TimeInterval");
            if (!(string.IsNullOrEmpty(stringTimeInterval)))
            {
                try
                {
                    intTimeInterval = Convert.ToInt16(stringTimeInterval);
                }
                catch
                {
                    intTimeInterval = 12;
                }
            }

            dataTable.Columns.Add("Year", typeof(int));
            dataTable.Columns.Add("Month", typeof(int));
            dataTable.Columns.Add("Monthly", typeof(string));
            dataTable.Columns.Add("Registered", typeof(int));
            dataTable.Columns.Add("Finished", typeof(int));
            dataTable.Columns.Add("Open", typeof(int));
            dataTable.Columns.Add("Close", typeof(int));
            dataTable.Columns.Add("AverageTime", typeof(double));

            var datetimePeriod = DateTime.UtcNow;

            for (int countRow = 1; countRow <= intTimeInterval; countRow++)
            {
                DataRow dataRow = dataTable.NewRow();

                dataRow["Year"] = datetimePeriod.Year;
                dataRow["Month"] = datetimePeriod.Month;
                dataRow["Monthly"] = datetimePeriod.ToString(periodFormat);
                dataRow["Registered"] = 0;
                dataRow["Finished"] = 0;
                dataRow["Open"] = 0;
                dataRow["Close"] = 0;
                dataRow["AverageTime"] = 0;

                dataTable.Rows.Add(dataRow);

                datetimePeriod = datetimePeriod.AddMonths(-1);
            }

            return dataTable;
        }

        private static RepositoryObjectList GetListByRelation(RepositoryObject repObj, string relations, string filter, RepositoryObject cycleOwner = null)
        {
            IConfiguration configuration = repObj.GetConfiguration();
            IRepository repository = configuration.GetRepository();
            RepositoryObjectList objectList = new RepositoryObjectList();
            var options = new QueryOptions { Comparison = SqlComparison.DefaultCaseInsensitive, IncludeDeleted = false };
            if (!relations.IsBlank())
            {
                foreach (string relation in relations.Split(new Char[] { ';' }))
                {
                    string relationName = relation.Trim();
                    switch (relationName)
                    {
                        case "ParentRevision":
                        case "->ParentRevision":
                        case "CurrInst->ParentRevision":
                            var parentRepositoryObject = configuration.FindObject(new Oid(repObj.Id.Id + "/" + repObj.ParentRevision));
                            if (parentRepositoryObject != null)
                            {
                                objectList.Add(parentRepositoryObject);
                            }
                            break;
                        case "RevisionChildren":
                        case "-<RevisionChildren":
                        case "CurrInst-<RevisionChildren":
                            options.Scope = ObjectsScope.DefaultRevisions;
                            var query0 =
                                new SelectExpression(
                                new ColumnExpressions(new[] { new ObjectIdExpression() }),
                                new TemplateSetTable(new string[] { repObj.Template }),
                                new WhereExpression(
                                    new EqualOperationExpression(
                                        new AttributeValueOperand(PredefinedObjectAttribute.ObjectId.Name),
                                        new ConstantOperand(repObj.Id.Id.ToString()))),
                                new OrderExpression(new OrderItemExpression(new AttributeValueOperand(PredefinedObjectAttribute.LocalRevision.Name))));
                            objectList.AddRange(repository.RunQsqlExpression(query0, configuration.Id, options).Where(item => item.ParentRevision == repObj.Id.Revision));
                            break;
                        case "DefaultLanguageObject":
                        case "-<DefaultLanguageObject":
                        case "CurrInst-<DefaultLanguageObject":
                            options.Scope = ObjectsScope.DefaultRevisionsAndLanguageVariants;
                            var repositoryData = repository.GetData();
                            var defaultLanguage = repositoryData.DefaultLanguage;
                            var query1 =
                                new SelectExpression(
                                new ColumnExpressions(new[] { new ObjectIdExpression() }),
                                new TemplateSetTable(new string[] { repObj.Template }),
                                new WhereExpression(
                                    new EqualOperationExpression(
                                        new AttributeValueOperand(PredefinedObjectAttribute.ObjectId.Name),
                                        new ConstantOperand(repObj.Id.Id.ToString()))),
                                new OrderExpression(new OrderItemExpression(new AttributeValueOperand(PredefinedObjectAttribute.LocalRevision.Name))));
                            objectList.AddRange(repository.RunQsqlExpression(query1, configuration.Id, options).Where(item => item.Language == defaultLanguage));
                            break;
                        case "LanguageVariants":
                        case "->LanguageVariants":
                        case "CurrInst->LanguageVariants":
                            options.Scope = ObjectsScope.DefaultRevisionsAndLanguageVariants;
                            var query2 =
                                new SelectExpression(
                                new ColumnExpressions(new[] { new ObjectIdExpression() }),
                                new TemplateSetTable(new string[] { repObj.Template }),
                                new WhereExpression(
                                    new EqualOperationExpression(
                                        new AttributeValueOperand(PredefinedObjectAttribute.ObjectId.Name),
                                        new ConstantOperand(repObj.Id.Id.ToString()))),
                                new OrderExpression(new OrderItemExpression(new AttributeValueOperand(PredefinedObjectAttribute.LocalRevision.Name))));
                            objectList.AddRange(repository.RunQsqlExpression(query2, configuration.Id, options).Where(item => item.Id != repObj.Id));
                            break;
                        default:
                            objectList.AddRange(QepHierarchyView.GetMetamodelReferenceForObject(relationName, repObj, cycleOwner));
                            break;
                    }
                }
                if (objectList.Count > 0)
                {
                    if (!filter.IsBlank())
                    {
                        RepositoryObjectList objectListFiltered = new RepositoryObjectList();
                        foreach (string template in filter.Split(new Char[] { ';' }))
                        {
                            string templateName = template.Trim();
                            objectListFiltered.AddRange(objectList.FilterByTemplate(templateName));
                        }
                        objectList = objectListFiltered;
                    }
                }
            }
            return objectList;
        }

        private static string ExecuteCmsByRelation(
            GovernanceMethodData methodData,
            string cmsCommand,
            MessageBlock message)
        {
            RepositoryObject saveGweObject = message.GweObject;
            IConfiguration configuration = message.GweObject.GetConfiguration();
            List<string> results = new List<string>();
            try
            {
                var methodParameters = ExtractDynamicMethodParameters(methodData);
                string templates = GetDynamicMethodParameterNamed(methodParameters, "TemplateFilter");
                string relations = GetDynamicMethodParameterNamed(methodParameters, "Relations");
                if (!relations.IsBlank())
                {
                    RepositoryObjectList objectList = GetListByRelation(message.GweObject, relations, templates, message.CycleOwner);
                    if (objectList.Count > 0)
                    {
                        string cmsAnswer = string.Empty;
                        foreach (RepositoryObject repObject in objectList)
                        {
                            message.GweObject = repObject;
                            switch (cmsCommand)
                            {
                                case "Approve":
                                    cmsAnswer = Approve(methodData, message);
                                    break;
                                case "Promote":
                                    cmsAnswer = Promote(methodData, message);
                                    break;
                                case "Freeze":
                                    cmsAnswer = FreezeObject(methodData, message);
                                    break;
                                case "NewRevision":
                                    cmsAnswer = CreateNewRevision(methodData, message);
                                    break;
                                case "AcknowledgeList":
                                    cmsAnswer = CreateAcknowledgeList(methodData, message);
                                    break;
                            }
                            if (cmsAnswer != "0" && !string.IsNullOrEmpty(cmsAnswer))
                            {
                                results.Add(cmsAnswer);
                            }
                            if (message.GweObject.Id.Id == saveGweObject.Id.Id) // if we are working with the gweobject
                            {
                                saveGweObject = message.GweObject; //if it changes we keep the changes
                            }
                            else
                            {
                                message.GweObject = saveGweObject; //since we are not working with gweobject, we will recover it
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                results.Add(string.Format("{0}: {1}", MethodBase.GetCurrentMethod().Name, ex.ToString()));
            }
            message.GweObject = saveGweObject;
            return results.Count > 0
                ? string.Join(Environment.NewLine, results.ToArray())
                : c_resultOk;
        }

        private delegate void DoEventActionSignature(
            ConfigurationContext configurationCtx,
            RepositoryObject eventAction,
            RepositoryObject repObject,
            Guid historyId);

        private static void DoEventActionAsync(ConfigurationContext configurationCtx, RepositoryObject eventAction, RepositoryObject repObject, Guid historyId)
        {
            DoEventActionSignature caller = new DoEventActionSignature(DoEventAction);  // Create the delegate.          
            IAsyncResult result = caller.BeginInvoke(configurationCtx, eventAction, repObject, historyId, null, null); // Initiate the asynchronous call.
        }

        private static void DoEventAction(ConfigurationContext configurationCtx, RepositoryObject eventAction, RepositoryObject repObject, Guid historyId)
        {
            string result = string.Empty;
            try
            {
                var gweStateMachine = new GweStateMachine(configurationCtx, configurationCtx.QisServer.CurrentSession.UserId, repObject);
                try
                {
                    if (gweStateMachine.Message.GovernanceDiagram != null && gweStateMachine.GweObject != null)
                    {
                        result = gweStateMachine.FireEventMessage(eventAction, historyId);
                        gweStateMachine.RegistryEventExecution(result, historyId);
                    }
                }
                catch (Exception ex)
                {
                    gweStateMachine.RegistryEventExecution(ex.Message, historyId);
                }
            }
            catch (Exception ex)
            {
                var logWriter = configurationCtx.QisServer
                                    .GetQefService<ILogService>()
                                    .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));
                logWriter.Error(ex.ToString());
            }
        }

        private delegate void DoPostObjectSignature(
            ConfigurationContext configurationCtx,
            RepositoryObject repObject);

        private static void DoPostObjectAsync(ConfigurationContext configurationCtx, RepositoryObject repObject)
        {
            DoPostObjectSignature caller = new DoPostObjectSignature(DoPostObject);  // Create the delegate.          
            IAsyncResult result = caller.BeginInvoke(configurationCtx, repObject, null, null); // Initiate the asynchronous call.
        }

        private static void DoPostObject(ConfigurationContext configurationCtx, RepositoryObject repObject)
        {
            var gweStateMachine = new GweStateMachine(configurationCtx, configurationCtx.QisServer.CurrentSession.UserId, repObject);
            if (gweStateMachine.GweObject != null)
            {
                gweStateMachine.Message.GovernanceStateBefore = gweStateMachine.CurrentState;
                gweStateMachine.CheckEvent("Post object - CheckEvent");
            }
        }

        private static string DoPublishContentAsync(MessageBlock message, RepositoryObject sourceObj = null)
        {
            // Create the delegate.
            DoPublishContentSignature caller = new DoPublishContentSignature(DoPublishContent);
            // Initiate the asynchronous call.
            IAsyncResult result = caller.BeginInvoke(message, sourceObj, null, null);

            return c_resultOk;
        }

        private delegate string DoPublishContentSignature(MessageBlock message, RepositoryObject sourceObj = null);
        private static string DoPublishContent(MessageBlock message, RepositoryObject sourceObj = null)
        {
            System.Runtime.Remoting.Messaging.CallContext.SetData("QisServer", message.CurrentContext.QisServer);

            string result = c_resultOk;
            try
            {
                if ((sourceObj != null) && (DoPublishByFileCopy(sourceObj, message.GweObject)))
                {
                    return result;
                }

                var configuration = message.GweObject.GetConfiguration();

                var qclInstanceId = QEP.GetHTMLQclInstance(configuration);
                if (qclInstanceId == null && configuration.GetData().IsPrivateWorkspace)
                {
                    qclInstanceId = QEP.GetHTMLQclInstance(configuration.GetBaseConfiguration());
                }

                if (qclInstanceId == null)
                {
                    return "Error: QCLEngine for publishing is not available";
                }

                var builderFullCommand = new StringBuilder();
                var rep = GweCallContextQisServer().FindRepository(message.GweObject.RepositoryId);
                var qclPublisherCmd = string.Format("{0}\t{1}\t{2}", message.GweObject.Id.Id, message.GweObject.Id.Revision, message.GweObject.GetConfiguration().Id);
                builderFullCommand.Append("HTMLEventPublisher(\"" + qclPublisherCmd + "\", 1);");

                string expressionText = builderFullCommand.ToString(); //.Replace("\"", "\\\"");
                QclEngineExtentions.ExecuteQclCommand(rep, qclInstanceId, expressionText);
            }
            catch (Exception ex)
            {
                result = ex.ToString();
            }

            return result;
        }

        private static string FastPublishContent(
            GovernanceMethodData methodData,
            MessageBlock message,
            RepositoryObject sourceObj)
        {
            var logWriter = message.CurrentContext.QisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));
            string result = c_resultOk;
            if (message == null)
            {
                result = "Can not publish: Empty message";
            }
            else
            {
                var messageFormat = ExtractMessageFormat(methodData, "Can not publish {0}: {1}");
                try
                {
                    if (message.GweObject == null)
                    {
                        result = string.Format(messageFormat, "", "Empty object");
                    }
                    else
                    {
                        var methodParameters = ExtractDynamicMethodParameters(methodData);
                        string publishMode = GetDynamicMethodParameterNamed(methodParameters, "PublishContent");
                        if (string.IsNullOrEmpty(publishMode))
                        {
                            publishMode = "Asynchronous";
                        }
                        switch (publishMode)
                        {
                            case "Synchronous":
                                result = DoPublishContent(message, sourceObj);
                                break;
                            case "Asynchronous":
                                result = DoPublishContentAsync(message, sourceObj);
                                break;
                        }
                        if (result != c_resultOk)
                        {
                            result = string.Format(messageFormat, message.GweObject.Name, result);
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = string.Format(messageFormat, message.GweObject.Name, ex.ToString());
                }
            }
            if (result != c_resultOk)
            {
                logWriter.Error(result);
            }
            return result;
        }

        private static bool DoPublishByFileCopy(RepositoryObject sourceObj, RepositoryObject targetObj)
        {
            bool copyDone = false;
            var qisServer = GweDynamicCode.GweCallContextQisServer();
            var sourceConfig = sourceObj.GetConfiguration();
            var repository = sourceConfig.GetRepository();
            var logWriter = qisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));

            if (targetObj.Id.Id != sourceObj.Id.Id)
            {
                logWriter.Error("DoPublishByFileCopy can not deal with target object different from source");
                return false;
            }
            var targetRevId = targetObj.Id.Revision;
            var sourceRevId = sourceObj.Id.Revision;

            var targetLang = targetObj.Language.ToString();
            var sourceLang = sourceObj.Language.ToString();

            var targetConfig = targetObj.GetConfiguration(); ;

            if (targetRevId == sourceRevId && targetConfig.Id == sourceConfig.Id)
            {
                return true;
            }

            var sourcePath = qisServer.GetGeneratedHtmlPath(repository.Id, sourceConfig.GetData().Name);
            var targetPath = qisServer.GetGeneratedHtmlPath(repository.Id, targetConfig.GetData().Name);

            var svgFile = Path.Combine(sourcePath, String.Format("Diagrams\\{0}-{1}.{2}", sourceRevId, sourceLang, "svg"));
            var pngFile = Path.Combine(sourcePath, String.Format("Diagrams\\{0}-{1}.{2}", sourceRevId, sourceLang, "png"));
            var htmFile = Path.Combine(sourcePath, String.Format("Diagrams\\{0}-{1}.{2}", sourceRevId, sourceLang, "htm"));

            var svgTarget = Path.Combine(targetPath, String.Format("Diagrams\\{0}-{1}.{2}", targetRevId, targetLang, "svg"));
            var pngTarget = Path.Combine(targetPath, String.Format("Diagrams\\{0}-{1}.{2}", targetRevId, targetLang, "png"));
            var htmTarget = Path.Combine(targetPath, String.Format("Diagrams\\{0}-{1}.{2}", targetRevId, targetLang, "htm"));

            try
            {
                if (!Directory.Exists(Path.Combine(targetPath, "Diagrams")))
                {
                    Directory.CreateDirectory(Path.Combine(targetPath, "Diagrams"));
                }
                if (File.Exists(svgFile))
                {
                    if (!File.Exists(svgTarget))
                    {
                        File.Copy(svgFile, svgTarget, true);
                    }
                    copyDone = true;
                }

                if (File.Exists(pngFile))
                {
                    if (!File.Exists(pngTarget))
                    {
                        File.Copy(pngFile, pngTarget, true);
                    }
                    copyDone = true;
                }

                if (File.Exists(htmFile))
                {
                    if (!File.Exists(htmTarget))
                    {
                        File.Copy(htmFile, htmTarget, true);
                    }
                    copyDone = true;
                }
            }
            catch (Exception exOnFileCopy)
            {
                logWriter.Error(exOnFileCopy.ToString());
                return false;
            }
            return copyDone;
        }

        private static RepositoryObjectList GetRepositoryObjectSet(
            ConfigurationContext ctx,
            IReadOnlyCollection<Oid> identifiers,
            QueryOptions options)
        {
            if (identifiers.Count == 0)
            {
                return new RepositoryObjectList();
            }

            var defaultObjects = identifiers.Where(x => x.IsDefaultRevision).Select(x => x.Id);
            var specificObjects = identifiers.Where(x => !x.IsDefaultRevision).Select(x => x.Revision);

            var defaultQuery = new SelectExpression(
                new ColumnExpressions(new[] { new ObjectIdExpression() }),
                new ObjectListTable(defaultObjects));

            var specificQuery = new SelectExpression(
                new ColumnExpressions(new[] { new ObjectIdExpression() }),
                new ObjectListTable(specificObjects));

            options = new QueryOptions
            {
                Comparison = SqlComparison.DefaultCaseInsensitive,
                CurrentLanguage = options.CurrentLanguage,
                IncludeDeleted = options.IncludeDeleted,
                Scope = ObjectsScope.AllRevisionsAndLanguageVariantsInRepository,
            };

            var foundDefaultObjects = ctx.Repository.RunQsqlExpression(defaultQuery, null, options);
            var foundSpecificObjects = ctx.Repository.RunQsqlExpression(specificQuery, null, options);

            return new RepositoryObjectList(
                foundDefaultObjects
                    .Union(foundSpecificObjects)
                    .Where(x => x.Language == (options.CurrentLanguage ?? ctx.Repository.CurrentLanguage)));
        }

        private static void PostAcknowledgedEmailTask(
            MessageBlock message, RepositoryObject notificRevision, RepositoryObjectList personList,
            RepositoryObjectList includedList, RepositoryObjectList removedList, RepositoryObjectList unchangedList)
        {
            var configuration = message.CurrentContext.Configuration;

            foreach (var person in personList)
            {
                message.MessageActor = person;
                if (message.MessageActor != null)
                {
                    try
                    {
                        var msgReplacements = new Dictionary<string, string>()
                        {
                            { "{EntityIncludedList}", GetAckHtmlLinkList(message.CurrentContext, includedList, message.MessageActor) },
                            { "{EntityRemovedList}", GetAckHtmlLinkList(message.CurrentContext, removedList, message.MessageActor) },
                            { "{EntityUnchangedList}", GetAckHtmlLinkList(message.CurrentContext, unchangedList, message.MessageActor) },
                            { "{NewEntityList}", GetAckHtmlLinkList(message.CurrentContext, new RepositoryObjectList(unchangedList.ToList().Concat(includedList.ToList())), message.MessageActor) },
                            { "{OldEntityList}", GetAckHtmlLinkList(message.CurrentContext, new RepositoryObjectList(unchangedList.ToList().Concat(removedList.ToList())), message.MessageActor) }
                        };
                        RepositoryGWE.PostNotification(notificRevision, message, msgReplacements);
                    }
                    catch (Exception ex)
                    {
                        RegistrySubTaskResult(message, ex.Message, message.MessageActor.Id.Id.ToString(), "Post Acknowledged Email Task");
                    }
                }
            }
        }
        private static void RegistrySubTaskResult(MessageBlock message, string resultMsg, string taskSourceId = "", string taskOwner = "Event Action Engine")
        {
            Guid taskId = Guid.NewGuid();

            const string tableName = "[Task_History]";
            DataSet taskHistorySet = message.AdoUtility.GetShemaForTable(tableName);
            DataRow taskHistoryRow = taskHistorySet.Tables[0].NewRow();
            taskHistoryRow["GweTaskId"] = taskId.ToString();
            taskHistoryRow["HistoryId"] = message.HistoryId.ToString();
            if (string.IsNullOrEmpty(taskSourceId))
            {
                taskHistoryRow["GweTaskSourceId"] = string.Format("{0}", message.GweObject.Id.Id);
            }
            else
            {
                taskHistoryRow["GweTaskSourceId"] = taskSourceId;
            }

            taskHistoryRow["GweTaskOwner"] = taskOwner;
            if (string.IsNullOrEmpty(resultMsg) || resultMsg.TrimQlmAttributeValue().Replace("\n", "") == c_resultOk)
            {
                taskHistoryRow["GweTaskStatus"] = 0;
                taskHistoryRow["GweTaskStatusMsg"] = $"OK - {taskOwner} Done";
            }
            else
            {
                taskHistoryRow["GweTaskStatus"] = 999;
                taskHistoryRow["GweTaskStatusMsg"] = resultMsg;
            }

            taskHistorySet.Tables[0].Rows.Add(taskHistoryRow);
            message.AdoUtility.InsertFromDataSet(taskHistorySet, tableName);
        }
        private static string GetObjectUrlList(ConfigurationContext ctx, RepositoryObject player, HashSet<Oid> oidList)
        {
            var resolvedObjects = GetRepositoryObjectSet(
                ctx,
                oidList,
                new QueryOptions())
                .WithAuditAndConfiguration();

            StringBuilder objectUrlList = new StringBuilder();
            var gweAdoUtility = new GweAdoUtility(ctx);
            if (oidList.Count() < 1)
            {
                return string.Empty;
            }
            objectUrlList.AppendLine("<div class=\"GovernanceEmailContent\">");
            objectUrlList.AppendLine("<table class=\"GovernanceEmailTable\">");
            objectUrlList.AppendLine("<tr>");
            objectUrlList.AppendLine($"<th>Entity</th>");
            objectUrlList.AppendLine($"<th>Status</th>");
            objectUrlList.AppendLine("</tr>");
            foreach (var oid in oidList)
            {
                var revision = resolvedObjects.FindObject(oid);
                if (revision != null)
                {
                    objectUrlList.AppendLine("<tr>");
                    objectUrlList.AppendLine($"<td>{QEP.CreateHtmlLink(revision).Replace("\"", "\\\"")}...</td>");
                    var currentStateHl = revision.GweState?.HyperLink;
                    if (currentStateHl != null)
                    {
                        var currentState = ctx.Configuration.FindObject(currentStateHl);
                        if (currentState != null)
                        {
                            objectUrlList.AppendLine($"<td>{QEP.CreateHtmlLink(currentState).Replace("\"", "\\\"")}...</td>");
                        }
                        else
                        {
                            objectUrlList.AppendLine("<td>Not Defined</td>");
                        }
                    }
                    else
                    {
                        objectUrlList.AppendLine("<td>Not Defined</td>");
                    }
                    objectUrlList.AppendLine("</tr>");
                }
            }
            objectUrlList.AppendLine("</table>");
            objectUrlList.AppendLine("</div>");

            return objectUrlList.ToString();
        }
        private static string GetAckHtmlLinkList(ConfigurationContext ctx, RepositoryObjectList revisonList, RepositoryObject person)
        {
            StringBuilder objectUrlList = new StringBuilder();
            var gweAdoUtility = new GweAdoUtility(ctx);
            if (revisonList.Count() < 1)
            {
                return string.Empty;
            }
            objectUrlList.AppendLine("<div class=\"GovernanceEmailContent\">");
            objectUrlList.AppendLine("<table class=\"GovernanceEmailTable\">");
            objectUrlList.AppendLine("<tr>");
            objectUrlList.AppendLine($"<th>Entity</th>");
            objectUrlList.AppendLine($"<th>Status</th>");
            objectUrlList.AppendLine("</tr>");
            foreach (var revision in revisonList)
            {
                if (revision != null)
                {
                    //objectUrlList.AppendLine($"  {QEP.CreateHtmlLink(revision).Replace("\"", "\\\"")} - {GetAckCurrentStatus(gweAdoUtility, revision, person)}");

                    objectUrlList.AppendLine("<tr>");
                    objectUrlList.AppendLine($"<td>{QEP.CreateHtmlLink(revision).Replace("\"", "\\\"")}...</td>");
                    objectUrlList.AppendLine($"<td>{GetAckCurrentStatus(gweAdoUtility, revision, person)}...</td>");
                    objectUrlList.AppendLine("</tr>");
                }
            }
            objectUrlList.AppendLine("</table>");
            objectUrlList.AppendLine("</div>");

            return objectUrlList.ToString();
        }
        private static string GetAckCurrentStatus(GweAdoUtility gweAdoUtility, RepositoryObject entity, RepositoryObject person)
        {
            string answer = "Pending";
            DataSet LocalAcknowledge = gweAdoUtility.GetAcknowledgeGrouped(entity.Id, person.Id.Id);

            if (LocalAcknowledge == null || LocalAcknowledge.Tables.Count == 0 || LocalAcknowledge.Tables[0].Rows.Count == 0)
                answer = "Requires update";
            else
            {
                int acknowledgeFlag = 2;
                if (LocalAcknowledge.Tables[0].Rows[0]["AcknowledgeFlag"] != DBNull.Value)
                {
                    acknowledgeFlag = Convert.ToInt16(LocalAcknowledge.Tables[0].Rows[0]["AcknowledgeFlag"]);
                }
                switch (acknowledgeFlag)
                {
                    case 0:
                        answer = "To be acknowledged";
                        break;
                    case 1:
                        answer = "Acknowledged";
                        break;
                    default:
                        answer = "Requires update";
                        break;
                }
            }
            return answer;
        }

        private static IUser QefUserLogin(RepositoryObject person)
        {
            if (person.IsStub)
            {
                throw new ArgumentException("Person is a stub");
            }

            string login = person.Attributes["LOGIN"].Value.ToString();
            if (string.IsNullOrEmpty(login))
            {
                throw new ArgumentException("No LOGIN value for the person");
            }

            IUser user = null;
            IUserStorageService userStorageService = CallContext.QisServer.GetQefService<IUserStorageService>();

            UserLookupParameters lookupParameters = new UserLookupParameters(login)
            {
                LookupFields = UserLookupFields.Login,
                MaximumCount = 10
            };

            UserLookupResult lookupResult = userStorageService.LookUpUsers(lookupParameters);
            //user = lookupResult.Items.FirstOrDefault();


            var users = lookupResult.Items.Where(u => u.Login == login);
            var matchedByPersonId = users.FirstOrDefault(u => u.Id == person.Id.Id.Value);
            if (matchedByPersonId != null)
            {
                return matchedByPersonId;
            }

            IAttributeValue alternativeIdValue;
            if (person.Attributes.TryGetValue("AlternativeID", out alternativeIdValue) && !alternativeIdValue.IsEmpty)
            {
                Guid alternativeId;
                if (Guid.TryParse(alternativeIdValue.GetContent(), out alternativeId))
                {
                    var matchedByAlternativeId = users.FirstOrDefault(u => u.Id == alternativeId);
                    if (matchedByAlternativeId != null)
                    {
                        return matchedByAlternativeId;
                    }
                }
            }

            user = lookupResult.Items.FirstOrDefault();

            return user;
        }

        private static bool CanBeValidByDate(RepositoryObject revision)
        {
            return revision.Attributes.Contains(c_validFromString) &&
                revision.Attributes.Contains(c_validToString);
        }

        private static DateTime ProcessRevisionValidFrom(RepositoryObject revision)
        {
            var validFrom = revision.Attributes.GetDateTime(c_validFromString);
            if (validFrom == null)
            {
                validFrom = DateTime.UtcNow;
                revision.SetAttributeValue(c_validFromString, new QDateTime(DateTime.UtcNow));
            }

            return validFrom.Value;
        }

        private static DateTime ProcessRevisionValidTo(RepositoryObject revision, DateTime validFrom, int days)
        {
            var validTo = revision.Attributes.GetDateTime(c_validToString);
            if (validTo == null)
            {
                validTo = validFrom + new TimeSpan(days, 0, 0, 0);
                revision.SetAttributeValue(c_validToString, new QDateTime(validTo.Value.ToUniversalTime()));
            }

            return validTo.Value;
        }

        [Obsolete("This method is obsolete in 6.6 version, please use different parameters", true)]
        private static string PostEmailTask(GweSmtpUtility smtpUtility, SendMailContext sendMailContext, string toList, string subject, string messageBody)
        {
            return PostEmailTask(smtpUtility, sendMailContext, smtpUtility.GetMailMessage(toList, subject, messageBody, true));
        }
        private static string PostEmailTask(GweSmtpUtility smtpUtility, SendMailContext sendMailContext, MailMessage mailMessage)
        {
            #region Argument Check

            smtpUtility.EnsureNotNull("smtpUtility");
            sendMailContext.EnsureNotNull("sendMailContext");

            #endregion

            switch (smtpUtility.DeliveryOptions)
            {
                case GweSmtpDeliveryOptions.Asynchronous:
                    smtpUtility.SendMessageTask(sendMailContext, mailMessage);
                    break;
                default:
                    smtpUtility.PostMessageTask(sendMailContext, mailMessage);
                    break;
            }

            return string.Empty;
        }

        #endregion

        #region Public Methods

        #region Server Methods // Executing Dynamic Code from QCL Script.

        /// <summary>
        ///	 Sample:
        ///		 Print(callServerScript(RepositoryID, "GweDynamicCode", "GWE_SM_TestDynamic",  "LDyn_Always"));
        ///		 Print(callServerScript(RepositoryID, "GweDynamicCode", "GWE_SM_TestDynamic",  "LDyn_Never"));
        /// </summary>
        public static void GWE_SM_TestDynamic(GeneralScriptArgs args)
        {
            try
            {
                StringBuilder debugMessages = new StringBuilder();

                string dynamicFunctionName = args.Parameters[0];

                MethodInfo methodInfo = typeof(GweDynamicCode).GetMethod(
                    dynamicFunctionName,
                    new[] { typeof(GovernanceMethodData), typeof(MessageBlock) });

                debugMessages.AppendFormat("Executing, Dynamically [{0}]", dynamicFunctionName);
                debugMessages.AppendLine();
                debugMessages.AppendFormat("Method in library GweDynamicCode is: [{0}]", methodInfo);

                if (methodInfo.IsNotNull())
                {
                    args.Result = ExecuteDynamicFunction(ConfigurationContext.Create(args.Configuration), methodInfo);
                }

                args.Message = string.Format("{0}\n{1}", MethodBase.GetCurrentMethod().Name, debugMessages);
            }
            catch (Exception ex)
            {
                args.Message = ex.ToString();
            }
        }

        /// <summary>  
        ///	 Inside QCLCode GWEVerifyMethod(Obj_)  
        /// </summary>
        public static void GWE_SM_VerifyMethodSignature(GeneralScriptArgs args)
        {
            IConfiguration configuration = args.Configuration;
            string objectId = args.Parameters[0];
            string revisionId = args.Parameters[1];
            RepositoryObject methodSignatureRepositoryObject = configuration.FindObject(new Oid(objectId + "/" + revisionId));
            if (methodSignatureRepositoryObject == null)
            {
                args.Result = string.Format("Msg00-->Can not find a MethodSignature for {0}/{1}", objectId, revisionId);
            }
            else
            {
                args.Result = VerifyMethodSignature(methodSignatureRepositoryObject);
            }
        }

        #endregion

        #region Execution of Dynamic Code
        /// Deal with the Execution of DynamicCode

        public GovernanceState UserDefaultStateExecute(MessageBlock message)
        {
            MethodInfo methodInfo = typeof(GweDynamicCode)
                .GetMethod("UserGetDefaultState", new[] { typeof(GovernanceMethodData), typeof(MessageBlock) });
            if (methodInfo.IsNull())
            {
                return null;
            }

            var function = (GweObjectDynamicFunction)Delegate.CreateDelegate(typeof(GweObjectDynamicFunction), methodInfo);
            if (function.IsNull())
            {
                return null;
            }

            var state = GovernanceState.Create(function(GovernanceMethodData.Empty, message));
            return state;
        }

        public bool ExecuteMethodSignatureLogical(Oid methodSignatureId)
        {
            var method = AllGovernanceMethods.FindBooleanMethod(m_configurationCtx, methodSignatureId?.Id);
            if (method == null)
            {
                return false;
            }

            return method.Execute(m_message);
        }

        public string ExecuteMethodSignatureString(Oid methodSignatureId)
        {
            var method = AllGovernanceMethods.FindStringMethod(m_configurationCtx, methodSignatureId?.Id);
            if (method == null)
            {
                return $"Can not execute Method Oid({methodSignatureId.Id})";
            }

            return method.Execute(m_message);
        }

        public string ExecuteMethodSignatureString(GovernanceStringMethod method)
        {
            if (method == null)
            {
                return $"Can not execute Method Oid({method.Data.Id.Id}) = {method.Data.Name}";
            }

            return method.Execute(m_message);
        }

        public DataTable ExecuteMethodSignatureDataTable(RepositoryObject methodSignature)
        {
            var method = AllGovernanceMethods.CreateTableMethod(methodSignature);
            if (method == null)
            {
                throw new Exception(string.Format(
                    "{0} Logical error: Cannot prepare to play method: \"{1}\".",
                    MethodBase.GetCurrentMethod().Name,
                    methodSignature.Name));
            }

            return method.Execute(m_message);
        }

        public static string StringDynFuncByQclCode(GovernanceMethodData methodData, string expression, MessageBlock message)
        {
            var builderFullCommand = new StringBuilder();

            var rep = message.CurrentContext.Repository;
            builderFullCommand.Append("local GweMessageBlock;");
            builderFullCommand.AppendQCLInfo("GweMessageBlock.AfterDateLimitOnState", message.AfterDateLimitOnState);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.AnalyticMaxRecords", message.AnalyticMaxRecords);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.AnalyticStartRecord", message.AnalyticStartRecord);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.AnalyticUseInterval", message.AnalyticUseInterval);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.Answer", message.Answer);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.BeforeDateLimitOnState", message.BeforeDateLimitOnState);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.CurrentDynamicMethod", methodData.Id);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.CycleCount", message.CycleCount);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.CycleOwner", message.CycleOwner.Id);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.ExtendedDate", message.ExtendedDate);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.ForceTransition", message.ForceTransition);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.GovernanceDiagram", message.GovernanceDiagram.Id);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.GovernanceStateAfter", message.GovernanceStateAfter?.Id);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.GovernanceStateBefore", message.GovernanceStateBefore?.Id);
            builderFullCommand.AppendQCLInfoWithRev("GweMessageBlock.GweObject", message.GweObject.Id);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.HistoryId", message.HistoryId);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.LimitDate", message.LimitDate);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.MessageActor", message.MessageActor.Id);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.MessageCanBeForced", message.MessageCanBeForced);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.MessageComment", message.MessageComment);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.MessageReceived", message.MessageReceived.Id);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.MessageReceivedDate", message.MessageReceivedDate);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.MessageSLA", message.MessageSLA);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.MsgPctCompleted", message.MsgPctCompleted);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.OnStatusSince", message.OnStatusSince);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.RecyclingCount", message.RecyclingCount);
            builderFullCommand.AppendQCLInfo("GweMessageBlock.WarningDate", message.WarningDate);

            builderFullCommand.Append(expression + ";");
            string expressionText = builderFullCommand.ToString().Replace("\"", "\\\"");

            expressionText = string.Format("LocObject.QepRunCodeWithCurrInst(\"{0}\");", expressionText);
            expressionText = expressionText.QCLInfoWithRev("LocObject", message.GweObject.Id);

            var qclInstanceId = QEP.GetHTMLQclInstance(message.CurrentContext.Configuration);
            if (qclInstanceId == null)
            {
                return "Msg21-->There is no Instance to Execute QCL, Check your HTMLProfile";
            }

            var syntaxAnswer = QclEngineExtentions.ExecuteQclCommand(rep, qclInstanceId, expressionText);

            var duration = syntaxAnswer.Duration;

            var result = syntaxAnswer.Result;

            var Messages = syntaxAnswer.Messages;
            return syntaxAnswer.Result;
        }

        public static string ExecuteDynamicFunction(ConfigurationContext configurationCtx, MethodInfo method)
        {
            var message = new MessageBlock(configurationCtx);

            var methodToRun = (GweLogicalDynamicFunction)Delegate.CreateDelegate(typeof(GweLogicalDynamicFunction), method);
            bool result = methodToRun(GovernanceMethodData.Empty, message);

            return result
                ? Boolean.TrueString
                : Boolean.FalseString;
        }

        public static IQisServer GweCallContextQisServer()
        {
            var qisServer = Qis.Common.Scripting.CallContext.QisServer;
            if (qisServer == null)
            {
                qisServer = (IQisServer)System.Runtime.Remoting.Messaging.CallContext.GetData("QisServer");
            }

            return qisServer;
        }

        #endregion

        #region Governance Methods By C# Script
        //Deal with public methods: (Bool, Action, Check, Get, Measurement and DataTable)

        #region GovernanceBoolMethod ByCSScript
        //Deal with public methods: Bool

        /// <summary>  
        ///	 Return always <b>true</b>  
        /// </summary>
        public static bool Always(GovernanceMethodData self, MessageBlock message)
        {
            return true;
        }
        public static bool Never(GovernanceMethodData self, MessageBlock message)
        {
            return false;
        }

        /// <summary>  
        ///	 Check if the governance object is a diagram.
        /// </summary>
        public static bool IsDiagram(GovernanceMethodData self, MessageBlock message)
        {
            return message.GweObject.IsDiagram;
        }

        public static bool IsPromoted(GovernanceMethodData self, MessageBlock message)
        {
            if (message.GweObject.ConfigurationInfo.IsInheritedRevision)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>  
        ///		Returns <b>true</b> when the the object can be subscribe by the message actor.
        /// </summary>
        public static bool AllowSubscription(GovernanceMethodData self, MessageBlock message) //If we have a team subscription we also can not do a person subscription
        {
            var objectSubscriptions = ChangeManagement.GetTeamSubscriptions(message.CurrentContext, message.MessageActor);
            if (objectSubscriptions.Contains(message.GweObject.Id))
            {
                return false;
            }
            return !AllowUnSubscription(self, message);
        }

        /// <summary>  
        ///		Returns <b>true</b> When the the object has a personal subscription from the message actor. Means he can unsubscrib
        /// </summary>
        public static bool AllowUnSubscription(GovernanceMethodData self, MessageBlock message)
        {
            return CMS.FindSubscribers(message.GweObject, true).Contains(message.MessageActor.Id);
        }

        /// <summary>  
        ///		Returns <b>true</b> when the object is Frozen.
        /// </summary>
        public static bool IsFrozen(GovernanceMethodData self, MessageBlock message)
        {
            return message.GweObject.IsFrozen;
        }

        /// <summary>  
        ///		Returns <b>true</b> when the language of object is equal the one the method is programed to find, by default, the defaut language.
        /// </summary>
        public static bool IsSpecificLanguage(GovernanceMethodData self, MessageBlock message)
        {
            var methodParameters = ExtractDynamicMethodParameters(self);
            string requestedLanguage = GetDynamicMethodParameterNamed(methodParameters, "Language");
            IConfiguration configuration = message.GweObject.GetConfiguration();
            IRepository repository = configuration.GetRepository();
            var repositoryData = repository.GetData();
            var specificLanguage = repositoryData.DefaultLanguage;
            if ((!string.IsNullOrEmpty(requestedLanguage)) && (!StringComparer.OrdinalIgnoreCase.Equals(requestedLanguage, "Default")))
            {
                if (StringComparer.OrdinalIgnoreCase.Equals(requestedLanguage, "Current"))
                {
                    specificLanguage = repository.CurrentLanguage;
                }
                else
                {
                    specificLanguage = LanguageId.Parse(requestedLanguage);
                }
            }

            return (message.GweObject.Language == specificLanguage);
        }

        /// <summary>  
        ///	Returns true when the the object can be acknowledged by the message actor
        /// </summary>
        public static bool AllowAcknowledge(GovernanceMethodData self, MessageBlock message)
        {
            return message.AdoUtility.GetAcknowledgeForObject(message.GweObject.Id, 0).Contains(message.MessageActor.Id);
        }

        /// <summary>  
        ///	 Returns true when the deadline for completion of the task has been overcome.
        /// </summary>
        public static bool TimeLimitExceeded(GovernanceMethodData self, MessageBlock message)
        {
            return (DateTime.UtcNow > message.AfterDateLimitOnState);
        }

        /// <summary>  
        ///		Returns <b>true</b> when the the object was not rated by the message actor.
        /// </summary>
        public static bool AllowRating(GovernanceMethodData self, MessageBlock message)
        {
            DataSet ratingSet = message.AdoUtility.GetRating(message.GweObject, message.MessageActor);

            return ratingSet.Tables[0].Rows.Count <= 0;
        }

        /// <summary>  
        ///		Returns <b>true</b> when the the object can be updated
        /// </summary>
        public static bool CanUpdateObject(GovernanceMethodData self, MessageBlock message)
        {
            string result = CheckUnableToUpdateObject(self, message);
            if (result.IsBlank())
                return true;
            else
            {
                ILogWriter logWriter = message.CurrentContext.QisServer
                    .GetQefService<ILogService>()
                    .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));
                logWriter.Error(result);
                return false;
            }
        }

        /// <summary>  
        ///		Returns true when two dates satisfy a condition.
        /// </summary>
        public static bool CompareDate(GovernanceMethodData self, MessageBlock message)
        {
            bool checkResult = string.IsNullOrEmpty(DoCompareDate(self, message));
            return checkResult;
        }

        /// <summary>  
        ///		Returns true when the warning date of the task has been overcome.
        ///		When this happen, we consider the need to change the status of the object 
        ///		Critical or close to be considered a delayed.
        /// </summary>
        public static bool TimeSlaCritical(GovernanceMethodData self, MessageBlock message)
        {
            DataSet statusSet = message.AdoUtility.GetGweQuery(message.GweObject, "[ObjectCurrentStatus]");
            if (statusSet.Tables[0].Rows.Count == 1)
            {
                if (statusSet.Tables[0].Rows[0]["WarningDate"] != DBNull.Value)
                {
                    DateTime? warningDateTime = Convert.ToDateTime(statusSet.Tables[0].Rows[0]["WarningDate"]);
                    return (DateTime.UtcNow > warningDateTime);
                }
            }

            return false;
        }

        /// <summary>  
        ///		Returns <b>true</b> when the the object ApprovalState is equal "Approved"
        /// </summary>
        public static bool ApprovalStateApproved(GovernanceMethodData self, MessageBlock message)
        {
            var logWriter = message.CurrentContext.QisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));

            bool result = StringComparer.OrdinalIgnoreCase.Equals(message.GweObject.Attributes.AttributePlainValueSafely("ApprovalState"), "Approved");

            logWriter.DebugDynamicCodeL2(string.Format(
                "Checking if the state of {0}[{1}/{2}] rev. {3} is approved. Answer = {4}[{5}]",
                message.GweObject.Name,
                message.GweObject.Id.Id,
                message.GweObject.Id.Revision,
                message.GweObject.LocalRevision,
                result,
                message.GweObject.Attributes.AttributePlainValueSafely("ApprovalState")));

            if (result)
            {
                result = message.GweObject.IsFrozen;
                logWriter.DebugDynamicCodeL2(string.Format(
                    "Checking if {0}[{1}/{2}] Rev. {3}, approved is frozen. Answer = {4}",
                    message.GweObject.Name,
                    message.GweObject.Id.Id,
                    message.GweObject.Id.Revision,
                    message.GweObject.LocalRevision,
                    result));
            }

            return result;
        }

        /// <summary>  
        ///		Returns <b>true</b> when the limit date of the task has been overcome.
        ///		When this happen, we consider the need to change the status of the object already delayed.
        /// </summary>
        public static bool TimeSlaDelayed(GovernanceMethodData self, MessageBlock message)
        {
            DataSet statusSet = message.AdoUtility.GetGweQuery(message.GweObject, "[ObjectCurrentStatus]");
            if (statusSet.Tables[0].Rows.Count == 1)
            {
                if (statusSet.Tables[0].Rows[0]["LimitDate"] != DBNull.Value)
                {
                    DateTime? limitDateTime = Convert.ToDateTime(statusSet.Tables[0].Rows[0]["LimitDate"]);

                    return DateTime.UtcNow > limitDateTime;
                }
            }

            return false;
        }

        /// <summary>  
        ///	 Returns <b>true</b> if the TemplateList on the Workflow diagram contains the template of the object.
        /// </summary>
        public static bool UseTemplateList(GovernanceMethodData self, MessageBlock message)
        {
            var logWriter = message.CurrentContext.QisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));
            string templateName = message.GweObject.Template;

            var result = message.GovernanceDiagram.TemplateList.Contains(templateName);

            logWriter.DebugDynamicCodeL2(string.Format(
                    "Checking if {0} is a valid template for diagram {1}. Result={2}",
                    templateName,
                    message.GovernanceDiagram.Name,
                    result));

            return result;
        }

        /// <summary>
        ///	 Verifies correctness of name/value pairs of parameters sent to the dynamic method.
        /// </summary>
        /// <param name="message">
        ///	 Method call context.
        /// </param>
        /// <returns>
        ///	 <b>True</b> when parameters are correct; <b>false</b> otherwise.
        /// </returns>
        public static bool VerifyAttributeList(GovernanceMethodData self, MessageBlock message)
        {
            bool checkResult = string.IsNullOrEmpty(CheckAttributeList(self, message));
            return checkResult;
        }

        /// <summary>
        ///	 Verifies OldData to find attribute changes in a list.
        /// </summary>
        /// <param name="message">
        ///	 Method call context.
        /// </param>
        /// <returns>
        ///	 <b>true</b> when find changes in some attribute in the list; <b>false</b> otherwise.
        /// </returns>
        public static bool VerifyChangesOnAttrList(GovernanceMethodData self, MessageBlock message)
        {
            bool checkResult = !string.IsNullOrEmpty(CheckChangesOnAttrList(self, message));  //Return true if any attribute in the list was changed
            return checkResult;
        }
        public static bool IsDefaultRevision(GovernanceMethodData self, MessageBlock message)
        {
            bool checkResult = message.GweObject.ConfigurationInfo.IsDefaultRevision;

            return checkResult;
        }
        public static bool SkipOnPreviousError(GovernanceMethodData self, MessageBlock message)
        {
            if (message.ThereIsError)
            {
                return false;
            }
            if (message.ThereIsWarning)
            {
                if (message.ForceTransition)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
        public static bool BypassOnPreviousError(GovernanceMethodData self, MessageBlock message) // Only for compatibility issue
        {
            return SkipOnPreviousError(self, message);
        }
        public static bool IsRevisionValidFromDate(GovernanceMethodData self, MessageBlock message)
        {
            var logWriter = message.CurrentContext.QisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));

            IConfiguration configuration = message.GweObject.GetConfiguration();
            IConfigurationData configData = configuration.GetData();
            if (configData.IsPrivateWorkspace && !message.GweObject.ConfigurationInfo.IsInheritedRevision)
            {
                //if (ApprovalStateApproved(message) && IsFrozen(message))
                {
                    var ValidFromAttributeValue = (QDateTime)message.GweObject.Attributes["RevisionValidFrom"].Value;
                    if (DateTime.UtcNow >= ValidFromAttributeValue.ToDateTime(TimeZoneInfo.Utc))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        public static bool NewParticipantList(GovernanceMethodData self, MessageBlock message)
        {
            var repository = message.CurrentContext.Repository;

            message.OldApproverList = new List<ObjectId>();
            using (var transaction = repository.BeginGweTransaction())
            {
                var commitments = transaction
                    .Database
                    .Commitments
                    .GetCommitmentsForTarget(message.GweObject.Id);

                commitments
                    .DoForEach(c => message.OldApproverList.Add(ObjectId.Parse(c.CommittedUser.Value)));

                transaction.Commit();
            }

            message.NewApproverList = new List<ObjectId>();
            foreach (RepositoryObject person in GweStateMachine.GetFutureApprovers(message))
            {
                message.NewApproverList.Add(person.Id.Id);
            }

            foreach (ObjectId oldPersonOid in message.OldApproverList)
            {
                if (!message.NewApproverList.Contains(oldPersonOid))
                {
                    return true;
                }
            }
            foreach (ObjectId newPersonOid in message.NewApproverList)
            {
                if (!message.OldApproverList.Contains(newPersonOid))
                {
                    return true;
                }
            }
            return false;
        }
        public static bool IsInStateByRelation(GovernanceMethodData self, MessageBlock message)
        {
            bool checkResult = string.IsNullOrEmpty(InStateByRelation(self, message));
            return checkResult;
        }
        private static bool DoesContainThisPerson(RepositoryObject ObjectToCheck, string PersonID)
        {
            IConfiguration configuration = ObjectToCheck.GetConfiguration();

            switch (ObjectToCheck.Template)
            {
                case "Role":
                    {
                        var o = ObjectToCheck.GetAttributeValue("HasActor").ToHyperLinkList();
                        return DoesHyperLinkListContainUser(configuration, o, PersonID);
                    }
                case "OrganizationUnit":
                    {
                        var o = ObjectToCheck.GetAttributeValue("HasResponsible").ToHyperLinkList();
                        return DoesHyperLinkListContainUser(configuration, o, PersonID);
                    }
                case "Position":
                    {
                        var o = ObjectToCheck.GetAttributeValue("HasPositionHolder").ToHyperLinkList();
                        return DoesHyperLinkListContainUser(configuration, o, PersonID);
                    }
                case "InterestGroup":
                    {
                        var o = ObjectToCheck.GetAttributeValue("HasParticipant").ToHyperLinkList();
                        return DoesHyperLinkListContainUser(configuration, o, PersonID);
                    }
                case "Person":
                    return PersonID == ObjectToCheck.Id.Id.ToString();

                case "GovernanceRole":
                    {
                        var o = ObjectToCheck.GetAttributeValue("HasActor").ToHyperLinkList();
                        return DoesHyperLinkListContainUser(configuration, o, PersonID);
                    }
            }
            return false;
        }
        public static bool DiagramContentPermissions(GovernanceMethodData self, MessageBlock message)
        {
            RepositoryObject childContentObj = message.GweObject;

            string user = CallContext.QisServer.CurrentSession.UserId.ToString();

            IConfiguration configuration = message.GweObject.GetConfiguration();

            #region Governance Master Role Handling
            GweStateMachine gweStateMachine = new GweStateMachine(message.CurrentContext, CallContext.QisServer.CurrentSession.UserId, message.GweObject);

            if (gweStateMachine.Message.GovernanceDiagram != null)
            {
                foreach (var role in message.GovernanceDiagram.MasterGovernanceRole)
                {
                    if (DoesContainThisPerson(configuration.FindObject(role.Id), user))
                    {
                        return true;
                    }
                }
            }
            #endregion

			IAttributeValue childContentHasResponsibleValue = null;
			if (!childContentObj.Attributes.TryGetValue("HasResponsible", out childContentHasResponsibleValue))
			{
				childContentHasResponsibleValue = null;
			}
            //childContentHasResponsibleValue = childContentObj.GetAttributeValue("HasResponsible");
			
			IAttributeValue childContentHasOwnerValue = null;
			if (!childContentObj.Attributes.TryGetValue("OwnedBy", out childContentHasOwnerValue))
			{
				childContentHasOwnerValue = null;
			}
           // childContentHasOwnerValue = childContentObj.GetAttributeValue("OwnedBy");

            if (childContentHasResponsibleValue != null)//if (childContentHasResponsibleValue.ToString() != "")
            {
				if (childContentHasResponsibleValue.ToString() != "")
				{
					if (DoesContainThisPerson(configuration.FindObject(childContentHasResponsibleValue.ToHyperLinkList().FirstOrDefault()), user))
					{
                    return true;
					}
				}
				
            }
            else if (childContentHasOwnerValue != null)// else if (childContentHasOwnerValue.ToString() != "")
            {
                if(childContentHasOwnerValue.ToString() != "")
				{
					if (DoesContainThisPerson(configuration.FindObject(childContentHasOwnerValue.ToHyperLinkList().FirstOrDefault()), user))
					{
						return true;
					}
				}
				
            }
            //else
            //{
				//return true;
                RepositoryObjectList parentlist = childContentObj.GetContainedIn();

                foreach (RepositoryObject parent in parentlist)
                {
                    bool IsPersonInHasResponsible = DoesHyperLinkListContainUser(configuration, parent.GetAttributeValue("HasResponsible").ToHyperLinkList(), user);

                    bool IsPersonInOwnedBy = false;

                    if (!IsPersonInHasResponsible)
                    {
                        IsPersonInOwnedBy = DoesHyperLinkListContainUser(configuration, parent.GetAttributeValue("OwnedBy").ToHyperLinkList(), user);
                    }
                    if (IsPersonInHasResponsible || IsPersonInOwnedBy)
                    {
                        return true;
                    }
                }
           // }
            return false;
        }
        private static bool DoesHyperLinkListContainUser(IConfiguration configuration, QHyperLinkList list, string PersonObjId)
        {
            if (list != null)
            {
                foreach (var o in list)
                {
                    if (DoesContainThisPerson(configuration.FindObject(o), PersonObjId))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        public static bool ShowIfRelevantTemplate(GovernanceMethodData self, MessageBlock message)
        {
            RepositoryObject obj = message.GweObject;

            string template = obj.Template;

            var parameterPar = ExtractDynamicMethodParameters(self).ToArray();

            if (parameterPar.Length == 0)
            {
                return false;
            }
            string[] templates = parameterPar[0].Item2.Trim('{', '}', ' ').Split(new Char[] { ';' });

            foreach (var temp in templates)
            {
                if (temp == template)
                {
                    return true;
                }
            }

            return false;
        }

        #endregion

        #region GovernanceActionMethod ByCSScript
        //Deal with public methods: Action

        public static string PublishContent(GovernanceMethodData self, MessageBlock message)
        {
            var logWriter = message.CurrentContext.QisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));

            string result = c_resultOk;
            if (message == null)
            {
                result = "Can not publish: Empty message";
            }
            else
            {
                var messageFormat = ExtractMessageFormat(self, "Can not publish {0}: {1}");
                try
                {
                    if (message.GweObject == null)
                    {
                        result = string.Format(messageFormat, "", "Empty object");
                    }
                    else
                    {
                        var methodParameters = ExtractDynamicMethodParameters(self);
                        string publishMode = GetDynamicMethodParameterNamed(methodParameters, "PublishContent");
                        if (string.IsNullOrEmpty(publishMode))
                        {
                            publishMode = "Asynchronous";
                        }
                        switch (publishMode)
                        {
                            case "Synchronous":
                                result = DoPublishContent(message);
                                break;
                            case "Asynchronous":
                                result = DoPublishContentAsync(message);
                                break;
                        }
                        if (result != c_resultOk)
                        {
                            result = string.Format(messageFormat, message.GweObject.Name, result);
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = string.Format(messageFormat, message.GweObject.Name, ex.ToString());
                }
            }
            if (result != c_resultOk)
            {
                logWriter.Error(result);
            }
            return result;
        }
        public static string SetGWECycleOwner(GovernanceMethodData self, MessageBlock message)
        {
            string result = c_resultOk;
            try
            {
                message.CycleOwner = message.MessageActor;
                DataSet statusSet = message.AdoUtility.GetGweQuery(message.GweObject, "[ObjectCurrentStatus]");
                if (statusSet.Tables[0].Rows.Count == 1)
                {
                    if (message.CycleOwner.IsNotNull())
                    {
                        statusSet.Tables[0].Rows[0]["GWECycleOwner"] = message.CycleOwner.Id.Id.ToStringSafely();
                        message.AdoUtility.UpdateStatus(statusSet);
                    }
                    else
                    {
                        result = "Can not set GWECycleOwner to null";
                    }
                }
                else
                {
                    result = "Can not update GWECycleOwner";
                }
            }
            catch (Exception ex)
            {
                result = ex.ToString();
            }

            return result;
        }
        public static string CreateNewRevision(GovernanceMethodData self, MessageBlock message)
        {
            string result = c_resultOk;

            try
            {
                var parentObj = message.GweObject;

                if (parentObj.IsNotNull())
                {
                    var configuration = parentObj.GetConfiguration();
                    var repository = configuration.GetRepository();
                    var logWriter = message.CurrentContext.QisServer
                        .GetQefService<ILogService>()
                        .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));
                    var qisServer = message.CurrentContext.QisServer;
                    var lockResult = parentObj.GetLockStatus();
                    if (lockResult.LockCount > 0 && lockResult.SessionId != qisServer.CurrentSession.Id)
                    {
                        string fullUserName = Common.FindUserFullName(qisServer, lockResult.UserId);
                        result = string.Format("Can not create new revision.\n{0} is locked in another session by {1}.", parentObj.Name, fullUserName);
                        return result;
                    }
                    RepositoryObject newRevision = configuration.CreateObjectRevision(parentObj.Id);

                    var objectRevId = newRevision.Id.Revision;
                    var parentRevId = parentObj.Id.Revision;

                    if (objectRevId == parentRevId)
                    {
                        result = string.Format("Can not create new revision of {0}", parentObj.Name);
                    }
                    else
                    {
                        message.GweObject = newRevision;
                        try
                        {
                            var msgOnPublishing = FastPublishContent(self, message, parentObj);
                            if (!(string.IsNullOrEmpty(msgOnPublishing) || msgOnPublishing == "0"))
                            {
                                logWriter.Error(msgOnPublishing);
                            }
                        }
                        catch (Exception exOnPublishing)
                        {
                            logWriter.Error(exOnPublishing.ToString());
                        }
                    }
                }
                else
                {
                    result = "Can not create new revision of an empty object";
                }
            }
            catch (Exception ex)
            {
                result = string.Format(
                    "Cannot create new revision of {0}. Error:\n{1}",
                    message.GweObject.Name,
                    ex.ToString());
            }

            return result;
        }
        public static string FreezeObject(GovernanceMethodData self, MessageBlock message)
        {
            string result = c_resultOk;
            try
            {
                result = message.GweObject.IsNull()
                ? "Can not freeze a empty object"
                    : message.GweObject.IsFrozen
                        ? string.Format("Can not freeze {0}", message.GweObject.Name)
                : c_resultOk;

                if (result == c_resultOk)
                {
                    ObjectScripts.FreezeObject(message.GweObject);
                }
            }
            catch (Exception ex)
            {
                result = string.Format(
                    "Cannot freeze revision {0}. Error:\n{1}",
                    message.GweObject.Name,
                    ex.ToString());
            }

            return result;
        }
        public static string SetObjectAttributeList(GovernanceMethodData self, MessageBlock message)
        {
            var logWriter = message.CurrentContext.QisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));
            var debugMessages = new StringBuilder();
            debugMessages.Append("SetObjectAttributeList: ");
            List<string> results = new List<string>();
            string result = string.Empty;
            bool mustSaveData = false;
            bool mustUpdateStatus = false;
            bool canUpdateStatus = true;

            DataSet statusSet = message.AdoUtility.GetGweQuery(message.GweObject, "[ObjectCurrentStatus]");
            if (statusSet.Tables[0].Rows.Count != 1)
            {
                canUpdateStatus = false;
            }

            try
            {
                debugMessages.AppendLine();
                debugMessages.AppendFormat("\tDynamicMethod=[{0}]", self.Name);

                var parameterNameValues = ExtractDynamicMethodParameters(self);
                var messageFormat = ExtractMessageFormat(self, "Attribute {0} can not be updated with content[{1}]");

                foreach (var pair in parameterNameValues)
                {
                    var attributeName = pair.Item1.Trim();
                    var attributeValueText = pair.Item2.Trim();
                    IAttributeValue attributeValue = new PlainText(attributeValueText);
                    if (!attributeName.IsBlank() && !attributeValueText.IsBlank())
                    {

                        string[] arrayCheckShift = attributeValueText.Split(new string[] { "}+" }, StringSplitOptions.RemoveEmptyEntries);
                        string[] arrayShift = attributeValueText.Split(new string[] { "+" }, StringSplitOptions.RemoveEmptyEntries);
                        int integerShift = 0;
                        if (arrayCheckShift.Count() > 1 && !string.IsNullOrEmpty(arrayShift[1]))
                        {
                            if (int.TryParse(arrayShift[1], out integerShift))
                            {
                                attributeValueText = arrayShift[0];
                            }
                        }

                        if (message.GweObject.MetamodelAttributesContains(attributeValueText.Trim('{', '}', ' '))) // if the source value is a attribute
                        {
                            if (message.GweObject.Attributes.Contains(attributeValueText.Trim('{', '}', ' ')))
                            {
                                attributeValue = message.GweObject.Attributes[attributeValueText.Trim('{', '}', ' ')].Value;
                            }
                            else
                            {
                                attributeValue = null;
                            }

                            // attributeValue contains the object attribute value
                        }
                        else
                        {
                            attributeValue = GetDynamicVariable(message, attributeValueText).ToAttributeValue();
                            // attributeValue contains the governance attribute value, or a static value
                        }

                        attributeValueText = attributeValue.GetContent().Trim('{', '}', ' ');

                        if (message.GweObject.MetamodelAttributesContains(attributeName.Trim('{', '}', ' '))) // if the target is an attribute to be changed
                        {
                            result = SetGweAttributeQlm(message.GweObject, attributeName.Trim('{', '}', ' '), attributeValue, integerShift);
                            if (!result.IsBlank())
                            {
                                results.Add(result);
                            }

                            mustSaveData = true;
                        }
                        else // if the target is not an attribute to be changed, it must be a governance attribute to be changed
                        {
                            if (statusSet.Tables[0].Columns.Contains(ExtractStatusAttributeName(attributeName)))
                            {
                                if (canUpdateStatus)
                                {
                                    result = SetGweAttributeStatus(message.GweObject, statusSet, attributeName, attributeValue);
                                    if (!result.IsBlank())
                                    {
                                        results.Add(result);
                                    }

                                    mustUpdateStatus = true;
                                }
                                else
                                {
                                    debugMessages.AppendLine();
                                    debugMessages.AppendFormat(messageFormat, attributeName, attributeValueText);
                                    results.Add(string.Format(messageFormat, attributeName, attributeValueText));
                                }
                            }
                            else
                            {
                                debugMessages.AppendLine();
                                debugMessages.AppendFormat(messageFormat, attributeName, attributeValueText);
                                results.Add(string.Format(messageFormat, attributeName, attributeValueText));
                            }
                        }

                        debugMessages.AppendLine();
                        debugMessages.AppendFormat("\t\tAttr[{0}]=[{1}]", attributeName, attributeValueText);
                    }
                }

                if (mustSaveData)
                {
                    message.GweObject.SaveData();
                }

                if (mustUpdateStatus)
                {
                    message.AdoUtility.UpdateStatus(statusSet);
                }
            }
            catch (Exception ex)
            {
                results.Add(string.Format("{0} - {1}", "SetObjectAttributeList", ex.ToString()));
            }

            logWriter.DebugDynamicCodeL2(debugMessages.ToString());

            return results.Count > 0
                ? string.Join("\n", results.ToArray())
            : c_resultOk;
        }
        public static string SetAttributeListByRelation(GovernanceMethodData self, MessageBlock message)
        {
            RepositoryObject saveGweObject = message.GweObject;
            var configuration = message.CurrentContext;
            List<string> results = new List<string>();
            var messageFormat = ExtractMessageFormat(self, "{0}: {1} - ByRelation Error Message:\n{2}");
            var methodParameters = ExtractDynamicMethodParameters(self);
            string templates = GetDynamicMethodParameterNamed(methodParameters, "TemplateFilter");
            string relations = GetDynamicMethodParameterNamed(methodParameters, "Relations");
            if (!relations.IsBlank())
            {
                RepositoryObjectList objectList = GetListByRelation(message.GweObject, relations, templates, message.CycleOwner);
                if (objectList.Count > 0)
                {
                    string cmsAnswer = string.Empty;
                    foreach (RepositoryObject repObject in objectList)
                    {
                        try
                        {
                            message.GweObject = repObject;
                            cmsAnswer = SetObjectAttributeList(self, message);
                            if (cmsAnswer != "0" && !string.IsNullOrEmpty(cmsAnswer))
                            {
                                results.Add(cmsAnswer);
                            }
                        }
                        catch (Exception ex)
                        {
                            results.Add(string.Format(messageFormat, MethodBase.GetCurrentMethod().Name, repObject.Name, ex.ToString()));
                        }
                    }
                }
            }
            message.GweObject = saveGweObject;
            return results.Count > 0
                ? string.Join(Environment.NewLine, results.ToArray())
                : c_resultOk;
        }
        public static string ResetCanApprove(GovernanceMethodData self, MessageBlock message)
        {
            var logWriter = message.CurrentContext.QisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));
            var debugMessages = new StringBuilder();
            debugMessages.Append("ResetCanApprove: ");
            string result = c_resultOk;
            bool mustSaveData = false;
            try
            {
                debugMessages.AppendLine();
                debugMessages.AppendFormat("\tDynamicMethod=[{0}]", self.Name);

                if (message.GweObject.MetamodelAttributesContains("CanApprove"))
                {
                    var canApproveList = message.GweObject.Attributes
                        .GetListViewLinks("CanApprove")
                        .ToObjects(message.CurrentContext.Configuration);
                    if (canApproveList.Any())
                    {
                        foreach (var canApprove in canApproveList)
                        {
                            message.AdoUtility.DeleteCanPlay(message.GweObject, canApprove);
                        }
                        message.GweObject.Attributes["CanApprove"].Value.Clear();
                        mustSaveData = true;
                    }
                }

                if (mustSaveData)
                {
                    message.GweObject.SaveData();
                }

            }
            catch (Exception ex)
            {
                result = string.Format("{0} - {1}", "ResetCanApprove", ex.ToString());
            }

            logWriter.DebugDynamicCodeL2(debugMessages.ToString());

            return result;
        }
        public static string ResetCanApproveByRelation(GovernanceMethodData self, MessageBlock message)
        {
            var logWriter = message.CurrentContext.QisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));
            var debugMessages = new StringBuilder();
            debugMessages.Append("ResetCanApprove: ");
            List<string> results = new List<string>();
            string result = string.Empty;
            bool mustSaveData = false;
            try
            {
                debugMessages.AppendLine();
                debugMessages.AppendFormat("\tDynamicMethod=[{0}]", self.Name);

                var messageFormat = ExtractMessageFormat(self, "{0}: {1} - ByRelation Error Message:\n{2}");
                var methodParameters = ExtractDynamicMethodParameters(self);
                string templates = GetDynamicMethodParameterNamed(methodParameters, "TemplateFilter");
                string relations = GetDynamicMethodParameterNamed(methodParameters, "Relations");
                if (!relations.IsBlank())
                {
                    RepositoryObjectList objectList = GetListByRelation(message.GweObject, relations, templates, message.CycleOwner);
                    if (objectList.Count > 0)
                    {
                        string cmsAnswer = string.Empty;
                        foreach (RepositoryObject repObject in objectList)
                        {
                            if (repObject.MetamodelAttributesContains("CanApprove"))
                            {
                                var canApproveList = repObject.Attributes
                                    .GetListViewLinks("CanApprove")
                                    .ToObjects(message.CurrentContext.Configuration);
                                if (canApproveList.Any())
                                {
                                    foreach (var canApprove in canApproveList)
                                    {
                                        message.AdoUtility.DeleteCanPlay(repObject, canApprove);
                                    }
                                    repObject.Attributes["CanApprove"].Value.Clear();
                                    mustSaveData = true;
                                }
                            }
                            if (mustSaveData)
                            {
                                repObject.SaveData();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                results.Add(string.Format("{0} - {1}", "ResetCanApprove", ex.ToString()));
            }

            logWriter.DebugDynamicCodeL2(debugMessages.ToString());

            return results.Count > 0
                ? string.Join("\n", results.ToArray())
            : c_resultOk;
        }
        public static string Approve(GovernanceMethodData self, MessageBlock message)
        {
            string messageFormat = ExtractMessageFormat(self, "Approve: Error on {0}: {1}");

            string result = c_resultOk;
            try
            {
                CmsApproveAttribute cmsApproveAttribute = new CmsApproveAttribute();
                cmsApproveAttribute.ApproveContent = true;
                cmsApproveAttribute.SendMailNotification = false;
                cmsApproveAttribute.RollbackOnFailure = false;
                cmsApproveAttribute.SkipGweContent = true;
                cmsApproveAttribute.SetApprovalState = "GovernanceStateBefore";

                cmsApproveAttribute = FillApproveAttribute(self, message, cmsApproveAttribute);

                if (cmsApproveAttribute.SetApprovalState == "ApprovalState")
                    cmsApproveAttribute.SetApprovalState = CMS.c_approvedApprovalState;
                else if (cmsApproveAttribute.SetApprovalState == "GovernanceStateBefore")
                    cmsApproveAttribute.SetApprovalState = message.GovernanceStateBefore?.Name;
                else if (cmsApproveAttribute.SetApprovalState == "GovernanceStateAfter")
                    cmsApproveAttribute.SetApprovalState = message.GovernanceStateAfter?.Name;

                CMS.ApproveObject(
                    message.CurrentContext.QisServer.GetQefService<IUserStorageService>(),
                    message.CurrentContext.QisServer.GetQefService<IMailService>(),
                    message.GweObject,
                    message.CurrentContext.QisServer.CurrentSession.UserId,
                    cmsApproveAttribute.SetApprovalState,
                    cmsApproveAttribute.ApproveContent,
                    cmsApproveAttribute.SendMailNotification,
                    rollbackOnFailure: cmsApproveAttribute.RollbackOnFailure,
                    adminApproval: true,
                    SkipGweContent: cmsApproveAttribute.SkipGweContent);

                //msg += string.Format("CMS.ApproveObject: {0}ms",sw.ElapsedMilliseconds - prev);
                //prev = sw.ElapsedMilliseconds;
            }
            catch (Exception ex)
            {
                result = string.Format(messageFormat, message.GweObject.Name, ex.ToString());
            }

            //logger.Debug(string.Format("GWE_DynamicCode.Approve: {0}ms", sw.ElapsedMilliseconds));

            return result;
        }
        public static string Promote(GovernanceMethodData self, MessageBlock message)
        {
            var logWriter = message.CurrentContext.QisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));
            List<string> results = new List<string>();
            RepositoryObject objectToBePromoted = message.GweObject;
            IConfiguration configuration = objectToBePromoted.GetConfiguration();
            IConfigurationData configData = configuration.GetData();
            var debugMessages = new StringBuilder();
            debugMessages.Append("Promote: ");

            var messageFormat = ExtractMessageFormat(self, "Promote: Error on {0}: {1}");
            if (!configData.IsPrivateWorkspace)
            {
                string errorText = "Cannot promote object since current configuration is not a private workspace : {0}";
                results.Add(string.Format(errorText, configData.Name));
            }
            else
            {
                try
                {
                    CmsPromoteAttribute promoteAttribute = new CmsPromoteAttribute();
                    promoteAttribute.PromoteContent = true;
                    promoteAttribute.SkipGweContent = true;
                    promoteAttribute = FillPromoteAttribute(self, message, promoteAttribute);

                    OidList toPromote = GetObjectIdsToPromote(configuration, objectToBePromoted.Id, promoteAttribute);
                    if (toPromote.Count == 0)
                    {
                        debugMessages.AppendLine();
                        debugMessages.Append("No changes found comparing to the base configuration. Nothing to promote.");
                    }
                    else
                    {
                        OidResultList promotionResults = configuration.PromoteToBaseConfiguration(toPromote);
                        var promotionObjects = configuration.GetBaseConfiguration().GetObjectSet(new OidList(promotionResults.Select(x => x.Id)));
                        foreach (OidResult promotionResult in promotionResults)
                        {
                            var promotedObject = promotionObjects.First(x => x.Id == promotionResult.Id);
                            if (promotionResult.Error == null)
                            {
                                debugMessages.AppendLine();
                                debugMessages.AppendFormat("Success {0}", promotedObject.Name);
                                if (message.GweObject.Id.Id == promotedObject.Id.Id)
                                {
                                    message.GweObject = promotedObject;
                                }
                                try
                                {
                                    var msgOnPublishing = FastPublishContent(self, message, objectToBePromoted);
                                    if (!(string.IsNullOrEmpty(msgOnPublishing) || msgOnPublishing == "0"))
                                    {
                                        logWriter.Error(msgOnPublishing);
                                    }
                                }
                                catch (Exception exOnPublishing)
                                {
                                    logWriter.Error(exOnPublishing.ToString());
                                }
                            }
                            else
                            {
                                debugMessages.AppendLine();
                                debugMessages.AppendFormat(messageFormat, promotedObject.Name, promotionResult.Error.Message);

                                results.Add(string.Format(messageFormat, promotedObject.Name, promotionResult.Error.Message));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    results.Add(string.Format("{0}: {1}", MethodBase.GetCurrentMethod().Name, ex.ToString()));
                }
            }

            logWriter.DebugDynamicCodeL2(debugMessages.ToString());

            return results.Count > 0
                ? string.Join(Environment.NewLine, results.ToArray())
            : c_resultOk;
        }
        public static string CreateLanguageVariants(GovernanceMethodData self, MessageBlock message)
        {
            var logWriter = message.CurrentContext.QisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));
            var results = new List<string>();
            var ObjBaseLanguage = message.GweObject.Language;
            var ObjBaseLanguageInfo = message.GweObject.GetLanguageInfo();
            IConfiguration configuration = message.GweObject.GetConfiguration();
            IConfigurationData configData = configuration.GetData();
            IRepository repository = configuration.GetRepository();
            var currentLanguage = repository.CurrentLanguage;
            var debugMessages = new StringBuilder();
            debugMessages.Append("CreateLanguageVariants: ");

            var messageFormat = ExtractMessageFormat(self, "CreateLanguageVariants: Error on {0}: {1}");
            if (ObjBaseLanguage != currentLanguage)
            {
                string errorText = "Cannot create Language variant since object language ({0}) is different from current repository Language ({1})";
                results.Add(string.Format(errorText, ObjBaseLanguage, currentLanguage));
            }
            else
            {
                try
                {
                    LanguageIdList langList = repository.GetData().WorkingLanguages;

                    var query =
                        new SelectExpression(
                        new ColumnExpressions(new[] { new ObjectIdExpression() }),
                        new TemplateSetTable(new string[] { message.GweObject.Template }),
                        new WhereExpression(
                            new EqualOperationExpression(
                                new AttributeValueOperand(PredefinedObjectAttribute.ObjectId.Name),
                                new ConstantOperand(message.GweObject.Id.Id.ToString()))),
                        new OrderExpression(new OrderItemExpression(new AttributeValueOperand(PredefinedObjectAttribute.LocalRevision.Name))));
                    var options = new QueryOptions { Comparison = SqlComparison.DefaultCaseInsensitive, IncludeDeleted = false };
                    options.Scope = ObjectsScope.DefaultRevisionsAndLanguageVariants;
                    RepositoryObjectList languageVariantList = new RepositoryObjectList(
                               repository.RunQsqlExpression(query, configuration.Id, options));
                    foreach (var lang in langList)
                    {
                        bool langNotExists = true;
                        foreach (var exitentObject in languageVariantList)
                        {
                            if (exitentObject.Language == lang)
                            {
                                langNotExists = false;
                            }

                        }
                        if (langNotExists)
                        {
                            RepositoryObject newObjectLangVar = configuration.CreateObjectLanguageVariant(message.GweObject.Id, lang);
                        }
                    }
                }
                catch (Exception ex)
                {
                    results.Add(string.Format("{0}: {1}", MethodBase.GetCurrentMethod().Name, ex.ToString()));
                }
            }

            logWriter.DebugDynamicCodeL2(debugMessages.ToString());

            return results.Count > 0
                ? string.Join(Environment.NewLine, results.ToArray())
            : c_resultOk;
        }
        public static string CreateAcknowledgeList(GovernanceMethodData self, MessageBlock message)
        {
            var results = new List<string>();
            var messageFormat = ExtractMessageFormat(self, "CreateAcknowledgeList: Error on {0}: {1}");
            try
            {
                var configurationCtx = message.CurrentContext;
                var revisionToAcknowlege = message.GweObject.Id;
                var takenBy = message.GweObject.GetConfiguration().ResolveUserObject();
                var gweAdoUtility = message.AdoUtility;

                var sourceTable = RepositoryGWE.GetAcknowledgeSource(configurationCtx, revisionToAcknowlege);
                if (sourceTable.RowsCount > 0)
                {
                    var ObjectDictionary = GweAdoUtility.GetObjectsInValueTable(configurationCtx, sourceTable);
                    var oldsource = new Oid($"{sourceTable.GetValue(0, "AcknowledgeList")}");
                    var sourceRevision = configurationCtx.Configuration.FindObject(oldsource);
                    List<ObjectId> personList = new List<ObjectId>();
                    for (int i = 0; i < sourceTable.RowsCount; i++)
                    {
                        var currentSource = new Oid($"{sourceTable.GetValue(i, "AcknowledgeList")}");

                        if (currentSource.Revision != oldsource.Revision)
                        {
                            personList = new List<ObjectId>(personList.Distinct());
                            if (!gweAdoUtility.RefreshAcknowledge(revisionToAcknowlege, personList, takenBy.Id.Id, sourceRevision))
                            {
                                results.Add(string.Format("Error on AcknowledgeList [{0}].", sourceRevision.Name)); ;
                            }
                            sourceRevision = configurationCtx.Configuration.FindObject(currentSource);
                            personList = new List<ObjectId>();
                            oldsource = currentSource;
                        }

                        var objectId = sourceTable.GetValue<ObjectId>(i, "ObjectId");
                        var revisionId = sourceTable.GetValue<RevisionId>(i, "RevisionId");
                        var oidRepositoryObject = new Oid(objectId, revisionId);

                        RepositoryObject whoMustAcknowlegedRevision;
                        ObjectDictionary.TryGetValue(oidRepositoryObject, out whoMustAcknowlegedRevision);
                        if (whoMustAcknowlegedRevision != null)
                        {
                            foreach (var personId in PersonStructure.GetListOfPersonId(configurationCtx, whoMustAcknowlegedRevision))
                            {
                                personList.Add(personId);
                            }

                        }
                    }
                    if (personList.Count > 0)
                    {
                        personList = new List<ObjectId>(personList.Distinct());
                        if (!gweAdoUtility.RefreshAcknowledge(revisionToAcknowlege, personList, takenBy.Id.Id, sourceRevision))
                        {
                            results.Add(string.Format(messageFormat, message.GweObject.Name, $"Can not generate acknowledge list"));
                        }
                    }
                }
                else
                {
                    results.Add(string.Format(messageFormat, message.GweObject.Name, "There is no acknowledge list"));
                }
            }
            catch (Exception ex)
            {
                results.Add(string.Format(messageFormat, message.GweObject.Name, ex.ToString()));
            }

            return results.Count > 0 ? string.Join("\n", results.ToArray()) : "0";
        }
        public static string ApproveByRelation(GovernanceMethodData self, MessageBlock message)
        {
            return ExecuteCmsByRelation(self, "Approve", message);
        }
        public static string PromoteByRelation(GovernanceMethodData self, MessageBlock message)
        {
            return ExecuteCmsByRelation(self, "Promote", message);
        }
        public static string FreezeByRelation(GovernanceMethodData self, MessageBlock message)
        {
            return ExecuteCmsByRelation(self, "Freeze", message);
        }
        public static string NewRevisionByRelation(GovernanceMethodData self, MessageBlock message)
        {
            return ExecuteCmsByRelation(self, "NewRevision", message);
        }
        public static string AcknowledgeListByRelation(GovernanceMethodData self, MessageBlock message)
        {
            return ExecuteCmsByRelation(self, "AcknowledgeList", message);
        }
        public static string PostByRelation(GovernanceMethodData self, MessageBlock message)
        {
            IConfiguration configuration = message.CurrentContext.Configuration;
            RepositoryObject localObject = message.GweObject;
            if (configuration.Id != localObject.GetConfiguration().Id)
            {
                localObject = configuration.FindObject(localObject.Id);
            }
            List<string> results = new List<string>();
            var messageFormat = ExtractMessageFormat(self, "{0}: {1} - Post Error Message:\n{2}");
            var methodParameters = ExtractDynamicMethodParameters(self);
            string templates = GetDynamicMethodParameterNamed(methodParameters, "TemplateFilter");
            string relations = GetDynamicMethodParameterNamed(methodParameters, "Relations");
            string postStrategy = GetDynamicMethodParameterNamed(methodParameters, "PostStrategy");
            if (string.IsNullOrEmpty(postStrategy))
            {
                postStrategy = "Asynchronous";
            }
            if (!relations.IsBlank())
            {
                RepositoryObjectList objectList = GetListByRelation(localObject, relations, templates, message.CycleOwner);
                if (objectList.Count > 0)
                {
                    foreach (RepositoryObject repObject in objectList)
                    {
                        try
                        {
                            switch (postStrategy)
                            {
                                case "Synchronous":
                                    DoPostObject(message.CurrentContext, repObject);
                                    break;
                                case "Asynchronous":
                                    DoPostObjectAsync(message.CurrentContext, repObject);
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            results.Add(string.Format(messageFormat, MethodBase.GetCurrentMethod().Name, repObject.Name, ex.ToString()));
                        }
                    }
                }
            }
            return results.Count > 0
                ? string.Join(Environment.NewLine, results.ToArray())
                : c_resultOk;
        }
        public static string EventActionByRelation(GovernanceMethodData self, MessageBlock message)
        {
            IConfiguration configuration = message.CurrentContext.Configuration;
            RepositoryObject localObject = message.GweObject;
            if (configuration.Id != localObject.GetConfiguration().Id)
            {
                localObject = configuration.FindObject(localObject.Id);
            }
            List<string> results = new List<string>();
            var messageFormat = ExtractMessageFormat(self, "{0}: {1} - Event Action Error Message:\n{2}");
            var methodParameters = ExtractDynamicMethodParameters(self);
            string eventMessageId = GetDynamicMethodParameterNamed(methodParameters, "GovernanceEventMessage");
            var eventMessage = message.CurrentContext.Configuration.FindObject(new Oid(eventMessageId));
            string templates = GetDynamicMethodParameterNamed(methodParameters, "TemplateFilter");
            string relations = GetDynamicMethodParameterNamed(methodParameters, "Relations");
            string eventStrategy = GetDynamicMethodParameterNamed(methodParameters, "Strategy");
            if (string.IsNullOrEmpty(eventStrategy))
            {
                eventStrategy = "Asynchronous";
            }
            if (!relations.IsBlank())
            {
                RepositoryObjectList objectList = GetListByRelation(localObject, relations, templates, message.CycleOwner);
                if (objectList.Count > 0)
                {
                    foreach (RepositoryObject repObject in objectList)
                    {
                        try
                        {
                            var x = message.HistoryId;
                            switch (eventStrategy)
                            {
                                case "Synchronous":
                                    DoEventAction(message.CurrentContext, eventMessage, repObject, message.HistoryId);
                                    break;
                                case "Asynchronous":
                                    DoEventActionAsync(message.CurrentContext, eventMessage, repObject, message.HistoryId);
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            results.Add(string.Format(messageFormat, MethodBase.GetCurrentMethod().Name, repObject.Name, ex.ToString()));
                        }
                    }
                }
            }
            return results.Count > 0
                ? string.Join(Environment.NewLine, results.ToArray())
                : c_resultOk;
        }
        public static string OnAcknowledgeListChange(GovernanceMethodData self, MessageBlock message)
        {
            IConfiguration configuration = message.CurrentContext.Configuration;
            RepositoryObject localObject = message.GweObject;
            if (configuration.Id != localObject.GetConfiguration().Id)
            {
                localObject = configuration.FindObject(localObject.Id);
            }
            List<string> results = new List<string>();
            var messageFormat = ExtractMessageFormat(self, "{0}: {1} - Acknowledge List Change Error Message:\n{2}");
            var methodParameters = ExtractDynamicMethodParameters(self);

            string notificationId = GetDynamicMethodParameterNamed(methodParameters, "NewUsers");
            var NewUsersNotification = message.CurrentContext.Configuration.FindObject(new Oid(notificationId));
            notificationId = GetDynamicMethodParameterNamed(methodParameters, "RemovedUsers");
            var RemovedUsersNotification = message.CurrentContext.Configuration.FindObject(new Oid(notificationId));
            notificationId = GetDynamicMethodParameterNamed(methodParameters, "ObjectChanges");
            var ObjectChangesNotification = message.CurrentContext.Configuration.FindObject(new Oid(notificationId));

            List<string> entityStringList = new List<string>();
            GetDynamicMethodParameterNamed(methodParameters, "StateFilter")
                    .Split(new Char[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                    .DoForEach(item =>
                    {
                        ObjectId objectId;
                        if (ObjectId.TryParse(item, out objectId))
                        {
                            entityStringList.Add(objectId.ToString());
                        }
                    });

            if (message.GweObject.Template != "AcknowledgeList")
            {
                return string.Format(messageFormat, MethodBase.GetCurrentMethod().Name, message.GweObject.Name, "Only deal with AcknowledgeList");
            }

            var takenBy = message.GweObject.GetConfiguration().ResolveUserObject();

            var currentAckKRevision = message.GweObject;
            var parentAckLRevision = configuration.FindObject(new Oid(currentAckKRevision.Id.Id + "/" + currentAckKRevision.ParentRevision));
            if (message.OldData != null) //If we are monitoring changes in the current revision of the object, the parent revision is not necessary
            {
                parentAckLRevision = message.OldData;
            }

            var whoMustAcknowledge = PersonStructure.GetListOfPerson(
                message.CurrentContext, 
                currentAckKRevision.AttributeGetLinksSafely("WhoMustAcknowledgeList"));
            List<ObjectId> whoMustAcknowledgeList = new List<ObjectId>();
            foreach (var person in whoMustAcknowledge)
            {
                whoMustAcknowledgeList.Add(person.Id.Id);
            }

            var includedPersons = whoMustAcknowledge; 
            var removedPersons = new RepositoryObjectList();
            var unchangedPersons = new RepositoryObjectList();

            var toBeAcknowledged =  new RepositoryObjectList(
                currentAckKRevision.AttributeGetLinksSafely("ToBeAcknowledged")
                    .Where(entity => (entityStringList.Count() == 0 || entityStringList.Contains(entity.GweState?.HyperLink?.ObjectId.ToString()))));
            var includedRevisions = toBeAcknowledged;
            var removedRevisions = new RepositoryObjectList();
            var unchangedRevisions = new RepositoryObjectList();

            if (parentAckLRevision != null)
            {
                var whoMustAcknowledgeFromParent = PersonStructure.GetListOfPerson(
                    message.CurrentContext, 
                    parentAckLRevision.AttributeGetLinksSafely("WhoMustAcknowledgeList"));
                if (whoMustAcknowledgeFromParent.Any())
                {
                    removedPersons = new RepositoryObjectList(whoMustAcknowledgeFromParent.Except(whoMustAcknowledge, RepositoryObjectByIdComparer.Instance));
                    includedPersons = new RepositoryObjectList(whoMustAcknowledge.Except(whoMustAcknowledgeFromParent, RepositoryObjectByIdComparer.Instance));
                    unchangedPersons = new RepositoryObjectList(whoMustAcknowledge.Intersect(whoMustAcknowledgeFromParent, RepositoryObjectByIdComparer.Instance));
                }

                var toBeAcknowledgedFromParent = new RepositoryObjectList(parentAckLRevision.AttributeGetLinksSafely("ToBeAcknowledged")
                                        .Where(entity => (entityStringList.Count() == 0 || entityStringList.Contains(entity.GweState?.HyperLink?.ObjectId.ToString())))).ToArray();
                if (toBeAcknowledgedFromParent.Any())
                {
                    removedRevisions = new RepositoryObjectList(toBeAcknowledgedFromParent.Except(toBeAcknowledged, RepositoryObjectByIdComparer.Instance));
                    includedRevisions = new RepositoryObjectList(toBeAcknowledged.Except(toBeAcknowledgedFromParent, RepositoryObjectByIdComparer.Instance));
                    unchangedRevisions = new RepositoryObjectList(toBeAcknowledged.Intersect(toBeAcknowledgedFromParent, RepositoryObjectByIdComparer.Instance));
                }
            }

            if (!includedRevisions.Any() && !removedRevisions.Any() && 
                !includedPersons.Any() && !removedPersons.Any())
            {
                return "No changes";
            }

            try
            {
                foreach (RepositoryObject revision in includedRevisions)
                {
                    if (message.AdoUtility.RefreshAcknowledge(revision.Id, whoMustAcknowledgeList, takenBy.Id.Id, message.GweObject))
                    {
                        RegistrySubTaskResult(message, c_resultOk, revision.Id.Id.ToString(), "Refresh Acknowledge Engine");
                    }
                    else
                    {
                        results.Add(string.Format(messageFormat, revision.Name, "Error on Refresh Acknowledge"));
                    }
                }
                foreach (RepositoryObject revision in removedRevisions)
                {
                    if (message.AdoUtility.RefreshAcknowledge(revision.Id, whoMustAcknowledgeList, takenBy.Id.Id, message.GweObject))
                    {
                        RegistrySubTaskResult(message, c_resultOk, revision.Id.Id.ToString(), "Refresh Acknowledge Engine");
                    }
                    else
                    {
                        results.Add(string.Format(messageFormat, revision.Name, "Error on Refresh Acknowledge"));
                    }
                }
                if (includedPersons.Any() || removedPersons.Any())
                {
                    foreach (RepositoryObject revision in unchangedRevisions)
                    {
                        if (message.AdoUtility.RefreshAcknowledge(revision.Id, whoMustAcknowledgeList, takenBy.Id.Id, message.GweObject))
                        {
                            RegistrySubTaskResult(message, c_resultOk, revision.Id.Id.ToString(), "Refresh Acknowledge Engine");
                        }
                        else
                        {
                            results.Add(string.Format(messageFormat, revision.Name, "Error on Refresh Acknowledge"));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                results.Add(string.Format(messageFormat, message.GweObject.Name, ex.ToString()));
            }

            if (ObjectChangesNotification != null && unchangedPersons.Count() > 0 && ((includedRevisions.Count() + removedRevisions.Count()) > 0))
            {
                PostAcknowledgedEmailTask(message, ObjectChangesNotification, unchangedPersons, includedRevisions, removedRevisions, unchangedRevisions);
            }
            if (NewUsersNotification != null && includedPersons.Count() > 0)
            {
                PostAcknowledgedEmailTask(message, NewUsersNotification, includedPersons, includedRevisions, removedRevisions, unchangedRevisions);
            }
            if (RemovedUsersNotification != null && removedPersons.Count() > 0)
            {
                PostAcknowledgedEmailTask(message, RemovedUsersNotification, removedPersons, includedRevisions, removedRevisions, unchangedRevisions);
            }

            return results.Count > 0
                ? string.Join(Environment.NewLine, results.ToArray())
                : c_resultOk;
        }
        public static string UpdateValidFromAndValidTo(GovernanceMethodData self, MessageBlock message)
        {
            var repository = message.CurrentContext.Repository;
            var parent = message.GweObject;
            if (!CanBeValidByDate(parent))
            {
                return $"Diagram {parent.Name} is not validatable by date.";
            }

            var lockStatus = repository.LockObject(parent.Id);
            if (lockStatus.Type != ObjectLockResultType.Locked)
            {
                return string.Format("Revision {0} was not locked. Status: {1}", parent.Name, lockStatus.Type);
            }

            const string c_GWE_Method_Parameter_ValidFrom = "Valid(Days)";
            const string c_GWE_Method_Parameter_ExcludeConnections = "ExcludeConnections";
            const string c_GWE_Method_Parameter_SkipAllContent = "SkipAllContent";
            const string c_GWE_Method_Parameter_SkipLockedContent = "SkipLockedContent";

            var parameters = ExtractDynamicMethodParameters(self);
            var validDays = GetDynamicMethodParameterNamed(parameters, c_GWE_Method_Parameter_ValidFrom).ToInt(365);
            var excludeConnections = GetDynamicMethodParameterNamed(parameters, c_GWE_Method_Parameter_ExcludeConnections).ToBoolean(true);
            var skipAllContent = GetDynamicMethodParameterNamed(parameters, c_GWE_Method_Parameter_SkipAllContent).ToBoolean();
            var skipLockedContent = GetDynamicMethodParameterNamed(parameters, c_GWE_Method_Parameter_SkipLockedContent).ToBoolean();

            var toBeSaved = new RepositoryObjectList();

            var parentValidFrom = ProcessRevisionValidFrom(parent);
            var parentValidTo = ProcessRevisionValidTo(parent, parentValidFrom, validDays);
            toBeSaved.Add(parent);

            if (skipAllContent)
            {
                toBeSaved.SaveData(PostActions.ReleaseLock);
                return string.Empty;
            }

            var children = parent.GetContains().WithFullData();
            if (excludeConnections)
            {
                children = new RepositoryObjectList(children.Where(x => !c_connectionTemplateList.Contains(x.Template)))
                    .WithFullData();
            }

            var result = new StringBuilder();
            var toBeFrozen = new OidList();

            foreach (var child in children)
            {
                if (!CanBeValidByDate(child))
                {
                    continue;
                }

                var childValidFrom = child.Attributes.GetDateTime(c_validFromString);
                var childValidTo = child.Attributes.GetDateTime(c_validToString);
                if ((childValidFrom.HasValue && childValidFrom.Value <= parentValidFrom) &&
                    (childValidTo.HasValue && childValidTo.Value >= parentValidTo))
                {
                    continue;
                }

                if (skipLockedContent && child.IsFrozen)
                {
                    continue;
                }

                var childLockStatus = repository.LockObject(child.Id);
                if (childLockStatus.Type != ObjectLockResultType.Locked)
                {
                    if (!skipLockedContent)
                    {
                        result.AppendLine(string.Format("Revision {0} was not locked. Status: {1}", child.Name, lockStatus.Type));
                    }
                    continue;
                }

                var targetChild = child;
                if (child.IsFrozen)
                {
                    targetChild = message.CurrentContext.Configuration.CreateObjectRevision(targetChild.Id);
                    toBeFrozen.Add(targetChild.Id);
                }

                if (!childValidFrom.HasValue || childValidFrom.Value > parentValidFrom)
                {
                    targetChild.SetAttributeValue(c_validFromString, new QDateTime(parentValidFrom));
                }

                if (!childValidTo.HasValue || childValidTo.Value < parentValidTo)
                {
                    targetChild.SetAttributeValue(c_validToString, new QDateTime(parentValidTo));
                }

                toBeSaved.Add(targetChild);
            }

            var savingResult = toBeSaved.SaveData(PostActions.ReleaseLock);
            savingResult
                .Where(r => r.IsError)
                .DoForEach(r => result.AppendLine(r.Error.BuildMessage()));

            if (toBeFrozen.Any())
            {
                var freezingResult = repository.FreezeObjectSet(toBeFrozen);
                freezingResult
                    .Where(r => r.IsError)
                    .DoForEach(r => result.AppendLine(r.Error.BuildMessage()));
            }

            return result.ToString();
        }
        public static string UpdateParticipantList(GovernanceMethodData self, MessageBlock message)
        {
            var nextApprovers = GweStateMachine.GetFutureApprovers(message);
            message.AdoUtility.UpdateParticipants(message.GweObject, nextApprovers);

            return null;
        }
        public static string GlobalUpdateParticipantList(GovernanceMethodData self, MessageBlock message)
        {
            try
            {
                if (message.GweObject != null)
                {
                    throw new QInvalidOperationException("Global update participant list can not be executed at Object level");
                }
                var conditionalMessage = message.MessageReceived as GovernanceConditionalMessage;
                if (conditionalMessage != null)
                {
                    if (conditionalMessage.Level != GovernanceConditionalMessageLevel.WorkflowLevel)
                    {
                        throw new QInvalidOperationException("Global update participant list must be executed at workflow level");
                    }
                    else
                    {
                        var personMailTobeSentDictionary = new Dictionary<ObjectId, PersonMailToBeSentDictionaryEntry>();
                        RepositoryGWE.UpdatePersonMailTobeSentDictionary(personMailTobeSentDictionary, message);
                    }
                }
                else
                {
                    throw new QInvalidOperationException("Action can only be fired at conditional message");
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "";
        }
        public static string EmailAllParticipants(GovernanceMethodData self, MessageBlock message)
        {
            try
            {
                if (message.GweObject != null)
                {
                    throw new QInvalidOperationException("Email all participant can not be executed at Object level");
                }
                var workflowLevel = false;
                var conditionalMessage = message.MessageReceived as GovernanceConditionalMessage;
                if (conditionalMessage != null)
                {
                    if (conditionalMessage.Level != GovernanceConditionalMessageLevel.GovernanceLevel)
                    {
                        if (conditionalMessage.Level != GovernanceConditionalMessageLevel.WorkflowLevel)
                        {
                            throw new QInvalidOperationException("Email all participant Must be executed at workflow level");
                        }
                        else
                        {
                            workflowLevel = true;
                            if (message.GovernanceDiagram == null)
                            {
                                throw new QInvalidOperationException("Workflow must be defined to execute at workflow level");
                            }
                        }
                    }
                }
                else
                {
                    throw new QInvalidOperationException("Action can only be fired at conditional message");
                }

                var configuration = message.CurrentContext.Configuration;

                var methodParameters = ExtractDynamicMethodParameters(self);
                var dateFilter = GetDynamicMethodParameterNamed(methodParameters, "DateFilter").ToInt(0);

                string notificationId = GetDynamicMethodParameterNamed(methodParameters, "NotificationTemplate");
                var notificTemplate = message.CurrentContext.Configuration.FindObject(new Oid(notificationId));
                if (notificTemplate == null)
                {
                    throw new QInvalidOperationException("No Notification was defined");
                }

                SendMailContext sendMailContext;
                var smtpUtility = RepositoryGWE.GetSmtpUtilityForMessage(notificTemplate, message, out sendMailContext);
                if (smtpUtility == null)
                {
                    throw new QInvalidOperationException("No SMTP utility was created.");
                }

                var filter = string.Empty;
                if (workflowLevel)
                {
                    filter = $"AND CST.[WorkFlowId] = '{message.GovernanceDiagram.Id.Id}'";
                }
                if (dateFilter > 0)
                {
                    filter += $" AND CST.[OnStatusSince] >= GetDate() - {dateFilter}";
                }
                var order = "ORDER BY CMT.[PersonId] ASC, CST.[WorkFlowId] ASC, CMT.[RevId] ASC";
                var datatable = message.AdoUtility.GetAllGovernanceTasks(false, filter, order);
                QHyperLinkList LocPersonHLList = new QHyperLinkList();
                if (datatable != null)
                {
                    var currentPersonId = datatable.Rows[0]["PersonId"];
                    var oidList = new HashSet<Oid>();
                    for (int i = 0; i < datatable.Rows.Count; i++)
                    {
                        if (datatable.Rows[i]["PersonId"] == currentPersonId)
                        {
                            oidList.Add(new Oid($"{datatable.Rows[i]["ObjId"]}/{datatable.Rows[i]["RevId"]}"));
                        }
                        else
                        {
                            message.MessageActor = configuration.FindObject(new Oid($"{currentPersonId}"));
                            if (message.MessageActor != null)
                            {
                                var msgReplacements = new Dictionary<string, string>()
                                {
                                    { "{ObjectUrlList}", GetObjectUrlList(message.CurrentContext, message.MessageActor, oidList) }
                                };
                                RepositoryGWE.PostNotification(notificTemplate, message, msgReplacements);
                            }
                            currentPersonId = datatable.Rows[i]["PersonId"];
                            oidList = new HashSet<Oid>();
                        }
                    }
                    message.MessageActor = configuration.FindObject(new Oid($"{currentPersonId}"));
                    if (message.MessageActor != null)
                    {
                        var msgReplacements = new Dictionary<string, string>()
                                {
                                    { "{ObjectUrlList}", GetObjectUrlList(message.CurrentContext, message.MessageActor, oidList) }
                                };
                        RepositoryGWE.PostNotification(notificTemplate, message, msgReplacements);
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "0";
        }
        public static string GlobalAcknowledgeListChange(GovernanceMethodData self, MessageBlock message)
        {
            try
            {
                if (message.GweObject != null)
                {
                    throw new QInvalidOperationException("Global acknowledge list change can not be executed at Object level");
                }
                var conditionalMessage = message.MessageReceived as GovernanceConditionalMessage;
                if (conditionalMessage != null)
                {
                    if (conditionalMessage.Level != GovernanceConditionalMessageLevel.GovernanceLevel)
                    {
                        throw new QInvalidOperationException("Global acknowledge list must be executed at governance level");
                    }
                }
                else
                {
                    throw new QInvalidOperationException("Action can only be fired at conditional message");
                }

                var configuration = message.CurrentContext.Configuration;

                var methodParameters = ExtractDynamicMethodParameters(self);

                List<string> sourceStateStringList = new List<string>();
                GetDynamicMethodParameterNamed(methodParameters, "SourceStateFilter")
                        .Split(new Char[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                        .DoForEach(item =>
                        {
                            ObjectId objectId;
                            if (ObjectId.TryParse(item, out objectId))
                            {
                                sourceStateStringList.Add(objectId.ToString());
                            }
                        });

                List<string> revisionStateStringList = new List<string>();
                GetDynamicMethodParameterNamed(methodParameters, "RevisionStateFilter")
                        .Split(new Char[] { ';' }, StringSplitOptions.RemoveEmptyEntries)
                        .DoForEach(item =>
                        {
                            ObjectId objectId;
                            if (ObjectId.TryParse(item, out objectId))
                            {
                                revisionStateStringList.Add(objectId.ToString());
                            }
                        });

                HashSet<Oid> oidList = new HashSet<Oid>();
                var sourceList = new RepositoryObjectList(configuration
                                .FindObjects("AcknowledgeList")
                                .WithFullData()
                                .ToList())
                                .Where(entity => (sourceStateStringList.Count() == 0 || sourceStateStringList.Contains(entity.GweState?.HyperLink?.ObjectId.ToString())));
                foreach (var source in sourceList)
                {
                    source.AttributeGetLinksSafely("ToBeAcknowledged")
                        .Where(entity => (revisionStateStringList.Count() == 0 || revisionStateStringList.Contains(entity.GweState?.HyperLink?.ObjectId.ToString())))
                        .Select(entity => (entity.Id))
                        .DoForEach(entity => oidList.Add(entity));
                }
                foreach (var oid in oidList)
                {
                    if (message.AdoUtility.RefreshAcknowledge(oid))
                    {
                        RegistrySubTaskResult(message, c_resultOk, oid.Id.ToString(), "Refresh Acknowledge Engine");
                    }
                    else
                    {
                        RegistrySubTaskResult(message, "-1", oid.Id.ToString(), "Error on Refresh Acknowledge");
                    }
                }

            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "0";
        }

        public static string GlobalCheckAcknowledgementSLA(GovernanceMethodData self, MessageBlock message)
        {
            try
            {
                var now = DateTime.UtcNow.Date;
                if (message.GweObject != null)
                {
                    throw new QInvalidOperationException("Global check acknowledgement sla can not be executed at Object level");
                }
                var conditionalMessage = message.MessageReceived as GovernanceConditionalMessage;
                if (conditionalMessage != null)
                {
                    if (conditionalMessage.Level != GovernanceConditionalMessageLevel.GovernanceLevel)
                    {
                        throw new QInvalidOperationException("Global check acknowledgement sla  must be executed at governance level");
                    }
                }
                else
                {
                    throw new QInvalidOperationException("Action can only be fired at conditional message");
                }

                var configuration = message.CurrentContext.Configuration;

                var methodParameters = ExtractDynamicMethodParameters(self);
                string notificationId = GetDynamicMethodParameterNamed(methodParameters, "WarningNotification");
                var warningNotification = message.CurrentContext.Configuration.FindObject(new Oid(notificationId));
                if (warningNotification == null)
                {
                    throw new QInvalidOperationException("No Warning Notification was defined");
                }

                notificationId = GetDynamicMethodParameterNamed(methodParameters, "LimitNotification");
                var limitNotification = message.CurrentContext.Configuration.FindObject(new Oid(notificationId));
                if (limitNotification == null)
                {
                    throw new QInvalidOperationException("No Limit Notification was defined");
                }

                SendMailContext sendMailContext;
                var smtpUtility = RepositoryGWE.GetSmtpUtilityForMessage(warningNotification, message, out sendMailContext);
                if (smtpUtility == null)
                {
                    throw new QInvalidOperationException("No SMTP utility was created.");
                }

                var filter = $" AND A.GetDate() > [WarningDate]";
                var order = "ORDER BY A.[SourceId] A.[PersonId] ASC, A.[ObjId] ASC, A.[RevId] ASC";
                var datatable = message.AdoUtility.GetAllPendingAcknowledgement(filter, order);
                QHyperLinkList LocPersonHLList = new QHyperLinkList();
                if (datatable != null)
                {
                    var currentSourceKey = $"{datatable.Rows[0]["SourceId"]}";
                    var currentPersonKey = $"{datatable.Rows[0]["PersonId"]}";
                    var currentControlKey = $"{currentSourceKey}/{currentPersonKey}";
                    var oidWarningList = new HashSet<Oid>();
                    var oidLimitList = new HashSet<Oid>();
                    for (int i = 0; i < datatable.Rows.Count; i++)
                    {
                        var limitDate = now;
                        if (datatable.Rows[i]["LimitDate"] != DBNull.Value)
                        {
                            limitDate = (DateTime)datatable.Rows[i]["LimitDate"];
                        }
                        if ($"{datatable.Rows[0]["SourceId"]}/{datatable.Rows[0]["PersonId"]}" == currentControlKey)
                        {
                            if (now > limitDate)
                            {
                                oidLimitList.Add(new Oid($"{datatable.Rows[i]["ObjId"]}/{datatable.Rows[i]["RevId"]}"));
                            }
                            else
                            {
                                oidWarningList.Add(new Oid($"{datatable.Rows[i]["ObjId"]}/{datatable.Rows[i]["RevId"]}"));
                            }
                        }
                        else
                        {
                            message.MessageActor = configuration.FindObject(new Oid($"{currentPersonKey}"));
                            if (message.MessageActor != null)
                            {
                                message.GweObject = configuration.FindObject(new Oid($"{currentSourceKey}"));
                                var msgReplacements = new Dictionary<string, string>()
                                {
                                    { "{ObjectWarningList}", GetObjectUrlList(message.CurrentContext, message.MessageActor, oidWarningList) },
                                    { "{ObjectLimitList}", GetObjectUrlList(message.CurrentContext, message.MessageActor, oidLimitList) }
                                };
                                RepositoryGWE.PostNotification(warningNotification, message, msgReplacements);
                                RepositoryGWE.PostNotification(limitNotification, message, msgReplacements);
                            }
                            currentSourceKey = $"{datatable.Rows[0]["SourceId"]}";
                            currentPersonKey = $"{datatable.Rows[0]["PersonId"]}";
                            currentControlKey = $"{currentSourceKey}/{currentPersonKey}";
                            oidLimitList = new HashSet<Oid>();
                            oidWarningList = new HashSet<Oid>();
                        }
                    }
                    message.MessageActor = configuration.FindObject(new Oid($"{currentPersonKey}"));
                    if (message.MessageActor != null)
                    {
                        message.GweObject = configuration.FindObject(new Oid($"{currentSourceKey}"));
                        var msgReplacements = new Dictionary<string, string>()
                                {
                                    { "{ObjectWarningList}", GetObjectUrlList(message.CurrentContext, message.MessageActor, oidWarningList) },
                                    { "{ObjectLimitList}", GetObjectUrlList(message.CurrentContext, message.MessageActor, oidLimitList) }
                                };
                        RepositoryGWE.PostNotification(warningNotification, message, msgReplacements);
                        RepositoryGWE.PostNotification(limitNotification, message, msgReplacements);
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "0";
        }

        #endregion

        #region GovernanceActionMethod in CheckMode ByCSScript
        //Deal with public methods: Check

        public static string CheckAttributeList(GovernanceMethodData self, MessageBlock message)
        {
            var logWriter = message.CurrentContext.QisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));

            var debugMessages = new StringBuilder();
            debugMessages.Append("CheckAttributeList: ");
            List<string> results = new List<string>();

            var messageFormat = ExtractMessageFormat(self, "Invalid content in attribute \"{1}\".");
            var messageLevel = ExtractMessageLevel(self);

            var parameters = ExtractDynamicMethodParameters(self);
            if (parameters.Count() > 0)
            {
                foreach (var pair in parameters)
                {
                    string leftSideAsDefined = pair.Item1.Trim().Replace("\r", "");
                    if (!string.IsNullOrEmpty(leftSideAsDefined))
                    {
                        string rightSideAsDefined = pair.Item2.Trim().Replace("\r", "");
                        if (string.IsNullOrEmpty(rightSideAsDefined))
                        {
                            rightSideAsDefined = "!{}";
                        }

                        string leftSideContent = message.ResolveExpression(leftSideAsDefined);
                        string rightSideContent = message.ResolveExpression(rightSideAsDefined);
                        {
                            debugMessages.AppendLine();
                            debugMessages.AppendFormat("\t\tContent of [{0}]=[{1}] becomes [{2}]=[{3}]",
                                    leftSideAsDefined,
                                    rightSideAsDefined,
                                    leftSideContent,
                                    rightSideContent);
                            if (!rightSideContent.GweCompareTo(leftSideContent))
                            {
                                results.Add(string.Format(messageFormat, messageLevel, leftSideAsDefined.Replace("{", "").Replace("}", ""), leftSideContent, rightSideAsDefined, rightSideContent));
                            }
                        }
                    }
                }
            }

            logWriter.DebugDynamicCodeL2(debugMessages.ToString());

            string stringResult = string.Join(Environment.NewLine, results.ToArray().Where(s => !string.IsNullOrEmpty(s)));
            return stringResult;
        }
        public static string CheckAttributeInDiagramContent(GovernanceMethodData self, MessageBlock message)
        {
            var logWriter = message.CurrentContext.QisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));
            var debugMessages = new StringBuilder();
            debugMessages.Append("CheckAttributeInDiagramContent: ");

            List<string> results = new List<string>();
            debugMessages.AppendLine();
            debugMessages.AppendFormat("\tDynamicMethod=[{0}]", self.Name);

            var messageFormat = ExtractMessageFormat(self, "{0} : You must write a valid content in attribute {1} for object {5} of template {6},.");
            var messageLevel = ExtractMessageLevel(self);

            var parameters = ExtractDynamicMethodParameters(self);
            if (parameters.Count() > 0)
            {
                RepositoryObjectList diagramContains = message.GweObject.GetContains();
                RepositoryObjectList diagramContainsByTemplate = new RepositoryObjectList();
                string templates = GetDynamicMethodParameterNamed(parameters, "TemplateFilter");
                if (!templates.IsBlank())
                {
                    foreach (string template in templates.Split(new Char[] { ';' }))
                    {
                        string templateName = template.Trim();
                        diagramContainsByTemplate.AddRange(diagramContains.Where(x => x.Template == templateName));
                    }

                    debugMessages.AppendLine();
                    debugMessages.AppendFormat("\t\t{0} filtered contains {1} object", message.GweObject.Name, diagramContainsByTemplate.Count);
                }
                else
                {
                    diagramContainsByTemplate = diagramContains;
                    debugMessages.AppendLine();
                    debugMessages.AppendFormat("\t\t{0} with no filter contains {1} object", message.GweObject.Name, diagramContainsByTemplate.Count);
                }

                if (diagramContainsByTemplate.Count < 1)
                {
                    debugMessages.AppendLine();
                    debugMessages.AppendFormat("\t\tNo content on Diagram {0} to be checked", message.GweObject.Name);
                    results.Add(string.Format(" No content on Diagram {0} to be checked", message.GweObject.Name));
                }
                foreach (var pair in parameters)
                {
                    string leftSideAsDefined = pair.Item1.Trim().Replace("\r", "");
                    if (!string.IsNullOrEmpty(leftSideAsDefined) && leftSideAsDefined != "TemplateFilter")
                    {
                        string rightSideAsDefined = pair.Item2.Trim().Replace("\r", "");
                        if (string.IsNullOrEmpty(rightSideAsDefined))
                        {
                            rightSideAsDefined = "!{}";
                        }
                        var saveGweObject = message.GweObject;
                        try
                        {
                            foreach (RepositoryObject containedItem in diagramContainsByTemplate)
                            {
                                message.GweObject = containedItem;

                                string leftSideContent = message.ResolveExpression(leftSideAsDefined);
                                string rightSideContent = message.ResolveExpression(rightSideAsDefined);
                                {
                                    debugMessages.AppendLine();
                                    debugMessages.AppendFormat("\t\tContent of [{0}]=[{1}] becomes [{2}]=[{3}]",
                                            leftSideAsDefined,
                                            rightSideAsDefined,
                                            leftSideContent,
                                            rightSideContent);
                                    if (!rightSideContent.GweCompareTo(leftSideContent))
                                    {
                                        results.Add(string.Format(messageFormat, messageLevel,
                                                                                    leftSideAsDefined.Replace("{", "").Replace("}", ""),
                                                                                    leftSideContent,
                                                                                    rightSideAsDefined,
                                                                                    rightSideContent,
                                                                                    message.GweObject.Name,
                                                                                    message.GweObject.Template));
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            messageFormat = "Exception on {0} checking {1}: " + ex.Message;
                            results.Add(string.Format(messageFormat, leftSideAsDefined.Replace("{", "").Replace("}", ""), rightSideAsDefined, message.GweObject.Name, message.GweObject.Template));
                            message.GweObject = saveGweObject;
                        }
                        message.GweObject = saveGweObject;
                    }
                }
            }
            else
            {
                debugMessages.AppendLine();
                debugMessages.Append("\t\tNo parameters to be checked in current dynamic method");
                results.Add(string.Format(" No parameters to be checked in current dynamic method[{0}]", self.Name));
            }

            logWriter.DebugDynamicCodeL2(debugMessages.ToString());

            string stringResult = string.Join(Environment.NewLine, results.ToArray().Where(s => !string.IsNullOrEmpty(s)));

            return stringResult;
        }
        public static string CheckAttributeListByRelation(GovernanceMethodData self, MessageBlock message)
        {
            var logWriter = message.CurrentContext.QisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));
            var debugMessages = new StringBuilder();
            debugMessages.Append("CheckAttributeListByRelation: ");

            List<string> results = new List<string>();
            debugMessages.AppendLine();
            debugMessages.AppendFormat("\tDynamicMethod=[{0}]", self.Name);

            var messageFormat = ExtractMessageFormat(self, "{0} : You must write a valid content in attribute {1} for object {5} of template {6},.");
            var messageLevel = ExtractMessageLevel(self);

            var parameters = ExtractDynamicMethodParameters(self);
            if (parameters.Count() > 0)
            {
                string templates = GetDynamicMethodParameterNamed(parameters, "TemplateFilter");
                string relations = GetDynamicMethodParameterNamed(parameters, "Relations");
                if (string.IsNullOrEmpty(relations))
                {
                    relations = "Contains";
                }
                RepositoryObjectList objectList = GetListByRelation(message.GweObject, relations, templates, message.CycleOwner);
                RepositoryObjectList objectListByTemplate = new RepositoryObjectList();
                if (!templates.IsBlank())
                {
                    foreach (string template in templates.Split(new Char[] { ';' }))
                    {
                        string templateName = template.Trim();
                        objectListByTemplate.AddRange(objectList.Where(x => x.Template == templateName));
                    }

                    debugMessages.AppendLine();
                    debugMessages.AppendFormat("\t\t{0} filtered contains {1} object", message.GweObject.Name, objectListByTemplate.Count);
                }
                else
                {
                    objectListByTemplate = objectList;
                    debugMessages.AppendLine();
                    debugMessages.AppendFormat("\t\t{0} with no filter contains {1} object", message.GweObject.Name, objectListByTemplate.Count);
                }

                if (objectListByTemplate.Count < 1)
                {
                    debugMessages.AppendLine();
                    debugMessages.AppendFormat("\t\tNo {0} on {1} to be checked", relations, message.GweObject.Name);
                    results.Add(string.Format(" No {0} on {1} to be checked", relations, message.GweObject.Name));
                }
                foreach (var pair in parameters)
                {
                    string leftSideAsDefined = pair.Item1.Trim().Replace("\r", "");
                    if (!string.IsNullOrEmpty(leftSideAsDefined)
                        && leftSideAsDefined != "TemplateFilter"
                        && leftSideAsDefined != "Relations")
                    {
                        string rightSideAsDefined = pair.Item2.Trim().Replace("\r", "");
                        if (string.IsNullOrEmpty(rightSideAsDefined))
                        {
                            rightSideAsDefined = "!{}";
                        }
                        var saveGweObject = message.GweObject;
                        try
                        {
                            foreach (RepositoryObject containedItem in objectListByTemplate)
                            {
                                message.GweObject = containedItem;

                                string leftSideContent = message.ResolveExpression(leftSideAsDefined);
                                string rightSideContent = message.ResolveExpression(rightSideAsDefined);
                                {
                                    debugMessages.AppendLine();
                                    debugMessages.AppendFormat("\t\tContent of [{0}]=[{1}] becomes [{2}]=[{3}]",
                                            leftSideAsDefined,
                                            rightSideAsDefined,
                                            leftSideContent,
                                            rightSideContent);
                                    if (!rightSideContent.GweCompareTo(leftSideContent))
                                    {
                                        results.Add(string.Format(messageFormat, messageLevel,
                                                                                    leftSideAsDefined.Replace("{", "").Replace("}", ""),
                                                                                    leftSideContent,
                                                                                    rightSideAsDefined,
                                                                                    rightSideContent,
                                                                                    message.GweObject.Name,
                                                                                    message.GweObject.Template));
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            messageFormat = "Exception on {0} checking {1}: " + ex.Message;
                            results.Add(string.Format(messageFormat, leftSideAsDefined.Replace("{", "").Replace("}", ""), rightSideAsDefined, message.GweObject.Name, message.GweObject.Template));
                            message.GweObject = saveGweObject;
                        }
                        message.GweObject = saveGweObject;
                    }
                }
            }
            else
            {
                debugMessages.AppendLine();
                debugMessages.Append("\t\tNo parameters to be checked in current dynamic method");
                results.Add(string.Format(" No parameters to be checked in current dynamic method[{0}]", self.Name));
            }

            logWriter.DebugDynamicCodeL2(debugMessages.ToString());

            string stringResult = string.Join(Environment.NewLine, results.ToArray().Where(s => !string.IsNullOrEmpty(s)));

            return stringResult;
        }
        public static string DoCompareDate(GovernanceMethodData self, MessageBlock message)
        {
            var logWriter = message.CurrentContext.QisServer
                    .GetQefService<ILogService>()
                    .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));

            var debugMessages = new StringBuilder();
            debugMessages.Append("DoCompareDate: ");

            var messageFormat = ExtractMessageFormat(self, "Invalid Date \"{1}\":{2} compared with  \"{3}\":{4} ");
            var messageLevel = ExtractMessageLevel(self);

            var methodParameters = ExtractDynamicMethodParameters(self);
            string leftSideAsDefined = GetDynamicMethodParameterNamed(methodParameters, "Date1");
            string rightSideAsDefined = GetDynamicMethodParameterNamed(methodParameters, "Date2");

            string leftSideContent = message.ResolveExpression(leftSideAsDefined);
            string rightSideContent = message.ResolveExpression(rightSideAsDefined);
            var result = string.Compare(leftSideContent, rightSideContent, true);

            string operation = GetDynamicMethodParameterNamed(methodParameters, "Operation");

            debugMessages.AppendLine();
            debugMessages.AppendFormat("\t\tDate [{0}]=[{1}] compared[{4}] with [{2}]=[{3}] result {5}",
                        leftSideAsDefined,
                        leftSideContent,
                        rightSideAsDefined,
                        rightSideContent,
                        operation,
                        result);

            bool boolError = false;
            switch (operation)
            {
                case "GT":
                    boolError = (result > 0);
                    break;
                case "GE":
                    boolError = (result >= 0);
                    break;
                case "LE":
                    boolError = (result <= 0);
                    break;
                case "LT":
                    boolError = (result < 0);
                    break;
                default:  //EQ
                    boolError = (result == 0);
                    break;
            }

            logWriter.DebugDynamicCodeL2(debugMessages.ToString());

            if (boolError)
            {
                return string.Format(messageFormat, messageLevel,
                                                            leftSideAsDefined.Replace("{", "").Replace("}", ""),
                                                            leftSideContent,
                                                            rightSideAsDefined.Replace("{", "").Replace("}", ""),
                                                            rightSideContent,
                                                            message.GweObject.Name,
                                                            message.GweObject.Template);
            }
            return string.Empty;
        }

        public static string CheckChangesOnAttrList(GovernanceMethodData self, MessageBlock message)
        {
            var logWriter = message.CurrentContext.QisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));

            var debugMessages = new StringBuilder();
            debugMessages.Append("CheckChangesOnAttrList: ");
            List<string> results = new List<string>();

            var messageFormat = ExtractMessageFormat(self, "Changes in attribute \"{1}\". From [{2}] to [{3}]");
            var messageLevel = ExtractMessageLevel(self);

            if (message.OldData==null)
            {
                return string.Empty; // "Is is not possible to check for changes";
            }

            var parameters = ExtractDynamicMethodParameters(self);
            if (parameters.Count() > 0)
            {
                foreach (var pair in parameters)
                {
                    string attributeName = pair.Item2.Trim().Replace("\r", "");
                    if (!string.IsNullOrEmpty(attributeName))
                    {
                        string currentContent = string.Empty;
                        if (message.GweObject.Attributes.Contains(attributeName))
                        {
                            currentContent = message.GweObject.GweValueAsString(attributeName);
                        }
                        string oldDataContent = string.Empty;
                        if (message.OldData.Attributes.Contains(attributeName))
                        {
                            oldDataContent = message.OldData.GweValueAsString(attributeName);
                        }
                        debugMessages.AppendLine();
                        debugMessages.AppendFormat("\t\tCheck content of {0}: [{1}]===>[{2}]",
                                attributeName,
                                oldDataContent,
                                currentContent);
                        if (!currentContent.GweCompareTo(oldDataContent))
                        {
                            results.Add(string.Format(messageFormat, messageLevel, attributeName.Replace("{", "").Replace("}", ""), oldDataContent, currentContent));
                        }
                    }
                }
            }

            logWriter.DebugDynamicCodeL2(debugMessages.ToString());

            string stringResult = string.Join(Environment.NewLine, results.ToArray().Where(s => !string.IsNullOrEmpty(s)));
            return stringResult;
        }
        public static string AllowCreateNewRevision(GovernanceMethodData self, MessageBlock message)
        {
            var logWriter = message.CurrentContext.QisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));
            string stringResult = string.Empty;

            var messageFormat = ExtractMessageFormat(self, "There is already a new revision in {1}");
            var messageLevel = ExtractMessageLevel(self);

            if (!IsDefaultRevision(self, message))
            {
                stringResult = string.Format(messageFormat, messageLevel, message.GweObject.GetConfiguration().GetData().Name);
            }

            logWriter.DebugDynamicCodeL2("AllowCreateNewRevision: " + stringResult);

            return stringResult;
        }

        /// <summary>  
        ///	 Return a error message if the object can not be Updated (Touched)  
        /// </summary>
        public static string CheckUnableToUpdateObject(GovernanceMethodData self, MessageBlock message)
        {
            var logWriter = message.CurrentContext.QisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));
            var debugMessages = new StringBuilder();
            debugMessages.Append("CheckUnableToUpdateObject: ");

            debugMessages.AppendLine();
            debugMessages.AppendFormat("\tDynamicMethod=[{0}]", self.Name);

            debugMessages.AppendLine();

            var lockResult = message.GweObject.GetLockStatus();

            List<string> results = new List<string>();

            if (message.GweObject.IsFrozen)
            {
                var errMessage = string.Format("{0} is frozen and can not be modified.", message.GweObject.Name);
                debugMessages.AppendFormat(errMessage);
                results.Add(errMessage);
            }
            else if (lockResult.LockCount > 0 && lockResult.SessionId != message.CurrentContext.QisServer.CurrentSession.Id)
            {
                string fullUserName = Common.FindUserFullName(message.CurrentContext.QisServer, lockResult.UserId);
                var errMessage = string.Format("{0} is locked in another session by {1}.", message.GweObject.Name, fullUserName);
                debugMessages.AppendFormat(errMessage);
                results.Add(errMessage);
            }

            logWriter.DebugDynamicCodeL2(debugMessages.ToString());

            return string.Join("\n", results.ToArray());
        }

        /// <summary> 
        ///	 Return a error message if the Content of the object can not be Updated (Touched)
        /// </summary>
        public static string CheckUnableToUpdateContent(GovernanceMethodData self, MessageBlock message)
        {
            var logWriter = message.CurrentContext.QisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));
            var debugMessages = new StringBuilder();
            debugMessages.Append("CheckUnableToUpdateContent: ");

            List<string> results = new List<string>();
            debugMessages.AppendLine();
            debugMessages.AppendFormat("\tDynamicMethod=[{0}]", self.Name);

            var diagramContains = message.GweObject.GetContains();
            var diagramContainsByTemplate = new RepositoryObjectList();
            var messageFormat = ExtractMessageFormat(self, " Lock on {0} by: {1} [Lock count: {2}]{3} Try again later.");
            var parameters = ExtractDynamicMethodParameters(self);
            if (parameters.Count() > 0)
            {
                string templates = GetDynamicMethodParameterNamed(parameters, "TemplateFilter");

                if (!templates.IsBlank())
                {
                    foreach (string template in templates.Split(new Char[] { ';' }))
                    {
                        string templateName = template.Trim();
                        diagramContainsByTemplate.AddRange(diagramContains.Where(x => x.Template == templateName));
                    }

                    debugMessages.AppendLine();
                    debugMessages.AppendFormat("\t\t{0} filtered contains {1} object(s)", message.GweObject.Name, diagramContainsByTemplate.Count);
                }
                else
                {
                    diagramContainsByTemplate = diagramContains;
                    debugMessages.AppendLine();
                    debugMessages.AppendFormat("\t\t{0} with no filter contains {1} object(s)", message.GweObject.Name, diagramContainsByTemplate.Count);
                }
            }
            else
            {
                diagramContainsByTemplate = diagramContains;
            }

            if (diagramContainsByTemplate.Count < 1)
            {
                var noContentErrorMessage = self.Legacy.String1.IsBlank()
                    ? " No content on Diagram {0} to be checked"
                    : self.Legacy.String1;

                debugMessages.AppendLine();
                debugMessages.AppendFormat("\t\tNo content on Diagram {0} to be checked", message.GweObject.Name);

                results.Add(string.Format(noContentErrorMessage, message.GweObject.Name));
            }

            foreach (var contained in diagramContainsByTemplate)
            {
                var lockResult = contained.GetLockStatus();
                string fullUserName = Common.FindUserFullName(message.CurrentContext.QisServer, lockResult.UserId);

                string LocFrozenMessage = ". It is not frozen.";
                if (contained.IsFrozen)
                    LocFrozenMessage = ". It is frozen.";

                string lockDebugMessage = "Lock on {0} by: {1} [Lock count: {2}]{3}";
                debugMessages.AppendLine();
                debugMessages.AppendFormat(lockDebugMessage, contained.Name, fullUserName, lockResult.LockCount, LocFrozenMessage);
                if (lockResult.LockCount > 0 || contained.IsFrozen)
                {
                    results.Add(string.Format(messageFormat, contained.Name, fullUserName, lockResult.LockCount, LocFrozenMessage));
                }
            }

            logWriter.DebugDynamicCodeL2(debugMessages.ToString());

            return string.Join("\n", results.ToArray());
        }
        public static string MustHavePlayerForRole(GovernanceMethodData self, MessageBlock message)
        {
            var messageFormat = ExtractMessageFormat(self, "\"{0}\" has no participant for taking action in {1}");
            RepositoryObjectList hasActorRepositoryObjectList = GweStateMachine.GetFutureApprovers(message);

            return hasActorRepositoryObjectList != null && hasActorRepositoryObjectList.Count > 0
                ? string.Empty
                : string.Format(messageFormat, message.GweObject.Name, message.GovernanceStateAfter?.Name, self.Name);
        }
        public static string MustHaveResponsible(GovernanceMethodData self, MessageBlock message)
        {
            var messageFormat = ExtractMessageFormat(self, "There is no responsible for \"{0}\"");
            if (message.GweObject.Attributes.Contains("HasResponsible"))
            {
                var responsibleLinks = message.GweObject.Attributes["HasResponsible"].Value.GetHyperLinks();

                return responsibleLinks.Any()
                ? string.Empty
                    : string.Format(messageFormat, message.GweObject.Name);
            }

            return string.Empty;
        }

        /// <summary>  
        ///	 Return a error message if the Object has pendent actions
        /// </summary>
        public static string ChecksPendingAction(GovernanceMethodData self, MessageBlock message)
        {
            var logWriter = message.CurrentContext.QisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));
            var debugMessages = new StringBuilder();
            debugMessages.Append("ChecksPendingAction: ");
            List<string> results = new List<string>();

            var messageFormat = ExtractMessageFormat(self, "ChecksPendingAction-->There is {1} pending actions on {0}");
            int LocCount = message.AdoUtility.GetPendingActionCount(message.GweObject);
            if (LocCount > 0)
            {
                results.Add(string.Format(
                    messageFormat,
                    message.GweObject.Name,
                    LocCount));
            }

            logWriter.DebugDynamicCodeL2(debugMessages.ToString());

            return string.Join("\n", results.ToArray());
        }

        /// <summary>  
        ///	 Return a error message if some object in the relation is not in a specific state
        /// </summary>
        public static string InStateByRelation(GovernanceMethodData self, MessageBlock message)
        {
            var configuration = message.GweObject.GetConfiguration();
            var logWriter = message.CurrentContext.QisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));
            var debugMessages = new StringBuilder();
            debugMessages.Append("InStateByRelation: ");
            List<string> results = new List<string>();

            var messageFormat = ExtractMessageFormat(self, "{0} ({1}) is not at expected state ({2}). {3} ");
            var methodParameters = ExtractDynamicMethodParameters(self);
            string templates = GetDynamicMethodParameterNamed(methodParameters, "TemplateFilter");
            string relations = GetDynamicMethodParameterNamed(methodParameters, "Relations");
            string stateFilterType = GetDynamicMethodParameterNamed(methodParameters, "StateFilterType");
            if (string.IsNullOrEmpty(stateFilterType) || (stateFilterType != "ById" && stateFilterType != "ByBehavior" && stateFilterType != "ByBehaviour"))
            {
                stateFilterType = "ByName";
            }
            string stateFilter = GetDynamicMethodParameterNamed(methodParameters, "StateFilter");
            if (!relations.IsBlank())
            {
                RepositoryObjectList objectList = GetListByRelation(message.GweObject, relations, templates, message.CycleOwner);
                if (objectList.Count > 0)
                {
                    foreach (RepositoryObject repObject in objectList)
                    {
                        if (repObject.IsGweObject)
                        {
                            var repStateLink = repObject.GweState.HyperLink;
                            if (repStateLink != null)
                            {
                                var repStateObj = configuration.FindObject(repStateLink);
                                if (repStateObj != null)
                                {
                                    if (stateFilterType == "ByName" && repStateObj.Name != stateFilter)
                                    {
                                        results.Add(string.Format(
                                            messageFormat,
                                            message.GweObject.Name,
                                            message.GweObject.Template,
                                            stateFilter,
                                            "Current state = " + repStateObj.Name));
                                    }
                                    else
                                    {
                                        if (stateFilterType == "ById" && repStateObj.Id.Id != ObjectId.Parse(stateFilter))
                                        {
                                            results.Add(string.Format(
                                                messageFormat,
                                                message.GweObject.Name,
                                                message.GweObject.Template,
                                                stateFilter,
                                                "Current state = " + repStateObj.Name));
                                        }
                                        else
                                        {
                                            if ((stateFilterType == "ByBehavior" || stateFilterType == "ByBehaviour")
                                                && repStateObj.AttributePlainValueSafely("StateBehavior") != stateFilter)
                                            {
                                                results.Add(string.Format(
                                                    messageFormat,
                                                    message.GweObject.Name,
                                                    message.GweObject.Template,
                                                    stateFilter,
                                                    "Current state = " + repStateObj.Name));
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    results.Add(string.Format(
                                        messageFormat,
                                        message.GweObject.Name,
                                        message.GweObject.Template,
                                        stateFilter,
                                        "State object invalid"));
                                }
                            }
                            else
                            {
                                results.Add(string.Format(
                                    messageFormat,
                                    message.GweObject.Name,
                                    message.GweObject.Template,
                                    stateFilter,
                                    "State link invalid"));
                            }
                        }
                        else
                        {
                            results.Add(string.Format(
                                messageFormat,
                                message.GweObject.Name,
                                message.GweObject.Template,
                                stateFilter,
                                "Not GweObject"));
                        }
                    }
                }
                else
                {
                    results.Add(string.Format(
                        messageFormat,
                        "",
                        "...",
                        stateFilter,
                        "There is No object related."));
                }
            }
            else
            {
                results.Add(string.Format(
                    messageFormat,
                    "",
                    "...",
                    stateFilter,
                    "You must define a relation."));
            }

            logWriter.DebugDynamicCodeL2(debugMessages.ToString());

            return string.Join("\n", results.ToArray());
        }

        #endregion

        #region GovernanceGetMethod ByCSScript
        //Deal with public methods: Get

        //A method specifically for getting (and formatting) link.
        public static string GetAttributeLink(GovernanceMethodData self, MessageBlock message)
        {
            var methodParameters = ExtractDynamicMethodParameters(self);
            var attribute = GetDynamicMethodParameterNamed(methodParameters, "Attribute");
            var text = GetDynamicMethodParameterNamed(methodParameters, "Text");
            var revision = message.GweObject;

            IAttributeValue attrValue;
            if (!revision.Attributes.TryGetValue(attribute, out attrValue))
            {
                return string.Empty;
            }

            if (attrValue.Format != AttributeFormat.PlainText)
            {
                return string.Empty;
            }

            string content = attrValue.GetContent();
            string encodedContent = HttpUtility.HtmlEncode(content);
            return string.Format("<a href='{0}'>{1}</a>", encodedContent, text);
        }

        //A method specifically for inserting html content.
        public static string GetHtmlContent(GovernanceMethodData self, MessageBlock message)
        {
            var methodParameters = ExtractDynamicMethodParameters(self);
            var content = GetDynamicMethodParameterNamed(methodParameters, "Content");
            return content;
        }

        public static string GetAttribute(GovernanceMethodData self, MessageBlock message)
        {
            var attributeValue = string.Empty;
            var methodParameters = ExtractDynamicMethodParameters(self);
            var objectSelector = GetDynamicMethodParameterNamed(methodParameters, "Object");
            var attribute = GetDynamicMethodParameterNamed(methodParameters, "Attribute");
            var attributeFormat = GetDynamicMethodParameterNamed(methodParameters, "Format");
            var attributeSeparator = GetDynamicMethodParameterNamed(methodParameters, "Separator");
            var revision = message.GweObject;
            var revisionList = new RepositoryObjectList();
            var attributeValueFormat = AttributeFormat.PlainText;
            switch (objectSelector)
            {
                case "Object":
                    if (!string.IsNullOrEmpty(attribute))
                    {
                        attributeValueFormat = ObjectScripts.GetAttributeValue(revision, attribute, true).Format;
                        if (attributeValueFormat == AttributeFormat.MultiLink)
                        {
                            revisionList = revision.AttributeGetLinksSafely(attribute);
                        }
                    }

                    break;
                case "MessageActor":
                    revision = message.MessageActor;
                    if (!string.IsNullOrEmpty(attribute))
                    {
                        attributeValueFormat = ObjectScripts.GetAttributeValue(revision, attribute, true).Format;
                        if (attributeValueFormat == AttributeFormat.MultiLink)
                        {
                            revisionList = revision.AttributeGetLinksSafely(attribute);
                        }
                    }

                    break;
                case "Message/Transition":
                    if (!string.IsNullOrEmpty(attribute))
                    {
                        revision = message.CurrentContext.Configuration
                            .FindObject(message.MessageReceived.RemoteLink);
                        if (revision != null)
                        {
                            attributeValueFormat = ObjectScripts.GetAttributeValue(revision, attribute, true).Format;
                            if (attributeValueFormat == AttributeFormat.MultiLink)
                            {
                                revisionList = revision.AttributeGetLinksSafely(attribute);
                            }
                        }
                    }

                    break;
                case "Subscribers":
                    attributeValueFormat = AttributeFormat.MultiLink;
                    revisionList = CMS.FindSubscribers(message.GweObject).Distinct();
                    break;
                case "AcknowledgeList":
                    attributeValueFormat = AttributeFormat.MultiLink;
                    revisionList = message.AdoUtility.GetAcknowledgeForObject(message.GweObject.Id, 0);
                    break;
                case "NextPlayers":
                    attributeValueFormat = AttributeFormat.MultiLink;
                    revisionList = message.AdoUtility.GetCommitmentObj(message.GweObject, 0);
                    break;
                case "CycleOwner":
                    revision = message.CycleOwner;
                    if (!string.IsNullOrEmpty(attribute))
                    {
                        attributeValueFormat = ObjectScripts.GetAttributeValue(revision, attribute, true).Format;
                        if (attributeValueFormat == AttributeFormat.MultiLink)
                        {
                            revisionList = revision.AttributeGetLinksSafely(attribute);
                        }
                    }
                    break;
            }

            if (attributeValueFormat == AttributeFormat.PlainText && string.IsNullOrEmpty(attribute))
            {
                attributeValueFormat = AttributeFormat.MultiLink;
                revisionList.Add(revision);
            }

            if (attributeValueFormat == AttributeFormat.PlainText
                || attributeValueFormat == AttributeFormat.Xhtml)
            {
                return revision.AttributePlainValueConvertLinksSafely(attribute);
            }

            if (attributeValueFormat == AttributeFormat.DateTime)
            {
                var invalidString = new List<string> { "Name", "HtmlUrl", "EMail", "HTMLUrl", "Login", "FullNotify" };  // can not be attributeFormat for DateTime
                if (string.IsNullOrEmpty(attributeFormat)
                    || invalidString.Contains(attributeFormat, StringComparer.OrdinalIgnoreCase))
                {
                    attributeFormat = "dd MMM HH:mm:ss";
                }
                var iAttributeValue = GetDynamicVariable(message, attribute).ToAttributeValue();
                DateTime? dateTimeValue = revision.GetDateTimeSafely(attribute);
                if (dateTimeValue.HasValue)
                    attributeValue = ((DateTime)dateTimeValue).ToString(attributeFormat);
                else
                    attributeValue = null;
                return attributeValue;
            }
            if (string.IsNullOrEmpty(attributeFormat))
            {
                attributeFormat = "Name";
            }

            if (attributeValueFormat == AttributeFormat.SingleLink)
            {
                RepositoryObject objectLinked = revision.AttributeGetSingleLinkSafely(attribute);
                if (attributeFormat == "HtmlUrl" || attributeFormat == "HTMLUrl")
                {
                    return QEP.CreateHtmlLink(objectLinked).ToStringSafely();
                }
                else
                {
                    return objectLinked.AttributePlainValueSafely(attributeFormat);
                }
            }

            if (attributeValueFormat == AttributeFormat.MultiLink)
            {
                if (attributeFormat == "EMail" || attributeFormat == "Login" || attributeFormat == "FullNotify")
                {
                    attributeSeparator = string.IsNullOrEmpty(attributeSeparator) ? "; " : attributeSeparator;
                    if (attributeFormat == "EMail" || attributeFormat == "FullNotify")
                    {
                        PersonStructure.GetListOfPerson(message.CurrentContext, revisionList)
						    .WithFullData()
                            .Select(person => person.AttributePlainValueSafely("EMail"))
                            .Where(person => !person.IsBlank())
                            .DoForEach(person => attributeValue += string.Format("{0}{1}", attributeValue.IsBlank() ? string.Empty : attributeSeparator, person));
                    }
                    if (attributeFormat == "Login" || attributeFormat == "FullNotify")
                    {
                        PersonStructure.GetListOfPerson(message.CurrentContext, revisionList)
						    .WithFullData()
                            .Select(person => QefUserId.Parse(person.Id.Id.Value).ToString())
                            .Where(person => !person.IsBlank())
                            .DoForEach(person => attributeValue += string.Format("{0}{1}", attributeValue.IsBlank() ? string.Empty : attributeSeparator, person));
                    }
                    return attributeValue;
                }
                else
                {
                    if (attributeFormat == "HtmlUrl" || attributeFormat == "HTMLUrl")
                    {
                        attributeSeparator = string.IsNullOrEmpty(attributeSeparator) ? ", " : attributeSeparator;
                        revisionList
                            .Select(objectRef => QEP.CreateHtmlLink(objectRef).ToStringSafely())
                            .Where(objectRef => !objectRef.IsBlank())
                            .DoForEach(objectRef => attributeValue += string.Format("{0}{1}", attributeValue.IsBlank() ? string.Empty : attributeSeparator, objectRef));
                        return attributeValue;
                    }
                    else
                    {
                        revisionList
                            .Select(objectRef => objectRef.AttributePlainValueSafely(attributeFormat))
                            .Where(objectRef => !objectRef.IsBlank())
                            .DoForEach(objectRef => attributeValue += string.Format("{0}{1}", attributeValue.IsBlank() ? string.Empty : attributeSeparator, objectRef));
                        return attributeValue;
                    }
                }
            }

            return string.Empty;
        }
        public static string GetInformation(GovernanceMethodData self, MessageBlock message)
        {
            var methodParameters = ExtractDynamicMethodParameters(self);
            var attribute = GetDynamicMethodParameterNamed(methodParameters, "Attribute");

            var attributeValue = GetDynamicVariable(message, attribute).ToAttributeValue();
            return attributeValue.GetContent();
        }
        public static string GetUrlFromGweObject(GovernanceMethodData self, MessageBlock message)
        {
            return QEP.CreateHtmlLink(message.GweObject).ToStringSafely();
        }
        public static string GetStandardSubject(GovernanceMethodData self, MessageBlock message)
        {
            var messageFormat = ExtractMessageFormat(self, "Governance task ({0}) required for {1}.");
            return string.Format(messageFormat,
                message.GovernanceStateAfter?.Name,
                message.GweObject.Name);
        }
        public static string GetInfoFromAddedParticipantList(GovernanceMethodData self, MessageBlock message)
        {
            var configuration = message.CurrentContext.Configuration;
            var repository = message.CurrentContext.Repository;

            string attributeFormat = "Name";
            string attributeSeparator = ", ";
            string parameterText = string.Empty;
            var methodParameters = ExtractDynamicMethodParameters(self);
            if (methodParameters.Count() > 0)
            {
                parameterText = GetDynamicMethodParameterNamed(methodParameters, "Format");
                if (parameterText != string.Empty)
                {
                    attributeFormat = parameterText;
                }
                parameterText = GetDynamicMethodParameterNamed(methodParameters, "Separator");
                if (parameterText != string.Empty)
                {
                    attributeSeparator = parameterText;
                }
            }

            string participantListStringText = string.Empty;
            string stringSepatator = string.Empty;
            foreach (ObjectId newPersonOid in message.NewApproverList)
            {
                if (!message.OldApproverList.Contains(newPersonOid))
                {
                    var person = configuration.FindObject(new Oid(newPersonOid.ToString()));
                    switch (attributeFormat)
                    {
                        case "EMail":
                        case "Login":
                        case "FullNotify":
                            if (attributeFormat == "EMail" || attributeFormat == "FullNotify")
                            {
                                parameterText = person.GetAttributeValue("EMail").ToString();
                                if (!string.IsNullOrEmpty(parameterText))
                                {
                                    participantListStringText = participantListStringText + stringSepatator + parameterText;
                                }
                            }
                            if (attributeFormat == "Login" || attributeFormat == "FullNotify")
                            {
                                parameterText = QefUserId.Parse(person.Id.Id.Value).ToString();
                                if (!string.IsNullOrEmpty(parameterText))
                                {
                                    participantListStringText = participantListStringText + stringSepatator + parameterText;
                                }
                            }
                            break;
                        case "HtmlUrl":
                        case "HTMLUrl":
                            parameterText = QEP.CreateHtmlLink(person).ToStringSafely();
                            if (!string.IsNullOrEmpty(parameterText))
                            {
                                participantListStringText = participantListStringText + stringSepatator + parameterText;
                            }
                            break;
                        default:
                            participantListStringText = participantListStringText + stringSepatator + person.Name;
                            break;
                    }
                    stringSepatator = attributeSeparator;
                }
            }
            return participantListStringText;
        }
        public static string GetInfoFromRemovedParticipantList(GovernanceMethodData self, MessageBlock message)
        {
            var configuration = message.CurrentContext.Configuration;
            var repository = message.CurrentContext.Repository;

            string attributeFormat = "Name";
            string attributeSeparator = ", ";
            string parameterText = string.Empty;
            var methodParameters = ExtractDynamicMethodParameters(self);
            if (methodParameters.Count() > 0)
            {
                parameterText = GetDynamicMethodParameterNamed(methodParameters, "Format");
                if (parameterText != string.Empty)
                {
                    attributeFormat = parameterText;
                }
                parameterText = GetDynamicMethodParameterNamed(methodParameters, "Separator");
                if (parameterText != string.Empty)
                {
                    attributeSeparator = parameterText;
                }
            }

            string participantListStringText = string.Empty;
            string stringSepatator = string.Empty;
            foreach (ObjectId oldPersonOid in message.OldApproverList)
            {
                if (!message.NewApproverList.Contains(oldPersonOid))
                {
                    var person = configuration.FindObject(new Oid(oldPersonOid.ToString()));
                    switch (attributeFormat)
                    {
                        case "EMail":
                        case "Login":
                        case "FullNotify":
                            if (attributeFormat == "EMail" || attributeFormat == "FullNotify")
                            {
                                parameterText = person.GetAttributeValue("EMail").ToString();
                                if (!string.IsNullOrEmpty(parameterText))
                                {
                                    participantListStringText = participantListStringText + stringSepatator + parameterText;
                                }
                            }
                            if (attributeFormat == "Login" || attributeFormat == "FullNotify")
                            {
                                parameterText = QefUserId.Parse(person.Id.Id.Value).ToString();
                                if (!string.IsNullOrEmpty(parameterText))
                                {
                                    participantListStringText = participantListStringText + stringSepatator + parameterText;
                                }
                            }
                            break;
                        case "HtmlUrl":
                        case "HTMLUrl":
                            parameterText = QEP.CreateHtmlLink(person).ToStringSafely();
                            if (!string.IsNullOrEmpty(parameterText))
                            {
                                participantListStringText = participantListStringText + stringSepatator + parameterText;
                            }
                            break;
                        default:
                            participantListStringText = participantListStringText + stringSepatator + person.Name;
                            break;
                    }
                    stringSepatator = attributeSeparator;
                }
            }
            return participantListStringText;
        }
        public static string GetParticipantListStringText(GovernanceMethodData self, MessageBlock message)
        {
            string participantListStringText = string.Empty;
            string RemovedInParticipantList = string.Empty;
            string AddedInparticipantList = string.Empty;

            string headerForRemovedText = "Removed from Participant List: ";
            string headerForAddedText = "New in Participant List: ";
            string headerText = string.Empty;
            var methodParameters = ExtractDynamicMethodParameters(self);
            if (methodParameters.Count() > 0)
            {
                headerText = GetDynamicMethodParameterNamed(methodParameters, "HeaderForRemoved");
                if (headerText != string.Empty)
                {
                    headerForRemovedText = headerText;
                }
                headerText = GetDynamicMethodParameterNamed(methodParameters, "HeaderForAdded");
                if (headerText != string.Empty)
                {
                    headerForAddedText = headerText;
                }
            }

            RemovedInParticipantList = GetInfoFromRemovedParticipantList(self, message);
            if (RemovedInParticipantList != string.Empty)
            {
                participantListStringText = headerForRemovedText + RemovedInParticipantList + "\r\n";
            }

            AddedInparticipantList = GetInfoFromAddedParticipantList(self, message);
            if (AddedInparticipantList != string.Empty)
            {
                participantListStringText = participantListStringText + headerForAddedText + AddedInparticipantList + "\r\n";
            }

            return participantListStringText;
        }
        public static string GetDropDownEntries(GovernanceMethodData self, MessageBlock message)
        {
            var stringAnswer = string.Empty;
            var jsonStringBuilder = new StringBuilder();
            RepositoryObjectList entryList = new RepositoryObjectList(); // include here the source of entries
                                                                         // Gonna use the transition and the object as sample
            {
                entryList.Add(message.GweObject);
                //entryList.Add(message.MessageReceived);
            }

            if (entryList.Count < 1)
                return stringAnswer;

            jsonStringBuilder.AppendLine("{");

            jsonStringBuilder.AppendLine(string.Format("\"Count\":{0}", entryList.Count));
            jsonStringBuilder.AppendLine(",\"Entries\":");
            jsonStringBuilder.AppendLine("{");

            int seq = 0;
            string separator = "";
            entryList
                .DoForEach(item =>
                {
                    jsonStringBuilder.AppendLine(string.Format("{0}\"Btn{1}\":{{", separator, ++seq));
                    {
                        jsonStringBuilder.AppendJsonAttr("", "Entry", item);
                        jsonStringBuilder.AppendJsonAttr(",", "OtherInfo", "Something");
                    }
                    jsonStringBuilder.AppendLine("}");
                    separator = ",";
                });

            jsonStringBuilder.AppendLine("}");

            jsonStringBuilder.AppendLine("}");
            return jsonStringBuilder.ToString();
        }
        public static string GetPlayersByRole(GovernanceMethodData self, MessageBlock message)
        {
            var configuration = message.CurrentContext.Configuration;
            var attributeValue = string.Empty;
            var methodParameters = ExtractDynamicMethodParameters(self);
            var attributeFormat = GetDynamicMethodParameterNamed(methodParameters, "Format");
            var attributeSeparator = GetDynamicMethodParameterNamed(methodParameters, "Separator");
            var roleName = GetDynamicMethodParameterNamed(methodParameters, "SourceRole").Trim('{', '}', ' ');
            if (!string.IsNullOrEmpty(roleName))
            {
                var role = AllGovernanceRoles.FindRole(message.CurrentContext, roleName);
                if (role != null)
                {
                    RepositoryObjectList personList = RepositoryGWE.GetPlayersForRole(
                        configuration,
                        message.GweObject,
                        role,
                        message.CycleOwner,
                        message.AdoUtility);
                    if (personList.Count() > 0)
                    {
                        if (attributeFormat == "EMail" || attributeFormat == "Login" || attributeFormat == "FullNotify")
                        {
                            attributeSeparator = string.IsNullOrEmpty(attributeSeparator) ? "; " : attributeSeparator;
                            if (attributeFormat == "EMail" || attributeFormat == "FullNotify")
                            {
                                personList
                                    .Select(person => person.AttributePlainValueSafely("EMail"))
                                    .Where(person => !person.IsBlank())
                                    .DoForEach(person => attributeValue += string.Format("{0}{1}", attributeValue.IsBlank() ? string.Empty : attributeSeparator, person));
                            }
                            if (attributeFormat == "Login" || attributeFormat == "FullNotify")
                            {
                                personList
                                    .Select(person => QefUserId.Parse(person.Id.Id.Value).ToString())
                                    .Where(person => !person.IsBlank())
                                    .DoForEach(person => attributeValue += string.Format("{0}{1}", attributeValue.IsBlank() ? string.Empty : attributeSeparator, person));
                            }
                            return attributeValue;
                        }
                        else
                        {
                            if (attributeFormat == "HtmlUrl" || attributeFormat == "HTMLUrl")
                            {
                                attributeSeparator = string.IsNullOrEmpty(attributeSeparator) ? ", " : attributeSeparator;
                                personList
                                    .Select(objectRef => QEP.CreateHtmlLink(objectRef).ToStringSafely())
                                    .Where(objectRef => !objectRef.IsBlank())
                                    .DoForEach(objectRef => attributeValue += string.Format("{0}{1}", attributeValue.IsBlank() ? string.Empty : attributeSeparator, objectRef));
                                return attributeValue;
                            }
                            else
                            {
                                personList
                                    .Select(objectRef => objectRef.AttributePlainValueSafely(attributeFormat))
                                    .Where(objectRef => !objectRef.IsBlank())
                                    .DoForEach(objectRef => attributeValue += string.Format("{0}{1}", attributeValue.IsBlank() ? string.Empty : attributeSeparator, objectRef));
                                return attributeValue;
                            }
                        }
                    }
                }
            }

            return string.Empty;
        }

        #endregion

        #region RealizedMeasurement ByCSScript
        //Deal with public methods: RealizedMeasurement

        #endregion

        #region AnalyticDataTableMethod ByCSScript
        //Deal with public methods: AnalyticDataTable

        public static DataTable GetAllObjFromTemplate(GovernanceMethodData self, MessageBlock message)
        {
            DataTable dataTable = new DataTable();

            var conf = message.CurrentContext.Configuration;
            var rep = message.CurrentContext.Repository;
            var methodParameters = ExtractDynamicMethodParameters(self);
            string templateName = GetDynamicMethodParameterNamed(methodParameters, "TemplateName");

            AttributeDefinitions attributeList = rep.Metamodel.GetTemplateDefinition(templateName).Attributes;
            dataTable.Columns.Add("Template", typeof(string));
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("ObjId", typeof(string));
            dataTable.Columns.Add("RevId", typeof(string));
            foreach (var attribute in attributeList)
            {
                if (new[] { AttributeFormat.PlainText, AttributeFormat.Xhtml }.Contains(attribute.Format)) //PlainTextAttribute, SingleLinkAttribute, MultiLinkAttribute, DateTimeAttribute
                    if (!new[] { "Name", "Template" }.Contains(attribute.Name))
                        dataTable.Columns.Add(attribute.Name, typeof(string));
            }

            if (message.AnalyticStartRecord == 0 && message.AnalyticMaxRecords == 0)
                return dataTable;
            int sequenceRow = 0;
            int countRow = 0;
            foreach (var repositoryObject in conf.FindName(templateName))
            {
                if (!message.AnalyticUseInterval || (sequenceRow >= message.AnalyticStartRecord) && (countRow < message.AnalyticMaxRecords))
                {
                    countRow++;
                    DataRow dataRow = dataTable.NewRow();

                    foreach (DataColumn dataColumn in dataTable.Columns)
                    {
                        dataRow[dataColumn.ColumnName] = repositoryObject.AttributePlainValueSafely(dataColumn.ColumnName);
                    }

                    dataTable.Rows.Add(dataRow);
                    sequenceRow++;
                }
            }
            return dataTable;
        }
        public static DataTable GetTemplateCounters(GovernanceMethodData self, MessageBlock message)
        {
            var conf = message.CurrentContext.Configuration;

            DataTable dataTable = GetEmptyCounterTable(self);
            var methodParameters = ExtractDynamicMethodParameters(self);
            string templateName = GetDynamicMethodParameterNamed(methodParameters, "TemplateName");
            foreach (var repositoryObject in conf.FindName(templateName))
            {
                dataTable.GovernanceCounterUpdate(repositoryObject);
            }

            return dataTable;
        }
        public static DataTable GetGovernanceWorkFlowCounters(GovernanceMethodData self, MessageBlock message)
        {
            DataTable dataTable = GetEmptyCounterTable(self);

            GweAdoUtility gweAdoUtility = new GweAdoUtility(message.CurrentContext);
            foreach (var repositoryObject in gweAdoUtility.GetGovernanceStatusList(message.CurrentContext.QisServer, message.GweObject, false))
            {
                dataTable.GovernanceCounterUpdate(repositoryObject);
            }

            return dataTable;
        }
        public static DataTable GetTemplateCountersForObject(GovernanceMethodData self, MessageBlock message)
        {
            IConfiguration conf = message.CurrentContext.Configuration;

            DataTable dataTable = GetEmptyCounterTable(self);
            var methodParameters = ExtractDynamicMethodParameters(self);
            string templateName = GetDynamicMethodParameterNamed(methodParameters, "TemplateName");
            string attributeLink = GetDynamicMethodParameterNamed(methodParameters, "AttributeLink");
            var objectList = message.GweObject.GetLinkedBy().FilterByTemplate(templateName).Where(item => item.AttributeGetLinksSafely(attributeLink).Contains(message.GweObject.Id));
            foreach (var repositoryObject in objectList)
            {
                dataTable.GovernanceCounterUpdate(repositoryObject);
            }

            return dataTable;
        }
        public static DataTable GetTemplateCountersForObjectRelatedToObject(GovernanceMethodData self, MessageBlock message)
        {
            DataTable dataTable = GetEmptyCounterTable(self);
            var methodParameters = ExtractDynamicMethodParameters(self);

            string templateName = GetDynamicMethodParameterNamed(methodParameters, "TemplateName");
            string attributeLink = GetDynamicMethodParameterNamed(methodParameters, "AttributeLink");
            string relatedTemplateName = GetDynamicMethodParameterNamed(methodParameters, "RelatedTemplateName");
            string relatedAttributeLink = GetDynamicMethodParameterNamed(methodParameters, "RelatedAttributeLink");

            var objectList = message.GweObject.GetLinkedBy().FilterByTemplate(templateName).Where(item => item.AttributeGetLinksSafely(attributeLink).Contains(message.GweObject.Id));
            foreach (var repositoryObject in objectList)
            {
                var relatedObjectList = repositoryObject.GetLinkedBy().FilterByTemplate(relatedTemplateName).Where(item => item.AttributeGetLinksSafely(relatedAttributeLink).Contains(repositoryObject.Id));
                foreach (var relatedRepositoryObject in relatedObjectList)
                {
                    dataTable.GovernanceCounterUpdate(relatedRepositoryObject);
                }
            }

            return dataTable;
        }
        public static DataTable GetHierarchyView(GovernanceMethodData self, MessageBlock message)
        {
            var methodParameters = ExtractDynamicMethodParameters(self);
            string hierarchyViewId = GetDynamicMethodParameterNamed(methodParameters, "HierarchyView");
            var hierarchView = message.CurrentContext.Configuration.FindObject(new Oid(hierarchyViewId));
            if (hierarchView != null)
            {
                return QepHierarchyView.GetHierarchyView(message.CurrentContext.Configuration, hierarchView, message.GweObject, message.AnalyticUseInterval, message.AnalyticStartRecord, message.AnalyticMaxRecords);
            }
            else
            {
                string sLevel = GetDynamicMethodParameterNamed(methodParameters, "Level");
                string sRelationFilter = GetDynamicMethodParameterNamed(methodParameters, "RelationFilter").Replace(",", "\n"); ;
                string sRelation = GetDynamicMethodParameterNamed(methodParameters, "Relation").Replace(",", "\n"); ;
                string sTemplateFilter = GetDynamicMethodParameterNamed(methodParameters, "TemplateFilter").Replace(",", "\n"); ;
                string sAttribute = GetDynamicMethodParameterNamed(methodParameters, "Attribute").Replace(",", "\n"); ;
                string sRoot = GetDynamicMethodParameterNamed(methodParameters, "Root");
                var bIncludeParent = GetDynamicMethodParameterNamed(methodParameters, "IncludeParent").ToBoolean();

                var rootObject = message.CurrentContext.Configuration.FindObject(new Oid(sRoot));
                if (rootObject == null)
                {
                    rootObject = message.GweObject;
                }
                return QepHierarchyView.GetHierarchyView(message.CurrentContext.Configuration, rootObject, sLevel, sRelationFilter, sRelation, sTemplateFilter, sAttribute, bIncludeParent);
            }
        }

        public static DataTable GetAllGovernanceTasks(GovernanceMethodData self, MessageBlock message)
        {
            var utility = new GweAdoUtility(message.CurrentContext);
            return utility.GetAllGovernanceTasks(false);
        }

        public static DataTable GetAllRatings(GovernanceMethodData self, MessageBlock message)
        {
            var datasetResult = (new GweAdoUtility(message.CurrentContext)).GetAllRatings();

            List<Oid> ResolveList = new List<Oid>();
            foreach (DataRow row in datasetResult.Tables[0].Rows)
            {
                ResolveList.Add(new Oid(row[0].ToString() + "/" + row[1].ToString()));
            }
            var RepositoryObjectList = message.GweObject.GetConfiguration().GetObjectSet(new OidList(ResolveList), ObjectInstantiationLevel.IdAndLanguage);

            var clonedTable = datasetResult.Tables[0].Clone();
            int j;
            for (j = 0; j < datasetResult.Tables[0].Columns.Count; j++)
            {
                if (datasetResult.Tables[0].Columns[j].ColumnName == "RatingScore")
                {
                    break;
                }
            }
            clonedTable.Columns[j].DataType = typeof(string);
            clonedTable.Columns.Add("RevisionNumber", typeof(string));
            foreach (DataRow row in datasetResult.Tables[0].Rows)
            {
                clonedTable.ImportRow(row);
            }
            for (int i = 0; i < clonedTable.Rows.Count; i++)
            {
                var res = RepositoryObjectList.Where(x => x.Id.Revision.ToString() == clonedTable.Rows[i][1].ToString());
                if (res != null && res.Count() > 0)
                {
                    clonedTable.Rows[i][clonedTable.Columns.Count - 1] = res.First().LocalRevision.ToString();
                }
            }
            int MaxRatingScore = 5;
            string inactiveStarString = "<a class=\"rating-inactive\"> </a>";
            string activeStarString = "<a class=\"rating-active\"> </a>";

            foreach (DataRow row in clonedTable.Rows)
            {
                try
                {
                    string ratingString = string.Empty;
                    int score = int.Parse(row[j].ToString());
                    for (int i = 0; i < MaxRatingScore; i++)
                    {
                        ratingString += (score > i ? activeStarString : inactiveStarString);
                    }
                    row[j] = ratingString;
                }
                catch (Exception)
                {
                    //ignore and carry on filling the datarows
                }
            }
            return clonedTable;
        }


        #endregion

        #region Dynamic Consistency CMN ==> (Consistency Method Name)
        //Deal with Consistency of  methods
        public static string BasicDynamicConsistency(IQisServer qisServer, RepositoryObject methodSignatureRepositoryObject)
        {
            var logWriter = qisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));
            var debugMessages = new StringBuilder();
            debugMessages.AppendFormat("BasicDynamicConsistency: Method Signature=[{0}]", methodSignatureRepositoryObject.Name);

            SignatureChecker checker = new SignatureChecker(methodSignatureRepositoryObject);
            checker.CheckExists("xmlCoupleParameters", "You must define Parameters.");

            logWriter.DebugDynamicCodeL2(debugMessages.ToString());

            return checker.ToString();
        }
        public static string GweCMN_GetAttribute(IQisServer qisServer, RepositoryObject methodSignatureRepositoryObject)
        {
            return BasicDynamicConsistency(qisServer, methodSignatureRepositoryObject);
        }
        public static string GweCMN_GetInformation(IQisServer qisServer, RepositoryObject methodSignatureRepositoryObject)
        {
            return BasicDynamicConsistency(qisServer, methodSignatureRepositoryObject);
        }
        public static string GweCMN_VerifyAttributeList(IQisServer qisServer, RepositoryObject methodSignatureRepositoryObject)
        {
            return BasicDynamicConsistency(qisServer, methodSignatureRepositoryObject);
        }
        public static string GweCMN_SetObjectAttributeList(IQisServer qisServer, RepositoryObject methodSignatureRepositoryObject)
        {
            return BasicDynamicConsistency(qisServer, methodSignatureRepositoryObject);
        }
        public static string GweCMN_CheckAttributeList(IQisServer qisServer, RepositoryObject methodSignatureRepositoryObject)
        {
            return BasicDynamicConsistency(qisServer, methodSignatureRepositoryObject);
        }
        public static string GweCMN_CheckAttributeInDiagramContent(IQisServer qisServer, RepositoryObject methodSignatureRepositoryObject)
        {
            return BasicDynamicConsistency(qisServer, methodSignatureRepositoryObject);
        }
        public static string GweCMN_CheckAttributeListByRelation(IQisServer qisServer, RepositoryObject methodSignatureRepositoryObject)
        {
            return BasicDynamicConsistency(qisServer, methodSignatureRepositoryObject);
        }
        public static string GweCMN_AllowCreateNewRevision(IQisServer qisServer, RepositoryObject methodSignatureRepositoryObject)
        {
            return string.Empty;
        }
        public static string GweCMN_CheckUnableToUpdateObject(IQisServer qisServer, RepositoryObject methodSignatureRepositoryObject)
        {
            var logWriter = qisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));
            var debugMessages = new StringBuilder();
            debugMessages.AppendFormat("GweCMN_CheckUnableToUpdateObject: Method Signature=[{0}]", methodSignatureRepositoryObject.Name);

            SignatureChecker checker = new SignatureChecker(methodSignatureRepositoryObject);
            checker.CheckNotExists("MethodAttrList1", "This Method does not requires setup on List1.");
            checker.CheckNotExists("MethodAttrList2", "This Method does not requires setup on List2.");
            checker.CheckNotExists("MethodAttrInt1", "This Method does not requires setup on Value[1], remove {0}", true);
            checker.CheckNotExists("MethodAttrInt2", "This Method does not requires setup on Value[2], remove {0}", true);
            checker.CheckNotExists("MethodAttrInt3", "This Method does not requires setup on Value[3], remove {0}", true);
            checker.CheckNotExists("MethodAttrInt4", "This Method does not requires setup on Value[4], remove {0}", true);
            checker.CheckExists("MethodMessageFormat", "Message Format must be defined to be used when the object can not be updated.");

            logWriter.DebugDynamicCodeL2(debugMessages.ToString());

            return checker.ToString();
        }
        public static string GweCMN_CheckUnableToUpdateContent(IQisServer qisServer, RepositoryObject methodSignatureRepositoryObject)
        {
            var logWriter = qisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));
            var debugMessages = new StringBuilder();
            debugMessages.AppendFormat("GweCMN_CheckUnableToUpdateContent: Method Signature=[{0}]", methodSignatureRepositoryObject.Name);

            SignatureChecker checker = new SignatureChecker(methodSignatureRepositoryObject);
            checker.CheckNotExists("MethodAttrList1", "This Method does not requires setup on List1.");
            checker.CheckNotExists("MethodAttrList2", "This Method does not requires setup on List2.");
            checker.CheckNotExists("MethodAttrInt1", "This Method does not requires setup on Value[1], remove {0}", true);
            checker.CheckNotExists("MethodAttrInt2", "This Method does not requires setup on Value[2], remove {0}", true);
            checker.CheckNotExists("MethodAttrInt3", "This Method does not requires setup on Value[3], remove {0}", true);
            checker.CheckNotExists("MethodAttrInt4", "This Method does not requires setup on Value[4], remove {0}", true);
            checker.CheckExists("MethodMessageFormat", "Message Format must be defined to be used when the object can not be updated.");

            logWriter.DebugDynamicCodeL2(debugMessages.ToString());

            return checker.ToString();
        }
        public static string GweCMN_Approve(IQisServer qisServer, RepositoryObject methodSignatureRepositoryObject)
        {
            var logWriter = qisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));
            var debugMessages = new StringBuilder();
            debugMessages.AppendFormat("GweCMN_Approve: Method Signature=[{0}]", methodSignatureRepositoryObject.Name);

            SignatureChecker checker = new SignatureChecker(methodSignatureRepositoryObject);
            checker.CheckExists("xmlCoupleParameters", "You must define parameters");

            logWriter.DebugDynamicCodeL2(debugMessages.ToString());

            return checker.ToString();
        }
        public static string GweCMN_EmailAllParticipants(IQisServer qisServer, RepositoryObject methodSignatureRepositoryObject)
        {
            var logWriter = qisServer
                .GetQefService<ILogService>()
                .GetLogWriter(QisConst.QisId, Source.Parse(MethodBase.GetCurrentMethod().Name));
            var debugMessages = new StringBuilder();
            debugMessages.AppendFormat("GweCMN_EmailAllParticipants: Method Signature=[{0}]", methodSignatureRepositoryObject.Name);

            const string associatedWith = "AssociatedWith";
            SignatureChecker checker = new SignatureChecker(methodSignatureRepositoryObject);
            checker.CheckExists(associatedWith, "You must define the email template associated with");

            var notificTemplate = methodSignatureRepositoryObject.AttributeGetLinksSafely(associatedWith).FilterByTemplate("NotificationTemplate").FirstOrDefault();
            checker.CheckNotNull(notificTemplate, "You must define the email template to be used");

            logWriter.DebugDynamicCodeL2(debugMessages.ToString());

            return checker.ToString();
        }

        #endregion

        #endregion

        #region Public Support Method

        #endregion

        #endregion
    }
}