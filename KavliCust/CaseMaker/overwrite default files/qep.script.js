if (!Array.prototype.reduce) {
	Array.prototype.reduce = function(callback /*, initialValue*/) {
		'use strict';
		if (this == null) {
			throw new TypeError('Array.prototype.reduce called on null or undefined');
		}
		if (typeof callback !== 'function') {
			throw new TypeError(callback + ' is not a function');
		}
		var t = Object(this), len = t.length >>> 0, k = 0, value;
		if (arguments.length == 2) {
			value = arguments[1];
		} 
		else {
			while (k < len && !(k in t)) {
				k++; 
			}
			if (k >= len) {
				throw new TypeError('Reduce of empty array with no initial value');
			}
			value = t[k++];
		}
		for (; k < len; k++) {
			if (k in t) {
				value = callback(value, t[k], k, t);
			}
		}
		return value;
	};
}

if (!Array.prototype.remove) {
	Array.prototype.remove = function(s) {
		if (typeof s == "undefined") {
			s = "";
		}
		for(var i = 0; i < this.length; i++) {
			while(this[i] === s){
				this.splice(i, 1)[0];
			}
		}
		return this;
	};
}

if (!Array.prototype.insert) {
	Array.prototype.insert = function (index, item) {
	  this.splice(index, 0, item);
	};
}

if (!Array.prototype.add) {
	Array.prototype.add = function() {
		var args = arguments;
		if (!arguments.length) {
			return this;
		}
		for(var i = 0; i < args.length; i++) {
			if (!this.match(args[i])) {
				this.push(args[i]);
			}
		}
		return this;
	};
}

if (!Array.prototype.match) {
	Array.prototype.match = function() {
		if (!arguments.length) {
			return [];
		}

		var args = arguments;
		var a = [];
		for (var i = 0; i<args.length; i++ ) {
			var b = args[i];
			if (typeof b == "string") {
				a = a.concat(b.split(/,|;/));
			}
			else if (typeof b == "number") {
				a = a.concat([b]);
			}
		}

		if (!a.length) {
			return null;
		}

		a = a.concat(this);
		a = a.sort().remove();		

		var match = [];
		for (var i = 0; i < a.length - 1; i++) {
			if (String(a[i + 1]).toLowerCase() == String(a[i]).toLowerCase()) {
				match.push(a[i]);
			}
		}
		return match.length ? match : null;
	};
}

if (!Array.prototype.clear) {
	Array.prototype.clear = function() {
		while (this.length) {
			this.pop();
		}
		return this;
	};
}

if (!Array.prototype.unique) {
	Array.prototype.unique = function(a) {
		return function(){ return this.filter(a) }
	}(function(a,b,c){ return c.indexOf(a,b+1) < 0 });
}

if (!Array.prototype.filter) {
	Array.prototype.filter = function(fun /*, thisp */) {
		"use strict";
		
		if (this === void 0 || this === null) {
			throw new TypeError();
		}
		
		var t = Object(this);
		var len = t.length >>> 0;
		if (typeof fun !== "function") {
			throw new TypeError();
		}

		var res = [];
		var thisp = arguments[1];
		for (var i = 0; i < len; i++) {
			if (i in t) {
				var val = t[i];
				if (fun.call(thisp, val, i, t)) {
					res.push(val);
				}
			}
		}
		return res;
	};
}

if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(obj, start) {
		for (var i = (start || 0), j = this.length; i < j; i++) {
			if (this[i] === obj) { 
				return i; 
			}
		}
		return -1;
	};
}

if (!Array.prototype.diff) {
	Array.prototype.diff = function(a) {
		return this.filter(function(i) {return !(a.indexOf(i) > -1);});
	};
}

if (!Array.prototype.exclude) {
	Array.prototype.exclude = function(list){
		return this.filter(function(el){return list.indexOf(el)<0;})
	};
}

$.xhrPool = [];

$.xhrPool.abortAll = function() {
	$.async(false);
    $(this).each(function(idx, jqXHR) { 
        jqXHR.abort();
    });
    $.xhrPool.length = 0
};

$.ajaxSetup({
	beforeSend: function(jqXHR) {
		$.xhrPool.push(jqXHR);
	},
	complete: function(jqXHR) {
		var index = $.xhrPool.indexOf(jqXHR);
		if (index > -1) {
			$.xhrPool.splice(index, 1);
		}
	},
	contentType: "application/json; charset=utf-8",
	dataType: "json"
});

$.callPageMethod = function (methodName, args, onSuccess, onFail, async) {
	$.ajax({
		type: "POST",
		url: PageMethods.get_path() + "/" + methodName,
		data: typeof args === "string" ? args : JSON.stringify(args),
		async: typeof async === "undefined" ? true : async,
		success: onSuccess,
		fail: onFail
	});
}

$.initSessionParameters = function() {
	$QisSession.RepName = $QisSession.Repository.Name;
	$QisSession.RepId = $QisSession.Repository.Id;
	
	$.each($QisSession.Configurations, function(key, value) {
		$QisSession.BaseConfId = key;
		$QisSession.BaseConfName = value;
		return false;
	});
	
	$.each($QisSession.Languages, function(key, value) {
		$QisSession.DefaultLangCode = key;
		$QisSession.DefaultLangName = value;
		return false;
	});
	
	var parms = {
		oid: null,
		lc: $QisSession.DefaultLangCode,
		cid: $QisSession.BaseConfId
	};
	
	var valid = true;
	
	if (window.location.search) {
		var url = window.location.search.slice(1).split("#")[0];
		var parameters = url.split("&");
		for (var i = 0; i < parameters.length; i++) {
			var tmp = parameters[i].split("=");
			parms[tmp[0].toLowerCase()] = decodeURI(tmp[1]);
		}
	}
		
	if (parms.lc == $QisSession.DefaultLangCode 
	&& parms.lc == $.cookie("LangCode")) {
		parms.lc = null;
	}
	else if (parms.lc) {
		if (parms.lc == $QisSession.DefaultLangCode 
		&& !$.cookie("LangCode")) {
			parms.lc = null;
		}
		else if ($QisSession.Languages[parms.lc]) {
			$QisSession["LangCode"] = parms.lc;
			if (!$.cookie("NewLangCode") 
			&& parms.lc != $.cookie("LangCode")) {
				$QisSession.FncQueue.push( function() {
					if (parms.lc != $.cookie("LangCode")) {
						changeRepositoryLanguage.call({Code: parms.lc});
						return true;
					}
					return false;
				});
			}
		}
		else {
			$.cookie("InvLangCode", parms.lc);
			parms.lc = null;
			valid = false;
		}
	}
	
	if (parms.cid && !parms.rid) {
		if (parms.cid == $QisSession.BaseConfId) {
			parms.cid = null;
		}
		else  {
			if ($QisSession.Configurations[parms.cid]){
				$QisSession["ConfId"] = parms.cid;
				$QisSession["ConfName"] = $QisSession.Configurations[parms.cid];
			}
			else {
				$.cookie("InvConfId", parms.cid);
				parms.cid = null;
				valid = false;
			}
		}
	}
	
	if (!valid) {
		var url = location.href.split("?")[0];
		$.each(parms, function(idx, key) {
			if (parms[key]) {
				url += key + "=" + parms[key];
			}
		});
		return location.replace(url);
	}
	
	if (!$QisSession["ConfId"]) {
		$QisSession["ConfId"] = $QisSession.BaseConfId;
		$QisSession["ConfName"] = $QisSession.BaseConfName;
	}
	
	if (!$QisSession["LangCode"]) {
		$QisSession["LangCode"] = $QisSession.DefaultLangCode;
	}
	
	if ($.cookie("InvLangCode")) {
		$.async(function() {
			if (typeof $.fn.qtip != "function") {
				return;
			}
			$.async(false, this);
			$.jnotify({
				title: $.LL("Warning"),
				message: $.LL("Invalid Language:") + " " + $.cookie("InvLangCode") + "<br/><br/>" + $.LL("Default language is used")
			});
			$.removeCookie("InvLangCode");
		}, 100, true);
	}
	
	if ($.cookie("InvConfId")) {
		$.async(function() {
			if (typeof $.fn.qtip != "function") {
				return;
			}
			$.async(false, this);
			$.jnotify({
				title: $.LL("Warning"),
				message: $.LL("Invalid Configuration:") + " " + parms.cid + "<br/><br/>" + $.LL("Default configuration is used")
			});
			$.removeCookie("InvConfId");
		}, 100, true);
	}
		
	$.browserCache("check");
	$.initSessionScripts();
	return parms;
}

$.initSessionScripts = function() {
    var prefix = $QisSession.ConfName + "/Scripts/";
    var postfix = "-" + $QisSession.LangCode + ".js";

    var scripts = [
        prefix + "_qep.settings" + postfix,
        prefix + "_template.settings" + postfix,
        prefix + "_template.definitions" + postfix,
        "js/kendoui/messages/kendo.messages." + postfix.substring(1)
    ];

    var queue = $.map(scripts, function(script) {
        var data = $.browserCache("get", $QisSession.ConfId + "_" + script);

        if (data && !$.browser.compatible) {
            window.eval.call(window, data);
            return;
        }

        return $.getScript(script + "?_t=" + $QisSession.TimeStamp).done(function(data) {
            $.browserCache("set", $QisSession.ConfId + "_" + script, data);
        })
		.fail(function(xhr, result, error){
			$.browser.error("Script error: Unable to parse " + script + "\n" + (error.message || error.description));
		});
    });

    var onScriptLoad = function() {
		var configuration =  getConfigurationInfo($QisSession.ConfId, {RoleFilter: true});
		var cid = configuration.Id;
		if (cid != $QisSession.ConfId) {
			var $cnt = $("<div />")
				.append("<h3>" + $.LL("Configuration not accessible") + "</h3>")
				.append("<div>" + $.LL("Redirecting to a valid configuration") + "..." + "</div>");
			
			$.each($QisSession.Configurations, function(key, value) {
				var $conf = getConfigurationInfo(key, {RoleFilter: true});
				if ($conf.Id) {
					cid = $conf.Id;
					return false;
				}
			});
			
			if (typeof $.fancybox === "function") {
				$.fancybox($cnt);
			}
			else {
				$.async(function(){
					if (typeof $.fancybox === "function") {
						$.async(false, this);
						$.fancybox($cnt);
					}
				}, 100);
			}
			
			if (!cid) {
				var chgRole = false;
				$cnt = $("<div />").width(450)
					.append("<h1>" + $.LL("No configuration available") + "</h1>")
					.append("<div>" + $.LL("There is no valid configuration available for your role.") + "</div>")
					.append("<br/><div>" + $.LL("Role:") + " " + $QisSession.UserInfo.Role + "</div>");
				
				var $btn = $("<div/>")
					.append("<button class=\"btnRole\" style=\"display:none\">" + $.LL("Change Role") + "</button>")
					.append("<button class=\"btnLogout\">" + $.LL("Logout") + "</button>")
					.css({
						"text-align": "right",
						"margin-top": "2em"
					})
					.appendTo($cnt);
					
				if ($QisSession.UserInfo.AltRoles && $QepSettings.UserRoleChangeEnabled) {
					$btn.children().first().button().show()
						.on($.browser.click, function(){
							chgRole = true;
							$.initUserRoleChange(function(){
								endQisSession(location.href, null, false);
							}); 
						});
				}
				
				$btn.children()
					.css({
						"line-height": "2em",
						"font-size": "0.9em",
						"margin-left": "4px"
					})
					.last().button()
						.on($.browser.click, function(){
							endQisSession();
						});
					
				$.fancybox($cnt, {
					beforeClose: function(){
						if (!chgRole) {
							endQisSession(null, false, false);
						}
					}
				});
			}
			else {
				location.href = $QisSession.HTMLLocation + "?cid=" + cid;
			}			
			
			return true;
		}
		
		$QisSession.IsInPrivateWorkspace = (configuration.IsPrivateWorkspace === true);
   	};
	
	$.when.apply(null, queue).done(function(){
		var addJs = $QepSettings.Functions.AdditionalJS || $.noop;
	        addJs.call();
	        $.initSessionStyles();
	        $.initPageStyle();
	        $.addTemplateLogo();
       
	        if ($QepSettings.QChat.Enable) {
	            if (!window.MangoChatConfig) {
	                $.getScript("./res.axd?v=56.0")
	                    .done(function() {
	                        $.initChatSession();
	                    })
	                    .fail(function (data) {
	                        $.jnotify({
	                            title: $.LL("Chat Initialization Failed"),
	                            message: $.LL("Resource:") + " " + "res.axd?v=56.0 <br/>" + $.LL("Error:") + " " + data.status + " " + data.statusText
	                        });
	                    });
	            } else {
	                $.initChatSession();
	            }
	        }
		
		$QisSession.FncQueue.push(onScriptLoad);
		initQisSession();
	});
}

$.initSessionStyles = function() {
	var sessiontheme = $.browserCache("get", "persistent-sessiontheme" + $QisSession.ConfId);
	if ($QepSettings.Theme) {
		if (sessiontheme) {
			$QepSettings.Theme.Css = sessiontheme;
		}
		else {
			$.browserCache("set", "persistent-sessiontheme" + $QisSession.ConfId, $QepSettings.Theme.Css);
		}
	}
	
	if (!String($QepSettings.Theme.Css).match(/default/i)) {
		if (!$("head").find("session-theme").length) {
			$("head").append($("<link/>")
			.attr("href", "css/" + $QepSettings.Theme.Css + "/" + $QepSettings.Theme.Css + ".css")
			.attr("type", "text/css")
			.attr("rel", "stylesheet")
			.addClass("session-theme"));
		}
	}
	
	var imgCss = "";
	
	switch (String($QepSettings.Theme.Css).toLowerCase()) {	
		case "qep-metro-light": 
			imgCss = "qep.icons.black"; 
			break;
		
		case "qep-metro-dark": 
			imgCss = "qep.icons.white"; 
			break;
			
		default: imgCss = "qep.icons.white";
	}
	
	$('<link />').attr({ 
		rel : "stylesheet", 
		type : "text/css", 
		href : "css/" + imgCss + ".css"
	}).appendTo($('head'));

	$("html,body").addClass($QepSettings.Theme.Css)
		.css("font-family", $QepSettings.FontFamily);
	$("#bar:first").addClass("ui-widget-header");
	
	var addCss = $QepSettings.Functions.AdditionalCSS;		
	if (addCss) {
		$("body").prepend("<style>" + addCss + "</style>");
	}
}

$.initPageStyle = function() {
	History.enabled = $QepSettings.SinglePageApplication && $.browser.singlepage;
	
	if (History.enabled && !History.binding) {
		History.binding = true;
		History.Adapter.bind(window, "statechange", function() { 
			$.instantClick();
		});
	}
	if ($QepSettings.ResponsiveLayout){
		if ($(window).innerWidth() < 1000 || $.browser.mobile) {
			$("#contentLeft, #contentHeaderLeft").hide();
			$.cookie("_ResponsiveLeft_", 1);
		}
		if ($(window).innerHeight() < 700 || $.browser.mobile) {
			$("#header").css("margin-top", - $("#header").height());
			$.cookie("_ResponsiveTop_", 1);
		}
	}
	else {
		$.removeCookie("_ResponsiveTop_");
		$.removeCookie("_ResponsiveLeft_");
	}
	
	var resize = -1;
	
	$(window).resize(function (e) {
		var nt = $(e.target).get(0).nodeType || 9;
		
		if (nt != 9) {
			return;
		}
		
		$.async(false, resize);	
		resize = $.async(function(){
			$.adjustTemplate(true);
			$.async(function(){$.adjustTemplate(true);},100);
		}, 150)
	});
}

$.initChatSession = function() {
	MangoChatConfig.AvatarClicked = function (userId) {	};
	$.extend(true, MangoChatConfig, $QepSettings.QChat.MangoChatConfig);
	MangoChatConfig.Stylesheets.ThemeStylesheetUrl = "";
	
	if ($.cookie("MangoChatSessionCookie")) {
		mcClient = new MangoChatClient();
		mcClient.start();
		
		$.async(function(){
			var $cnt = $(".mc-main-container");
			var $opts = {
				axis: "x", 
				containment: "body",
				handle: ".mc-dialog-body",
				start: function(event, ui) {
					ui.helper.bind("click.prevent", function(event) { event.preventDefault(); });
				},
				stop: function(event, ui) {
					setTimeout(function(){ui.helper.unbind("click.prevent");}, 300);
				}
			};
			$cnt.find(".mc-column").draggable($opts);
		}, 1000);
	}
	else {
		try {
			$QisSession.FncQueue.push(function(){
				var date = new Date();
				var time = date.getTime();
				var offs = (date.format().split(/UTC|GMT|EDT|CDT|MDT|PDT|EST|CST|MST|PST/i)[1]).split(" ")[0];
				var user = $QisSession.UserInfo.Login;
				
				if (user.match(/@/)) {
					user = user.split("@")[0]
				}
				else if (user.match(/\\/)) {
					user = user.split("\\")[1];
				}
				
				user = user.replace(/ |\./g, "");
				user = user.replace(/-/g, "_");
				
				var data = {
					SessionStartTime: "/Date(" + time + offs + ")/",
					SessionUser: {
						UserId: user ,
						Username: $QisSession.UserInfo.FullName,
						ImageUrl: null
					}
				};
				
				$.cookie("MangoChatSessionCookie", window.JSON.stringify(data));
				
				mcClient = new MangoChatClient();
				mcClient.start();
				
				$.async(function(){
					var $cnt = $(".mc-main-container");
					var $opts = {
						axis: "x", 
						containment: "body",
						handle: ".mc-dialog-body",
						start: function(event, ui) {
							ui.helper.bind("click.prevent",	function(event) { event.preventDefault(); });
						},
						stop: function(event, ui) {
							setTimeout(function(){ui.helper.unbind("click.prevent");}, 300);
						}
					};
					$cnt.find(".mc-column").draggable($opts);
				}, 1000);
			});
		}
		catch(e){
			$.browser.warn("Chat module is not available");
		}
	}
	
	
	$(document).onchange(function(){
		var $chat = $(".mc-main-container");
		
		if (!$chat.length) {
			return;
		}
		
		$(document).onchange(false);
		
		$("head").find("link[href='']").detach().remove();
		
		$chat.css({
			"font-size": "80%"
		}).hide();		
				
		if ($QisSession.UserInfo.FullName) {
			$chat.show();
		}
		else {
			$QisSession.FncQueue.push(function(){
				$chat.show();
			});
		}	
	});
}

$.instantClick = function() {
	$.each($QisSession.Init, function(key, val){
		if (key.indexOf("-poll") > -1) {
			clearInterval(val);
			delete $QisSession.Init[key];
		}
	});
	$.async(false);
	
	var $data = $.pageCache("get", "clicked") || null;
	if ($data) {
		$.pageCache("del", "clicked");
	}
	
	return initQisObject($data);
}

$.isAFunction = function () {
    if (!arguments.length) {
        return false;
    }

    return (typeof window[arguments[0]] === "function")
}

$.keyCount =  function() {
	var obj = arguments[0];
	
	if(typeof obj == "object" && obj) {
		var i, count = 0;
		var isJQuery = obj.jquery;
		var excluded = "context,prevObject,selector";
		
		if (obj.length && !isJQuery) {
			return obj.length;
		}
		
		for(i in obj) {
			if (isJQuery && excluded.indexOf(i)>-1) {
				continue;
			}
			if (obj.hasOwnProperty(i)) {
				count++;
			}
		}
		return count;
	} 
	else {
		return 0;
	}
}

$.cleanUpDom = function() {
	$("#main").removeClass("no-background");
	$("#header").css("opacity", 1);
	$("#bar").css("opacity", 1);
	$("#content").contents().off();
	$("#content").show().siblings(".quali-wall").off().remove();
		
	$(".wallpaper, .role-button, .theme-button, #responsive").html("").off().remove();
	$(".k-list-container").html("").off().remove();
	$(".autocomplete-suggestions").html("").off().remove();
	$(".timer").html("").off().remove();
}

$.adjustTemplate = function () {
	var onResize = false;
    
	if (typeof arguments[0] == "boolean") {
		onResize = arguments[0]
	}
	
	$.initResponsiveLayout();

	var pagW = $(window).width() - 1;	

	$("html, body").addClass("no-overflow");
	var pagH = $(window).height() - 1;
	
	if ($.browser.version < 10 && $.browser.msie) {
		pagW -= 13;
		pagH -= 5;
	}
    
	var hdrH = $("#header").outerHeight() 
			 + (parseInt($("div#header").css("margin-top")) || 0)
			 + $("#bar").outerHeight();
    var cntH = pagH - hdrH;
    var cntW = pagW;

    var $cnt = $("table#content");
    $cnt.css({
        "width": pagW + "px",
        "height": pagH - cntH + "px",
        "min-height": pagH - cntH + "px"
    })

    var $dtc = $cnt.find("#dataCenter");
	
    if ($dtc.length) {
        var cntPx = $cnt.outerWidth() - $cnt.width();
        var cntPy = $cnt.outerHeight() - $cnt.height();
        var dtcH = cntH - cntPy - $cnt.find("tr#contentHeader").outerHeight();
        var dtcW = cntW - cntPx;

        if ( !$("#contentLeft").css("display").match(/none/i) ) {
            dtcW -= parseInt($("#contentLeft").children().css("min-width") || $("#contentLeft").outerWidth());
			dtcW -= parseInt($("#contentLeft").css("margin-left"));
        }

        if ( !$("#contentRight").css("display").match(/none/i) ) {
            dtcW -= $("#contentRight").children().css("min-width") || $("#contentRight").outerWidth();
        }
		
        $dtc.css({
            "width": dtcW + "px",
            "height": dtcH + "px"
        });

        var $tabs = $dtc.find(".tabs");
		
		if ($tabs.length) {
			var tabPx = $tabs.outerWidth() - $tabs.width();
			var tabPy = $tabs.outerHeight() - $tabs.height();

			$tabs
				.css({
					"width": dtcW - tabPx + "px",
					"height": dtcH - tabPy - cntPy / 2 + "px"
				});

			var $tabcnt = $tabs.find(".tabContent");

			var tabCntPx = $tabcnt.outerWidth() - $tabcnt.width();
			var tabCntPy = $tabcnt.outerHeight() - $tabcnt.height();
			var tabCntW = $tabs.width() - tabCntPx;
			var tabCntH = $tabs.height() - $tabs.find(".ui-tabs-scroll-container").outerHeight() - tabCntPy;

			$tabcnt.css({
				"width": tabCntW + "px",
				"height": tabCntH + "px",
				"overflow": "auto"
			});
			
			$tabs.adjustTabScroll();
		}
    }

    if ($.browser.compatible) {
        $cnt.css("padding-bottom", 0);
    }

    $("html, body").addClass("overflow-y");
    $("#topmenu").adjustMenu();
}

$.initResponsiveLayout = function () {
	if (!$QepSettings.ResponsiveLayout) {
		return;
	}

	if (typeof _ResponsiveTemplate_ != "undefined"){
		$.async(false, _ResponsiveTemplate_);
	}
	
	_ResponsiveTemplate_ = $.async(function(){	
		if ( $(window).innerHeight() < 700 || $.browser.mobile) {
			if ( !parseInt($("div#header").css("margin-top")) ) {
				$.cookie("_ResponsiveTop_", 1);
				$("html").addClass("responsive-top");
				$("div#header, div#topmenu").animate(
					{"margin-top":"-75px"}, 
					function(){
						$.adjustTemplate();
					});
			}
		}
		else {
			if ( parseInt($("div#header").css("margin-top")) < 0) {
				$.removeCookie("_ResponsiveTop_");
				$("html").removeClass("responsive-top");
				$("div#header, div#topmenu").animate(
					{"margin-top":"0px"}, 
					function(){
						$.adjustTemplate();
					});
			}
		}
		
		var $left = $("#contentHeaderLeft, #contentLeft");
		var $btns = $("#btn-SlidePanel, #btn-Application");
		
		if ( $(window).innerWidth() < 1000 || $.browser.mobile ) {
			var $responsive = $("tr#responsive");
			if ( !$responsive.length && $("#dataLeft").length) {
				$.cookie("_ResponsiveLeft_", 1);
				$("html").addClass("responsive-left");
				var moveContent = function (){
					$btns.hide();
					var $cnt = $("<div/>").addClass("responsive-content")
						.html($("#contentLeft").contents().detach()); 
					$("#content").append($("<tr/>")
						.attr("id", "responsive")
						.append($("<td/>").hide())
						.append($("<td/>").html($cnt.css({
								"font-size": "85%",
								"margin-left": 0
							})
							.data("width", $cnt.children().width())
							.data("offx", parseInt($cnt.children().css("margin-left")))
							.children().css({
								"margin-left": 0,
								"width": "100%"
							}).end())));
					$$.GovernanceActions(null, null, false);
					$.async(function(){
						$.adjustTemplate();
					});
				};
				moveContent();
				if (!$left.data("collapsed")) {
					$left.hide();
				}
			}
		}
		else {
			var $responsive = $("tr#responsive");
			if ( $responsive.length ) {
				$.removeCookie("_ResponsiveLeft_");
				$("html").removeClass("responsive-left");
				var restoreContent = function(){
					$btns.show();
					var $cnt = $responsive.children("td:last").contents().detach();
					$("#contentLeft").html($cnt.contents().css({
						"width": "100%",
						"margin-left": $cnt.data("offx")  + "px"
					}));
					$responsive.remove();
					$$.GovernanceActions(null, null, false);
					$.async(function(){
						$.adjustTemplate();
					});
				};
				restoreContent();
				if (!$left.data("collapsed")) {
					$left.show();
				}
			}
		}
				
		delete _ResponsiveTemplate_;
		
		var $tabs = $(".tabs:first");
		if (!$tabs.length) {
			return;
		}
		
		var $cnt = $tabs.getActiveTab();
		if (!$cnt.length) {
			return;
		}
		
		var $graph = $cnt.find(".graphicalData");
		if (!$graph.length) {
			return;
		}
		if ($graph.hasClass("svg")) {
			if (!window["$$"]) {
				return;
			}
			
			var $defs = $QepSettings[$$.TemplateDefinition["SettingsFile"]] ||
				$QepSettings.Definitions || { Svg: {} };
			if ($defs.Svg.ZoomFit) {
				$graph.svgZoom(-100);
			}
		}
	}, 100);
}

$.adjustScroll = function(){
	if (typeof _AdjustTemplate_ != "undefined"){
		$.async(false, _AdjustTemplate_);
	}
	
	_AdjustTemplate_ = $.async(function(){
		var scrollAdj = $("body").hasClass("scroll");
		var scrollBar = $(document).outerHeight() > $(window).outerHeight();
		
		if ( scrollBar && !scrollAdj ){
			$("body").addClass("scroll");
			$.adjustTemplate();
		}
		else if ( !scrollBar && scrollAdj ){
			$("body").removeClass("scroll");
			$.adjustTemplate();
		}
		delete _AdjustTemplate_;
	}, 200);
}

$.addTemplateLogo = function () {
	var logoimg = new Image();
	var logofn = $QepSettings.HTMLLogoFile;

	var reload = arguments[0] || false;
	
	if (!logofn) {
		switch ($.browserCache("get", "persistent-sessiontheme" + $QisSession.ConfId)) {
			case "qep-metro-dark":
				logofn = "css/qep-metro-dark/images/_logo_white.png";
				break;
				
			case "qep-metro-white":
				logofn = "css/qep-metro-white/images/_logo_black.png";
				break;
				
			default: logofn = "images/_logo.png";
		}
	}
	else {
		logofn = "images/" + logofn.substring(logofn.lastIndexOf("\\") + 1);
	}
	
	var $logo = $("<div id=\"logo\" >/").appendTo($("#header").empty());
	
	logoimg.onload = function() {
		if (!this.width || !this.height) {
			logoimg.onload = null;
			delete logoimg;
			return (reload ? null : $.addTemplateLogo(true));
		}
		var dw = Math.min(265/this.width, 1);
		var dh = Math.min(75/this.height, 1);

		var fc = Math.min(dw,dh) / Math.max(dw,dh);
		$(this).css({
			"width": this.width*fc + "px",
			"height": this.height*fc + "px",
			"top": ((75-this.height*fc))/2 + "px",
			"position": "relative"
		});
		
		$logo.html(logoimg);
	};
	
	logoimg.src = logofn;
	
	$(logoimg).on($.browser.click, function () {
		var cid = getUrlParams("cid").split("#")[0];
		var lcd = getUrlParams("lc").split("#")[0];
		
		location.href = $QisSession.HTMLLocation
			+ ((cid || lcd) ? "?" : "")
			+ (cid ? ("cid=" + cid) : "")
			+ (lcd ? ("lc=" + lcd) : "");
		
		return false;
	});
}

$.chainFade = function() {
	if (!arguments.length) {
		return;
	}
	
	var $list = arguments[0];
	var speed = arguments[1] ? arguments[1] : 300;
	var reverse = arguments[2] ? true : false;
	var callback = arguments[3] ? arguments[3] : $.noop;
	var selector = arguments[4] ? arguments[4] : null;
	
	var length = $list.length;

	if (!reverse) {
		$list.each(function(idx) {
			var $item = selector ? $(this).find(selector) : $(this);
			if ( idx < (length-1) ) {
				$item.delay(idx*speed*0.75).fadeIn(speed);
			}
			else {
				$item.delay(idx*speed*0.75).fadeIn(speed, function(){ callback.call($list); });
			}
		});
	} 
	else {
		$list.each(function(idx) {
			var $item = selector ? $(this).find(selector) : $(this);
			if ( idx < (length-1) ) {
				$item.delay((length-idx)*speed*0.75).fadeOut(speed);
			}
			else {
				$item.delay((length-idx)*speed*0.75).fadeOut(speed, function(){ callback.call($list); });
			}
		});
	}
	
	return $list;
}

$.jribbon = function () {
	var msg = arguments.length 
		? arguments[0] 
		: ($QisSession.IsInPrivateWorkspace 
			? getConfigurationInfo($QisSession.ConfId).Title 
			: null);
			
	if (!msg){
		return;
	}

	var $msg = $("<div />")
		.addClass("ribbon persistent-message")
		.hide()		
		.html($("<a/>")
			.html(msg)
			.css("cursor","default"))
		.fadeIn(1000);
	
	if ($.browser.compatible) {
		$msg.css("top", 0).find("a")
			.css("padding-left",0);
	}
	
	if ( $.browser.version < 10 && $.browser.msie) {
		$msg.css({
			"-ms-transform" : "none",
			"top": 0
		});
	}
	
	$("body").append($msg);
}

$.jbar = function(options) {
	var $opts = {
		bgcolor: 'transparent',
		opacity: 1,
		messageColor: 'rgb(4,106,56)',
		captionColor: 'black',
		position: 'top',
		messageSize: '20pt',
		captionSize: '12pt',
		removebutton: false,
		title: "Message...",
		caption: "caption...",
		position: "top",
		time: 10,
		onShow: function () { },
		onHide: function () { }
	};
	
	if (arguments[0]) {
		$opts = $.extend($opts, arguments[0]);
		if ($opts.time === 0) {
			$opts.time = 1;
		}
	}
		
	var $bar = $(document.body).children(".jbar");
	
	if (!$bar.length){
		$(".persistent-message").remove();		
		$bar = $("<div/>").hide();		

		var timer = -1;
		var barH = $("#header").outerHeight() + $("#bar").outerHeight();
		var $msg = $("<span/>")
			.addClass('jbar-content')
			.html($opts.title)
			.css({
				"color": $opts.messageColor,
				"font-size": $opts.messageSize,
				"height": $opts.messageSize,
				"line-height": $opts.messageSize,
				"margin-top": barH/4 - 2 + "px"
			});
		
		var $caption = $("<span/>")
			.addClass('jbar-caption')
			.html($opts.caption)
			.css({
				"color": $opts.captionColor,
				"font-size": $opts.captionSize,
				"height": $opts.captionSize,
				"line-height": $opts.captionSize,
				"display": "block",
				"margin-top": "6px"
			});
				
		if ($opts.position == 'bottom'){
			$bar.addClass('jbar jbar-bottom')
		}
		else{
			$bar.addClass('jbar jbar-top')
				.css({
					"height": barH + "px",
					"line-height": barH + "px"
				})
		}
		
		$bar
			.css({
				"background-color" : $opts.bgcolor,
				"filter": "progid:DXImageTransform.Microsoft.Alpha(opacity="+$opts.opacity*100+")",
				"opacity": $opts.opacity,
				"-moz-opacity": $opts.opacity
			})
			.append($msg)
			.append($caption);
		
		if ($opts.removebutton){
			var $cross = $("<span/>")
				.addClass('jbar-remove')
				.css({
					"position": "absolute",
					"right": "15px",
					"top": barH / 3 + "px",
					"font-size": $opts.captionSize,
					"height": $opts.captionSize,
					"line-height": $opts.captionSize,
					"color": "black",
					"cursor": "pointer"
				})
				.html("[x] ")
				.append($("<span/>")
					.addClass("jbar-remove-time")
					.css({
						"font-size": "0.85em",
						"position": "relative",
						"top": "1px"
					})
					.html($opts.time + "s.")
				)
				.on($.browser.click, function(e){
					$.async(false, timer);
					$(".jbar").animate({"top": "-100px"},750, function(){
						$.jribbon();
						$(".jbar").remove();
					});
					$opts.onHide.call();
				})
				.appendTo($bar)
		}
		else{				
			$bar
				.css({"cursor"	: "pointer"})
				.on($.browser.click, function(e){
					$.async(false, timer);
					$(".jbar").animate({"top": "-100px"},750, function(){
						$.jribbon();
						$(".jbar").remove();
					});
					$opts.onHide.call();
				})
		}
		
		$opts.onShow.call();
		$("body").append($bar.fadeIn());
		
		if ($opts.time > 0) {
			timer = $.async(function(){
				$opts.time--;
				if ($opts.removebutton) {
					$(".jbar").find(".jbar-remove-time").html($opts.time + "s.");
				}
				
				if ($opts.time) {
					return;
				}
				
				if ($(".jbar").length){
					$.async(false, this);
					$(".jbar").animate({"top":"-100px"}, 750, function(){
						$.jribbon();
						$(".jbar").remove();
					});
					$opts.onHide.call();
				}
				else {
					$.async(false, this);
				}
			}, 1000, true);
		}
	}
	
	return $bar;
}

$.jnotify = function() {
	var $opts = {
		title: $.LL("Information"),
		message: null,
		lifespan: 5000,
		closebtn: true,
		width: 275,
		persistent: false,
		callback: $.noop,
		reload: false
	}
	
	if (arguments.length) {
		$opts = $.extend($opts, arguments[0]);
	}
	
	if (!$opts.message){
		return;
	}

	var $growl = $(".jgrowl-container");
	
	if (!$growl.length) {
		$growl = $("<div/>")
			.addClass("jgrowl-container")
			.css({
				"width": "275px"
			})
			.appendTo($("body"));
	}
	
	if ($opts.classid) {
		$("." + $opts.classid).hide();
	}

    $('<div/>').qtip({
		content: {
			text: $opts.message,
			title: {
				text: $opts.title,
				button: $opts.closebtn
			}
		},
		position: {
			target: [0,0],
			container: $growl
		},
		show: {
			event: true,
			ready: true,
			effect: function() {
				$(this).stop(0, 1).fadeIn();
			},
			delay: 0,
			persistent: $opts.persistent
		},
		hide: {
			event: false,
			effect: function(api) {
				$(this).fadeOut("slow");
			}
		},
		style: {
			width: $opts.width,
			classes: ($opts.reload ? "reload " : "") 
				+ ($opts.classid ? ($opts.classid + " ") : "") 
				+ "jgrowl qtip-jtools",
			tip: false
		},
		events: {
			show: function (event, api) { 
				var $nots = $("body").children(".growl").css("position","static").detach();
				$growl.find(".jgrowl:last").css("position","static").append($nots);
			},
			hide: function(){ $opts.callback.call();},
			render: function(event, api) {
				if (!api.options.show.persistent) {
					$(this).bind('mouseover mouseout', function(e) {
						if (api.timer) {
							$.async(false, api.timer);
						}
						if (e.type !== 'mouseover') {
							api.timer = $.async(function() { 
								api.hide(e); 
								$.async(function(){
									api.destroy(true);
								},1000);
							}, $opts.lifespan);
						}
					})
					.triggerHandler('mouseout');
				}
			}
		}
	})	
}

$.loader = function(){
	var $opts = {
		container: $("body"),
		text: $.LL("Loading"),
		dots: true
	};
	
	if (arguments[0]) {
		$.extend(true, $opts, arguments[0]);
	}
	else if (arguments[0] === false) {
		if (arguments[1]) {
			$opts.container = arguments[1];
		}
		var $loader = $opts.container.find(".ajaxloader-large");
		if ($loader.length) {
			$loader.fadeOut(600, function(){
				$loader.remove();
			});
		}
		return;
	}
		
	var $loader = $("<div/>").appendTo($opts.container).addClass("ajaxloader-large")
		.append($("<span/>").addClass("loader-large"))
		.append("<h3>" + $.LL($opts.text) + ($opts.dots ? ("...") : "") + "</h3>");
	
	return $loader;
}

$.overlay = function() {
	var $opts = {
		container: $("body"),
		init: true,
		loader: false,
		text: null,
		hideOnClick: false,
		transparent: false,
		instant: false
	};
	
	if (arguments.length) {
		if (arguments[0] === false) {
			$opts.init = false;
			$opts.instant = arguments[1] || $opts.instant;
		}
		else {
			$.extend(true, $opts, arguments[0]);
		}
	}
	
	var $cnt = $opts.container;
	var $overlay = $cnt.find("div.page-overlay");
	
	if (!$opts.init) {
		if ($overlay.length) {
			if ($overlay.next().hasClass("fancybox-overlay")) {
				$opts.instant = true;
			}
			$opts.instant ?
				$overlay.stop().detach().remove() :
				$overlay.stop().fadeOut(200, function(){$overlay.remove();});
			$("body").removeClass("overlay");
		}
		else if ($("body").hasClass("overlay")) {
			$.async(function() {
				$.overlay($opts);
			}, 500);
		}
		return;
	}
	
	if ($overlay.length) {
		$.overlay(false);
	}
		
	$overlay = $("<div/>")
		.addClass("fancybox-overlay")
		.addClass("fancybox-overlay-fixed")
		
	$overlay.addClass("page-overlay");

	if ($opts.transparent || $.fancybox.current) {
		$overlay.addClass("page-overlay-transparent");
	}
	
	$overlay.on($.browser.click, function(e){
		e.preventDefault();
		if ($opts.hideOnClick) {
			$.overlay(false);
		}
	});
	
	if ($opts.loader) {
		$overlay.append($("<div/>")
			.attr("id", "fancybox-loading")
			.addClass("fancybox-loading")
			.addClass("loading-image")
			.html("<div></div>"));
	}
	
	if ($opts.text) {
		if ($opts.text === true) {
			$opts.text = $.LL("Loading") + "...";
		}
		
		var $text = $("<div/>")
			.appendTo($overlay)
			.addClass("loading-text")
			.html($opts.text);
	}
	
	$("body").addClass("overlay");
	
	$.async(function(){
		$overlay.appendTo($cnt);
		$opts.instant ?
			$overlay.show() :
			$overlay.fadeIn(200);
	}, 500);
	
	$overlay.bind("keydown", function(e){
		var key = e.which ? e.which : e.charCode;
		if (key == 27) {
			e.preventDefault();
			$overlay.fadeOut(200, function(){
				$(this).remove();
				$("body").removeClass("overlay");
			});
		}
	});
	
	return;
}

$.SearchFullText = function() {
	var nohighlights = arguments[0] || false;
	
	var $form = $("<div/>").addClass("searchfulltext-dialog");
	
	var $heading = $("<h1/>").addClass("searchfulltext-heading")
		.html(window["QSEHeading"] || $.LL("Fritekstsøk"))
		.appendTo($form);
	
	var $text = $("<div/>").addClass("searchfulltext-caption")
		.appendTo($form);
		
	//var $caption = $("<div/>").html(window["QSECaption"] || "Full-text queries perform linguistic searches against text data in full-text indexes by operating on words and phrases based on the rules of a particular language such as English or Danish. Full-text queries can include simple words and phrases or multiple forms of a word or phrase. A full-text query returns any objects or documents that contain at least one match (also known as a hit). A match occurs when a target object or document contains all the conditions specified in the full-text query.")
	var $caption = $("<div/>").html(window["QSECaption"] || "Fritekstsøk")
		.appendTo($text);
	/*
	var $examples = $("<ul/>").appendTo($text)
		.append($("<li/>").html("<div>" + $.LL("Vanlig tekst:") + "</div>Hapå"))
		.append($("<li/>").html("<div>" + $.LL("Asterix *:") + "*ost*"))
		.append($("<li/>").html("<div>" + $.LL("Search in text attributes:") + "</div>Name:Economy AND HasResponsible:\"John Doe\""))
		.append($("<li/>").html("<div>" + $.LL("Search in date attribute:") + "</div>Date#OriginatedDate:[20170101 20170630]"))
		.find("li").css("margin-bottom", "5px").find("div").css("font-weight", "bold");
	*/
	var $cnt = $("<div/>").addClass("searchfulltext-query")
		.appendTo($form);
	
	var timeout = -1;
	
	var $input = $("<input/>").addClass("searchfulltext-input")
	.attr("placeholder", $.LL("Enter search criteria..."))
	.on("keyup", function(e){
		$.async(false, timeout);
		var val = $input.val();
		var key = e.which ? e.which : e.charCode;
		if (key == 13) {
			$.browserCache("del", "search-"+$QisSession.ConfId);
			$form.addClass("full-search").removeClass("lim-search");
			QseSearch(val);
		}
		else if (val.length > 2) {
			timeout = $.async(function(){
				$form.addClass("lim-search").removeClass("full-search");
				QseSearch(val, 5, 0, true);
			}, 600);
		}
		else if (!val.length) {
			timeout = $.async(function(){
				$form.removeClass("lim-search").removeClass("full-search");
				$text.fadeIn();
				$search.css("margin-top", "-7px");
			}, 1000);
		}
	})
	.appendTo($cnt);
	
	var $search = $("<button/>").addClass("searchfulltext-button")
		.html($.LL("Search"))
		.button().on($.browser.click, function(e){
			e.preventDefault();
			$(this).blur();
			QseSearch($input.val());
		}).appendTo($cnt);
	
	var $result = $("<div/>").addClass("searchfulltext-result")
		.appendTo($form);
	
	var $ctx = new QisCommon.RepositoryContextId($QisSession.RepId, $QisSession.ConfId);
	
	var showResult = function($this, q){
		var $div = $("<div/>").addClass("searchfulltext-hit")
			.appendTo($result);

		$qis.objects.findObjectById($ctx, $this.id.objectId, 3).done(function(data){
			var $o = data.attributes;
			var $obj = new RepositoryObject({
				Name: $o.Name.value.content,
				Template: $o.Template.value.content,
				ObjId: $this.id.objectId,
				RevisionId: $this.id.revisionId,
			});
			
			$div.append($obj.HyperLink().addClass("searchfulltext-link"))
				.append($("<span/>").addClass("searchfulltext-template").html($.LL($obj.Data.Template)))
				.append($("<hr/>").addClass("searchfulltext-splitter"));
			
			if (!nohighlights) {
				$.each($this.highlights, function(key, val) {
					var query = q.replace(/AND|OR/gi, "").split(" ");
					$.each(query, function(idx, sq){
						if (sq.indexOf(":")>-1) {
							sq = sq.split(":")[1];
						}
						
						val = (val||"").replace(new RegExp($.trim(sq), "gi"), function(matched){
							return "<span class=\"searchfulltext-marker\" >" + matched + "</span>";
						});
					});
					
					var $h = $("<div/>").addClass("searchfulltext-highlight")
						.append($("<div/>").addClass("searchfulltext-key")
							.html("[" + $.LL(key) + "]"))
						.append($("<div/>").addClass("searchfulltext-value").html(val))
						.appendTo($div);
				});
			}
		});
	};
	
	var $cache = $.browserCache("get", "search-"+$QisSession.ConfId, $result.html());
	if ($cache) {
		var tmp = $cache.split("_#####_");
		var q = tmp[0];
		var data = $.parseJSON(tmp[1]);
		$text.hide();
		$search.css("margin-top", "-7px");
		$input.val(q);
		$.each(data, function(){
			showResult(this, q);
		});
		
		var $summary = $("<div/>").css("text-align", "center")
			.css("margin-top", "15px")
			.append($("<span/>").html($.LL("Showing:") + " " + data.length  + " " + "hits"));
				
		$summary.addClass("hyperlink")
			.append($("<span/>").html(" ("))
			.append($("<a/>").html($.LL("Load more hits"))
				.on($.browser.click, function(){
					$summary.empty().remove();
					QseSearch(q, null, data.length);
				}))
			.append(")");
				
		$result.append($summary);
		$.fancybox.reposition();
	}

	var $data = [];
	var QseSearch = function(){
		var q = arguments[0] || null;
		var n = arguments[1] || 20;
		var x = arguments[2] || 0;
		var l = false;
		
		if (typeof arguments[3] !== "undefined") {
			l = arguments[3];
		}
		
		if (!x) {
			$data = [];
			$result.empty();
		}
		
		if (!q) {
			$result.html("<i>" + $.LL("Search query with at least 3 characters is required...") + "</i>");
			$.async(function(){
				$result.empty();
			}, 5000);
			$text.show();
			$search.css("margin-top", "-7px");
			$.fancybox.reposition();
		}
		else {
			$text.fadeOut();
			$search.css("margin-top", "-6px");
			$result.ajaxLoaderText();
			$qis.text.searchWithHighlights($ctx, q,  n, x).done(function(data){
				$result.ajaxLoaderText(false);
				if (!data.length) {
					if (!x) {
						$result.html("<h3>" + $.LL("No results. Try a different search criteria...")+ "</h3>");
					}
					else {
						var $summary = $("<div/>").css("text-align", "center")
						.css("margin-top", "15px")
						.append($("<span/>").html($.LL("Showing:") + " " + parseInt(n+x)  + " " + "hits"))
						.append($("<span/>").html(" (" + $.LL("No more hits") + ")"));
							
						$result.append($summary);
					}
					return;
				}
										
				$.each(data, function(){
					if (l && $form.hasClass("full-search")) {
						return false;
					}
					
					$data.push(this);
					showResult(this, q);
				});
				
				if (l && $form.hasClass("lim-search")) {
					return false;
				}
				
				var $summary = $("<div/>").css("text-align", "center")
					.css("margin-top", "15px")
					.append($("<span/>").html($.LL("Showing:") + " " + parseInt(n+x)  + " " + "hits"));
						
				if (data.length == n) {
					$summary.addClass("hyperlink")
						.append($("<span/>").html(" ("))
						.append($("<a/>").html($.LL("Load more hits"))
							.on($.browser.click, function(){
								$summary.empty().remove();
								QseSearch(q, l ? null : n, parseInt(n+x));
							}))
						.append(")");
							
					$result.append($summary);
				}
				
				$.fancybox.reposition();
				$.browserCache("set", "search-"+$QisSession.ConfId, q + "_#####_" + JSON.stringify($data), 120000);
			});
		}
	};
	
	$.fancybox($form);
}

$.SearchForm = function() {
	var qurl = $QepSettings.Search.Query;
	var $parms = $QepSettings.Search.Parameters || {};
		
	if (!$.keyCount($parms) || !qurl) {
		$.fancybox($div.html("<h1>Search query is not configured</h1>"));
		return;
	}
	
	var $vals = arguments[0] ? $.parseJSON(arguments[0]) : {};
	var callback = arguments[1] ? arguments[1] : $.noop;
	
	var $cnt = $("<div/>")
		.addClass("searchform")
		.append("<h3>" + $.LL("Search") + "</h3>");

	$.each($parms, function() {
		var initval = $vals[this.Attribute] || "";
		
		switch (String(this.Type).toLowerCase()) {
			case "input":
				var $divInput = $("<div/>")
					.append("<label for=\"" + this.Attribute + "\">" + this.Prompt + "</label>")
					.append("<input name=\"" + this.Attribute + "\" value=\"" + initval + "\" />")
					.appendTo($cnt);
				break;
			
			case "select":
			case "multiselect":
				var valarr = initval ? initval.split(",") : [];
				var ismsel = String(this.Type).toLowerCase()==="multiselect";
				
				var $divChoice = $("<div/>")
					.append("<label for=\"" + this.Attribute + "\">" + this.Prompt + "</label>")
					.append("<select name=\"" + this.Attribute + "\" " + (ismsel ? "multiple=\"multiple\"" : "") + ">");
					
				$divChoice.find("select")
					.append("<option value=\"\" " + (valarr.length ? "" : "selected") + ">" + $.LL("Select") + "..." + "</option>");
				
				$.each(this.Options.split(","), function(idx, val){
					var selected = valarr.match(val);
					$divChoice.children("select")
						.append("<option value=\"" + val + "\" " + (selected ? "selected" : "") + ">" + val + "</option>");
				});
				
				$divChoice.append("</select>").appendTo($cnt);
				
				if (ismsel) {
					$divChoice.find("select").children().first().remove();
					$divChoice.find("select").addClass("multiselect").multiselect({
						selectedList: 1
					});
				}
				else {
					$divChoice.find("select").addClass("singleselect");
				}
				break;
			
			default: break;
		}
	});

	$cnt.addClass("fancy-dialog search-dialog")
		.css({
			"width": "400px",
			"font-size": "9pt"
		})
		.find("h3")
			.css({
				"margin-bottom": "0.8em", 
				"font-size": "1.3em",
				"padding-bottom": "0.4em",
				"border-bottom": "solid 1px #000"
			}).end()
		.find("div")
			.css({
				"line-height": "1.5em",
				"margin": "4px 5px"
			})
			.find("label")
				.css({
					"display": "inline-block",
					"width": "100px",
					"vertical-align": "top",
					"position": "relative",
					"top": "2.5px"
				}).end()
			.find("input")
				.css({
					"display": "inline-block", 
					"width": "200px"
				})
				.end()
			.find("select").css({
					"height": "16px",
					"width": "202px"
				}).end()
			.find("select.singleselect")
				.selectmenu()
				.on("selectmenuopen", function(event, ui){
					$(".ui-selectmenu-open").css({
						"font-size": $QepSettings.FontSize
					})
					.find(".ui-menu-item").css({
						"font-family": $QepSettings.FontFamily,
						"line-height": "20px"
					})
				}).end()
			.find(".ui-selectmenu-text")
				.css({
					"font-family": $QepSettings.FontFamily
				}).end().end()
			.find(".ui-multiselect")
				.css({
					"height": "22px",
					"width": "205px",
					"font-size": $QepSettings.FontSize*1.1,
					"font-family": $QepSettings.FontFamily
				})
				.on($.browser.click, function(){
					$(".ui-multiselect-checkboxes")
					.css({
						"font-size": $QepSettings.FontSize*0.9,
						"font-family": $QepSettings.FontFamily
					}).end()
				})
				.parent().css({
					"margin-top": "-5px"
				}).end();
			
	
	var $btn = $("<div/>")
		.append("<button class=\"btnSubmit\">" + $.LL("Search") + "</button>")
		.append("<button class=\"btnCancel\">" + $.LL("Cancel") + "</button>")
		.css({
			"text-align": "right",
			"margin-top": "2em"
		})
		.appendTo($cnt);
		
	$btn.children(".btnSubmit").each(function(){
		$(this).button().on($.browser.click, function(){
			$btn.children(".btnSubmit").attr("disabled", true);
			$.overlay({
				loader: true,
				transparent: true
			});
			$.async(function(){
				callback.call($cnt, $parms);
			}, 250);
		})
		.css({
			"line-height": "2em",
			"font-size": "0.9em",
			"margin-left": "4px"
		});
	})
	
	$btn.children(".btnCancel").each( function(){
		$(this).button().on($.browser.click, function(){
			$.fancybox.close();
		})
		.css({
			"line-height": "2em",
			"font-size": "0.9em",
			"margin-left": "4px"
		});
	});
	
	$cnt.bind("keydown", function(e) {
		if(e.which == 13) {
			e.preventDefault();
			$btn.children(".btnSubmit").trigger($.browser.click);
		}
	});
	
	$.fancybox($cnt);
}

$.initSearchQuery = function() {
	var oid = $QepSettings.Search.Query;
	
	if (!oid) {
		return;
	}
	
	var href = "?oid=" + oid;
	
	if ($QisSession.ConfId !== $QisSession.BaseConfId) {
		href += "&cid=" + $QisSession.ConfId;
	}
	
	if ($QisSession.LangCode !== $QisSession.DefaultLangCode) {
		href += "&lc=" + $QisSession.LangCode;
	}

	var $anch = $("<a/>")
		.attr("href", href)
		.data({
			objid: oid,
			template: "HTMLQueryResultView",
			confid: $QisSession.ConfId,
			repid: $QisSession.RepId
		})
		.setTarget(false)
		.trigger($.browser.click);
	
	$.async(function(){
		$.fancybox.close();
	}, 1500);
}

$.PrintReport = function() {
	if (!arguments.length) {
		return;
	}
	
	var callback = arguments[0];
	var $obj = arguments[1] || $$;
	
	var $defs = $QepSettings[$$.TemplateDefinition["SettingsFile"]];
	
	if (!$defs) {
		return;
	}
	
	var $report = $defs.Report;

	if (!$.keyCount($report)) {
		return;
	}
		
	var $cnt = $("<div />")
		.append($("<h3/>")
			.html("<span>" + $.LL("Select Print Report") + "</span>"))
		.append($("<div/>").addClass("formselector"));
	
	var $usr = $QisSession.UserInfo;
	
	$.each($report, function(idx){
		if (isValidRole(this.RoleFilter)
		&& isValidCondition(this.Condition, $obj)) {
			var $opt = $("<span/>")
				.append($("<input/>")
					.attr("type", "radio")
					.attr("name", "fcyinput")
					.attr("id", this.Id)
					.data("name", this.Name || "")
					.data("static", this.Static || false)
					.data("disabled", this.Disabled || false)
					.data("template", this.Template))
				.append($("<label/>")
					.attr("for", this.Id)
					.html(this.Name))
				.appendTo($cnt.find(".formselector"));
		}
	});
	
	$cnt.find("input:first").attr("checked", true);

	$cnt.addClass("fancy-dialog print-dialog")
		.css({
			"width": "300px",
			"font-size": "9pt"
		})
		.find("h3")
			.css({
				"margin-bottom": "0.8em", 
				"font-size": "1.3em",
				"padding-bottom": "0.4em",
				"border-bottom": "solid 1px #000"
			})
			.end()
		.find(".formselector")
			.css("min-height", "150px")
			.find("span")
				.css({
					"display": "block",
					"line-height": "1.5em",
					"margin-left": "2px",
					"vertical-align": "middle"
				})
				.find("input")
					.css({
						"position": "relative",
						"top": "2px",
						"cursor": "pointer"
					})
					.end()
				.find("label")
					.css({
						"position": "relative",
						"left": "5px",
						"top": "-1px",
						"cursor": "pointer"
					})
					.end();

	$btn = $("<div/>")
		.append("<button class=\"btnSubmit\">" + $.LL("Print") + "</button>")
		.append("<button class=\"btnCancel\">" + $.LL("Cancel") + "</button>")
		.css({
			"text-align": "right",
			"margin-top": "2em"
		})
		.appendTo($cnt);
		
	$btn.children(".btnSubmit").each(function(){
		$(this).button().on($.browser.click, function(){
			$.fancybox.close();			
			var $sel = $cnt.find(".formselector input:checked");
			callback.call($obj, $sel.attr("id"), $sel.data());
		})
		.css({
			"line-height": "2em",
			"font-size": "0.9em",
			"margin-left": "4px"
		});
	})

	$btn.children(".btnCancel").each(function(){
		$(this).button().on($.browser.click, function(){
			$.fancybox.close();
		})
		.css({
			"line-height": "2em",
			"font-size": "0.9em",
			"margin-left": "4px"
		});
	});
	
	$cnt.bind("keydown", function(e){
		var key = e.which ? e.which : e.charCode;
		if (key == 13) {
			e.preventDefault();
			$cnt.find(".btnSubmit").trigger($.browser.click);
		}
	});
		
	$.fancybox($cnt);
}

$.ConfigurationSelector = function(){	
	var fncOnSubmit = arguments.length ? arguments[0] : $.noop;
	var fncOnCancel = arguments.length > 1 ? arguments[1] : $.noop;

	var $cnt = $("<div />")
		.append($("<h3/>")
			.html("<span>"+$.LL("Change Configuration")+"</span>"))
		.append($("<div/>").hide()
			.attr("id", "baseconfiguration")
			.addClass("formsplitter")
			.html("<span>"+$.LL("Base")+"</span>")
			.append($("<div/>").addClass("formselector baseoptions")))
		.append($("<div/>").hide()
			.attr("id", "pwsconfiguration")
			.attr("class", "formsplitter")
			.html("<span>"+$.LL("Private Workspace")+"</span>")
			.append($("<div/>").addClass("formselector pwsoptions")));
			
	$.each($QisSession.Configurations||{}, function(idx, key){
		var $conf = getConfigurationInfo(key, {RoleFilter: true});
		if (!$conf.Id) {
			return true;
		}
		var $opt = $("<span/>")
			.append($("<input/>")
				.attr("type", "radio")
				.attr("name", "fcyinput")
				.attr("id", "input" + $conf.Id)
				.val($conf.Id))
			.append($("<label/>")
				.attr("for", "input" + $conf.Id)
				.html($conf.Title))
			.appendTo($conf.IsPrivateWorkspace 
						? $cnt.find("div.pwsoptions") 
						: $cnt.find("div.baseoptions"));
		
		if ($conf.Id == $QisSession.ConfId) {
			$opt.find("input").attr("checked", true);
		}
	})
	
	$cnt.addClass("fancy-dialog configuration-dialog")
		.css({
			"width": "450px",
			"font-size": "9pt"
		})
		.find("h3")
			.css({
				"margin-bottom": "0.8em", 
				"font-size": "1.3em",
				"padding-bottom": "0.4em",
				"border-bottom": "solid 1px #000"
			})
			.end()
		.find(".formsplitter").each(function(){
			if ($(this).find("input").length) {
				$(this).show();
			}
		})
		.end()
		.find(".formsplitter > span")
			.css({
				"margin": "0.5em 0 0.25em 0",
				"display": "block",
				"padding": "0.2em 0.5em"
			})
			.addClass("ui-state-default")
			.end()
		.find(".formselector")
			.css({
				"display": "block",
				"line-height": "1.5em",
				"vertical-align": "middle"
			})
			.find("span")
				.css({
					"display": "block",
					"line-height": "1.5em",
					"margin-left": "2px",
					"vertical-align": "middle"
				})
				.find("input")
					.css({
						"position": "relative",
						"top": "2px",
						"cursor": "pointer"
					})
					.end()
				.find("label")
					.css({
						"position": "relative",
						"left": "5px",
						"top": "-1px",
						"cursor": "pointer"
					})
					.end()
	
	$btn = $("<div/>")
		.append("<button class=\"btnSubmit\">" + $.LL("Submit") + "</button>")
		.append("<button class=\"btnCancel\">" + $.LL("Cancel") + "</button>")
		.css({
			"text-align": "right",
			"margin-top": "2em"
		})
		.appendTo($cnt);
		
	$btn.children(".btnSubmit").each(function(){
		$(this).button().on($.browser.click, function(){
			var $conf = getConfigurationInfo($cnt.find("input:checked").val());
			$.fancybox.close();
			fncOnSubmit.call($conf);
		})
		.css({
			"line-height": "2em",
			"font-size": "0.9em",
			"margin-left": "4px"
		});
	});
	
	$btn.children(".btnCancel").each(function(){
		$(this).button().on($.browser.click, function(){
			$.fancybox.close();
			fncOnCancel.call();
		})
		.css({
			"line-height": "2em",
			"font-size": "0.9em",
			"margin-left": "4px"
		});
	});
	
	$cnt.bind("keydown", function(e){
		var key = e.which ? e.which : e.charCode;
		if (key == 13) {
			e.preventDefault();
			$cnt.find(".btnSubmit").trigger($.browser.click);
		}
	});
		
	$.fancybox($cnt);
}

$.LanguageSelector = function(){	
	var fncOnSubmit = arguments[0] ? arguments[0] : $.noop;
	var fncOnCancel = arguments[1] ? arguments[1] : $.noop;
	
	var clang = $QisSession.LangCode;

	var $cnt = $("<div />")
		.append($("<h3/>")
			.html("<span>" + $.LL("Change Working Language") + "</span>"))
		.append($("<div/>").addClass("formselector"));
		
	$.each($LanguageInfo.Data, function(idx){
		var $opt = $("<span/>")
			.append($("<input/>")
				.attr("type", "radio")
				.attr("name", "fcyinput")
				.attr("id", "input" + idx)
				.val(this.Code))
			.append($("<label/>")
				.attr("for", "input" + idx)
				.append($("<img/>")
					.attr("src", "images/language/" + this.Code + ".png"))
				.append(this.Name))
			.appendTo($cnt.find(".formselector"));
			
		if (this.Code == clang) {
			$opt.find("input").attr("checked", true);
		}
	});

	$cnt.addClass("fancy-dialog language-dialog")
		.css({
			"width": "300px",
			"font-size": "9pt"
		})
		.find("h3")
			.css({
				"margin-bottom": "0.8em", 
				"font-size": "1.3em",
				"padding-bottom": "0.4em",
				"border-bottom": "solid 1px #000"
			})
			.end()
		.find(".formselector")
			.css("min-height", "150px")
			.find("span")
				.css({
					"display": "block",
					"line-height": "1.5em",
					"vertical-align": "middle"
				})
				.find("input")
					.css({
						"position": "relative",
						"top": "1px",
						"cursor": "pointer"
					})
					.end()
				.find("label")
					.css({
						"position": "relative",
						"left": "3px",
						"cursor": "pointer"
					})
					.find("img")
						.css({
							"width": "32px",
							"height": "32px",
							"margin-right": "5px",
							"position": "relative",
							"top": "7px"
						})
					.end();

	$btn = $("<div/>")
		.append("<button class=\"btnSubmit\">" + $.LL("Submit") + "</button>")
		.append("<button class=\"btnCancel\">" + $.LL("Cancel") + "</button>")
		.css({
			"text-align": "right",
			"margin-top": "2em"
		})
		.appendTo($cnt);
		
	$btn.children(".btnSubmit").each(function(){
		$(this).button().on($.browser.click, function(){
			var val = $cnt.find("input:checked").val();
			
			if ($QisSession.LangCode == val) {
				$.fancybox.close();
				return;
			}

			$lang = getLanguageInfo(val);
					
			$.fancybox.close();
			fncOnSubmit.call($lang);
		})
		.css({
			"line-height": "2em",
			"font-size": "0.9em",
			"margin-left": "4px"
		});
	});

	$btn.children(".btnCancel").each(function(){
		$(this).button().on($.browser.click, function(){
			$.fancybox.close();
			fncOnCancel.call();
		})
		.css({
			"line-height": "2em",
			"font-size": "0.9em",
			"margin-left": "4px"
		});
	});
	
	$cnt.bind("keydown", function(e){
		var key = e.which ? e.which : e.charCode;
		if (key == 13) {
			e.preventDefault();
			$cnt.find(".btnSubmit").trigger($.browser.click);
		}
	});
		
	$.fancybox($cnt);
}

$.UserRoleSelector = function() {
	var fncOnSubmit = arguments[0] || $.noop;
	var fncOnCancel = arguments[1] || $.noop;
	
	var arole = $QisSession.UserInfo.AltRoles.split(";");
	var crole = $QisSession.UserInfo.Role;
	
	arole.push(crole); 
	arole.sort();

	var $cnt = $("<div />")
		.append($("<h3/>")
			.html("<span>" + $.LL("Change User Role") + "</span>"))
		.append($("<div/>").addClass("formselector"));
		
	$.each(arole, function(idx){
		var $opt = $("<span/>")
			.append($("<input/>")
				.attr("type", "radio")
				.attr("name", "fcyinput")
				.attr("id", "input" + idx)
				.val(this))
			.append($("<label/>")
				.attr("for", "input" + idx)
				.html(this))
			.appendTo($cnt.find(".formselector"));
			
		if (this == crole) {
			$opt.find("input").attr("checked", true);
		}
	});

	$cnt.addClass("fancy-dialog role-dialog")
		.css({
			"width": "300px",
			"font-size": "9pt"
		})
		.find("h3")
			.css({
				"margin-bottom": "0.8em", 
				"font-size": "1.3em",
				"padding-bottom": "0.4em",
				"border-bottom": "solid 1px #000"
			})
			.end()
		.find(".formselector")
			.css("min-height", "150px")
			.find("span")
				.css({
					"display": "block",
					"line-height": "1.5em",
					"vertical-align": "middle"
				})
				.find("input")
					.css({
						"position": "relative",
						"top": "2px",
						"cursor": "pointer"
					})
					.end()
				.find("label")
					.css({
						"position": "relative",
						"left": "5px",
						"top": "-1px",
						"cursor": "pointer"
					})
					.end();

	$btn = $("<div/>")
		.append("<button class=\"btnSubmit\">" + $.LL("Submit") + "</button>")
		.append("<button class=\"btnCancel\">" + $.LL("Cancel") + "</button>")
		.css({
			"text-align": "right",
			"margin-top": "2em"
		})
		.appendTo($cnt);
	
	var submit = false;
	
	$btn.children(".btnSubmit").each(function(){
		$(this).button().on($.browser.click, function(){
			var $role = {};
			$role.UserRole = $cnt.find(".formselector input:checked").val();
			
			if ($role.UserRole  == crole) {
				$.fancybox.close();
				return;
			}
			
			arole = [];
			$cnt.find(".formselector input:not(:checked)").each(function () {
				arole.push($(this).val());
			});
			
			$role.AlternativeRoles = arole;
			submit = true;
			$.fancybox.close();
			fncOnSubmit.call($role);
		})
		.css({
			"line-height": "2em",
			"font-size": "0.9em",
			"margin-left": "4px"
		});
	});

	$btn.children(".btnCancel").each(function(){
		$(this).button().on($.browser.click, function(){
			$.fancybox.close();
			fncOnCancel.call();
		})
		.css({
			"line-height": "2em",
			"font-size": "0.9em",
			"margin-left": "4px"
		});
	});
	
	$cnt.bind("keydown touch", function(e){
		var key = e.which ? e.which : e.charCode;
		if (key == 13) {
			e.preventDefault();
			$cnt.find(".btnSubmit").trigger($.browser.click);
		}
	});
		
	$.fancybox($cnt);
}

$.ThemeSelector = function() {
	var callback = arguments[0] || $.noop;
	var sessiontheme = $QepSettings.Theme ? $QepSettings.Theme.Css : "default";
			
	var $cnt = $("<div />")
		.append($("<h3/>")
			.html("<span>" + $.LL("Select theme") + "</span>"))
		.append($("<div/>").addClass("formselector"));
		
	var themes = [{
			Name: "QualiWare Default",
			Id: "default"
		},{
			Name: "QualiWare Metro-Dark",
			Id: "qep-metro-dark"
		},{
			Name: "QualiWare Metro-Light",
			Id: "qep-metro-light"
		}];
	
	var $tbl = $("<table/>")
		.css({
			"padding": 0,
			"border": 0
		});
	
	$.each(themes, function(idx){
		var $opt = $("<tr/>")
			.append($("<td/>").addClass("fancy-input")
				.append($("<input/>")
					.attr("type", "radio")
					.attr("name", "fcyinput")
					.attr("id", "input" + idx)
					.data("theme", this.Name)
					.val(this.Id)))
			.append($("<td/>").addClass("fancy-label")
				.append($("<label/>")
					.attr("for", "input" + idx)
					.append("<div>" + this.Name + "</div>")
					.append("<img src=\"./images/themes/" + this.Id + ".png\" />")))
			.appendTo($tbl);

		var regexp = new RegExp(sessiontheme, "i");
		if (this.Id.match(regexp)) {
			$opt.find("input").attr("checked", true);
		}
	});
	
	$tbl.appendTo($cnt.find(".formselector"));

	$cnt.addClass("fancy-dialog theme-dialog")
		.css({
			"width": "430px",
			"font-size": "9pt"
		})
		.find("h3")
			.css({
				"margin-bottom": "0.8em", 
				"font-size": "1.3em",
				"padding-bottom": "0.4em",
				"border-bottom": "solid 1px #000"
			})
			.end()
		.find(".formselector")
			.css("min-height", "500px")
			.find("td")
				.css({
					"padding-top": "10px",
					"padding-left": "30px",
					"line-height": "1.5em",
					"vertical-align": "middle"
				})
				.find("input")
					.css({
						"position": "relative",
						"top": "2px",
						"cursor": "pointer"
					})
					.end()
				.find("label")
					.css({
						"font-weight": "bold",
						"position": "relative",
						"left": "-20px",
						"cursor": "pointer"
					})
					.find("span").css({
						"margin-bottom": "2px"
					})
					.end();

	$btn = $("<div/>")
		.append("<button class=\"btnSubmit\">" + $.LL("Save") + "</button>")
		.append("<button class=\"btnCancel\">" + $.LL("Cancel") + "</button>")
		.css({
			"text-align": "right",
			"margin-top": "2.2em"
		})
		.appendTo($cnt);
	
	$btn.children(".btnSubmit").each(function(){
		$(this).button().on($.browser.click, function(){
			$.fancybox.close(true);
			callback.call({
				Name: $cnt.find(".formselector input:checked").data("theme"),
				Id: $cnt.find(".formselector input:checked").val()
			});
		})
		.css({
			"line-height": "2em",
			"font-size": "0.9em",
			"margin-left": "4px"
		});
	});

	$btn.children(".btnCancel").each(function(){
		$(this).button().on($.browser.click, function(){
			$.fancybox.close();
		})
		.css({
			"line-height": "2em",
			"font-size": "0.9em",
			"margin-left": "4px"
		});
	});
	
	$cnt.bind("keydown touch", function(e){
		var key = e.which ? e.which : e.charCode;
		if (key == 13) {
			e.preventDefault();
			$cnt.find(".btnSubmit").trigger($.browser.click);
		}
	});
	
	$.fancybox($cnt);
}

$.initUserRoleChange = function () {
	onCancel = arguments[0] || $.noop;
	$.UserRoleSelector( function() {
		changeRepositoryRole.call(this);	
	}, onCancel)
}

$.LoginForm = function() {
	var fncOnSubmit = arguments[0] || $.noop;
	var fncOnCancel = arguments[1] || $.noop;
	var invalidText = arguments[2] || null;
	
	var $opts = {
		RequiresFullAuthentication : false,
		AuthenticationText : false
	};
	
	if (arguments[3]) {
		$.extend(true, $opts, arguments[3]);
	}
		
	var $cnt = $("div.authentication-form");
	
	if ($cnt.length) {
		$.overlay(false);
		$cnt.empty()
			.data("retry", $cnt.data("retry")+1);
	}
	else {
		$cnt = $("<div/>")
			.addClass("authentication-form")
			.data("retry", 0)
	}
	
	$cnt.data("submit", false)
		.append($("<h3/>")
			.html("<span>" + $.LL("Authentication Required") + "</span>"));
	
	if ($opts.AuthenticationText) {
		$cnt.append($("<div/>").addClass("authtext")
			.html($.parseString($opts.AuthenticationText)));
	}
	
	$cnt.append($("<div/>").addClass("formselector"));
	
	$("<span/>")
		.append($("<input />")
			.addClass("username")
			.attr("id", "username")
			.attr("required", "")
			.attr("type", "text")
			.attr("placeholder", $.LL("Username"))
			.val(""))
		.appendTo($cnt.find(".formselector"));
		
	$("<span/>")
		.append($("<input />")
			.addClass("password")
			.attr("id", "password")
			.attr("required", "")
			.attr("type", "password")
			.attr("placeholder", $.LL("Password"))
			.val(""))
		.appendTo($cnt.find(".formselector"));
	
	var domain = $QisSession.UserInfo.Login.split("@")[1];
	
	if (domain) {
		$("<span/>")
			.append($("<select />")
				.addClass("domain")
				.attr("id", "domain")
				.attr("required", "")
				.attr("placeholder", $.LL("Domain"))
				.append($("<option/>")
					.attr("selected", "selected")
					.html($.LL("Domain:")+ " " + domain))
				.append($("<option/>")
					.html($.LL("Domain:") + " " + "Local")))
			.appendTo($cnt.find(".formselector"));
	}
		
	if (invalidText) {
		$("<span/>")
			.css({
				"color": "red",
				"margin-bottom": "-14px",
				"display": "block"
			})
			.addClass("invalid")
			.html(invalidText)
			.appendTo($cnt.find(".formselector"));
	}
		
	$cnt.addClass("fancy-dialog login-dialog")
		.css({
			"width": "350px",
			"font-size": "9pt"
		})
		.find("h3")
			.css({
				"margin-bottom": "0.8em", 
				"font-size": "1.3em",
				"padding-bottom": "0.4em",
				"border-bottom": "solid 1px #000"
			})
			.end()
		.find(".authtext")
			.find("br").hide().end()
			.find("p").css("margin-top","10px").end()
			.end()
		.find(".formselector")
			.css({
				"min-height": "50px",
				"margin-top": "20px"
			})
			.find("span")
				.css({
					"display": "block"
				})
				.find("input")
					.css({
						"padding": "9px 15px 7px 30px",
						"margin": "0 0 15px 25px",
						"width": "250px",
						"border": "1px solid #ccc",
						"-moz-border-radius": "5px",
						"-webkit-border-radius": "5px",
						"border-radius": "5px",
						"-moz-box-shadow": "0 1px 1px #ccc inset, 0 1px 0 #fff",
						"-webkit-box-shadow": "0 1px 1px #ccc inset, 0 1px 0 #fff",
						"box-shadow": "0 1px 1px #ccc inset, 0 1px 0 #fff"
					}).end()
				.find("input.username")
					.css({
						"background": "#f1f1f1 url(images/login-sprite.png) no-repeat 5px -9px"
					}).end()
				.find("input.password")
					.css({
						"background": "#f1f1f1 url(images/login-sprite.png) no-repeat 5px -58px"
					}).end()
				.find("select.domain")
					.selectmenu().end()
				.find(".ui-selectmenu-button")
					.css({
						"margin": "0 25px 5px 25px",
						"width": "285px",
						"padding": "4px"
					}).end();
	
	if (!$opts.RequiresFullAuthentication) {
		$cnt.find("input.username")
			.val($QisSession.UserInfo.Login)
			.hide();
	}

	if (domain) {
		$.async(function(){
			$("input.username").on("keyup", function(){
				var data = $("input.username").val();
				if (data.match(/\\/)) {
					$cnt.find(".ui-selectmenu-text")
						.text($.LL("Domain:") + " " + data.split("\\")[0]);
					$cnt.find("select.domain").selectmenu({disabled: true});
					$cnt.find(".ui-selectmenu-disabled").css("opacity", 0.5);
					$cnt.find(".ui-selectmenu-button").css("opacity", 0.5);
					return;
				}
				else if (data.match(/@/)) {
					$cnt.find(".ui-selectmenu-text")
						.text($.LL("Domain:") + " " + data.split("@")[1]);
					$cnt.find("select.domain").selectmenu({disabled: true});
					$cnt.find(".ui-selectmenu-button").css("opacity", 0.5);
					return;
				}
				$cnt.find("select.domain").selectmenu({disabled: false});
				$cnt.find(".ui-selectmenu-button").css("opacity", 1);
				return;
			});
		}, 100);
	}
	
	$btn = $("<div/>")
		.append("<button class=\"btnSubmit\">" + $.LL("Login") + "</button>")
		.append("<button class=\"btnCancel\">" + $.LL("Cancel") + "</button>")
		.css({
			"text-align": "right",
			"margin-top": "2em"
		})
		.appendTo($cnt);
		
	$btn.children(".btnSubmit").each(function(){
		$(this).button().on($.browser.click, function(e){
			var login = $cnt.find("input.username").val();
			var password = $cnt.find("input.password").val();
						
			var valid = true;
			
			if (!login) {
				$cnt.find("input.username")
					.animate({"border-color": "red"}, 500)
					.delay(1000)
					.animate({"border-color": "#ccc"}, 1000);
				valid = false;
			}
			
			if (!password) {
				$cnt.find("input.password")
					.animate({"border-color": "red"}, 500)
					.delay(1000)
					.animate({"border-color": "#ccc"}, 1000);
				valid = false;
			}
			
			if (!valid) {
				return;
			}
			
			$cnt.data("submit", true);
			$.overlay({loader: true, transparent: true});
			
			var authority = false;
			
			if (login.match(/\\|@/)) {
				authority = true;
			}
			else if (domain) {
				domain = $cnt.find(".ui-selectmenu-text").text();
				authority = (domain != ($.LL("Domain:") + " " + "Local"));
			
				if (authority) {
					domain = String($cnt.find(".ui-selectmenu-text").text())
						.replace($.LL("Domain:") + " ", "");
					login += "@" + domain;
				}
				else {
					domain = "";
				}
			}
			
			$.async(function(){
				fncOnSubmit.call({
					username : login,
					password : password,
					domain: domain,
					authority: authority
				});
			}, 750);
		})
		.css({
			"line-height": "2em",
			"font-size": "0.9em",
			"margin-left": "4px"
		});
	});

	$btn.children(".btnCancel").each(function(){
		$(this).button().on($.browser.click, function(){
			$.fancybox.close();
		})
		.css({
			"line-height": "2em",
			"font-size": "0.9em",
			"margin-left": "4px"
		});
	});
	
	if (!$cnt.data("retry")) {
		$cnt.bind("keydown", function(e){
			var key = e.which ? e.which : e.charCode;
			if (key == 13) {
				e.preventDefault();
				$cnt.find(".btnSubmit").trigger($.browser.click);
			}
		});
		
		var closeEvent = $.fancybox.defaults.beforeClose || $.noop;
		
		$.fancybox($cnt, {
			beforeClose: function() {
				closeEvent.call();
				$.overlay(false);
				if ($cnt.data("submit") === false) {
					$cnt.data("submit", true);
					fncOnCancel.call();
				}
			}
		});
	}
}

$.CommentDialog = function() {
	if (!arguments.length) {
		return;
	}
	
	var feature = this;
	
	var callback = arguments[0] || $.noop;
	var optional = arguments[1] || false;
	var ocomment = false;

	var $cnt = $("<div/>").addClass("fancy-dialog comment-dialog")
		.append(!feature.CommentText ? 
			$("<h3/>").html($.LL("Comment")) :
			$("<h4/>").html($.LL(feature.CommentText)))
		.append($("<textarea/>").attr("spellcheck", false)
			.css({"width": "388px", "height": "100px", "display": "block"}))
		.append($("<div/>").html($.LL("Invalid comment")).addClass("invalid-message"))
		.css({
			"width": "400px",
			"font-size": "9pt"
		})
		.find("h3").css({
			"margin-bottom": "1em", 
			"font-size": "1.3em",
			"padding-bottom": "0.4em",
			"border-bottom": "solid 1px #000"
		}).end()
		.find("h4").css({
			"margin-bottom": "1em", 
			"font-size": "1.1em"
		}).end()
		.find(".invalid-message").css({
			"position": "absolute",
			"bottom": "5px",
			"text-indent": "2px",
			"color": "red",
			"display": "none"
		}).end();
		
	$btn = $("<div/>")
		.append("<button class=\"btnSubmit\">&nbsp;" + $.LL("Submit") + "&nbsp;</button>")
		.append("<button class=\"btnSkip\">&nbsp;" + $.LL("Skip") + "&nbsp;</button>")
		.append("<button class=\"btnCancel\">" + $.LL("Cancel") + "</button>")
		.css({
			"text-align": "right",
			"margin-top": "2em"
		})
		.find("button").css({
			"line-height": "2em",
			"font-size": "0.9em",
			"margin-left": "4px"
		})
		.end()
		.appendTo($cnt);
		
	
	$btn.children(".btnSubmit").each(function(){
		$(this).button().on($.browser.click, function(){
			var $txt  = $cnt.find("textarea");
			ocomment = $txt.val();
			if (ocomment) {
				$.fancybox.close();
			}
			else {
				$cnt.find(".invalid-message").fadeIn();
				$.async(function(){
					$cnt.find(".invalid-message").fadeOut();
				}, 3000);
			}
		});
	});

	$btn.children(".btnSkip").each(function(){
		$(this).button().on($.browser.click, function(){
			ocomment = "";
			$.fancybox.close();
		});
	})
	.css({
		"display": optional ? "inline-block" : "none"
	});

	$btn.children(".btnCancel").each(function(){
		$(this).button().on($.browser.click, function(){
			ocomment = null;
			$.fancybox.close();
		});
	});
		
	$.fancybox($cnt, {
		modal: true,
		afterClose: function(){
			callback.call(null, ocomment);
		}
	});
}

$.RatingDialog = function() {
	if (!arguments.length) {
		return;
	}
	var $obj = this;
	
	var fnc = arguments[0];
	var ftr = arguments[1];
	var clb = arguments[2];
	var ret = null;
	
	var $cnt = $("<div/>").width(450)
		.append($("<h3/>").html("Rating").css({"margin": "0.2em 0", "font-size": "1.25em"}))
		.append("<div id=\"rateObject\" class=\"rateit\"/></div>")
		.append($("<div/>").html("<b>" + $.LL("Comment:") + "<b/>").css("margin","1em 0 0.2em 0"))
		.append($("<textarea/>").attr("spellcheck", false)
			.css({"width": "440px", "height": "100px", "display": "block"}));
		
	$btn = $("<div/>")
		.append("<button class=\"btnSubmit\">" + $.LL("Submit") + "</button>")
		.append("<button class=\"btnCancel\">" + $.LL("Cancel") + "</button>")
		.css({
			"text-align": "right",
			"margin-top": "1em"
		});
		
	$btn.children(".btnSubmit").each(function(){
		$(this).button().on($.browser.click, function(){
			ret = {
				objectAttributes: [
					{Name:"_RatingValue",Format:"PlainText",Value:$cnt.find("#rateObject").rateit("value")},
					{Name:"_RatingComment",Format:"PlainText",Value:$cnt.find("textarea").val()}
				]
			};
			$.fancybox.close();
		})
		.css({
			"line-height": "2em",
			"font-size": "0.9em",
			"margin-left": "4px"
		});
	})
	
	$btn.children(".btnCancel").each(function(){
		$(this).button().on($.browser.click, function(){
			$.fancybox.close();
			$obj.GovernanceActions();
		})
		.css({
			"line-height": "2em",
			"font-size": "0.9em",
			"margin-left": "4px"
		});
	});
					
	$.fancybox($cnt.append($btn), {
		modal: true,
		afterLoad: function() {
			if (typeof $.fn.rateit == "undefined") {
				$.getScript("js/rateit/jquery.rateit.min.js").done(function(){
					$cnt.find("#rateObject").rateit({step:1});
				});
			}
			else {
				$cnt.find("#rateObject").rateit({step:1});
			}
		},
		afterClose: function() {
			clb.call($obj, ret, fnc, ftr);
		}
	});
	
}

$.WarningDialog = function() {
	if (!arguments.length) {
		return;
	}
	
	var $data = arguments[0]
	var callback = arguments[1] || $.noop;
	
	var $msgs = $data.Messages;
	var isErr = $data.Result.match(/error/i);	
	
	if (!$msgs && $data.Message) {
		$msgs = { 
			0: {
				Type: $data.Result,
				Message: $data.Message
			}
		};
	}
	
	var nmsgs = $.keyCount($msgs);
	
	if (nmsgs === 0) {
		nmsgs = 1;
		isErr = true;
		$msgs = { 
			0: {
				Type: "Error",
				Description: $.LL("Failed executing governance action.") +  "<br/><br/>" 
					+ $.LL("View the QEF log entries for more information and details.")
			}
		};
	}
		
	var $cnt = $("<div/>").addClass("fancy-dialog warning-dialog")
		.width(500)
		.append($("<h3/>").css("margin-top", "0.5em"))
		.append($("<div/>").addClass("message-compact message-block")
			.append($("<div/>").addClass("message-error"))
			.append($("<div/>").addClass("message-warning"))
			.append($("<div/>").addClass("message-passed")));
		
	$.each($msgs, function() {
		var description = this.Description || null;
		var method = this.Method || "-";
		var message = this.Message || null;
		var msgtype = (this.Type || "").replace(/ok/i, "Passed");
		
		if (msgtype == "Passed") {
			nmsgs--;
		}
		
		if (!description) {
			description = message;
			message = null;
		}
				
		var $msg = $cnt.find(".message-" + msgtype.toLowerCase());
		if ($msg.length) {
			var $block = $("<div/>")
				.appendTo($msg)
				.addClass("message-check")
				.append($("<h4/>")
					.append($("<span/>").addClass("message-icon"))
					.append($("<span/>").html($.LL(msgtype))))
				.append($("<div/>").addClass("message-description").html(description))
				.append($("<div/>").addClass("message-method").html("<b>" + $.LL("Method:") + "</b> " + method));
			
			if (message) {
				$block.append($("<div/>").addClass("message-text")
					.html("<b>" + $.LL("More details:") + "</b><br/>" + message));
			}
		}
	});
	
	$cnt.find("h3").html(nmsgs + " " + (nmsgs > 1 ? $.LL("Messages") : $.LL("Message")));
			
	var $btn = $("<div/>")
		.append("<button class=\"btnDetails\">" + $.LL("Show details") + "</button>")
		.append("<button class=\"btnSubmit\">" + $.LL("Continue") + "</button>")
		.append("<button class=\"btnCancel\">" + (isErr ? $.LL("Close") : $.LL("Cancel")) + "</button>")
		.css({
			"text-align": "right",
			"margin-top": "2em"
		})
		.find("button").css({
			"line-height": "2em",
			"font-size": "0.9em",
			"margin-left": "4px"
		})
		.end()
		.appendTo($cnt);
	
	var $details = $btn.children(".btnDetails")
		.css({
			"float": "left",
			"margin-left": 0
		})
		.button().on($.browser.click, function(){
			var $block = $cnt.find(".message-block");
			var compact = $block.hasClass("message-compact");
			if (compact) {
				$block.removeClass("message-compact");
				$details.find(".ui-button-text").text($.LL("Hide details"));
			}
			else {
				$block.addClass("message-compact");
				$details.find(".ui-button-text").text($.LL("Show details"));
			}
			$.fancybox.reposition();
			$details.blur();
			$.async(function(){
				$details.blur();
			}, 100);
		});
		
	
	$btn.children(".btnSubmit").each(function(){
		$(this).button().on($.browser.click, function(){
			result = true;
			$.fancybox.close();
		});
	})
	.css({
		"display": isErr ? "none" : "inline-block"
	});

	$btn.children(".btnCancel").each(function(){
		$(this).button().on($.browser.click, function(){
			result = false;
			$.fancybox.close();
		});
	});
		
	$.fancybox($cnt, {
		modal: true,
		afterClose: function(){
			callback.call(null, result);
		}
	});
}

$.pageRefreshControlDialog = function() { 
	$.browserCache("clear");	
	$.initPageContent(null, true);			
}

$.ControlDialogInputValue = function(checkitemObj, $cnt2, $listOfCheckItems) {
	var objIdText = checkitemObj.attr("ObjId").text();
	var typeText = checkitemObj.attr("Type").text();
	var nameText = checkitemObj.attr("Name").text();
	var optParaText = checkitemObj.attr("OptionalTypeParameters").text();
			
	$cnt2.append("<hr style=\"width:98%\">");						
	$cnt2.append("<b>"+nameText+"</b><br/>");
	var desc ="";
	var temp = nameText.replace(/\./g,' ');
	var idName = temp.replace(/\s+/g, '');
				
	var eachPara = [];
	if(optParaText != "") {
		var OptTypePara = checkitemObj.attr("OptionalTypeParameters").text();
		var replaced = OptTypePara.replace(/\n|\r|<p>|<\/p>/g, "¤");
		eachPara = replaced.split("¤");
		eachPara.remove("");
	}
					
	if(checkitemObj.attr("ShortDescription").text() != "")
	{
		desc = 	checkitemObj.attr("ShortDescription").text();	
		$cnt2.append(desc + "</br>");
	}
					
	if(typeText != "") 
	{
			
		if(typeText == "Text") {
			$cnt2.append($("<textarea type=\"textarea\" name=\""+objIdText+"\" style=\"height:100px;width:400px;\"/>"));		
			$listOfCheckItems.push({id: objIdText, name: nameText, type: typeText, description:desc, question: nameText});
		}
					
			
		if(typeText == "Checkbox") {
			if(eachPara.toString() != "") {
				var maxArray = eachPara[eachPara.length-1].split(":");
				var minArray = eachPara[eachPara.length-2].split(":");
				var lngt = eachPara.length;
							
				maxPresent = maxArray[0].toString().search(/max/i);
				minPresent = minArray[0].toString().search(/min/i);

				var max = maxArray[1];
				var min = minArray[1];
							
				if(minPresent >= 0 && maxPresent >= 0) 
				{
					lngt = lngt-2;
								
				}
				for(var i = 0; i < lngt; i++) {
					$cnt2.append($("<input type=\"checkbox\" id=\""+eachPara[i]+"\" class=\""+objIdText+"\" name=\""+objIdText+"\" max=\""+max+"\"  min=\""+min+"\" value=\""+eachPara[i]+"\">"),eachPara[i]);
								
					if(i != lngt-1)
					{
						$cnt2.append("</br>");
					}
					$listOfCheckItems.push({id: objIdText, name: eachPara[i], type: typeText, description:desc, question: nameText});
				}
			}
			$("."+objIdText+"").on('change', function(e){
				$("."+objIdText+"").html("");
				if(max >= 0 && min >= 0) 
				{
					if((parseInt(min) <= $("."+objIdText+":checkbox:checked").length) && ($("."+objIdText+":checkbox:checked").length <= parseInt(max))) {
						$("."+objIdText+"").html("");	
					}
					else if($("."+objIdText+":checkbox:checked").length > parseInt(max)) {
						$("."+objIdText+"").html("");
						$("."+objIdText+"").html("</br>Info: Please choose maximum "+max+" boxes").css({"color":"red"});	
					}
					else if($("."+objIdText+":checkbox:checked").length < parseInt(min)) {
							$("."+objIdText+"").html("");
							$("."+objIdText+"").html("</br>Info: Please choose minimum "+min+" boxes").css({"color":"red"});	
					}
					else {
						$("."+objIdText+"").html("");
					}
				}
			});
			
			$cnt2.append("<span class=\""+objIdText+"\"><span>");
		}
					
			
		if(typeText == "Radio") {
			if(eachPara.toString() != "") {
				for(var i = 0; i < eachPara.length; i++) {
					$cnt2.append($("<input type=\"radio\" name=\""+objIdText+"\" value=\""+eachPara[i]+"\">"),eachPara[i]);
								
					if(i != eachPara.length-1) {
						 $cnt2.append("</br>");
					}
					$listOfCheckItems.push({id: objIdText, name: eachPara[i], type: typeText, description:desc, question: nameText});
				}												
			}
		}
			
		if(typeText == "Bool") {
			if(eachPara.toString() != "") {
				$cnt2.append($("<input type=\"radio\" name=\""+objIdText+"\" value=\""+eachPara[0]+"\">"),eachPara[0]+"</br>");
				$cnt2.append($("<input type=\"radio\" name=\""+objIdText+"\" value=\""+eachPara[1]+"\">"),eachPara[1]);
				$listOfCheckItems.push({id: objIdText, name: eachPara[0], type: typeText, description:desc, question: nameText});
				$listOfCheckItems.push({id: objIdText, name: eachPara[1], type: typeText, description:desc, question: nameText});
			}
			else {
				$cnt2.append($("<input type=\"radio\" id=\"True\" name=\""+objIdText+"\" value=\"True\"> "),"True </br>");
				$cnt2.append($("<input type=\"radio\" id=\"False\" name=\""+objIdText+"\" value=\"False\">"),"False");
				$listOfCheckItems.push({id: objIdText, name: "True", type: typeText, description:desc, question: nameText});
				$listOfCheckItems.push({id: objIdText, name: "False", type: typeText, description:desc, question: nameText});
			}				
						
		}
					
			
		if(typeText == "Number") {
			if(eachPara.length != 0) {					
				$cnt2
					.append("Number between "+eachPara[0]+" and "+eachPara[1]+"<br/>")
					.append($("<input type=\"number\" id=\""+idName+"\" name=\""+objIdText+"\" min=\""+eachPara[0]+"\" max=\""+eachPara[1]+"\" style=\"width:400px\">").change(function() {															
						if(parseInt(eachPara[0]) <= $(this).val() &&  $(this).val() <= parseInt(eachPara[1])) {
							$("."+idName+"").html("");
							$("#WithNonConformance").prop('checked', false);
						}
						else 
						{
							$("."+idName+"").html("");
							$("."+idName+"").html("</br>Info: The number does not match the allowed range - create a non conformance when finished").css({"color":"red"});
							$("#WithNonConformance").prop('checked', true);
						}
					}));
					
				$listOfCheckItems.push({id: objIdText, name: nameText, type: typeText, description:desc, question: nameText});
					
				$cnt2.append("<span class=\""+idName+"\"><span>");
			}
			else {
				$cnt2.append($("<input type=\"number\" id=\""+idName+"\" name=\""+objIdText+ "\" style=\"width:400px\">"));
				$listOfCheckItems.push({id: objIdText, name: nameText, type: typeText, description:desc, question: nameText});
			}			
		}

		if(typeText == "Date") {					
			$cnt2.append("<input type=\"text\" id=\""+objIdText+"\" name=\""+objIdText+"\" value=\"Choose date\" />");
			$(function() {
				$( "#"+objIdText+"" ).datepicker();
			});

			$listOfCheckItems.push({id: objIdText, name: nameText, type: typeText, description:desc, question: nameText});

		}
	}
	$cnt2.append("</br>");
}

$.GetSavedCheckResult = function(checklistVal, $cnt, $btn) {
	$.ajax({
		type: 'POST',
		url: initQisCsharpMethod("Asset_ServiceHandlers.GetCheckResult"),
		data: window.JSON.stringify({
			objectAttributes:[
				{Name:"ChecklistID", Format:"PlainText", Value: checklistVal.attr("ObjId").text()},
				{Name:"Concerns", Format:"PlainText", Value: $$.Data.ObjId}
			]
		})
	}).done(function(data) {
		if(data) {
	
		var $resultList = [];
	
		var loadExistingData = function(){
			var parsedXML = $.parseXML(data);
			var checklist = $(parsedXML).find('checklist');
			$.each($(checklist), function() {
				$(this).find('checkitem').each(function(){
				var oid = $(this).find('oid').text();
				var val = $(this).find('result').text();
					$resultList.push({id: oid, value:val});
				});					
			});
		};	
		
		$.async(function(){
			if ($cnt.isLoading()){
				return;
			}
			$.async(false, this);
			loadExistingData();
			if (!$resultList.length) 
			{
				$.browser.warn("Result list is empty");			
				return;
			}
			var textareas = $cnt.find('textarea');
			textareas.each(function() {
			var inputItem = $(this)[0];
			var nameId = inputItem.name;
				var inputVal = inputItem.value;
				for(var i = 0; i < $resultList.length;i++) {
					var resultItem = $resultList[i];
					if(resultItem.id == nameId) {
						this.value = resultItem.value;
					}
				}								
			});
			
			var checkitem = $cnt.find('input');	
			
			$cnt.find('input').each(function()  { 								
				var inputItem = $(this)[0];
				var nameId = inputItem.name;
				var inputVal = inputItem.value;
				for(var i = 0; i < $resultList.length;i++) {
					var resultItem = $resultList[i];
					if(resultItem.id == nameId) {
						switch(inputItem.type) {
							
							case "text":												
									inputItem.defaultValue = resultItem.value;												
								break;
							 case "radio":
								 if(inputItem.defaultValue == resultItem.value) {
									this.defaultChecked = true;
								 }
								break;
						case "number":
								inputItem.defaultValue = resultItem.value;
								if((parseInt(this.min) <= $(this).val() &&  $(this).val() <= parseInt(this.max)) || (this.min === "" && this.max === "")) {
									$("."+this.id+"").html("");																	
								}
								else {
									$("."+this.id+"").html("");
									$("."+this.id+"").html("</br>Info: The number does not match the desired range - create a non conformance when finished").css({"color":"red"});;
									$("#WithNonConformance").prop('checked', true);
								}
								break;
						case "checkbox":
							if(inputItem.defaultValue == resultItem.value) {
									this.defaultChecked = true;
									var max = this.max;
									var min = this.min;
									
									if(((parseInt(min) <= $("."+nameId+":checkbox:checked").length) && ($("."+nameId+":checkbox:checked").length <= parseInt(max))) || (this.min === "" && this.max === "")) {
										$("."+nameId+"").html("");	
									}
									else if($("."+nameId+":checkbox:checked").length > parseInt(max)) {
										$("."+nameId+"").html("");
										$("."+nameId+"").html("</br>Info: Please choose maximum "+max+" boxes").css({"color":"red"});	
									}
									else if($("."+nameId+":checkbox:checked").length < parseInt(min)) {
										$("."+nameId+"").html("");
										$("."+nameId+"").html("</br>Info: Please choose minimum "+min+" boxes").css({"color":"red"});	
									}
									else 
									{
										$("."+nameId+"").html("");
									}
								 }
								 break;
						default:
							break;
						}
					}
			}
			});
				
				$btn.find(".btnDelete").show()
					.click(
						function() 
						{
							$.ajax({
								type: 'POST',
								url: initQisCsharpMethod("Asset_ServiceHandlers.DeleteCheckResult"),
								data: window.JSON.stringify({
									objectAttributes:[
										{Name:"ChecklistId", Format:"PlainText", Value: checklistVal.attr("ObjId").text()},
										{Name:"AssetID", Format:"PlainText", Value: $$.Data.ObjId},
									]
								})
							}).done(function(){
								$.pageRefreshControlDialog();
							});
							
							$.fancybox.close();
						}
					);
			}, 100, true);
		}
	}).fail(function() {
		$.browser.error("GetSavedCheckResult failed");
	});	
}

$.ApproveCheckResult = function(checklistVal, $listOfCheckItems, callBack) {

	var $cntXML = "<xml><checklist><oid>"+checklistVal.attr("ObjId").text()+"</oid>";
	var resultlist2 = JSON.stringify($("#checkresult").serializeArray());			 
	var list = $.parseJSON(resultlist2);
	for(var listitem = 0; listitem < list.length; listitem++)
	{

		var objidList = list[listitem].name;
		var inputValue = list[listitem].value;

		for(var i = 0; i < $listOfCheckItems.length;i++) 
		{			
			if(objidList == $listOfCheckItems[i].id && inputValue != "") {	

				if($listOfCheckItems[i].type == "Checkbox" || $listOfCheckItems[i].type == "Radio" || $listOfCheckItems[i].type == "Bool") {
					if($listOfCheckItems[i].name == inputValue) {
					$cntXML = $cntXML + "<checkitem><question>"+$listOfCheckItems[i].question+"</question> <oid>"+$listOfCheckItems[i].id+"</oid><name>"+$listOfCheckItems[i].name+"</name>"+
					"<type>"+$listOfCheckItems[i].type+"</type>"+
					"<result>"+inputValue+"</result>"+
					"</checkitem>";
					}
				}
				else {
					if(inputValue != "thevalue" || inputValue != "Choose date") {
						$cntXML = $cntXML + "<checkitem><question>"+$listOfCheckItems[i].question+"</question><oid>"+$listOfCheckItems[i].id+"</oid><name>"+$listOfCheckItems[i].name+"</name>"+
					"<type>"+$listOfCheckItems[i].type+"</type>"+
					"<result>"+inputValue+"</result>"+
					"</checkitem>";
					}
				}
			}
		}
	}
	$cntXML = $cntXML + "</checklist></xml>";
	
	var checked = $('#WithNonConformance').is(":checked");
	$.ajax({
		type: 'POST',
		url: initQisCsharpMethod("Asset_ServiceHandlers.CreateCheckResult"),
		data: window.JSON.stringify({
			objectAttributes:[
				{Name:"ChecklistID", Format:"PlainText", Value: checklistVal.attr("ObjId").text()},
				{Name:"Value", Format:"PlainText", Value: $cntXML},
				{Name:"AssetID", Format:"PlainText", Value: $$.Data.ObjId},
				{Name:"NCChecked", Format:"PlainText", Value: checked},
				{Name:"UserID", Format:"PlainText", Value: $QisSession.UserInfo.ObjId},
				{Name:"Submit", Format:"PlainText", Value: "Submit"}
			]
		})
	}).done(function() {	
		callBack.call();
		$.pageRefreshControlDialog();		
	
	}).fail(function(error) { callBack.call(); });
	$.fancybox.close();
}

$.SaveCheckResult = function(checklistVal, $listOfCheckItems, callBack) {	
	var $cntXML = "<xml><checklist><oid>"+checklistVal.attr("ObjId").text()+"</oid>";
	var resultlist2 = JSON.stringify($("#checkresult").serializeArray());			 
	var list = $.parseJSON(resultlist2);
	for(var listitem = 0; listitem < list.length; listitem++)
	{
		var objidList = list[listitem].name;
		var inputValue = list[listitem].value;

		for(var i = 0; i < $listOfCheckItems.length;i++) 
		{			
			if(objidList == $listOfCheckItems[i].id && inputValue != "") {	
	
				if($listOfCheckItems[i].type == "Checkbox" || $listOfCheckItems[i].type == "Radio" || $listOfCheckItems[i].type == "Bool") {
					if($listOfCheckItems[i].name == inputValue) {
					$cntXML = $cntXML + "<checkitem><question>"+$listOfCheckItems[i].question+"</question> <oid>"+$listOfCheckItems[i].id+"</oid><name>"+$listOfCheckItems[i].name+"</name>"+
					"<type>"+$listOfCheckItems[i].type+"</type>"+
					"<result>"+inputValue+"</result>"+
					"</checkitem>";
					}
				}
				else {
					if(inputValue != "thevalue" || inputValue != "Choose date") {
						$cntXML = $cntXML + "<checkitem><question>"+$listOfCheckItems[i].question+"</question><oid>"+$listOfCheckItems[i].id+"</oid><name>"+$listOfCheckItems[i].name+"</name>"+
					"<type>"+$listOfCheckItems[i].type+"</type>"+
					"<result>"+inputValue+"</result>"+
					"</checkitem>";
					}
				}
			}
		}
	}
	$cntXML = $cntXML + "</checklist></xml>";

	$.ajax({
		type: 'POST',
		url: initQisCsharpMethod("Asset_ServiceHandlers.CreateCheckResult"),
		data: window.JSON.stringify({
			objectAttributes:[
					{Name:"ChecklistID", Format:"PlainText", Value: checklistVal.attr("ObjId").text()},
					{Name:"Value", Format:"PlainText", Value: $cntXML},
					{Name:"AssetID", Format:"PlainText", Value: $$.Data.ObjId},
					{Name:"NCChecked", Format:"PlainText", Value: "false"},
					{Name:"UserID", Format:"PlainText", Value: $QisSession.UserInfo.ObjId},
					{Name:"Submit", Format:"PlainText", Value: "Save"}
			]
		})
	}).done(function(){
		callBack.call();
		$.pageRefreshControlDialog();
	}).fail(function() {
		callBack.call();
	});	
	
	$.fancybox.close();
}

$.ControlDialog = function() {
	var checklistVal = this;
	var feature = arguments[0];
	var callBack = arguments[1] || $.noop;
	
	var $cnt = $("<form id=\"checkresult\" style=\"overflow:auto;border:1px solid #3d3d3d;padding:0px 0px 0px 5px\" />")
		.append($("<h1 style=\"text-align:center\"/>").html(checklistVal.attr("Name").text()))
			.width(500)
			.height(800);								
	$.fancybox($cnt,{modal:true});

	var $cnt1 = $("<span id=\"buttons\" />")
		.insertAfter(".fancybox-inner")	
		
	var $btn = $cnt1.append("<button class=\"btnSave\">" + $.LL("Save") + "</button>")
		.append("<button class=\"btnApprove\">" + $.LL("Approve") + "</button>")
		.append("<button class=\"btnCancel\">" + $.LL("Cancel") + "</button>")
		.append("<button class=\"btnDelete\">" + $.LL("Delete") + "</button>")
		.css({
			"text-align": "right",
			"margin-top": "2em"
		})
		.find("button").css({
			"line-height": "2em",
			"font-size": "0.9em",
			"margin-left": "4px"
		}).end()
		.find(".btnApprove").hide().end()
		.find(".btnDelete").hide().end();

	var ObjStatus = "";
	var $listOfCheckItems = [];
				
	var checklistObj = new RepositoryObject(checklistVal.attr("ObjId").text());
			
	checklistObj.Info("CheckItems", function(o) {
	var radioval;
		if(o.CheckItems!=null && o.CheckItems != "") 
		{				
			$.each(o.CheckItems.Value, function()
			{								
				var checkitem = this;
							
				var $cnt2 = $("<div style=\"padding:5px 0px 5px 0px\"/>").appendTo($cnt).ajaxLoaderText();
							
					var checkitemObj = new RepositoryObject(checkitem.ObjId);
					checkitemObj.init(function(q) {
						if(checkitemObj.Data.Template != "CheckList") 
						{
							$.ControlDialogInputValue(checkitemObj, $cnt2, $listOfCheckItems);
						}
						else
						{
							$.each(checkitemObj.Data.CheckItems.Value, function()
							{	
								var checkitem = this;
								var checkitemObj = new RepositoryObject(checkitem.ObjId);
								checkitemObj.init(function(q) {
									$.ControlDialogInputValue(checkitemObj, $cnt2, $listOfCheckItems);
								});
							});
						}
					});
				$cnt2.ajaxLoaderText(false);						
			});
				$.GetSavedCheckResult(checklistVal, $cnt, $btn);
		}				
	});  
				
	$.ajax({
		type: 'POST',
		url: initQisCsharpMethod("Asset_ServiceHandlers.GetLockStatus"),
		data: window.JSON.stringify({
			objectAttributes:[
				{Name:"ChecklistId", Format:"PlainText", Value: checklistVal.attr("ObjId").text()}
			]
		})
	}).done(function(data) {	
		if(data != "Locked") {
			$btn.find(".btnApprove").show()
			.click(function() {
				
				$.ApproveCheckResult(checklistVal, $listOfCheckItems, callBack);
			});
		}
						
		$cnt1.append($("<input type=\"checkbox\" id=\"WithNonConformance\" name=\"WithNonConformance\" value=\"WithNonConformancee\">"),$.LL("Create Non Conformance with approve"));
				
		if(data == "GrabLock") {
			$cnt1.append("<br/>N.B. You have the Asset open in another session <br/> - approving or saving the control will result in loss of unsaved changes.");
		}
		else if(data == "Locked") {
			$cnt1.append(" N.B. Cannot approve check (Asset is opened in another session)").css({"color":"red"});
		}
	});

	$btn.find(".btnSave").click(
	function() 
		{
			$.SaveCheckResult(checklistVal, $listOfCheckItems, callBack);
		}
	);
				
	$btn.find(".btnCancel")
	.click(
		function() {
			$.fancybox.close();
			$.pageRefreshControlDialog();
		});
				
	$btn.appendTo($cnt1).find("button").button();	
}

$.ExtendCompetenceLevelsDialog = function(personObjId, personName) 
{
	var $cnt = $("<form id=\"extenddialog\" style=\"overflow:auto;border:1px solid #3d3d3d;padding:0px 0px 0px 5px\" />")
		.append($("<h1 style=\"text-align:center\"/>").html(personName))
			.width(500)
			.height(500);								
	$.fancybox($cnt,{modal:true});

	var $cnt1 = $("<span id=\"buttons\" />").insertAfter(".fancybox-inner")	
		
	$cnt1
			.append("<button class=\"btnExtend\">" + $.LL("Extend") + "</button>")
			.append("<button class=\"btnCancel\">" + $.LL("Cancel") + "</button>")
			.css({
				"text-align": "right",
				"margin-top": "2em"
			})
			.find("button").css({
				"line-height": "2em",
				"font-size": "0.9em",
				"margin-left": "4px"
			}).button();		

	$cnt1.find(".btnCancel").click(function() {
		$.fancybox.close();
		$.pageRefreshControlDialog();
	});
	
	$cnt1.find(".btnExtend").click(function(){
		$list = [];
		$listOther = [];
				
		$('tr:has(input)').each(function() {
		   var row = this;
		   var values = "";
				
			var id = row.id;
			var validationPeriod = "";
			var date = "";
			
			$('input[type=text]', this).each(function() {
			
				date =  $(this).val();	
			});
		    $('input[type=checkbox]:checked', this).each(function() 
		    {	
				validationPeriod =  $(this).val();	
				if(date === "Choose date" && validationPeriod === "NotDefined") {
					$listOther.push({id: id, validationPeriod: validationPeriod, date: date});	
				}
				else {
					$list.push({id: id, validationPeriod: validationPeriod, date: date});		
				}						
		   });	
		});
						
		var comment = document.getElementById("comment").value;
				
		if($listOther.length > 0)
		{
			alert("You have not defined either a validation period or a date for one or more competence levels. Those will be ignored!");
		}
		$.ajax({
		type: 'POST',
		url: initQisCsharpMethod("Competence_ServiceHandlers.ExtendCompetenceLevels"),
		data: window.JSON.stringify({
			objectAttributes:[
						{Name:"extendResults", Format:"PlainText", Value: JSON.stringify($list)},
						{Name:"comment", Format:"PlainText", Value: comment}
				]
			})
		}).done(function(){
			$.fancybox.close();
			$.pageRefreshControlDialog();
		}).fail(function() { $cnt.append("The Extend failed...");});
	});
			
	$.ajax({
		type: 'POST',
		url: initQisCsharpMethod("Competence_ServiceHandlers.GetCompetenceLevelsForExtend"),
		data: window.JSON.stringify({
			objectAttributes:[
				{Name:"personObjId", Format:"PlainText", Value: personObjId},
				{Name:"withoutPendingApproval", Format:"PlainText", Value: "true"}
			]
		})
	}).done(function(data) {
		if(data) {	
			$checkAll = $("<div><input type=\"checkbox\" id=\"ChooseAll\" value=\"Checked\">Choose all</div>");
			$table = $("<table id=\"Extend\" style=\"width:100%;text-align:left\"><tr><th style=\"text-align:left\">Extend with (days):</th><th style=\"text-align:left\">Extend to:</th><th style=\"text-align:left\">Competence:</th><th style=\"text-align:left\">Status:</th><th style=\"text-align:left\">Choice:</th></tr>");	
			
			$.each($.parseJSON(data).Entries, function()
			{		
			
				var clObj = new RepositoryObject(this.Data.Value.ObjId);
				var entry = this;			
				clObj.init(function(q) 
				{
						var $row = $("<tr id=\""+q.ObjId+"\"/>");

						if(entry.GenericCompetence.Value.split(",")[1] != "")
						{

							$("<td>"+entry.GenericCompetence.Value.split(",")[1]+"</td>").appendTo($row);
							$("<td><input type=\"text\" id=\"datepicker"+q.ObjId+"\" name=\""+q.ObjId+"\" value=\"Choose date\" /></td>").appendTo($row);
							$("<td>"+ entry.GenericCompetence.Value.split(",")[0] + "</td>").appendTo($row);				
							$("<td>"+ q.AuditGS.Value[0].Name + "</td>").appendTo($row);
							$("<td><input type=\"checkbox\" name=\""+q.ObjId+"\" id=\"enabled"+q.ObjId+"\" value=\""+entry.GenericCompetence.Value.split(",")[1]+"\"></td>").appendTo($row).attr("enabled", true);
						}
						else 
						{
							$("<td style=\"color:red;\">Not defined</td>").appendTo($row);
							$("<td><input type=\"text\" id=\"datepicker"+q.ObjId+"\" name=\""+q.ObjId+"\" value=\"Choose date\" /></td>").appendTo($row);						
							$("<td>"+ entry.GenericCompetence.Value.split(",")[0] + "</td>").appendTo($row);				
							$("<td>"+ q.AuditGS.Value[0].Name + "</td>").appendTo($row);
							$("<td><input type=\"checkbox\" name=\""+q.ObjId+"\" id=\"enabled"+q.ObjId+"\" value=\"NotDefined\"></td>").appendTo($row).attr("enabled", true);
						}
						
						$row.appendTo($table);	
						$("#datepicker"+q.ObjId+"").datepicker();				
				});
			});
			$comment = $("<div style=\"aling:center\"><textarea type=\"textarea\" id=\"comment\" name=\"comment\" style=\"height:100px;width:480px;align:center\"/></div>");
			

			$checkAll.appendTo($cnt);
			$table.appendTo($cnt);
			$("#ChooseAll").on('change', function(e){
				   $('input:checkbox').not(this).prop('checked', this.checked);
			});
			$comment.appendTo($cnt);				

		}
	}).fail(function() { $cnt.append("The Extend dialog failed to load..."); });						
}

$.async = function(){
	if (!arguments.length) {
		return;
	}
	
	var callback = arguments[0];
	var interval = arguments[1] ? (arguments[1]).toString() : null;
	
	if (callback === false) {
		if (interval !== null) {
			if ($.inArray(interval, $QisSession.FncQueue)>-1) {
				if (interval.match(/i/i)) {
					clearInterval(parseInt(interval.replace(/p/i,"")));
				}
				else {
					clearTimeout(parseInt(interval.replace(/p/i,"")));
				}
				$QisSession.FncQueue.remove(interval);
			}
		}
		else {
			$QisSession.Delegate["async"] = 1;
			$.each($QisSession.FncQueue, function(idx, value) {
				if (!value) {
					return $.async(false);
				}
				
				if (typeof value === "function") {
					return;
				}
				
				if (value.indexOf("p") === 0) {
					return;
				}
				
				if (value.indexOf("i")>-1) {
					clearInterval(parseInt(value));
				}
				else {
					clearTimeout(parseInt(value));
				}
				$QisSession.FncQueue.remove(value);
			});
			delete $QisSession.Delegate["async"];
		}
		return;
	}
	
	if ($QisSession.Delegate["async"]) {
		return;
	}

	if (typeof callback != "function") {
		$.browser.warn("async(): Invalid function given for async.");
	}
	
	var recurrent = arguments[2] ? arguments[2] : false;
	var persistent = arguments[3] ? arguments[3] : false;
	var timer = "";

	interval = interval ? parseInt(interval) : 1;
	
	if (!recurrent){
		timer = setTimeout(function(){
			if (!$QisSession.Delegate["async"]) {
				$QisSession.FncQueue.remove(timer);
				callback.call();
			}
		}, interval);
	}
	else {
		timer = setInterval(function(){
			if (!$QisSession.Delegate["async"]) {
				callback.call(timer);
			}
		}, interval);
	}
	
	timer = (persistent ? "p" : "") + timer;
	timer += (recurrent ? "i" : "t");
	
	if ($.isArray($QisSession.FncQueue)) {
		$QisSession.FncQueue.push(timer);
	}
	else {
		$QisSession.FncQueue = [timer];
	}
	
	return timer;
}

$.extendSafely = function() {	
	if(arguments.length <= 0) {
		return {};
	}
	
	if(arguments.length == 1) {
		return arguments[0];
	}
	
	for (var i = 1; i < arguments.length; i++) {
		try{
			$.extend(true, arguments[0], arguments[i]);
		}
		catch (ex){
			// Ignore.
		}
	}
	
	return arguments[0];
}

$.execScript = function() {
	var $scope = this;
	var args = arguments;
	var script = args[0];
		
	if (!script) {
		return true;
	}
	
	if (typeof script === "string") {
		script = $.trim(script)
			.replace(/&quot;/g, "\"")
			.replace(/&amp;/g, "&")
			.replace(/&lt;/g, "<")
			.replace(/&gt;/g, ">");
	}
	
	for (var idx=0; idx<(args.length-1); idx++){
		args[idx] = args[idx+1];
	}
	
	delete args[args.length-1];
	args.length--;
	
	try {
		var fnc = $.noop;
		if (typeof script === "function") {
			fnc = script;
		}
		else if (typeof window[script] === "function") {
			fnc = window[script];
		}
		else {
			if (script.indexOf("function") != 0) {
				script = "function(){ " + script + "}";
			}
			fnc = eval("(" + script + ")");
		}
		
		if (typeof fnc === "function") {
			return fnc.apply($scope, args);
		}
		else {
			return true;
		}
	}
	catch(e) {
		$.browser.error("Script Excution Error: " + e);
		if ($QisSession.DEBUGMODE) {
			debugger;
		}
	}
	
	return false;
}

$.parseString = function () {
	if (!arguments.length) {
		return;
	}
	
	var $obj = this;
	if (!($obj instanceof RepositoryObject)) {
		$obj = $$ || new RepositoryObject();
	}
	
	var str = arguments[0];
	var plaintext = arguments[1] ? arguments[1] : false;
	
	if (typeof str === "undefined") {
		return "";
	}
	else if (!str.length) {
		return "";
	}

	var idx1 = str.indexOf("{") + 1;
	var idx2 = str.substr(idx1).indexOf("}");

	if (idx1>-1 && idx2>-1){
		var attr = str.substr(idx1, idx2);
		var text = "";
		
		if (attr) {
			if (attr.substr(0,1)=="{" && str.substr(idx1+idx2+1,1)=="}") {
				attr = str.substring(idx1+1, idx1+idx2);
				text = "<span class=\"attr-prompt\" style=\"display:none\">" + attr + "</span>";
				
				str = str.replace("{{" + attr + "}}", "");
				
				if (str.substr(idx1-1,1)=="{") {
					idx1 = str.indexOf("{") + 1;
					idx2 = str.substr(idx1).indexOf("}");
					attr = str.substr(idx1, idx2);
				}
				else {
					str = $.parseString.call($obj, str, plaintext);
				}
			}

			if (attr === "TemplateIcon") {
				text = $("<img/>")
					.attr("src", $QisSession.HTMLBase + "/images/templates/" + $obj.Data.Template + ".png")
					.css({
						"position": "relative",
						"top": "1.5px"
					}).get(0).outerHTML;
			}
			else if (attr.match(/\(\)/) 
			|| (attr.match(/\./) && attr.match(/\$/))) {
				text = eval(attr);
			}
			else {
				if (plaintext) {
					text += $obj.attr(attr).text();
				}
				else {
					var $val = $obj.attr(attr).children().css("display", "inline-block").end();
					if ($val.isLoading()) {
						var $cnt = $("<span/>").uniqueId().css("display", "inline-block")
							.addClass(attr.replace(/<|>/g,"")).ajaxLoaderText();
						var guid = $cnt.attr("id");
						text += $cnt.prop("outerHTML");
						$.async(function(){
							if (!$val.isLoading()) {
								$.async(false, this);
								$("#" + guid).html($val);
								if ($val.text()) {
									$("#" + guid).prev(".attr-prompt").show();
								}
							}
						}, 250, true);
					}
					else if ($val.text() || $val.find("img").length){
						text = text.replace("display:none","") + $val.htmldata();
					}
					else {
						text = "";
					}
				}
			}
		
			str = str.replace("{" + attr + "}", text);
			str = $.parseString.call($obj, str, plaintext);
		}
	}
		
	return str;
}

$.ll = function() {$.LL.apply(this, arguments);}
$.LL = function () {
	var value = "";

	if (!arguments.length) {
		return value;
	}
		
	var label = arguments[0];
	var $obj = arguments[1] ? arguments[1] : 
		(typeof $$ !== "undefined" ? $$ : new RepositoryObject());
	
	var plaintext = arguments[2] ? arguments[2] : false;		
	
	if (typeof $QepSettings != "undefined") {
		if (label in $QepSettings.MiscLabels) {
			value = $QepSettings.MiscLabels[label];
		}
	}
	
	if (!value) {
		value = label;
	}

	value = $.parseString.call($obj, value, plaintext);
		
	return value;
}

$.QDate = function(){
	var args = arguments;
  
	var js = args[0] || false;
	var date = args[1] || "";
	var addDays  = args[2] || 0;
  
	var format = $QepSettings.DefaultDateFormat || "yyyy MM dd hh.mm";
  
	var hours = date.substr(format.indexOf("h"),2) || 0;
	var minutes = date.substr(format.indexOf("m"),2) || 0;
 
	var cdate = date ? 
		new Date(parseInt(date.substr(format.indexOf("y"),4)), 
			parseInt(date.substr(format.indexOf("M",2)))-1, 
			parseInt(date.substr(format.indexOf("d"),2)) + parseInt(addDays), 
			parseInt(hours), 
			parseInt(minutes)) :
		new Date();

 	var qdate = js ? cdate : cdate.getFullYear() 
		+ "-" + (cdate.getMonth() >8 ? (cdate.getMonth() + 1) : "0" + (cdate.getMonth() + 1))
		+ "-" + (cdate.getDate() >9 ? cdate.getDate() : "0" + cdate.getDate())
		+ " " + (cdate.getHours() >9 ? cdate.getHours() : "0" + cdate.getHours())
		+ "." + (cdate.getMinutes() >9 ? cdate.getMinutes() : "0" + cdate.getMinutes());
  
	return qdate;
}

$.timer = function(){
	var init = arguments[0] || false;
	var $div = $("body").children(".timer");
	if (!init) {
		if ($div.length) {
			$.async(false, $div.data("timer"));
			$div.finish().removeData("timer");
			$.async(function(){
				if (!$div.data("timer")){
					$div.fadeOut();
				}
			}, 5000);
		}
		return parseFloat($div.text())*1000;
	}
		
	if (!$div.length) {
		$div = $("<div/>").addClass("timer")
			.css({
				"color" : "green",
				"background-color": "rgba(255,255,255,0.9)",
				"text-align": "center",
				"line-height": "20px",
				"width": "70px",
				"font-size": "11px",
				"position": "absolute",
				"top": "25px",
				"right": "25px",
				"z-index": 1000
			});
			
		$div.appendTo($("body"))
	}
	
	var timer = (new Date()).getTime();
	$div.finish().show().html("0.00s.").data("timer", $.async(function(){
		$div.text(parseFloat(((new Date()).getTime() - timer)/1000).toFixed(2) + "s.");
	}, 50, true));
	
	return timer;
}

changeRepositoryLanguage = function() {			
	var inf = getLanguageInfo(this.Code);
	var lcd = inf.Code === this.Code ? inf.Code : this.Code;
	var lnm = inf.Code === this.Code ? inf.Name : this.Code;
	
	var url = location.href.split("?")[0];
	var searchstr = window.location.search.slice(1).split("#")[0];
	var parameters = "";

	var silent = arguments[0] || false;
	
	if (searchstr) {
		var parms = {};
		searchstr = searchstr.split("&");
		for (var i = 0; i < searchstr.length; i++) {
			var tmp = searchstr[i].split("=");
			parms[tmp[0].toLowerCase()] = decodeURI(tmp[1]);
		}
			
		$.each(parms, function(key, value) {
			if( key.toLowerCase().indexOf("lc") < 0) {
				parameters += "&" + key + "=" + value;
			}
		});
	}				

	if (parameters) {
		url += "?";
		url += parameters ? parameters.substring(1) : "";
	}

	if (lcd != $QisSession.DefaultLangCode) {
		url += parameters ? "&" : "?";
		url += "lc=" + lcd;
	}

	var surl = $QisSession.HTMLBase + "/ChangeWorkingLanguage.ashx?_t=" + (new Date()).getTime()
		+ "&Language=" + lcd
		+ "&RepId=" + $QisSession.RepId;
	
	if (!silent) {
		$.jnotify({
			classid: "jnotify-info",
			persistent: true,
			title: $.LL("Information"),
			message: $.LL("Changing language to:") + " " + lnm
		});
	}

	$.get(surl).always(function (jqxhr, textStatus, error) {
		if (jqxhr.responseText) {
			return silent ? null : notifyRequestError(jqxhr, textStatus, error, surl);
		}
		else if (jqxhr.status != 200){
			if (!silent) {
				$.jnotify({
					classid: "jnotify-info",
					title: $.LL("Warning"),
					message: $.LL("Unable to change language") + "<br/>" 
						+ $.LL("Status:") + " " + jqxhr.status + "<br/><br/>" 
						+ $.LL("Message:") + " " + jqxhr.statusText
				});
			}
			return;
		}
		
		if (!silent) {
			$.jnotify({
				classid: "jnotify-info",
				title: $.LL("Information"),
				message: $.LL("Language changed to:") + " " + lnm
			});
		}
		
		$.browserCache("clear", true);
		$.cookie("LangCode", lcd);
		
		if (location.href.match(new RegExp(lcd, "i"))) {
			return $.initSessionParameters();
		}
		else if ( location.href.match(/lc=/i)) {
			return location.href = url;
		}
		else if (lcd == $QisSession.DefaultLangCode) {
			return $.initSessionParameters();
		}
			
		return location.href = url;
	});
}

changeRepositoryRole = function() {
	var usrRole = this.UserRole;
	var usrAlts = this.AlternativeRoles || [];
	
	$.cookie("UserRole", usrRole);
	$.cookie("UserAltRoles", usrAlts.join(";"));
	
	var silent = arguments[0] || false;
	var role = usrRole;

	if (typeof $RoleMap === "object") {
		role = $RoleMap[role] || role;
	}
	
	role = role.replace(/ /g, "_");
	
	var surl = $QisSession.HTMLBase + "/ChangeRepositoryRole.ashx?_t=" + (new Date()).getTime()
		+ "&RoleId=" + role
		+ "&RepId=" + $QisSession.RepId
		+ "&ConfId=" + $QisSession.ConfId
	if (!silent) {
		$.jnotify({
			classid: "jnotify-info",
			persistent: true,
			title: $.LL("Information"),
			message: $.LL("Changing role to:") + " " + usrRole
		});
	}
	
	$.get(surl).always(function (jqxhr, textStatus, error) {
		if (jqxhr.responseText) {
			return silent ? null : notifyRequestError(jqxhr, textStatus, error, surl);
		}
		else if (jqxhr.status != 200){
			if (!silent) {
				$.jnotify({
					classid: "jnotify-info",
					title: $.LL("Warning"),
					message: $.LL("Unable to change role") + "<br/>" 
					+ $.LL("Status:") + " " + jqxhr.status + "<br/><br/>" 
					+ $.LL("Message:") + " " + jqxhr.statusText
				});
			}
			return;
		}
		
		if (!silent) {
			$.jnotify({
				classid: "jnotify-info",
				title: $.LL("Information"),
				message: $.LL("Repository role changed to:") + " " + usrRole
			});
		}
		
		$.browserCache("clear", true);
		$.initSessionParameters();
	});
}

getRepositoryInfo = function() {
	var parm = arguments[0] || $QisSession.RepId;
	
	var data = {
		RepId: $QisSession.RepId,
		ConfId: $QisSession.ConfId,
		ConfName: $QisSession.ConfName,
		FollowLinks: true,
		WebApplication: location.pathname.replace(/\//g,""),
		Target: ""
	};
	
	if (parm !== $QisSession.RepId) {
		if (!$RemoteRepositories.Data
		|| !$RemoteRepositories.Data[parm]) {
			return {};
		}
		$.each($RemoteRepositories.Data || {}, function(id, $r) {
			if (id === parm) {
				$.extend(true, data, $r);
				data.RepId = id;
				return false;
			}
		});
	}
	
	return data;
}

getConfigurationInfo = function() {
	var parm = arguments[0] || $QisSession.ConfId;
	
	var $opts = {
		RoleFilter: false,
		SkipDisabled: true
	};
	
	if (arguments[1]) {
		$.extend(true, $opts, arguments[1]);
	}
	
	var data = {
		Id : !$opts.RoleFilter ? $QisSession.ConfId : "",
		Name : !$opts.RoleFilter ? $QisSession.ConfName : "",
		IsPrivateWorkspace : ""
	};
	
    $.each($ConfigurationInfo.Data || {}, function () {
		if (!$opts.SkipDisabled && !this.Enable) {
			return true;
		}
		if ($opts.RoleFilter && !isValidRole(this.RoleFilter)) {
			return true;
		}
		
        if (this.Id == parm || this.Name == parm 
		|| (parm === "Base" && !this.IsPrivateWorkspace)) {
            data.Id = this.Id;
			data.Name = this.Name;
			data.Title = this.Title || this.Name;
			data.IsPrivateWorkspace = this.IsPrivateWorkspace;
			return false;
        }
    });

    return data;
}

getLanguageInfo = function() {
	var parm = arguments[0] || $QisSession.LangCode;
	
	var data = {
		Code : $QisSession.DefaultLangCode,
		Name : $QisSession.DefaultLangName
	}
	
	if (!$LanguageInfo.Data) {
		return data;
	}

	$.each($LanguageInfo.Data, function () {
		if (this.Code == parm || this.Name == parm) {
			data.Code = this.Code;
			data.Name = this.Name;
			data.ShortName = this.ShortName;
			return false;
		}
	});
	
	return data;
}

getUrlParams = function (paramName, searchString, caseSensitive) {
	var parms, i, val;

	if (!paramName) {
		return "";
	}
	
	if (typeof searchString == "undefined") {
		searchString = window.location.search.slice(1);
	}
	
	if (typeof caseSensitive == "undefined") {
		caseSensitive = false;
	}
	
	searchString = decodeURI(searchString);
	
	if (searchString.indexOf("?")>-1) {
		searchString = searchString.split("?")[1];
		searchString = searchString.replace(/&amp;/g, "&");
	}
	
	if (!searchString) {
		return "";
	}

	parms = searchString.split("&");

	for (i = 0; i < parms.length; i++) {
		val = parms[i].split("=");
		if (caseSensitive) {
			if (paramName == val[0]) {
				return val[1];
			}
		}
		else {
			var regex = new RegExp(val[0], "i");
			if (paramName.match(regex)) {
				return decodeURI(val[1]);
			}
		}
	}

	return "";
} 

replaceUrlParam = function (url, parm, value) {
	if (!url) { 
		return;
	}
	
	if (!parm) {
		return url;
	}
	
	var plist, i, val, url2;
	
	url2 = url.split("?")[0]
	plist = url.split("?")[1];

	if (plist) {
		plist = plist.split("&");
		for (i = 0; i < plist.length; i++) {
			val = plist[i].split("=");
			if (val[0] == parm || val[0] == String(parm).toLowerCase()) {
				val[1] = value;
			}
			url2 += ( i ? "&" : "?" ) + val[0] + "=" + val[1];
		}
	}
	else {
		url2 += "?" + parm + "=" + value;
	}
	
	return url2;
}

isValidUrl = function(url) {
	if (!arguments[0]) {
		return false;
	}

    var RegExp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    return (RegExp.test(url));
}

isValidConfiguration = function(validconfigs){
	if (!validconfigs) {
		return true;
	}
	
	if (typeof validconfigs === "string") {
		validconfigs = $.trim(validconfigs);
		
		if (!validconfigs) {
			return true;
		}
		
		validconfigs = validconfigs.split(/;/g);
	}
			
	var isvalid = false;
	
	var $conf = getConfigurationInfo();
	
	$.each(validconfigs, function(key, c){
		if (c == $conf.Name || c == $conf.Id) {
			isvalid = true;
			return false;
		}
	});
	
	return isvalid;
}

isValidCondition = function(condition, $obj){
	if (!condition) {
		return true;
	}
	
	if (typeof $obj === "undefined") {
		$obj = $$;
	}
	
	if (typeof condition === "function"){
		return condition.call($obj);
	}
	
	if (typeof window[condition] === "function"){
		return window[condition].call($obj);
	}
	
	return $obj.ValidCondition(condition);
}

isValidRole = function (validroles){
	if (!validroles) {
		return true;
	}

	var $usr = $QisSession.UserInfo;
	
	if (typeof validroles === "string") {
		varlidroles = $.trim(validroles);
		
		if (!validroles) {
			return true;
		}
		
		validroles = validroles.split(/,|;/g);
	}
	
	if (!validroles.length) {
		return true;
	}
	
	var isvalid = false;
	var usrRole = $usr.Role;
	
	if (typeof $RoleMap === "object") {
		usrRole = $RoleMap[usrRole] || usrRole;
	}
	
	$.each(validroles, function(key, role){
		var exactMatch = (String(role).substring(0,1)!==":");
		role = role.replace(":","").replace("=","").replace(/_/g, ' ');
		
		if (exactMatch) {
			var regexp = new RegExp(usrRole, "gi");
			if (role.match(regexp)) {
				isvalid = true;
				return false;
			}
		}
		else {
			role = [role];
			if (role.match(usrRole, $usr.AltRoles)) {
				isvalid = true;
				return false;
			}
		}
	});
		
	return isvalid;
}

isValidJsonData = function(data, mthd) {
	if (typeof data == "string") {
		var $obj = (this instanceof RepositoryObject ? this : null);
		if (data.toLowerCase().indexOf("error")==0) {
			$.jnotify({
				title: $.LL("Webservice Request Failed"),
				message: $.LL("Method:") + " " + mthd + "<br/>" + data 
					+ ($obj ? ("<br/>"
						+ ($obj.Data.Name ? "<br/>" + $.LL("Name") + ": " + $obj.Data.Name : "")
						+ ($obj.Data.Template ? "<br/>" + $.LL("Template") + ": " + $obj.Data.Template : "")
						+ ($obj.Data.ObjId ? "<br/>" + $.LL("Object ID") + ": " + $obj.Data.ObjId : ""))
						: "")
			});
			return false;
		}
		
		if (!data) {
			return false;
		}
		
		try { 
			$.parseJSON(data); 
		}
		catch(e) { 
			$.browser.error("Error: Unable to parse JSON " + (mthd ? "from " + mthd : "") + "\n" + e);
			return false; 
		}
	}
			
	return true;
}

rgb2hex = function() {
	if (!arguments.length) {
		return "";
	}
	var r,g,b;
	if (arguments.length === 3) {
		r = arguments[0];
		g = arguments[1];
		b = arguments[2];
	}
	else {
		var str = arguments[0] || "";
		if (!str) { 
			return "";
		}
		
		str = str.replace(/rgb|(|)/i, "").split(",");
		r = $.trim(str[0]);
		g = $.trim(str[1]);
		b = $.trim(str[2]);
	}
	
    return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
}

getLocaleDateFormat = function() {
    var list = {"ar-SA":"dd/MM/yy","bg-BG":"dd.M.yyyy","ca-ES":"dd/MM/yyyy","zh-TW":"yyyy/M/d","cs-CZ":"d.M.yyyy","da-DK":"dd-MM-yyyy","de-DE":"dd.MM.yyyy","el-GR":"d/M/yyyy","en-US":"M/d/yyyy","fi-FI":"d.M.yyyy","fr-FR":"dd/MM/yyyy","he-IL":"dd/MM/yyyy","hu-HU":"yyyy. MM. dd.","is-IS":"d.M.yyyy","it-IT":"dd/MM/yyyy","ja-JP":"yyyy/MM/dd","ko-KR":"yyyy-MM-dd","nl-NL":"d-M-yyyy","nb-NO":"dd.MM.yyyy","pl-PL":"yyyy-MM-dd","pt-BR":"d/M/yyyy","ro-RO":"dd.MM.yyyy","ru-RU":"dd.MM.yyyy","hr-HR":"d.M.yyyy","sk-SK":"d. M. yyyy","sq-AL":"yyyy-MM-dd","sv-SE":"yyyy-MM-dd","th-TH":"d/M/yyyy","tr-TR":"dd.MM.yyyy","ur-PK":"dd/MM/yyyy","id-ID":"dd/MM/yyyy","uk-UA":"dd.MM.yyyy","be-BY":"dd.MM.yyyy","sl-SI":"d.M.yyyy","et-EE":"d.MM.yyyy","lv-LV":"yyyy.MM.dd.","lt-LT":"yyyy.MM.dd","fa-IR":"MM/dd/yyyy","vi-VN":"dd/MM/yyyy","hy-AM":"dd.MM.yyyy","az-Latn-AZ":"dd.MM.yyyy","eu-ES":"yyyy/MM/dd","mk-MK":"dd.MM.yyyy","af-ZA":"yyyy/MM/dd","ka-GE":"dd.MM.yyyy","fo-FO":"dd-MM-yyyy","hi-IN":"dd-MM-yyyy","ms-MY":"dd/MM/yyyy","kk-KZ":"dd.MM.yyyy","ky-KG":"dd.MM.yy","sw-KE":"M/d/yyyy","uz-Latn-UZ":"dd/MM yyyy","tt-RU":"dd.MM.yyyy","pa-IN":"dd-MM-yy","gu-IN":"dd-MM-yy","ta-IN":"dd-MM-yyyy","te-IN":"dd-MM-yy","kn-IN":"dd-MM-yy","mr-IN":"dd-MM-yyyy","sa-IN":"dd-MM-yyyy","mn-MN":"yy.MM.dd","gl-ES":"dd/MM/yy","kok-IN":"dd-MM-yyyy","syr-SY":"dd/MM/yyyy","dv-MV":"dd/MM/yy","ar-IQ":"dd/MM/yyyy","zh-CN":"yyyy/M/d","de-CH":"dd.MM.yyyy","en-GB":"dd/MM/yyyy","es-MX":"dd/MM/yyyy","fr-BE":"d/MM/yyyy","it-CH":"dd.MM.yyyy","nl-BE":"d/MM/yyyy","nn-NO":"dd.MM.yyyy","pt-PT":"dd-MM-yyyy","sr-Latn-CS":"d.M.yyyy","sv-FI":"d.M.yyyy","az-Cyrl-AZ":"dd.MM.yyyy","ms-BN":"dd/MM/yyyy","uz-Cyrl-UZ":"dd.MM.yyyy","ar-EG":"dd/MM/yyyy","zh-HK":"d/M/yyyy","de-AT":"dd.MM.yyyy","en-AU":"d/MM/yyyy","es-ES":"dd/MM/yyyy","fr-CA":"yyyy-MM-dd","sr-Cyrl-CS":"d.M.yyyy","ar-LY":"dd/MM/yyyy","zh-SG":"d/M/yyyy","de-LU":"dd.MM.yyyy","en-CA":"dd/MM/yyyy","es-GT":"dd/MM/yyyy","fr-CH":"dd.MM.yyyy","ar-DZ":"dd-MM-yyyy","zh-MO":"d/M/yyyy","de-LI":"dd.MM.yyyy","en-NZ":"d/MM/yyyy","es-CR":"dd/MM/yyyy","fr-LU":"dd/MM/yyyy","ar-MA":"dd-MM-yyyy","en-IE":"dd/MM/yyyy","es-PA":"MM/dd/yyyy","fr-MC":"dd/MM/yyyy","ar-TN":"dd-MM-yyyy","en-ZA":"yyyy/MM/dd","es-DO":"dd/MM/yyyy","ar-OM":"dd/MM/yyyy","en-JM":"dd/MM/yyyy","es-VE":"dd/MM/yyyy","ar-YE":"dd/MM/yyyy","en-029":"MM/dd/yyyy","es-CO":"dd/MM/yyyy","ar-SY":"dd/MM/yyyy","en-BZ":"dd/MM/yyyy","es-PE":"dd/MM/yyyy","ar-JO":"dd/MM/yyyy","en-TT":"dd/MM/yyyy","es-AR":"dd/MM/yyyy","ar-LB":"dd/MM/yyyy","en-ZW":"M/d/yyyy","es-EC":"dd/MM/yyyy","ar-KW":"dd/MM/yyyy","en-PH":"M/d/yyyy","es-CL":"dd-MM-yyyy","ar-AE":"dd/MM/yyyy","es-UY":"dd/MM/yyyy","ar-BH":"dd/MM/yyyy","es-PY":"dd/MM/yyyy","ar-QA":"dd/MM/yyyy","es-BO":"dd/MM/yyyy","es-SV":"dd/MM/yyyy","es-HN":"dd/MM/yyyy","es-NI":"dd/MM/yyyy","es-PR":"dd/MM/yyyy","am-ET":"d/M/yyyy","tzm-Latn-DZ":"dd-MM-yyyy","iu-Latn-CA":"d/MM/yyyy","sma-NO":"dd.MM.yyyy","mn-Mong-CN":"yyyy/M/d","gd-GB":"dd/MM/yyyy","en-MY":"d/M/yyyy","prs-AF":"dd/MM/yy","bn-BD":"dd-MM-yy","wo-SN":"dd/MM/yyyy","rw-RW":"M/d/yyyy","qut-GT":"dd/MM/yyyy","sah-RU":"MM.dd.yyyy","gsw-FR":"dd/MM/yyyy","co-FR":"dd/MM/yyyy","oc-FR":"dd/MM/yyyy","mi-NZ":"dd/MM/yyyy","ga-IE":"dd/MM/yyyy","se-SE":"yyyy-MM-dd","br-FR":"dd/MM/yyyy","smn-FI":"d.M.yyyy","moh-CA":"M/d/yyyy","arn-CL":"dd-MM-yyyy","ii-CN":"yyyy/M/d","dsb-DE":"d. M. yyyy","ig-NG":"d/M/yyyy","kl-GL":"dd-MM-yyyy","lb-LU":"dd/MM/yyyy","ba-RU":"dd.MM.yy","nso-ZA":"yyyy/MM/dd","quz-BO":"dd/MM/yyyy","yo-NG":"d/M/yyyy","ha-Latn-NG":"d/M/yyyy","fil-PH":"M/d/yyyy","ps-AF":"dd/MM/yy","fy-NL":"d-M-yyyy","ne-NP":"M/d/yyyy","se-NO":"dd.MM.yyyy","iu-Cans-CA":"d/M/yyyy","sr-Latn-RS":"d.M.yyyy","si-LK":"yyyy-MM-dd","sr-Cyrl-RS":"d.M.yyyy","lo-LA":"dd/MM/yyyy","km-KH":"yyyy-MM-dd","cy-GB":"dd/MM/yyyy","bo-CN":"yyyy/M/d","sms-FI":"d.M.yyyy","as-IN":"dd-MM-yyyy","ml-IN":"dd-MM-yy","en-IN":"dd-MM-yyyy","or-IN":"dd-MM-yy","bn-IN":"dd-MM-yy","tk-TM":"dd.MM.yy","bs-Latn-BA":"d.M.yyyy","mt-MT":"dd/MM/yyyy","sr-Cyrl-ME":"d.M.yyyy","se-FI":"d.M.yyyy","zu-ZA":"yyyy/MM/dd","xh-ZA":"yyyy/MM/dd","tn-ZA":"yyyy/MM/dd","hsb-DE":"d. M. yyyy","bs-Cyrl-BA":"d.M.yyyy","tg-Cyrl-TJ":"dd.MM.yy","sr-Latn-BA":"d.M.yyyy","smj-NO":"dd.MM.yyyy","rm-CH":"dd/MM/yyyy","smj-SE":"yyyy-MM-dd","quz-EC":"dd/MM/yyyy","quz-PE":"dd/MM/yyyy","hr-BA":"d.M.yyyy.","sr-Latn-ME":"d.M.yyyy","sma-SE":"yyyy-MM-dd","en-SG":"d/M/yyyy","ug-CN":"yyyy-M-d","sr-Cyrl-BA":"d.M.yyyy","es-US":"M/d/yyyy"};
    var lang = navigator.language?navigator.language:navigator['userLanguage'];
	
	return (((lang in list)? list[lang] : "yyyy MM dd") + " hh:mm:ss");
}

repaintRadControls = function (win) {
	if(!win || !win.$telerik) {
		return;
	}
	
	var allRadControls = win.$telerik.radControls;
	
	if(!allRadControls) {
		return;
	}
	
	for (var i = 0; i < allRadControls.length; i++) {
		var element = allRadControls[i];
		if(element.repaint) {
			element.repaint();
		}
	}
}

prototypeProperties = function(obj) {
	var result = [];
	for (var prop in obj) {
		if (!obj.hasOwnProperty(prop)) {
			result.push(prop);
		}
	}
	return result;
}

sHead = function (url, parm) {
	var xhr = new XMLHttpRequest();
	xhr.open('HEAD', url, false);
	xhr.send('');
	return parm ? xhr.getResponseHeader(parm) : xhr.getAllResponseHeaders();
}

openDocumentFile = function () {
	if (!arguments.length) {
		return false;
	}
	
	if (!window.ActiveXObject && !("ActiveXObject" in window)) {
		return false;
	}
	
	var file = String(arguments[0]);
	var prefix = "file://";
	    
	if (file.substr(0,2) === "\\\\"){
		file = prefix + file.substr(2);
	}
	else if (file.substr(1,1) === ":") {
		file = prefix + file;
	}
	
	file = file.replace(/\\/g,"/");

    var type = file.split('.');
    var doctype = String(type[type.length - 1]).toLowerCase();
    
	try {
		if (doctype == "doc" || doctype == "docx" || doctype == "docm"
		|| doctype == "xls" || doctype == "xlsx" || doctype == "xlm"
		|| doctype == "ppt" || doctype == "pptx" || doctype == "pptm" 
		|| doctype == "pps" || doctype == "ppsx" || doctype == "ppsm" ) {
			window.open(file);
			return true;
		}
	
		if (doctype == "dot" || doctype == "dotx" || doctype == "dotm") {
			var WordApp;
			WordApp = new ActiveXObject("Word.Application");
			WordApp.Documents.add(file);
			WordApp.visible=true;		
			return true;
		}

		if (doctype == "xlt" || doctype == "xltx" || doctype == "xltm") {
			var exApp;
			exApp = new ActiveXObject("Excel.Application");
			exApp.visible=true;
			exApp.Workbooks.add(file);
			return true;
		}

		if (doctype == "pot" || doctype == "potx" || doctype == "potm") {
			var pptApp;
			var pptTemp;
			pptApp = new ActiveXObject("Powerpoint.Application");
			pptApp.visible = true;
			pptTemp=pptApp.Presentations.Add();
			pptTemp.ApplyTemplate(file);
			return true;
		}

		if (doctype == "mpt") {
			var mspApp;
			mspApp = new ActiveXObject("MSProject.Application");
			mspApp.visible=true;
			mspApp = mspApp.Projects.Add(false, file, true);
			return true;
		}

		if (doctype == "mdb" || doctype == "accdb") {
			var mdbApp;
			var mdbTemp;
			mdbApp = new ActiveXObject("Access.Application");
			mdbApp.visible=true;
			mdbSheet = mdbApp.OpenCurrentDatabase(file, 0);
			return true;
		}
		
		if (doctype == "txt") {
			window.open(file);
			return true;
		}
		
		return false;
	}
	catch(e) {
		handleBrowserError(e, file);
		return true;
	}
}

handleTemplateDefinitionError = function () {
	var $obj = arguments[0] ? arguments[0] : $$;
	var $cnt = arguments[1] ? arguments[1] : null;
	
	var $msg = $("<div/>")
		.css({
			"width": "370px",
			"min-height": "100px",
			"font-size": "9pt",
			"padding" : "0 10px"
		})
		.append($("<h2/>").html($.LL("Template Defintion not available")))
		.append($("<div/>")
			.append($("<span/>").html($.LL("Name:")))
			.append($("<span/>").html($obj.Data.Name)))
		.append($("<div/>")
			.append($("<span/>").html($.LL("Template:")))
			.append($("<span/>").html($obj.Data.Template)))
		.append($("<div/>")
			.append($("<span/>").html($.LL("Object Id:")))
			.append($("<span/>").html($obj.Data.ObjId)))
		.append($("<div/>")
			.append($("<span/>").html($.LL("Lang Code:")))
			.append($("<span/>").html($QisSession.LangCode)))
		.append($("<div/>")
			.append($("<span/>").html($.LL("Configuration Id:")))
			.append($("<span/>").html($QisSession.ConfId)))
		.append($("<div/>")
			.append($("<span/>").html($.LL("Configuration Name:")))
			.append($("<span/>").html($QisSession.ConfName)))
		.append($("<div/>")
			.append($("<span/>").html($.LL("Repository:")))
			.append($("<span/>").html($QisSession.RepId)))
		.find("div span:first-child").css({
			"display": "inline-block",
			"width": "120px",
			"text-indent": "5px",
			"margin": "0 5px 5px 0"
		})
		.end();
	
	if ($cnt) {
		$cnt.html($msg);
	}
	else {
		$.fancybox($msg);
	}
}

handleBrowserError = function() {
	if (arguments.length) {
		var e = arguments[0];
		
		if (e.number == -2147024891) {
			handleAccessDeniedError.apply(this, arguments);
			return;
		}
		
		if (e.number == -2146827859) {
			handleActiveScriptError.apply(this, arguments);
			return;
		}
	}
	
	var txt = $.LL("Unknown Error") + "...";
	
	if (arguments[0]) {
		var e = arguments[0];
		if (e.message) {
			txt = e.message;
			if (e.description != e.message) {
				txt = txt + "<br/>" + "<br/>" + e.description;
			}
		}
	}
	
	$.fancybox($("<h2/>").width(400).html(txt));
}

handleAccessDeniedError = function() {
	var $cnt = $("<div/>");
	var $hdr = $("<h1>/").html($.LL("Access Denied")).appendTo($cnt);
	var href = $.LL("the URL of the Web site that is affected");

	if (arguments.length) {
		var e = arguments[0];
		if (e.message) {
			$hdr.html(e.message);
		}
		
		if (arguments[1]) {
			href = String(arguments[1]);
			if (href.indexOf("\\\\")===0) {
				href = "file://" + href.substr(2).replace(/\\/g, "/");
			}
			href = href.substr(href.indexOf("://") + 3);
			href = href.split("/")[0]
		}
	}
		
	var $msg = $("<div/>").html($.LL("To work around this issue, add the Web site that is affected to the <b>Trusted sites</b> security zone in Internet Explorer. To do this, follow these steps:"));
	var $ol = $("<ol/>").appendTo($msg);
	$ol.append("<li>" + $.LL("Start Internet Explorer") + "</li>");
	$ol.append("<li>" + $.LL("On the <b>Tools</b> menu, click <b>Internet Options</b>") + "</li>");
	$ol.append("<li>" + $.LL("On the <b>Security</b> tab, click <b>Trusted sites</b>, and then click <b>Sites</b>") + "</li>");
	$ol.append("<li>" + $.LL("In the <b>Add this Web site to the zone</b> box, type") + " <b>" + href + "</b> " + $.LL("and then click <b>Add</b>") + "</li>");
	$ol.append("<li>" + $.LL("Click <b>OK</b>") + "</li>");
	$ol.append("<li>" + $.LL("<b>Close all</b> your browser sessions and <b>reopen</b> the browser to launch your site") + "</li>");
	
	var width = $(window).width();
	
	$.fancybox($cnt.append($msg).width(width>600?600:width));
}

handleActiveScriptError = function() {
	var $cnt = $("<div/>");
	var $hdr = $("<h1>/").html($.LL("Permission Denied")).appendTo($cnt);
	var href = $.LL("the URL of the Web site that is affected");

	if (arguments.length) {
		var e = arguments[0];
		if (e.message) {
			$hdr.html(e.message);
		}
		
		if (arguments[1]) {
			href = arguments[1];
			href = href.substr(href.indexOf("://") + 3);
			href = href.split("/")[0]
		}
	}
		
	var $msg = $("<div/>").html($.LL("To work around this issue, enable ActiveX controls for your site. To do this, follow these steps:"));
	var $ol = $("<ol/>").appendTo($msg);
	$ol.append("<li>" + $.LL("Start Internet Explorer") + "</li>");
	$ol.append("<li>" + $.LL("On the <b>Tools</b> menu, click <b>Internet Options</b>") + "</li>");
	$ol.append("<li>" + $.LL("On the <b>Security</b> tab, Click on <b>Trusted Sites</b> (or <b>Local Intranet</b> depending on whether your site is trusted or not)") + "</li>");
	$ol.append("<li>" + $.LL("Click on <b>Custom Level</b>") + "</li>");
	$ol.append("<li>" + $.LL("Ensure that <b>Initialize and script ActiveX controls is not marked safe for scripting</b> is enabled - this comes under Activex controls and plug-ins section towards 1/4th of the scroll bar") + "..." + "</li>");
	$ol.append("<li>" + $.LL("Click <b>OK</b>") + "</li>");
	$ol.append("<li>" + $.LL("<b>Close all</b> your browser sessions and <b>reopen</b> the browser to launch your site") + "</li>");
	
	var width = $(window).width();
	
	$.fancybox($cnt.append($msg).width(width>600?600:width));
}

if (!jQuery.browser) {
    var agent = navigator.userAgent||navigator.vendor||window.opera;
    var uaMatch = function (ua) {
        ua = ua.toLowerCase();

        var match = /(chrome)[ \/]([\w.]+)/.exec(ua) ||
          /(firefox)[ \/]([\w.]+)/.exec(ua) ||
          /(webkit)[ \/]([\w.]+)/.exec(ua) ||
          /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) ||
          /(msie) ([\w.]+)/.exec(ua) ||
          ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) ||
          [];

        return {
            browser: match[1] || "",
            version: match[2] || "0"
        };
    };

    var matched = uaMatch(agent);
    var browser = {};

    if (matched.browser) {
        browser[matched.browser] = true;
        browser.version = matched.version;
		browser.legacy =  false;
		browser.compatible = false;
		browser.singlepage = true;
		browser.mobile = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(agent)
			||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(agent.substr(0,4));
		browser.click = browser.mobile ? "touchstart" : "click";
		browser.log = function(msg){if($QisSession["DEBUGMODE"]&&window.console!=null){console.log(msg);}};
		browser.info = function(msg){if(window.console!=null){console.info(msg);}};
		browser.warn = function(msg){if(window.console!=null){console.warn(msg);}};
		browser.error = function(msg){if(window.console!=null){console.error(msg);}};
		
		if (browser.msie) {
			if (document.documentMode < 8) {
				browser.legacy = true;
			}
			
			if (document.documentMode < 9) {
				browser.compatible = true;
			}
			
			if (document.documentMode < 11) {
				// Extension of setTimeout and setInterval with additional parameters. (Default not supported by IE 9 and earlier)
				(function(f){window.setTimeout=f(window.setTimeout);window.setInterval=f(window.setInterval);})(function(f){return function(c,t){var a=[].slice.call(arguments,2);return f(function(){c instanceof Function?c.apply(this,a):eval(c)},t)}});
				browser.singlepage = false;
			}
		}
    }

    if (browser.chrome) {
        browser.webkit = true;
    } else if (browser.webkit) {
        browser.safari = true;
    }
	
    jQuery.browser = browser;
	
	if (typeof $.storage == "undefined") {
		$.storage = amplify;
		$.storage.full = false;
		$.storage.flush = function() {
			$.each($.storage.store(), function (storeKey) {
				$.storage.store(storeKey, null);
			});
			if (typeof localStorage == "object") {
				localStorage.clear();
			}
		};
		$.storage.space = function() {
			var initial = localStorage.getItem("size") || 5;
			var total = 0;
			for (var item in localStorage){  
				total +=  ((localStorage[item].length * 2) / 1024 / 1024);
			}
			return parseFloat(initial - total.toFixed(3));
		};
	}
}

jQuery.browserTimeStamp = function(){
	var timestamp = Math.floor((new Date()).getTime() / 1000);
	$.browserCache("set", "TimeStamp" + $QisSession.ConfId, timestamp);
	$QisSession.TimeStamp = timestamp;
}

jQuery.browserCache = function() {
	var $data = null;
	
	if (!arguments[0]) {
		return $data;
	}
	
	var _p = "persistent-";
	
	switch (arguments[0]) {
		case "check":
			if ($.browserCache("get", _p + $Qef.tokenCookieName) != $.cookie($Qef.tokenCookieName) ) {
				$.browserCache("clear", true);
				$.browserCache("set", _p + $Qef.tokenCookieName, $.cookie($Qef.tokenCookieName));
				$.browserTimeStamp();
				$.browser.log("Session storage cleared.");
			}
			else {
				var timestamp = $.browserCache("get", "TimeStamp" + $QisSession.ConfId);
	
				if ($QisSession.TimeStamp < timestamp) {
					$QisSession.TimeStamp = timestamp;
				}
			}
			break;
			
		case "clear": 
			if (arguments[1]) {
				$.storage.flush();
			}
			else {
				var prefix = "__amplify__"
				$.each(localStorage, function(key, val){
					if (key.indexOf(prefix) > -1
					&& key.indexOf(_p) < 0) {
						$.storage.store(key.replace(prefix,""), null);
					}
				});
				$.browserTimeStamp();
			}
			$.pageCache("clear");
			break;

		case "del" : 
			var key = arguments[1];
			
			if (!key) {
				null;
			}
			
			$.storage.store(key, null);
			break;
			
		case "get" : 
			var key = arguments[1];
						
			if (!key) {
				return null;
			}

			$data = $.storage.store(key);
			
			if ($data) {
				try {
					$data = JSONfn.parse($data);
				}
				catch(e) {
					$data = null;
					if (key.match(/userid/i)) {
						window.localStorage.clear();
						$.removeCookie($Qef.tokenCookieName, { path:'/' });
						endQisSession(location.href);
					}
				}
			}
			break;
			
		case "set":			
			var key = arguments[1];
			var val = arguments[2];
			var ttl = arguments[3] ? arguments[3] : null;
			
			if ((typeof val == "undefined") || !key) {
				return;
			}
			
			val = JSONfn.stringify(val);
			
			try {
				if (ttl) {
					$.storage.store(key, val, {expires: ttl});
				}
				else {
					$.storage.store(key, val);
				}
				$.storage.full = false;
			}
			catch (e) {
				$.browser.warn("Storage: " + e + "\nStorage space: " + $.storage.space());
				$.storage.full = true;
			}
			break;
	}
	
	return $data;
}

jQuery.pageCache = function() {
	if (!arguments[0]) {
		return null;
	}
	
	switch (arguments[0]) {
		case "get":
			var key = arguments[1];
			if (!key) {
				return null;
			}
			else {
				if (key.toLowerCase().indexOf(".svc") > -1) {
					key = key.split("_t")[0];
				}
				if (arguments[2]) {
					return key in $QisSession.Cache;
				}
				else {
					return $QisSession.Cache[key];
				}
			}
			break;
		
		case "del": 
			key = arguments[1];
			if (!key) {
				return;
			}
			else {
				delete $QisSession.Cache[key];				
			}
			break;
		
		case "set":
			var key = arguments[1];
			var val = arguments[2];
			var ttl = $QepSettings.CacheTime;
			
			if (!key) {
				return null;
			}
			else {
				if (key.toLowerCase().indexOf(".svc") > -1) {
					key = key.split("_t")[0];
				}
				delete $QisSession.Init[key]
				$QisSession.Cache[key] = val;
			}
			
			if (arguments[3] === null || arguments[3]===false) {
				return;
			}
			
			if ($.isNumeric(arguments[3])) {
				ttl = parseInt(arguments[3]);
			}
			
			$.async(function(){ 
				if ($QisSession.DEBUGMODE) {
					$.jnotify({
						title: $.LL("Cache invalidated"),
						message: "TTL:" + ttl + "<br/>" + $.LL("key:") + " " + key
					});
				}
				delete $QisSession.Cache[key];
			}, ttl);

			break;
			
		case "init":
			var key = arguments[1];
			if (!key) {
				return null;
			}
			if (key.toLowerCase().indexOf(".svc") > -1) {
				key = key.split("_t")[0];
			}
			if (arguments[2]) {
				$QisSession.Init[key] = 1;
			}
			else {
				return key in $QisSession.Init;
			}
			break;
			
		case "clear":
			$QisSession.Cache = {};
			break;
	}
		
	return null;
}

jQuery.pageKeys = function(e) {
	if (!arguments.length) {
		return;
	}
	
	var key = e.which ? e.which : e.charCode;

	if (key == 116) {
		if (e.ctrlKey) {
			e.preventDefault();
			$.browserCache("clear");
			window.location.reload(true);
		}
		else if (e.altKey && $QclEngine.Data && $QisSession.DEBUGMODE){
			e.preventDefault();
			initCommandDialog();
		}
		else {
			e.preventDefault();
			if ( typeof $$ == "object" ) {
				$(".governanceactions").empty().ajaxLoaderText();
				if (History.enabled) {
					NProgress.start();
				}
				$$.init(function(){
					if (arguments[0] === false) {
						return window.location.replace($QisSession.HTMLLocation);
					}
					$.initPageContent(null, true);	
					if (History.enabled) {
						NProgress.done(true);
					}
					$(".jgrowl-container .reload").remove();
				}, null, null, true);
			}
		}
	}
	else if (key == 27) {
		if ($("body").hasClass("modal-window")) {
			return;
		}
		if (typeof $.window == "function") {
			if ($.window.getAll().length) {
				$.window.closeAll();
			}
		}
		if (typeof $.fancybox == "function") {
			$.fancybox.close();
		}
	}
	else if (key == 13) {
		// e.preventDefault();
	}
	else if (key == 8) {
		if ($(e.target).is("input") || $(e.target).is("input") || $(e.target).is("textarea")) {
			return;
		}
		if ($(".fancybox-overlay").length) {
			e.preventDefault();
			$.fancybox.close();
		}
		else if (parent.$.window.getAll().length) {
			e.preventDefault();
			parent.$.window.close();
		}
		else if (History.enabled) {
			$(".qtip").empty().remove();
		}
	}
}
